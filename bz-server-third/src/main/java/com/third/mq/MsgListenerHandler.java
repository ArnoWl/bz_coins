package com.third.mq;

import com.common.constatns.ThirdConstants;
import com.mall.model.third.dto.AddScheduleJobDto;
import com.mall.model.third.dto.ClosedScheduleJobDto;
import com.third.job.service.IXxlJobInfoService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 消息监听处理类
 * @author lt
 * 2020年3月24日15:38:17
 */
@Component
public class MsgListenerHandler {

    @Resource
    private IXxlJobInfoService iXxlJobInfoService;

    /**
     * 添加调度任务
     * @param jobDto
     */
    @RabbitListener(queues = ThirdConstants.QUEUES_ADD_SCHEDULE_JOB)
    @RabbitHandler
    public void addScheduleJob(AddScheduleJobDto jobDto) {
        iXxlJobInfoService.addScheduleJob(jobDto);
    }

    /**
     * 关闭调度任务
     * @param jobDto
     */
    @RabbitListener(queues = ThirdConstants.QUEUES_CLOSED_SCHEDULE_JOB)
    @RabbitHandler
    public void closedScheduleJob(ClosedScheduleJobDto jobDto) {
        iXxlJobInfoService.closedScheduleJob(jobDto);
    }


}

