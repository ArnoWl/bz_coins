package com.third.job.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.third.job.entity.XxlJobInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-05-15
 */
public interface XxlJobInfoMapper extends BaseMapper<XxlJobInfo> {

}
