package com.third.job.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lt
 * @since 2020-05-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("xxl_job_info")
@ApiModel(value="XxlJobInfo对象", description="")
public class XxlJobInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "执行器主键ID")
    private Integer jobGroup;

    @ApiModelProperty(value = "任务执行CRON")
    private String jobCron;

    private String jobDesc;

    private LocalDateTime addTime;

    private LocalDateTime updateTime;

    @ApiModelProperty(value = "作者")
    private String author;

    @ApiModelProperty(value = "报警邮件")
    private String alarmEmail;

    @ApiModelProperty(value = "执行器路由策略")
    private String executorRouteStrategy;

    @ApiModelProperty(value = "执行器任务handler")
    private String executorHandler;

    @ApiModelProperty(value = "执行器任务参数")
    private String executorParam;

    @ApiModelProperty(value = "阻塞处理策略")
    private String executorBlockStrategy;

    @ApiModelProperty(value = "任务执行超时时间，单位秒")
    private Integer executorTimeout;

    @ApiModelProperty(value = "失败重试次数")
    private Integer executorFailRetryCount;

    @ApiModelProperty(value = "GLUE类型")
    private String glueType;

    @ApiModelProperty(value = "GLUE源代码")
    private String glueSource;

    @ApiModelProperty(value = "GLUE备注")
    private String glueRemark;

    @ApiModelProperty(value = "GLUE更新时间")
    private LocalDateTime glueUpdatetime;

    @ApiModelProperty(value = "子任务ID，多个逗号分隔")
    private String childJobid;

    @ApiModelProperty(value = "调度状态：0-停止，1-运行")
    private Integer triggerStatus;

    @ApiModelProperty(value = "上次调度时间")
    private Long triggerLastTime;

    @ApiModelProperty(value = "下次调度时间")
    private Long triggerNextTime;


}
