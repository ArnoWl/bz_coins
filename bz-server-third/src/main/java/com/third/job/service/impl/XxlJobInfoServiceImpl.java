package com.third.job.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.exception.BaseException;
import com.common.utils.lang.StringUtils;
import com.mall.model.third.dto.AddScheduleJobDto;
import com.mall.model.third.dto.ClosedScheduleJobDto;
import com.third.job.entity.XxlJobInfo;
import com.third.job.mapper.XxlJobInfoMapper;
import com.third.job.service.IXxlJobInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-05-15
 */
@Service
public class XxlJobInfoServiceImpl extends ServiceImpl<XxlJobInfoMapper, XxlJobInfo> implements IXxlJobInfoService {

    @Override
    public void addScheduleJob(AddScheduleJobDto jobDto) {
        try {
            XxlJobInfo jobInfo = new XxlJobInfo();
            BeanUtils.copyProperties(jobDto,jobInfo);
            jobInfo.setAuthor("sys");
            jobInfo.setExecutorRouteStrategy("FIRST");
            jobInfo.setExecutorBlockStrategy("SERIAL_EXECUTION");
            jobInfo.setGlueType("BEAN");
            jobInfo.setGlueRemark("系统创建：GLUE代码初始化");
            jobInfo.setAddTime(LocalDateTime.now());
            jobInfo.setUpdateTime(LocalDateTime.now());
            jobInfo.setGlueUpdatetime(LocalDateTime.now());
            jobInfo.setExecutorFailRetryCount(10);
            jobInfo.setExecutorTimeout(180);
            // 创建状态为运行
            jobInfo.setTriggerStatus(1);
            this.save(jobInfo);
        } catch (Exception e) {
            throw new BaseException("创建调度任务失败",e);
        }
    }


    @Override
    public void closedScheduleJob(ClosedScheduleJobDto jobDto) {
        try {
            if(StringUtils.isNotBlank(jobDto.getExecutorHandler()) && StringUtils.isNotBlank(jobDto.getExecutorParam())){
                QueryWrapper<XxlJobInfo> wrapper = new QueryWrapper<>();
                wrapper.eq("executor_handler",jobDto.getExecutorHandler())
                        .eq("executor_param",jobDto.getExecutorParam());
                XxlJobInfo jobInfo = this.getOne(wrapper);
                if(jobInfo != null){
                    // 停止任务
                    jobInfo.setTriggerStatus(0);
                    this.updateById(jobInfo);
                }
            }
        } catch (Exception e) {
            throw new BaseException("创建调度任务失败",e);
        }
    }

}
