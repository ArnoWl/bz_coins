package com.third.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.model.third.dto.AddScheduleJobDto;
import com.mall.model.third.dto.ClosedScheduleJobDto;
import com.third.job.entity.XxlJobInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lt
 * @since 2020-05-15
 */
public interface IXxlJobInfoService extends IService<XxlJobInfo> {

    /**
     * 添加调度任务
     * @param jobDto
     */
    void addScheduleJob(AddScheduleJobDto jobDto);

    /**
     * 关闭调度任务
     * @param jobDto
     */
    void closedScheduleJob(ClosedScheduleJobDto jobDto);
}
