package com.third.config;

import com.common.constatns.ThirdConstants;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * rabbit消息服务配置
 * @author lt
 * 2020年3月24日15:07:48
 */
@Configuration
public class RabbitMqConfig {

    @Bean
    public Queue addScheduleJob(){
        return new Queue(ThirdConstants.QUEUES_ADD_SCHEDULE_JOB);
    }

    @Bean
    public Queue closedScheduleJob(){
        return new Queue(ThirdConstants.QUEUES_CLOSED_SCHEDULE_JOB);
    }

}
