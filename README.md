## 项目技术架构

![项目结构](http://img.cyfhvip.cn/20200813/342b6f2f53df42b4a31214bcfe39626e.png "基础结构图")

## 项目使用到的技术

- 主体框架：spring boot + cloud + Mybatis plus + JWT
- 数据库：mysql
- 中间件：nacos、redis、mongoDB、rabbitMq、xxl-Job
- 其他：netty、feign、swagger2、shiro、druid、Log4J

## 项目结构说明

``` lua
bz-coins
├── bz-core -- 公共核心模块
|    ├── com.common.base -- 基础模型
|    ├── com.common.config -- 全局公共配置
|    ├── com.common.constatns -- 常量类
|    ├── com.common.context -- 本地线程存取
|    ├── com.common.enums -- 枚举类
|    ├── com.common.exception -- 全局自定义异常类
|    ├── com.common.fileUpload -- 文件上传组件
|    ├── com.common.handler -- 全局处理方法（异常处理、过滤器异常处理）
|    ├── com.common.job -- xxl-job任务调度配置类（用到的项目需要手动引入配置）
|    ├── com.common.jwt -- JWT处理工具类
|    ├── com.common.sms -- 阿里云短信发送组件
|    └── com.common.utils -- 公共工具类
|
├── bz-model -- 公共模型、服务调用消费定义模块
|    ├── com.mall.annotation -- 注解类（验签、动态验签）
|    ├── com.mall.aspect -- 切面组件（请求参数日志打印、参数校验处理、验签拦截）
|    ├── com.mall.base -- 基础模型类
|    ├── com.mall.fallback -- 微服务调用熔断处理
|    ├── com.mall.feign -- 微服务调用消费定义接口
|    └── com.mall.model -- 微服务调用传输模型
|
├── bz-server-auth -- 授权服务模块
|    ├── auth-clent -- 鉴权客户端
|       ├──com.auth.client.annotation -- 注解（忽略鉴权）
|       ├──com.auth.config -- 客户端/用户登录鉴权配置、OKhttp传输安全token处理
|       ├──com.auth.interceptor -- 鉴权拦截
|       ├──com.auth.jwt -- 项目启动加载鉴权公钥
|       └──com.auth.runner -- Api业务方法处理
|    └── auth-server -- 鉴权服务端
|       ├──com.auth.configuration -- 客户端鉴权、JWT密钥配置
|       ├──com.auth.controller -- 获取平台用户鉴权token、数据安全
|       ├──com.auth.interceptor -- 客户端鉴权拦截、okhttp请求token拦截处理
|       ├──com.auth.provider -- 鉴权服务提供类
|       └──com.auth.runner -- 启动加载/生成RSA秘钥
|
├── bz-server-admin -- 平台总后台系统模块
|    ├── admin-web -- 平台管理后台Api
|       ├──com.admin.configuration -- 项目配置
|       ├──com.admin.controller -- 控制器
|       ├──com.admin.model -- 接口出入参视图模型
|       ├──com.admin.service -- Api业务方法处理
|       └──com.admin.shiro -- shiro框架支持
|    ├── admin-base -- 基础依赖（持久层、代码生成、基础业务处理类、子模块系统全局配置）
|    └── admin-server -- 平台数据微服务提供
|       ├──com.admin.configuration -- 项目配置
|       ├──com.admin.job -- 任务调度执行器处理方法
|       ├──com.admin.provider -- 业务服务提供类
|       └──com.admin.runner -- 启动加载平台配置到缓存
|
├── bz-server-user -- 用户中心系统模块
|    ├── user-api -- 用户端接口提供Api（小程序、用户端App）
|       ├──com.user.configuration -- 项目配置
|       ├──com.user.controller -- 控制器
|       ├──com.user.model -- 接口出入参视图模型
|       └──com.user.service -- Api业务方法处理
|    ├── user-base -- 基础依赖（持久层、代码生成、基础业务处理类、子模块系统全局配置）
|    └── user-server -- 用户微服务提供
|       ├──com.user.configuration -- 项目配置
|       ├──com.user.consumer -- 消息监听消费者
|       ├──com.user.job -- 任务调度执行器处理方法
|       └──com.user.provider -- 业务服务提供类
|
├── bz-server-market -- 市场服务模块
|    ├── com.market.common -- 公共（实时K线map工厂、常量）
|    ├── com.market.controller -- 获取历史K线数据
|    ├── com.market.handle -- 处理层（全局netty订阅通道、K线开启/取消订阅、火币网socket、mongoDB存储K线、服务端socket处理）
|    ├── com.market.job -- 任务调度（监测K线连接状态、开启/关闭/重启订阅第三方K线）
|    └── com.market.websocket -- 基于netty的socket客户端/服务端
|
├── bz-server-wallet -- 钱包服务模块
|    ├── com.wallet.config -- 协议配置
|    ├── com.wallet.consumer -- 消息监听消费者
|    ├── com.wallet.core -- 币种工具类
|    ├── com.wallet.job -- 任务调度（修改币种实时价、归集用户钱包）
|    └── com.wallet.provider -- 钱包服务提供者
|
├── bz-server-wallet -- 视频服务模块
|    ├── com.video.configuration -- 项目配置
|    └── com.video.provider -- 视频服务提供者
|
└── mall-server-third -- 第三方服务模块
|    ├── com.third.config -- 配置（基础项目配置、实例化mq消息队列）
|    ├── com.third.job -- xxl-Job调度任务持久层
|    ├── com.third.model -- 接口出入参视图模型
|    └── com.third.mq -- mq消息监听处理 
```
## 特殊模块描述

    1.bz-core、bz-model为所有项目公共依赖，而bz-model项目存放了所有微服务调用所需的消费定义和模型，所以如果修改了这两块，之后单独编译部署服务，
    需要注意不同服务之间调用，依赖的mall-model版本是否存在差异，如果存在差异会导致服务无法调用成功，从而熔断
    2.auth-client为其他微服务服务鉴权加载、拦截提供了处理方法：添加项目依赖，启动类添加@EnableAuthClient注解，参照其他微服务项目配置拦截

## 钱包模块描述
    1.钱包模块我们主要采用的是OMNI,ERC20协议；OMNI属于独立的节点服务器，ERC20通过"infura"连接节点；
    2.OMNI属于对接BTC公链的USDT，包含了归集，生成钱包，转账等方法通过RPC即可调用，可以产考OMNI API方法：http://cw.hubwiz.com/card/c/omni-rpc-api/1/1/1/
    3.ERC20 我们通过"infura"链接上ETH公网，对接智能合约，针对ETH，以及ETH的所有代币可进行操作
    3.bz-server-wallet 项目下core文件夹主要是针对OMNI 以及ERC20协议的基础操作方法类，consumer是用户注册异步生成钱包方法，job是钱包定时调度归集等方法
    4.注意：目前使用的火币申请的API KEY 是我自己的账号申请的KEY 后续你们可以换掉，注册火币:https://www.huobi.me/zh-cn/markets/,在个人中心申请API_KEY，获
    取到key之后，在NACOS的wallet_server,以及admin_server 都需要修改key；
    5.ERC20的钱包生成地址，在nacos的wallet_server配置中会指向服务器的一个文件夹目录，里面就是ERC20钱包的文件，这个以及生成的文件不能修改文件路径，也不能切换到其他的文件夹。
## 环境部署要求

- Jdk1.8+
- Mysql8.0+
- nacos1.2.0+
- redis4.0+
- rabbitMq3.7.0+
- mongoDB4.2.0+
