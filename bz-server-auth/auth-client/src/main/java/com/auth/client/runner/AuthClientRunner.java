package com.auth.client.runner;

import com.alibaba.fastjson.JSON;
import com.auth.client.config.ClientAuthConfig;
import com.auth.client.config.UserAuthConfig;
import com.auth.client.jwt.ClientAuthUtil;
import com.common.base.ResultVo;
import com.common.exception.ClientTokenException;
import com.common.exception.UserTokenException;
import com.common.jwt.RsaKeyHelper;
import com.mall.feign.auth.AuthClient;
import com.mall.model.auth.dto.AuthenticationClientDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.io.IOException;


@Configuration
@Slf4j
@EnableScheduling
public class AuthClientRunner implements CommandLineRunner {

    @Resource
    private ClientAuthConfig clientAuthConfig;
    @Resource
    private UserAuthConfig userAuthConfig;
    @Resource
    private AuthClient authClient;
    @Resource
    private ClientAuthUtil clientAuthUtil;

    @Override
    public void run(String... args){
        try {
            log.info("初始化加载鉴权pubKey");
            refreshPubKey();
            clientAuthUtil.refreshClientToken();
        }catch(Exception e){
            log.error("初始化加载鉴权pubKey失败,1分钟后自动重试!",e);
        }
    }

    @Scheduled(cron = "0 0/10 * * * ?")
    public void refreshPubKey() {
        AuthenticationClientDto clientDto = new AuthenticationClientDto();
        clientDto.setClientCode(clientAuthConfig.getClientCode());
        clientDto.setClientSecret(clientAuthConfig.getClientSecret());
        // 加载客户端鉴权公钥
        this.refreshClientPubKey(clientDto);
        // 加载用户鉴权公钥
        this.refreshUserPubKey(clientDto);
    }

    private void refreshClientPubKey(AuthenticationClientDto clientDto) {
        try {
            ResultVo resultVo = authClient.getClientPubKey(clientDto);
            if(resultVo.isSuccess()){
                this.clientAuthConfig.setPubKeyByte(RsaKeyHelper.toBytes(resultVo.getData().toString()));
                log.info("加载客户端鉴权公钥成功");
            }else{
                log.error(resultVo.getErrorMessage());
                log.error(clientAuthConfig.getClientCode() + "----" + clientAuthConfig.getClientSecret());
            }
        } catch (IOException e) {
            throw new ClientTokenException("加载客户端鉴权公钥失败");
        }
    }

    private void refreshUserPubKey(AuthenticationClientDto clientDto) {
        try {
            ResultVo resultVo = authClient.getUserPubKey(clientDto);
            if(resultVo.isSuccess()){
                this.userAuthConfig.setPubKeyByte(RsaKeyHelper.toBytes(resultVo.getData().toString()));
                log.info("加载用户端鉴权公钥成功");
            }else{
                log.error(clientAuthConfig.getClientCode() + "----" + clientAuthConfig.getClientSecret());
                log.error(resultVo.getErrorMessage());
            }
        } catch (IOException e) {
            throw new UserTokenException("加载用户鉴权公钥失败");
        }
    }

}