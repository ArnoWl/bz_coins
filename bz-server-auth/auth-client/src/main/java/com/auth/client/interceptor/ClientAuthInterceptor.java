package com.auth.client.interceptor;

import com.auth.client.annotation.IgnoreClientToken;
import com.auth.client.config.ClientAuthConfig;
import com.auth.client.jwt.ClientAuthUtil;
import com.common.exception.ClientTokenException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 客戶端鉴权拦截
 * @author lt
 * 2020年3月16日18:42:21
 */
@Slf4j
public class ClientAuthInterceptor extends HandlerInterceptorAdapter {

    @Resource
    private ClientAuthUtil clientAuthUtil;

    @Resource
    private ClientAuthConfig clientAuthConfig;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        // 配置该注解，说明不进行服务拦截
        IgnoreClientToken annotation = handlerMethod.getBeanType().getAnnotation(IgnoreClientToken.class);
        if (annotation == null) {
            annotation = handlerMethod.getMethodAnnotation(IgnoreClientToken.class);
        }
        if (annotation != null) {
            return super.preHandle(request, response, handler);
        }
        try {
            String token = request.getHeader(clientAuthConfig.getTokenHeader());
            clientAuthUtil.getInfoFromToken(token);
        } catch (Exception e) {
            log.error("授权失败",e);
            throw new ClientTokenException("未能通过客户端鉴权访问" + request.getHeader(clientAuthConfig.getTokenHeader()));
        }
        return super.preHandle(request, response, handler);
    }
}
