package com.auth.client.configuration;

import com.auth.client.config.ClientAuthConfig;
import com.auth.client.config.UserAuthConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 2017/9/15.
 */
@Configuration
@ComponentScan({"com.auth.client"})
public class AutoConfiguration {

    @Bean
    ClientAuthConfig getClientAuthConfig(){
        return new ClientAuthConfig();
    }

    @Bean
    UserAuthConfig getUserAuthConfig(){
        return new UserAuthConfig();
    }

}
