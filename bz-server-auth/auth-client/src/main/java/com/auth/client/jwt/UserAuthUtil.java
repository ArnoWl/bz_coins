package com.auth.client.jwt;

import com.auth.client.config.UserAuthConfig;
import com.common.exception.UserTokenException;
import com.common.jwt.JWTHelper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 2017/9/15.
 */
@Configuration
public class UserAuthUtil {

    @Resource
    private UserAuthConfig userAuthConfig;

    public Map<String, Object> getInfoFromToken(String token) throws Exception {
        try {
            return JWTHelper.getInfoFromToken(token, userAuthConfig.getPubKeyByte());
        }catch (ExpiredJwtException ex){
            throw new UserTokenException("用户登录过期");
        }catch (SignatureException ex){
            throw new UserTokenException("用户token有误" + token);
        }catch (IllegalArgumentException ex){
            throw new UserTokenException("未传入用户token");
        }
    }
}
