
package com.auth.client.jwt;

import com.auth.client.config.ClientAuthConfig;
import com.common.base.ResultVo;
import com.common.exception.ClientTokenException;
import com.common.jwt.JWTHelper;
import com.mall.feign.auth.AuthClient;
import com.mall.model.auth.dto.AuthenticationClientDto;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by ace on 2017/9/15.
 */
@Configuration
@Slf4j
@EnableScheduling
public class ClientAuthUtil {

    @Resource
    private ClientAuthConfig clientAuthConfig;

    @Resource
    private AuthClient authClient;

    public Map<String,Object> getInfoFromToken(String token) throws Exception {
        try {
            return JWTHelper.getInfoFromToken(token, clientAuthConfig.getPubKeyByte());
        } catch (ExpiredJwtException ex) {
            throw new ClientTokenException("Client token expired!");
        } catch (SignatureException ex) {
            throw new ClientTokenException("Client token signature error!");
        } catch (IllegalArgumentException ex) {
            throw new ClientTokenException("Client token is null or empty!");
        }
    }

    @Scheduled(cron = "0 0/10 * * * ?")
    public void refreshClientToken() {
        try {
            AuthenticationClientDto clientDto = new AuthenticationClientDto();
            clientDto.setClientCode(clientAuthConfig.getClientCode());
            clientDto.setClientSecret(clientAuthConfig.getClientSecret());
            ResultVo resultVo = authClient.getAccessToken(clientDto);
            if (resultVo.isSuccess()) {
                clientAuthConfig.setClientToken(resultVo.getData().toString());
            }else{
                log.error(resultVo.getErrorMessage());
            }
        } catch (Exception e) {
            throw new ClientTokenException("获取客户端鉴权Token失败");
        }
    }

}