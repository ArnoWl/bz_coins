package com.auth.client.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * 客户端鉴权配置
 * @author lt
 * 2020年3月23日11:43:48
 */
@Data
public class ClientAuthConfig {

    @Value("${auth.client.code}")
    private String clientCode;

    @Value("${auth.client.secret}")
    private String clientSecret;

    @Value("${auth.client.token-header}")
    private String tokenHeader;

    @Value("${spring.application.name}")
    private String applicationName;

    private byte[] pubKeyByte;

    private String clientToken;

}
