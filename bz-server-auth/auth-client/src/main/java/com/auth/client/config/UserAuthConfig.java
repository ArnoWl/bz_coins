package com.auth.client.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * 2017/9/15.
 */
@Data
public class UserAuthConfig {

    @Value("${auth.user.token-header}")
    private String tokenHeader;

    private byte[] pubKeyByte;

}
