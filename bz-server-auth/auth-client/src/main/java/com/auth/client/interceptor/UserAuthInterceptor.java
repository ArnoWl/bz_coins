package com.auth.client.interceptor;

import com.auth.client.annotation.IgnoreUserPhone;
import com.auth.client.annotation.IgnoreUserToken;
import com.auth.client.config.UserAuthConfig;
import com.auth.client.jwt.UserAuthUtil;
import com.common.constatns.CommonConstants;
import com.common.constatns.JWTConstants;
import com.common.context.BaseContextHandler;
import com.common.context.SysUserContext;
import com.common.context.UserContext;
import com.common.exception.BaseException;
import com.common.exception.UserTokenException;
import com.common.utils.lang.NumberUtils;
import com.mall.feign.auth.LoginAccountClient;
import com.mall.model.auth.vo.UserLoginAccountVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static com.common.constatns.JWTConstants.*;
import static com.common.constatns.RedisKeyConstants.USER_SSO_VERSION;

/**
 * 用户授权拦截
 * @author lt
 * 2020年3月16日18:42:21
 */
@CrossOrigin
@Slf4j
public class UserAuthInterceptor extends HandlerInterceptorAdapter {

    @Resource
    private UserAuthUtil userAuthUtil;

    @Resource
    private UserAuthConfig userAuthConfig;

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 过滤OPTIONS请求
        if(request.getMethod().equals("OPTIONS")){
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        // 配置该注解，说明不进行用户拦截
        IgnoreUserToken annotation = handlerMethod.getBeanType().getAnnotation(IgnoreUserToken.class);
        if (annotation == null) {
            annotation = handlerMethod.getMethodAnnotation(IgnoreUserToken.class);
        }
        if (annotation != null) {
            return super.preHandle(request, response, handler);
        }
        String token = request.getHeader(userAuthConfig.getTokenHeader());
        if (StringUtils.isBlank(token)) {
            throw new UserTokenException("用户未登录");
        }
        Map<String,Object> params = userAuthUtil.getInfoFromToken(token);
        String subject = params.get(JWT_SUBJECT).toString();
        String uid = this.handlerLoginUserInfo(subject,token,params);
        if(StringUtils.isBlank(subject) || !userAuthConfig.getTokenHeader().equals(subject)){
            throw new UserTokenException("用户token异常，非正常客户端请求");
        }
        if(subject.equals(JWT_KEY_USER_TYPE_USER)){
            // 用户端单点登录
            String ssoKey = USER_SSO_VERSION + uid;
            Integer ssoVersion = NumberUtils.createInteger(redisTemplate.opsForValue().get(ssoKey));
            if (ssoVersion == null || !ssoVersion.equals(params.get(JWT_KEY_USER_SSO))) {
                throw new UserTokenException("用户已在其他设备登录");
            }
        }
        return super.preHandle(request, response, handler);
    }

    /**
     * 处理登录用户信息
     * @param subject
     * @param params
     */
    private String handlerLoginUserInfo(String subject,String token,Map<String,Object> params){
        switch (subject){
            case JWT_KEY_USER_TYPE_USER:
                UserContext.setUserInfo(token,params);
                return UserContext.getUserID();
            case JWT_KEY_USER_TYPE_ADMIN:
                SysUserContext.setSysUserInfo(token,params);
                return null;
            default:
                throw new UserTokenException("登录用户类型信息与客户端服务不匹配" + subject);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        BaseContextHandler.remove();
        super.afterCompletion(request, response, handler, ex);
    }
}
