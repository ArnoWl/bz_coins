package com.auth.client.interceptor;

import com.auth.client.config.ClientAuthConfig;
import lombok.extern.java.Log;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;



@Log
@Component
public class OkHttpTokenInterceptor implements Interceptor {

    @Resource
    @Lazy
    private ClientAuthConfig clientAuthConfig;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request newRequest = chain.request()
                .newBuilder()
                .header(clientAuthConfig.getTokenHeader()
                        , clientAuthConfig.getClientToken() == null ? "" : clientAuthConfig.getClientToken())
                .build();
        return chain.proceed(newRequest);
    }


}
