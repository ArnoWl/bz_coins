package com.auth.provider;

import com.auth.configuration.JwtKeyConfiguration;
import com.auth.service.IClientService;
import com.common.base.ResultVo;
import com.common.jwt.RsaKeyHelper;
import com.mall.feign.auth.AuthClient;
import com.mall.model.auth.dto.AuthenticationClientDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 客户端认证服务提供类
 * @Author: lt
 * @Date: 2020/3/23 11:13
 */
@RestController
@RequestMapping(value = "client")
public class AuthClientProvider implements AuthClient {

    @Resource
    private JwtKeyConfiguration jwtKeyConfiguration;

    @Resource
    private IClientService iClientService;

    @Override
    public ResultVo getUserPubKey(@RequestBody AuthenticationClientDto clientDto){
        ResultVo resultVo = iClientService.authenticationClient(clientDto);
        if(!resultVo.isSuccess()){
            return resultVo;
        }
        resultVo.setData(RsaKeyHelper.toHexString(jwtKeyConfiguration.getUserPubKey()));
        return resultVo;
    }

    @Override
    public ResultVo getClientPubKey(@RequestBody AuthenticationClientDto clientDto){
        ResultVo resultVo = iClientService.authenticationClient(clientDto);
        if(!resultVo.isSuccess()){
            return resultVo;
        }
        resultVo.setData(RsaKeyHelper.toHexString(jwtKeyConfiguration.getClientPubKey()));
        return resultVo;
    }

    @Override
    public ResultVo getAccessToken(@RequestBody AuthenticationClientDto clientDto){
        return iClientService.getAccessToken(clientDto);
    }

}
