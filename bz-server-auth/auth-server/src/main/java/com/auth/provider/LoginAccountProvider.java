package com.auth.provider;

import com.auth.entity.LoginAccountInfo;
import com.auth.entity.LoginAccountRecords;
import com.auth.service.ILoginAccountInfoService;
import com.auth.service.ILoginAccountRecordsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.exception.BaseException;
import com.common.utils.*;
import com.mall.feign.auth.LoginAccountClient;
import com.mall.model.auth.dto.*;
import com.mall.model.auth.vo.LoginRecordListVo;
import com.mall.model.auth.vo.UserLoginAccountVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: lt
 * @Date: 2020/3/31 17:34
 */
@RestController
@RequestMapping(value = "loginAccount")
@Slf4j
public class LoginAccountProvider implements LoginAccountClient {

    @Resource
    private ILoginAccountInfoService iLoginAccountInfoService;
    @Resource
    private ILoginAccountRecordsService iLoginAccountRecordsService;

    @Override
    public UserLoginAccountVo getUserLoginAccount(@RequestParam("uid") String uid) {
        try {
            // 获取用户账户信息
            LoginAccountInfo loginAccount = iLoginAccountInfoService.getById(uid);
            if (loginAccount == null) {
                throw new BaseException("获取用户登录信息失败");
            }
            UserLoginAccountVo accountVo = new UserLoginAccountVo();
            BeanUtils.copyProperties(loginAccount, accountVo);
            return accountVo;
        } catch (Exception e) {
            throw new BaseException("获取用户登录信息失败", e);
        }
    }

    @Override
    public String getUidByPhoneAccount(String phoneAccount) {
        QueryWrapper<LoginAccountInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("phone_account", phoneAccount);
        LoginAccountInfo accountInfo = iLoginAccountInfoService.getOne(wrapper);
        if (accountInfo != null) {
            return accountInfo.getUid();
        }
        return null;
    }

    @Override
    public ResultVo updateUserPhoneAccount(String uid, String phoneAccount) {
        try {
            QueryWrapper<LoginAccountInfo> wrapper = null;
            wrapper = new QueryWrapper<>();
            wrapper.eq("phone_account", phoneAccount);
            int count = iLoginAccountInfoService.count(wrapper);
            if (count > 0) {
                return ResultVo.failure("该手机号在系统中已存在,修改手机号失败");
            }
            wrapper = new QueryWrapper<>();
            wrapper.eq("uid", uid);
            LoginAccountInfo loginAccountInfo = new LoginAccountInfo();
            loginAccountInfo.setPhoneAccount(phoneAccount);
            iLoginAccountInfoService.update(loginAccountInfo, wrapper);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("修改用户手机账号失败", e);
        }

    }

    @Override
    public ResultVo updateUserPassword(String uid, String newPassword) {
        try {
            QueryWrapper<LoginAccountInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", uid);
            LoginAccountInfo accountInfo = iLoginAccountInfoService.getOne(wrapper);
            if (accountInfo == null) {
                return ResultVo.failure("当前用户系统中不存在");
            }
            accountInfo.setPassword(newPassword);
            if (!iLoginAccountInfoService.updateById(accountInfo)) {
                return ResultVo.failure("修改用户登录密码失败");
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("修改用户登录密码失败", e);
        }
    }

    @Override
    public Page<LoginRecordListVo> getLogList(LoginRecordListDto listDto) {
        QueryWrapper<LoginAccountRecords> wrapper = new QueryWrapper<>();
        Page<LoginAccountRecords> recordsPage = new Page<>(listDto.getPageNo(), listDto.getPageSize());
        wrapper.eq("uid", listDto.getUid());
        wrapper.orderByDesc("login_time");
        iLoginAccountRecordsService.page(recordsPage, wrapper);
        // 组装出参
        Page<LoginRecordListVo> voPage = new Page<>();
        BeanUtils.copyProperties(recordsPage, voPage);
        if (!recordsPage.getRecords().isEmpty()) {
            voPage.setRecords(ExtBeanUtils.copyList(recordsPage.getRecords(), LoginRecordListVo.class));
        }
        return voPage;
    }
}
