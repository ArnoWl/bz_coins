package com.auth.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * JWT - 用户及服务鉴权 RSA秘钥配置
 * @author lt
 * 2020年3月18日13:42:13
 */
@Configuration
@Data
public class JwtKeyConfiguration {

    /** 客户端鉴权 -  rsa-secret*/
    @Value("${client.rsa-secret}")
    private String clientSecret;

    /** 客户端鉴权 -  公钥、私钥*/
    private byte[] clientPubKey;
    private byte[] clientPriKey;

    /** 用户鉴权 -  rsa-secret*/
    @Value("${jwt.rsa-secret}")
    private String userSecret;

    /** 用户鉴权 -  公钥、私钥*/
    private byte[] userPubKey;
    private byte[] userPriKey;

}
