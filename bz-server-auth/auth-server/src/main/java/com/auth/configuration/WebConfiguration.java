package com.auth.configuration;

import com.auth.interceptor.ClientAuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Collections;

/**
 * web拦截器配置
 * @author lt
 * 2020年3月18日14:09:26
 */
@Configuration("merchantWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * 客户端鉴权拦截处理
     * @return
     */
    @Bean
    ClientAuthInterceptor getClientAuthInterceptor() {
        return new ClientAuthInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getClientAuthInterceptor())
                .addPathPatterns(getIncludePathPatterns())
                // 放开swagger
                .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**")
                .excludePathPatterns("/**/error");
    }

    /**
     * 需要服务认证的请求路径
     * @return
     */
    private ArrayList<String> getIncludePathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/loginAccount/**"
        };
        Collections.addAll(list, urls);
        return list;
    }

}
