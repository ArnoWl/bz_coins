package com.auth.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *  服务鉴权配置
 * @author lt
 * 2020年3月18日13:41:37
 */
@Configuration
@Data
public class ClientConfiguration {

    @Value("${client.code}")
    private String clientCode;
    @Value("${client.secret}")
    private String clientSecret;
    @Value("${client.token-header}")
    private String clientTokenHeader;

}
