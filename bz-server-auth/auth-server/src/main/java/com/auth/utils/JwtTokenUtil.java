package com.auth.utils;

import com.auth.configuration.JwtKeyConfiguration;
import com.common.jwt.JWTHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * jwt工具类
 * @author lt
 * 2020年3月23日10:43:36
 */
@Component
public class JwtTokenUtil {

    @Value("${jwt.expire}")
    private int expire;

    @Autowired
    private JwtKeyConfiguration jwtKeyConfiguration;


    public String generateUserToken(Map<String,Object> params) throws Exception {
        return JWTHelper.generateToken(params, jwtKeyConfiguration.getUserPriKey(),expire);
    }

    public Map<String,Object> getInfoFromUserToken(String token) throws Exception {
        return JWTHelper.getInfoFromToken(token, jwtKeyConfiguration.getUserPubKey());
    }

    public String generateClientToken(Map<String,Object> params) throws Exception {
        return JWTHelper.generateToken(params, jwtKeyConfiguration.getClientPriKey(),expire);
    }

    public Map<String,Object> getInfoFromClientToken(String token) throws Exception {
        return JWTHelper.getInfoFromToken(token, jwtKeyConfiguration.getClientPubKey());
    }

}
