package com.auth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 代理商登录参数实体
 * @Author: lt
 * @Date: 2020年4月29日11:49:11
 */
@Data
@ApiModel(value = "代理商登录参数")
public class AgentLoginIn extends BaseLoginIn {

    private static final long serialVersionUID = 8343923432137699575L;

    @ApiModelProperty(value = "手机号")
    @NotBlank(message = "手机号不能为空")
    private String telphone;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "短信验证码")
    private String verificationCode;

}
