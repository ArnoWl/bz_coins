package com.auth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 移动端用户登录参数实体
 * @Author: lt
 * @Date: 2020/3/24 17:19
 */
@Data
@ApiModel(value = "移动端用户登录")
public class UserLoginIn extends BaseLoginIn {

    private static final long serialVersionUID = -1815363757564505216L;

    @ApiModelProperty(value = "手机账号")
    @NotBlank(message = "手机号不能为空")
    private String phoneAccount;

    @ApiModelProperty(value = "登录密码（MD5）")
    @NotBlank(message = "登录密码不能为空")
    private String passWord;

}
