package com.auth.model;

import com.mall.base.AppBaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author: lt
 * @Date: 2020/7/3 17:15
 */
@Data
@ApiModel(value = "修改登录密码入参")
public class UpdatePasswordIn implements Serializable {

    private static final long serialVersionUID = 198200002232596785L;

    @ApiModelProperty(value = "手机账号")
    @NotBlank(message = "手机号不能为空")
    private String phoneAccount;

    @ApiModelProperty(value = "验证码")
    @NotBlank(message = "验证码不能为空")
    private String verificationCode;

    @ApiModelProperty(value = "新密码（MD5）")
    @NotBlank(message = "新密码不能为空")
    private String newPassword;

}
