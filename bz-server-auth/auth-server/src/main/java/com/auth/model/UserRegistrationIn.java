package com.auth.model;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户信息注册参数数据实体
 *
 * @Author: lt
 * @Date: 2020/5/27 14:11
 */
@Data
@ApiModel(value = "移动端用户注册")
public class UserRegistrationIn implements Serializable {

    private static final long serialVersionUID = -4219132937788151523L;

    @ApiModelProperty(value = "手机账号")
    @NotBlank(message = "手机账号不能为空")
    private String phoneAccount;

    @ApiModelProperty(value = "验证码")
    @NotBlank(message = "验证码不能为空")
    private String verificationCode;

    @ApiModelProperty(value = "密码")
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty(value = "支付密码")
    @NotBlank(message = "支付密码不能为空")
    private String payPassword;

    @ApiModelProperty(value = "邀请码")
    @NotBlank(message = "邀请码不能为空")
    private String invitationCode;

}
