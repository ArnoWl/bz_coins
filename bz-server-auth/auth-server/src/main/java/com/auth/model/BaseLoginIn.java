package com.auth.model;

import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统登录基础入参实体
 * @Author: lt
 * @Date: 2020/5/17 11:45
 */
@Data
@ApiModel(value = "系统登录基础入参")
public class BaseLoginIn implements Serializable {

    private static final long serialVersionUID = -322707632658364232L;

    @ApiModelProperty(value = "登录终端：0-Android 1-IOS 2-小程序 3-PC/H5 4-第三方")
    private Integer terminal;

    @ApiModelProperty(value = "版本号")
    @Version
    private String version;

    @ApiModelProperty(value = "设备型号")
    private String deviceModel;

}
