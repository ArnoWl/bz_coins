package com.auth.mapper;

import com.auth.entity.Client;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-23
 */
public interface ClientMapper extends BaseMapper<Client> {

}
