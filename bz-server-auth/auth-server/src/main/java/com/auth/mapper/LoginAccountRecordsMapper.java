package com.auth.mapper;

import com.auth.entity.LoginAccountRecords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 平台用户登录记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
public interface LoginAccountRecordsMapper extends BaseMapper<LoginAccountRecords> {

}
