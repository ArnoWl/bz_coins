package com.auth.mapper;

import com.auth.entity.LoginAccountInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户账户表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
public interface LoginAccountInfoMapper extends BaseMapper<LoginAccountInfo> {

}
