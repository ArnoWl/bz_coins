package com.auth.runner;

import com.auth.configuration.JwtKeyConfiguration;
import com.common.jwt.RsaKeyHelper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.Map;

import static com.common.constatns.RedisKeyConstants.*;

@Configuration
public class AuthServerRunner implements CommandLineRunner {

    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private JwtKeyConfiguration jwtKeyConfiguration;

    @Override
    public void run(String... args) throws Exception {
        // 初始化客户端鉴权RSA秘钥到缓存
        if (redisTemplate.hasKey(REDIS_CLIENT_PRI_KEY) && redisTemplate.hasKey(REDIS_CLIENT_PUB_KEY)) {
            jwtKeyConfiguration.setClientPriKey(RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(REDIS_CLIENT_PRI_KEY)));
            jwtKeyConfiguration.setClientPubKey(RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(REDIS_CLIENT_PUB_KEY)));
        } else {
            Map<String, byte[]> keyMap = RsaKeyHelper.generateKey(jwtKeyConfiguration.getClientSecret());
            jwtKeyConfiguration.setClientPriKey(keyMap.get("pri"));
            jwtKeyConfiguration.setClientPubKey(keyMap.get("pub"));
            redisTemplate.opsForValue().set(REDIS_CLIENT_PRI_KEY, RsaKeyHelper.toHexString(keyMap.get("pri")));
            redisTemplate.opsForValue().set(REDIS_CLIENT_PUB_KEY, RsaKeyHelper.toHexString(keyMap.get("pub")));
        }
        // 初始化用户鉴权RSA秘钥到缓存
        if (redisTemplate.hasKey(REDIS_USER_PRI_KEY) && redisTemplate.hasKey(REDIS_USER_PUB_KEY)) {
            jwtKeyConfiguration.setUserPriKey(RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(REDIS_USER_PRI_KEY)));
            jwtKeyConfiguration.setUserPubKey(RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(REDIS_USER_PUB_KEY)));
         } else {
            Map<String, byte[]> keyMap = RsaKeyHelper.generateKey(jwtKeyConfiguration.getUserSecret());
            jwtKeyConfiguration.setUserPriKey(keyMap.get("pri"));
            jwtKeyConfiguration.setUserPubKey(keyMap.get("pub"));
            redisTemplate.opsForValue().set(REDIS_USER_PRI_KEY, RsaKeyHelper.toHexString(keyMap.get("pri")));
            redisTemplate.opsForValue().set(REDIS_USER_PUB_KEY, RsaKeyHelper.toHexString(keyMap.get("pub")));
        }
    }
}
