package com.auth.controller;

import com.auth.configuration.JwtKeyConfiguration;
import com.auth.entity.GetEncryptionRulesIn;
import com.auth.utils.JwtTokenUtil;
import com.common.base.ResultVo;
import com.common.constatns.RedisKeyConstants;
import com.common.exception.BaseException;
import com.common.jwt.JWTHelper;
import com.common.jwt.RsaKeyHelper;
import com.common.utils.RsaUtils;
import com.common.utils.codec.AESUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.common.constatns.JWTConstants.JWT_SUBJECT;

/**
 * 数据传输安全控制器
 * @author lt
 * 2020年3月16日13:41:14
 */
@CrossOrigin
@Api(description = "数据传输安全模块")
@RestController
@RequestMapping("dataSecurity")
@Slf4j
public class DataSecurityController {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Resource
    private JwtKeyConfiguration jwtKeyConfiguration;

    @Resource
    private JwtTokenUtil jwtTokenUtil;

    @ApiOperation(value = "获取服务端用户公钥")
    @RequestMapping(value = "getUserPubKey", method = RequestMethod.GET)
    public ResultVo getSysUserToke(){
        try {
            return ResultVo.success(jwtKeyConfiguration.getUserPubKey());
        } catch (Exception e) {
            throw new BaseException("获取服务端用户公钥失败",e);
        }
    }

    @ApiOperation(value = "获取加密规则")
    @RequestMapping(value = "getEncryptionRules", method = RequestMethod.POST)
    public ResultVo refreshToken(@RequestBody GetEncryptionRulesIn rulesIn) {
        try {
            Map<String,Object> params = jwtTokenUtil.getInfoFromUserToken(rulesIn.getToken());
            if(params != null && params.get(JWT_SUBJECT) != null){
                String subject = params.get(JWT_SUBJECT).toString();
                String uid = JWTHelper.getUserIdBySubject(subject,params);
                if(StringUtils.isNotBlank(uid)){
                    // 根据当前用户Id + 时间戳 生成AES加密规则
                    String AESCode = AESUtils.getHexStr(uid + System.currentTimeMillis(),jwtKeyConfiguration.getUserSecret().getBytes());
                    if(StringUtils.isNotBlank(AESCode)){
                        // 存储当前登录用户的加密规则到redis
                        redisTemplate.opsForValue().set(RedisKeyConstants.USER_AES_ENCRYPT_PREFIX + subject + ":" + uid, AESCode, 30, TimeUnit.MINUTES);
                        // 解密获取客户端RSA公钥
                        RsaKeyHelper keyHelper = new RsaKeyHelper();
                        String userPubKey = RsaUtils.segmentDecrypt(
                                (RSAPrivateKey) keyHelper.getPrivateKey(jwtKeyConfiguration.getUserPriKey())
                                ,RsaUtils.strToBase64(rulesIn.getClientPubKey()));
                        // 使用客户端公钥对 AES加密规则  进行加密再返回 （只有当前会话客户端可以解密）
                        return ResultVo.success(RsaUtils.decryptEncrypt((RSAPublicKey) keyHelper.getPublicKey(RsaUtils.strToBase64(userPubKey)),AESCode.getBytes()));
                    }
                }
            }
            return ResultVo.failure("用户信息异常，获取签名加密规则失败");
        } catch (Exception e) {
            throw new BaseException("获取签名加密规则失败",e);
        }
    }

}
