package com.auth.controller;

import com.auth.model.UpdatePasswordIn;
import com.auth.model.UserLoginIn;
import com.auth.model.UserRegistrationIn;
import com.auth.service.ILoginAccountInfoService;
import com.auth.utils.JwtTokenUtil;
import com.common.base.ResultVo;
import com.common.constatns.AdminConstants;
import com.common.exception.BaseException;
import com.common.exception.UserTokenException;
import com.google.common.collect.Maps;
import com.mall.feign.admin.SysUserClient;
import com.mall.model.admin.dto.SysUserLoginDto;
import com.mall.model.admin.vo.SysUserInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

import static com.common.constatns.JWTConstants.*;

/**
 * 用户授权控制器
 * @author lt
 * 2020年3月16日13:41:14
 */
@CrossOrigin
@Api(description = "用户授权模块")
@RestController
@RequestMapping("user")
public class UserAuthController {

    @Resource
    private JwtTokenUtil jwtTokenUtil;
    @Resource
    private SysUserClient sysUserClient;
    @Resource
    private ILoginAccountInfoService iLoginAccountInfoService;

    @ApiOperation(value = "获取系统用户登录授权token")
    @RequestMapping(value = "getAdminToken", method = RequestMethod.POST)
    public ResultVo getSysUserToke(@RequestBody SysUserLoginDto loginDto){
        try {
            SysUserInfoVo info = sysUserClient.getSysUserInfo(loginDto);
            if (info == null) {
                return ResultVo.failure("平台系统服务中断，获取系统用户登录信息失败");
            }
            if(StringUtils.isBlank(info.getUserAccount())){
                return ResultVo.failure("用户名或密码有误，登录失败");
            }
            if(AdminConstants.ENABLED_STATUS_1.toString().equals(info.getStatus())){
                return ResultVo.failure("用户状态被禁用，登录失败");
            }
            // 生成 JWT 令牌
            Map<String,Object> params = Maps.newHashMap();
            params.put(JWT_SUBJECT,JWT_KEY_USER_TYPE_ADMIN);
            params.put(JWT_KEY_SYS_USER_ID,info.getId());
            params.put(JWT_KEY_SYS_USER_ACCOUNT,info.getUserAccount());
            final String token = jwtTokenUtil.generateUserToken(params);
            return ResultVo.success(token);
        } catch (Exception e) {
            throw new BaseException("登录失败",e);
        }
    }

    @ApiOperation(value = "获取普通用户登录授权token")
    @RequestMapping(value = "getUserToken", method = RequestMethod.POST)
    public ResultVo getUserToken(@RequestBody UserLoginIn loginIn){
        return iLoginAccountInfoService.handleUserLogin(loginIn);
    }

    @ApiOperation(value = "用户注册")
    @RequestMapping(value = "userRegistration", method = RequestMethod.POST)
    public ResultVo userRegistration(@RequestBody UserRegistrationIn registrationIn) {
        return iLoginAccountInfoService.handleUserRegistration(registrationIn);
    }

    @ApiOperation(value = "忘记登录密码")
    @RequestMapping(value = "forgetPassword", method = RequestMethod.POST)
    public ResultVo forgetPassword(@RequestBody UpdatePasswordIn passwordIn) {
        return iLoginAccountInfoService.updateUserLoginPassword(passwordIn);
    }

    @ApiOperation(value = "更新token")
    @RequestMapping(value = "refreshToken", method = RequestMethod.GET)
    public ResultVo refreshToken(@RequestParam("oldToken") String oldToken){
        ResultVo resultVo  = ResultVo.success();
        if(StringUtils.isBlank(oldToken)){
            return ResultVo.failure("用户token不存在");
        }
        try {
            resultVo.setData(jwtTokenUtil.generateUserToken(jwtTokenUtil.getInfoFromUserToken(oldToken)));
            return resultVo;
        } catch (Exception e) {
            throw new UserTokenException("更新token失败，token参数异常");
        }
    }

    @ApiOperation(value = "获取系统当前时间")
    @RequestMapping(value = "getSystemTime", method = RequestMethod.GET)
    public ResultVo getSystemTime() {
        return ResultVo.success(LocalDateTime.now());
    }

    @ApiOperation(value = "验证token是否有效")
    @RequestMapping(value = "verify", method = RequestMethod.GET)
    public ResultVo verify(@RequestParam("token") String token){
        if(StringUtils.isBlank(token)){
            return ResultVo.failure("用户token不存在");
        }
        try {
            return ResultVo.success(jwtTokenUtil.getInfoFromUserToken(token));
        } catch (Exception e) {
            throw new UserTokenException("验证token失败，token参数异常");
        }
    }

}
