package com.auth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 平台用户登录记录表
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="LoginAccountRecords对象", description="平台用户登录记录表")
public class LoginAccountRecords implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "登录令牌")
    private String token;

    @ApiModelProperty(value = "登录用户UID")
    private String uid;

    @ApiModelProperty(value = "登录使用账号")
    private String account;

    @ApiModelProperty(value = "登录终端：0-Android 1-IOS 2-小程序 3-PC/H5 4-第三方")
    private Integer terminal;

    @ApiModelProperty(value = "版本号")
    private String version;

    @ApiModelProperty(value = "ip地址")
    private String ipAddr;

    @ApiModelProperty(value = "ip所在城市")
    private String ipArea;

    @ApiModelProperty(value = "设备型号")
    private String deviceModel;

    @ApiModelProperty(value = "登录时间")
    private LocalDateTime loginTime;


}
