package com.auth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户账户表
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="LoginAccountInfo对象", description="用户账户表")
public class LoginAccountInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "平台用户UID")
    @TableId(value = "uid", type = IdType.UUID)
    private String uid;

    @ApiModelProperty(value = "手机账号")
    private String phoneAccount;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "绑定的微信openId")
    private String openId;

    @ApiModelProperty(value = "绑定的微信unionId")
    private String unionId;

    @ApiModelProperty(value = "用户微信号")
    private String userWx;

    @ApiModelProperty(value = "账号状态：0-正常 1-锁定")
    private Integer status;

    @ApiModelProperty(value = "上次登录时间")
    private LocalDateTime lastLoginTime;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

}
