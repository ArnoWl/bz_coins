package com.auth.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 获取加密规则入参实体
 * @Author: lt
 * @Date: 2020/3/17 16:10
 */
@ApiModel(value = "获取加密规则入参")
@Data
public class GetEncryptionRulesIn extends AuthBaseParamIn{

    private static final long serialVersionUID = 7658901201430559691L;

    @ApiModelProperty(value = "用户token（登录授权后获取）",required = true)
    @NotBlank(message = "用户token不能为空")
    private String token;

    @ApiModelProperty(value = "客户端生成的RSA公钥 -- （通过服务端RSA公钥加密后的）",required = true)
    @NotBlank(message = "客户端生成的RSA公钥不能为空")
    private String clientPubKey;
}
