package com.auth.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 认证服务基础入参实体
 * @Author: lt
 * @Date: 2020/3/18 18:01
 */
@ApiModel(value = "基础入参")
@Data
public class AuthBaseParamIn implements Serializable {

    private static final long serialVersionUID = -2904017842938457320L;
    /**
     * 时间戳
     */
    @NotBlank(message="时间戳不能为空.")
    @ApiModelProperty(value = "时间戳",name = "时间戳",example = "15646123416",required = true)
    private String timestamp;


}
