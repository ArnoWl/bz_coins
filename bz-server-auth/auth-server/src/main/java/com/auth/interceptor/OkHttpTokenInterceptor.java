package com.auth.interceptor;

import com.auth.configuration.ClientConfiguration;
import com.auth.service.IClientService;
import com.mall.model.auth.dto.AuthenticationClientDto;
import lombok.extern.java.Log;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;


/**
 * @author ace
 */
@Component
@Log
public class OkHttpTokenInterceptor implements Interceptor {

    @Resource
    private ClientConfiguration clientConfiguration;
    @Resource
    private IClientService iClientService;

    @Override
    public Response intercept(Chain chain) throws IOException {
        AuthenticationClientDto clientDto = new AuthenticationClientDto();
        clientDto.setClientCode(clientConfiguration.getClientCode());
        clientDto.setClientSecret(clientConfiguration.getClientSecret());
        Request newRequest = chain.request()
                .newBuilder()
                .header(clientConfiguration.getClientTokenHeader()
                        , iClientService.getAccessToken(clientDto).getData().toString())
                .build();
        return chain.proceed(newRequest);
    }


}
