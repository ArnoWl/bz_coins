package com.auth.interceptor;

import com.auth.configuration.ClientConfiguration;
import com.auth.utils.JwtTokenUtil;
import com.common.exception.ClientTokenException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 客戶端鉴权拦截
 * @author lt
 * 2020年3月16日18:42:21
 */
public class ClientAuthInterceptor extends HandlerInterceptorAdapter {

    @Resource
    private JwtTokenUtil jwtTokenUtil;

    @Resource
    private ClientConfiguration clientConfiguration;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            String token = request.getHeader(clientConfiguration.getClientTokenHeader());
            jwtTokenUtil.getInfoFromClientToken(token);
        } catch (Exception e) {
            throw new ClientTokenException("未能通过客户端鉴权访问");
        }
        return super.preHandle(request, response, handler);
    }
}
