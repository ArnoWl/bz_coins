package com.auth;


import com.common.sms.SmsComponent;
import com.mall.aspect.WebLogAspect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

/**
 * @author tps
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.mall.feign")
@Import({WebLogAspect.class,SmsComponent.class})
public class AuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
    }

}
