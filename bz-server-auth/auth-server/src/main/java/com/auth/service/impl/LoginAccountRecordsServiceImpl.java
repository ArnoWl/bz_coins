package com.auth.service.impl;

import com.auth.entity.LoginAccountRecords;
import com.auth.mapper.LoginAccountRecordsMapper;
import com.auth.service.ILoginAccountRecordsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台用户登录记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
@Service
public class LoginAccountRecordsServiceImpl extends ServiceImpl<LoginAccountRecordsMapper, LoginAccountRecords> implements ILoginAccountRecordsService {

}
