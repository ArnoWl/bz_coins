package com.auth.service;

import com.auth.entity.LoginAccountInfo;
import com.auth.model.UpdatePasswordIn;
import com.auth.model.UserLoginIn;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;
import com.auth.model.UserRegistrationIn;

/**
 * <p>
 * 用户账户表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
public interface ILoginAccountInfoService extends IService<LoginAccountInfo> {

    /**
     * 处理用户登录
     * @param loginIn
     * @return
     */
    ResultVo handleUserLogin(UserLoginIn loginIn);

    /**
     * 处理用户注册
     * @param registrationDto
     * @return
     */
    ResultVo handleUserRegistration(UserRegistrationIn registrationDto);

    /**
     * 修改用户登录密码
     * @param passwordIn
     * @return
     */
    ResultVo updateUserLoginPassword(UpdatePasswordIn passwordIn);
}
