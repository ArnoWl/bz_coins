package com.auth.service;

import com.auth.entity.LoginAccountRecords;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台用户登录记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
public interface ILoginAccountRecordsService extends IService<LoginAccountRecords> {


}
