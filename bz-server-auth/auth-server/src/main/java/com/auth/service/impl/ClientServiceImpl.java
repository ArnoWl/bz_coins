package com.auth.service.impl;

import com.auth.entity.Client;
import com.auth.mapper.ClientMapper;
import com.auth.service.IClientService;
import com.auth.utils.JwtTokenUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.exception.ClientTokenException;
import com.google.common.collect.Maps;
import com.mall.model.auth.dto.AuthenticationClientDto;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

import static com.common.constatns.JWTConstants.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-23
 */
@Service
public class ClientServiceImpl extends ServiceImpl<ClientMapper, Client> implements IClientService {

    @Resource
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public ResultVo authenticationClient(AuthenticationClientDto clientDto) {
        try {
            // 查询客户端信息是否存在于数据库
            Client client = new Client();
            client.setCode(clientDto.getClientCode());
            client.setSecret(clientDto.getClientSecret());
            QueryWrapper<Client> wrapper = new QueryWrapper<>();
            wrapper.setEntity(client);
            client = this.getOne(wrapper);
            if(client != null){
                return ResultVo.success(client);
            }else{
                return ResultVo.failure("客户端未在平台注册服务");
            }
        } catch (Exception e) {
            throw new ClientTokenException("认证客户端信息失败");
        }
    }

    @Override
    public ResultVo getAccessToken(AuthenticationClientDto clientDto) {
        ResultVo resultVo = this.authenticationClient(clientDto);
        if(!resultVo.isSuccess()){
            return resultVo;
        }
        try {
            Client client = (Client) resultVo.getData();
            // 生成 JWT 令牌
            Map<String,Object> params = Maps.newHashMap();
            params.put(JWT_SUBJECT,JWT_KEY_CLIENT_TYPE_REST);
            params.put(JWT_KEY_CLIENT_CODE,client.getCode());
            params.put(JWT_KEY_CLIENT_NAME,client.getName());
            final String token = jwtTokenUtil.generateClientToken(params);
            return ResultVo.success(token);
        } catch (Exception e) {
            throw new ClientTokenException("获取请求token失败");
        }
    }
}
