package com.auth.service.impl;

import com.auth.entity.LoginAccountInfo;
import com.auth.entity.LoginAccountRecords;
import com.auth.mapper.LoginAccountInfoMapper;
import com.auth.model.UpdatePasswordIn;
import com.auth.model.UserLoginIn;
import com.auth.service.ILoginAccountInfoService;
import com.auth.service.ILoginAccountRecordsService;
import com.auth.utils.JwtTokenUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.config.RedisLock;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.exception.BaseException;
import com.common.jwt.RsaKeyHelper;
import com.common.sms.*;
import com.common.utils.IpUtils;
import com.google.common.collect.Maps;
import com.mall.feign.user.UserInfoClient;
import com.auth.model.UserRegistrationIn;
import com.mall.model.user.dto.CreateUserDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.common.constatns.JWTConstants.*;
import static com.common.constatns.RedisKeyConstants.REDIS_CLIENT_PRI_KEY;
import static com.common.constatns.RedisKeyConstants.USER_SSO_VERSION;

/**
 * <p>
 * 用户账户表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-31
 */
@Slf4j
@Service
public class LoginAccountInfoServiceImpl extends ServiceImpl<LoginAccountInfoMapper, LoginAccountInfo> implements ILoginAccountInfoService {

    @Resource
    private JwtTokenUtil jwtTokenUtil;
    @Resource
    private SmsComponent smsComponent;
    @Resource
    private UserInfoClient userInfoClient;
    @Resource
    private ILoginAccountRecordsService iLoginAccountRecordsService;
    @Resource
    private RedisTemplate<String,String> redisTemplate;
    @Resource
    private RedisLock redisLock;



    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo handleUserLogin(UserLoginIn loginIn) {
        try {
            LoginAccountInfo account = this.getUserAccount(loginIn.getPhoneAccount());
            if (account == null) {
                return ResultVo.failure("当前手机未注册");
            }
            if (!loginIn.getPassWord().equals(account.getPassword())) {
                return ResultVo.failure("密码错误");
            }
            // 验证用户状态信息
            ResultVo resultVo = userInfoClient.validateUserByUid(account.getUid());
            if (!resultVo.isSuccess()) {
                return resultVo;
            }
            String lockKey = RedisKeyConstants.LOCK_USER_LOGIN + account.getUid();
            String value = redisLock.getDefaultOutTime();
            if (!redisLock.lock(lockKey, value)) {
                return ResultVo.failure("登录中...");
            }
            // 生成 JWT 令牌
            Map<String, Object> params = Maps.newHashMap();
            // 单点登录
            String ssoKey = USER_SSO_VERSION + account.getUid();
            int ssoVersion = 0;
            if (redisTemplate.hasKey(ssoKey)) {
                ssoVersion = Integer.parseInt(redisTemplate.opsForValue().get(ssoKey)) + 1;
                params.put(JWT_KEY_USER_SSO, ssoVersion);
            }
            redisTemplate.opsForValue().set(ssoKey, String.valueOf(ssoVersion));
            params.put(JWT_SUBJECT, JWT_KEY_USER_TYPE_USER);
            params.put(JWT_KEY_USER_ID, account.getUid());
            params.put(JWT_KEY_USER_SSO, ssoVersion);
            params.put(JWT_KEY_USER_ACCOUNT, account.getPhoneAccount());
            final String token = jwtTokenUtil.generateUserToken(params);
            // 记录用户登录信息
            this.addLoginRecord(token, account.getUid(), loginIn);
            redisLock.unlock(lockKey, value);
            return ResultVo.success(token);
        } catch (Exception e) {
            throw new BaseException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo handleUserRegistration(UserRegistrationIn registrationDto) {
        try {
            // 校验用户注册验证码
            VerifySmsCodeDto codeDto = new VerifySmsCodeDto();
            codeDto.setSmsType(SmsSendTypeEnum.SMS_TYPE_1.getKey());
            codeDto.setPhone(registrationDto.getPhoneAccount());
            codeDto.setVerificationCode(registrationDto.getVerificationCode());
            ResultVo resultVo = smsComponent.verifySmsCode(codeDto);
            if(!resultVo.isSuccess()){
                return resultVo;
            }
            // 校验用户是否已存在登录信息
            LoginAccountInfo account = this.getUserAccount(registrationDto.getPhoneAccount());
            if(account != null){
                return ResultVo.failure("当前手机号已在平台注册，请前往登录");
            }
            // 创建登录账户信息
            account = new LoginAccountInfo();
            account.setPhoneAccount(registrationDto.getPhoneAccount());
            account.setPassword(registrationDto.getPassword());
            if(!this.save(account)) {
                return ResultVo.failure("执行添加用户登录账户信息失败");
            }
            // 创建用户基础信息
            CreateUserDto userDto = new CreateUserDto();
            userDto.setUid(account.getUid());
            userDto.setPayPassword(registrationDto.getPayPassword());
            userDto.setInvitationCode(registrationDto.getInvitationCode());
            resultVo = userInfoClient.createUser(userDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException(e.getMessage(), e);
        }
    }

    private LoginAccountInfo getUserAccount(String phoneAccount){
        QueryWrapper<LoginAccountInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("phone_account", phoneAccount);
        return this.getOne(wrapper);
    }

    /**
     * 记录用户登录信息
     *
     * @param token
     * @param uid
     * @param loginIn
     */
    private void addLoginRecord(String token, String uid, UserLoginIn loginIn) {
        LoginAccountRecords records = new LoginAccountRecords();
        records.setToken(token);
        records.setUid(uid);
        records.setAccount(StringUtils.isBlank(loginIn.getPhoneAccount()) ? uid : loginIn.getPhoneAccount());
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();
        records.setIpAddr(IpUtils.getRemoteAddr(request));
        records.setTerminal(loginIn.getTerminal());
        iLoginAccountRecordsService.save(records);
    }

    @Override
    public ResultVo updateUserLoginPassword(UpdatePasswordIn passwordIn) {
        try {
            // 根据手机号查询用户信息
            QueryWrapper<LoginAccountInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("phone_account", passwordIn.getPhoneAccount());
            LoginAccountInfo accountInfo = this.getOne(wrapper);
            if(accountInfo == null){
                return ResultVo.failure("当前手机号未注册，请前往注册");
            }
            // 校验手机验证码
            VerifySmsCodeDto codeDto = new VerifySmsCodeDto();
            codeDto.setPhone(passwordIn.getPhoneAccount());
            codeDto.setSmsType(SmsSendTypeEnum.SMS_TYPE_2.getKey());
            codeDto.setVerificationCode(passwordIn.getVerificationCode());
            ResultVo resultVo = smsComponent.verifySmsCode(codeDto);
            if(!resultVo.isSuccess()){
                return resultVo;
            }
            accountInfo.setPassword(passwordIn.getNewPassword());
            if(!this.updateById(accountInfo)){
                return ResultVo.failure("修改用户登录密码失败");
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("修改用户登录密码失败",e);
        }
    }
}
