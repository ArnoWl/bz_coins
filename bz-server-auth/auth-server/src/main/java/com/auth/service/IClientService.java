package com.auth.service;

import com.auth.entity.Client;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;
import com.mall.model.auth.dto.AuthenticationClientDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-23
 */
public interface IClientService extends IService<Client> {

    /**
     * 验证客户端
     * @param clientDto
     * @return
     */
    ResultVo authenticationClient(AuthenticationClientDto clientDto);

    /**
     * 获取请求token
     * @param clientDto
     * @return
     */
    ResultVo getAccessToken(AuthenticationClientDto clientDto);
}
