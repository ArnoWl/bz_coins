package com.wallet.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.common.base.ResultVo;
import com.common.config.RedisUtils;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.AgreeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.ListUtils;
import com.common.utils.lang.NumberUtils;
import com.common.utils.lang.StringUtils;
import com.google.common.collect.Lists;
import com.mall.feign.admin.BaseClient;
import com.mall.feign.user.UserInfoClient;
import com.mall.model.admin.dto.CoinAddrDto;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.dto.WalletRechargeDto;
import com.mall.model.user.vo.WalletInfoVo;
import com.mall.model.wallet.dto.BitDto;
import com.mall.model.wallet.vo.ERC20RechargeVo;
import com.wallet.config.ERC20Config;
import com.wallet.config.USDTPrice;
import com.wallet.core.btc.OmniUtils;
import com.wallet.core.eth.ERC20Utils;
import com.wallet.entity.RechargeLogs;
import com.wallet.model.vo.ImputedVo;
import com.wallet.service.IRechargeLogsService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.web3j.utils.Convert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.common.constatns.ThirdConstants.*;
import static com.common.constatns.WalletConstants.*;

/**
 * 任务调度：处理平台配置信息
 * @Author: lt
 * @Date: 2020/7/11 13:52
 */
@Slf4j
@Component
public class HandleWalletJob {

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private IRechargeLogsService iRechargeLogsService;
    @Resource
    private BaseClient baseClient;
    @Resource
    private OmniUtils omniUtils;
    @Resource
    private ERC20Utils erc20Utils;
    @Resource
    private ERC20Config erc20Config;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private UserInfoClient userInfoClient;

    /**
     * 修改 USDT 实时价
     * @return
     */
    @XxlJob(UPDATE_USDT_PRICE)
    public ReturnT<String> OffSiteOMNIRecharge(String params) {
        BigDecimal price= USDTPrice.getUsdtToCny();
        if(price.compareTo(BigDecimal.ZERO)>0){
            redisUtils.setUSDTPrice(price);
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 处理OMNI场外充值 每5分钟执行一次
     * @param params
     * @return
     */
    @XxlJob(OMNI_OFF_SITE_RECHARGE)
    @Transactional(rollbackFor = Exception.class)
    public ReturnT<String> OMNIOffSiteRecharge(String params) {
        try {
            List<BitDto> bitDtoList = omniUtils.getTxlist(2000, 0);
            if(!CollectionUtils.isEmpty(bitDtoList)){
                List<RechargeLogs> rechargeLogsList = Lists.newArrayList();
                List<WalletLogDto> walletLogDtos=new ArrayList<>();
                bitDtoList.forEach(b ->{
                    if (!b.isValid()) { return; }
                    BigDecimal usdt = new BigDecimal(b.getAmount());
                    QueryWrapper<RechargeLogs> wrapper = new QueryWrapper<>();
                    wrapper.eq("hash_code", b.getTxid());
                    if (iRechargeLogsService.count(wrapper) >= 1) {
                        return;
                    }
                    WalletInfoVo walletInfoVo = userInfoClient.getOMNIWalletByAddr(b.getReferenceaddress());
                    if(walletInfoVo == null){
                        return;
                    }
                    RechargeLogs rechargeLogs = new RechargeLogs();
                    rechargeLogs.setUid(walletInfoVo.getUid());
                    rechargeLogs.setCoinSymbol(SysCoinsTypeEnums.USDT.name());
                    rechargeLogs.setAgreeType(AgreeEnums.OMNI.name());
                    rechargeLogs.setHashCode(b.getTxid());
                    rechargeLogs.setWalletSendAddr(b.getSendingaddress());
                    rechargeLogs.setWalletReceivedAddr(b.getReferenceaddress());
                    rechargeLogs.setCoinNum(usdt);
                    rechargeLogsList.add(rechargeLogs);
                    // 处理保存用户钱包金额
                    WalletLogDto wallLogDto = new WalletLogDto(rechargeLogs.getUid(), rechargeLogs.getCoinSymbol(),
                            rechargeLogs.getHashCode(), WalletLogTypeEnums.RECHARGE_OUT.getTypeId(), UserConstants.WALLET_IN, rechargeLogs.getCoinNum(), "接收来至【" + rechargeLogs.getWalletSendAddr() + "】的充值");
                    walletLogDtos.add(wallLogDto);
                });
                if(!rechargeLogsList.isEmpty()){
                    iRechargeLogsService.saveBatch(rechargeLogsList);
                }
                //批量处理资金明细
                if(walletLogDtos!=null && walletLogDtos.size()>0){
                    ResultVo resultVo = userInfoClient.handleUserWalletMoneyMore(walletLogDtos);
                    if (!resultVo.isSuccess()) {
                        throw new BaseException(resultVo.getErrorMessage());
                    }
                }
            }
        } catch (Exception e) {
            throw new BaseException("处理OMNI场外充值失败",e);
        }
        return ReturnT.SUCCESS;
    }

    /**
     * erc20处理场外充值  每10分钟执行一次
     * @return
     */
    @XxlJob(ERC20_OFF_SITE_RECHARGE)
    public ReturnT<String> ERC20OffSiteRecharge(String params) {
        List<WalletRechargeDto> rechargeDtoList = userInfoClient.getRechargeByCoinSymbol(params);
        if(!CollectionUtils.isEmpty(rechargeDtoList)){
            // 拆分集合 单线程处理量：100
            List<List<WalletRechargeDto>> rechargeDtoLists = ListUtils.subWithLen(rechargeDtoList,100);
            // 多线程处理 50-200
            ExecutorService poolExecutor = new ThreadPoolExecutor(50, 200,
                    0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<>(1024), new ThreadPoolExecutor.AbortPolicy());
            rechargeDtoLists.forEach(r -> poolExecutor.execute(() -> handleERC20Recharge(r)));
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 处理ERC20充值
     * @param rechargeDtoList
     */
    public void handleERC20Recharge(List<WalletRechargeDto> rechargeDtoList){
        rechargeDtoList.parallelStream().forEach(r -> {
            //获取充值记录
            List<ERC20RechargeVo> rechargeVos = erc20Utils.handleERC20Recharge(r.getUid(), r.getContractAddr(), r.getWalletAddr());
            if(!CollectionUtils.isEmpty(rechargeVos)){
                for (ERC20RechargeVo e : rechargeVos) {
                    QueryWrapper<RechargeLogs> wrapper = new QueryWrapper<>();
                    wrapper.eq("hash_code", e.getHashCode());
                    if (iRechargeLogsService.count(wrapper) >= 1) {
                        break;
                    }
                    //处理保存用户钱包金额
                    WalletLogDto wallLogDto = new WalletLogDto(r.getUid(), e.getCoinSymbol(),
                        e.getHashCode(), WalletLogTypeEnums.RECHARGE_OUT.getTypeId(), UserConstants.WALLET_IN, e.getCoinNum(), "接收来至【" + e.getWalletSendAddr() + "】的充值");
                    ResultVo resultVo = userInfoClient.handleUserWalletMoney(wallLogDto);
                    if (!resultVo.isSuccess()) {
                       return;
                    }
                    //保存充值记录
                    RechargeLogs rechargeLogs = new RechargeLogs();
                    BeanUtils.copyProperties(e, rechargeLogs);
                    rechargeLogs.setUid(r.getUid());
                    rechargeLogs.setAgreeType(AgreeEnums.ERC20.name());
                    rechargeLogs.setContractAddr(e.getContractaddress());
                    iRechargeLogsService.save(rechargeLogs);
                }
            }
        });
    }

    /**
     * 归集
     * @param params
     * @return
     */
    @XxlJob(USER_WALLET_COLLECTION)
    public ReturnT<String> userWalletCollection(String params){
        // 超过这个金额才归集 不然浪费手续费不划算
        BigDecimal min = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.IMPURED_MIN));
        // 获取平台钱包地址配置
        List<CoinAddrDto> coinAddrList = baseClient.getCoinAddrList();
        if(CollectionUtils.isEmpty(coinAddrList)){
            return new ReturnT<>(ReturnT.FAIL_CODE,"未能获取的系统钱包地址，归集失败");
        }
        // 归集OMNI归集
        this.collectOMNIRecharge(min,coinAddrList);
        // 归集ERC20归集
        this.collectERC20Recharge(min,coinAddrList);
        return ReturnT.SUCCESS;
    }

    /**
     * 归集OMNI充值
     * @param min  最低归集金额
     * @param coinAddrList  系统钱包地址
     */
    private void collectOMNIRecharge(BigDecimal min, List<CoinAddrDto> coinAddrList){
        // OMNI主地址
        List<CoinAddrDto> mainAddrList = coinAddrList.stream().filter(c ->
                c.getAgreeType().equals(AGREE_TYPE_OMNI) && c.getType().equals(WALLET_ADDR_MAIN)).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(mainAddrList)){
            return ;
        }
        // OMNI手续费地址
        List<CoinAddrDto> feeAddrList = coinAddrList.stream().filter(c ->
                c.getAgreeType().equals(AGREE_TYPE_OMNI) && c.getType().equals(WALLET_ADDR_FEE)).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(feeAddrList)) {
            return;
        }
        ImputedVo imputedVo=new ImputedVo();
        imputedVo.setAgreeType(AGREE_TYPE_OMNI);
        imputedVo.setMin(min);
        /**
         * 循环手续费地址
         */
        for (CoinAddrDto  f: feeAddrList) {
            //每次取25条 如果没有记录
            List<ImputedVo> list = iRechargeLogsService.queryImputed(imputedVo);
            if(CollectionUtils.isEmpty(list)) {
                break;
            }
            int i=0;
            for (ImputedVo o :list) {
                /**
                 * 循环主地址  每次都换一个主地址归集  每次都换一个主地址归集 一直循环
                 */
                CoinAddrDto m=mainAddrList.get(i);
                i++;
                if(i>mainAddrList.size()-1){
                    i=0;
                }
                log.info("【自动归集】充值地址/发送方:" + o.getWalletReceivedAddr() + " ,接收USDT地址：" + m.getWalletAddr());
                String code = omniUtils.sendrawtransaction(o.getWalletReceivedAddr(),f.getWalletAddr(),
                        m.getWalletAddr(),o.getMoney().toPlainString());
                //-999表示当前这个手续费地址 矿池满了 直接换下一个手续费地址就可以
                if("-999".equals(code)){
                    break;//手续费地址满了 接续本次循环 直接下一个手续费地址再来
                }
                if(StringUtils.isNotBlank(code)){
                    // 执行批量修改充值记录  修改当前用户归集的记录
                    updateRechagrLog(o,m,code);
                }
            }
        }
    }

    /**
     * 归集ERC20充值
     * @param min
     * @param coinAddrList
     */
    private void collectERC20Recharge(BigDecimal min, List<CoinAddrDto> coinAddrList){
        ImputedVo imputedVo=new ImputedVo();
        imputedVo.setAgreeType(AGREE_TYPE_ERC20);
        imputedVo.setMin(min);
        List<ImputedVo> erc20List=iRechargeLogsService.queryImputed(imputedVo);
        if(!CollectionUtils.isEmpty(erc20List)){
            // ERC20主地址
            List<CoinAddrDto> erc20AddrList = coinAddrList.stream().filter(c ->
                    c.getAgreeType().equals(AGREE_TYPE_ERC20) && c.getType().equals(WALLET_ADDR_MAIN)).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(erc20AddrList)){
                return ;
            }
            // ERC20 手续费地址只需要一个就可以了
            Optional<CoinAddrDto> optional = coinAddrList.stream().filter(c ->
                    c.getAgreeType().equals(AGREE_TYPE_ERC20) && c.getType().equals(WALLET_ADDR_FEE)).findFirst();
            //如果没有手续费地址 返回
            if(!optional.isPresent()){
                return;
            }
            CoinAddrDto erc20FeeAddr = optional.get();
            // 获取当前ETH的最优手续费
            BigInteger price=erc20Utils.getGasPrice();
            BigInteger limit=Convert.toWei(erc20Config.getGasLimit(), Convert.Unit.WEI).toBigInteger();
            BigDecimal minFree= erc20Utils.getMinerFee(limit);
            int i=0;
            for(ImputedVo e:erc20List){
                // 检验用户钱包地址的ETH是否满足转账手续费条件
                BigDecimal balance = erc20Utils.getBlanceOf(e.getWalletReceivedAddr());
                // 不满足转账手续费，给用户钱包地址转入ETH
                BigDecimal addETH = minFree.subtract(balance);
                if(addETH.compareTo(BigDecimal.ZERO) > 0){
                    // 需要转账的ETH数量进行单位转换
                    String hashcode=erc20Utils.transTo(erc20FeeAddr.getWalletPath(),erc20FeeAddr.getWalletPassword(),e.getWalletReceivedAddr(),addETH,price,limit);
                    if(!StringUtils.isEmpty(hashcode)) {
                        log.info("给会员："+e.getWalletReceivedAddr()+"转ETH:"+addETH+"  交易单号："+hashcode);
                    }
                }else {
                    /**
                     * 循环主地址  每次都换一个主地址归集  每次都换一个主地址归集 一直循环
                     */
                    CoinAddrDto coinAddrDto = erc20AddrList.get(i);
                    i++;
                    if (i > erc20AddrList.size() - 1) {
                        i = 0;
                    }
                    WalletInfoVo walletInfoVo = userInfoClient.getERC20WalletByAddr(e.getWalletReceivedAddr());
                    String code = erc20Utils.erc20ContractTrans(walletInfoVo.getWalletPath(), walletInfoVo.getWalletPassWord()
                            , walletInfoVo.getContractAddr(), coinAddrDto.getWalletAddr(), e.getMoney(), minFree, walletInfoVo.getWalletDeclimal());
                    if (StringUtils.isNotBlank(code)) {
                        // 执行批量修改充值记录  修改当前用户归集的记录
                        updateRechagrLog(e, coinAddrDto, code);
                    }
                }
            }
        }
    }

    public void updateRechagrLog(ImputedVo o,CoinAddrDto m,String code){
        // 执行批量修改充值记录  修改当前用户归集的记录
        RechargeLogs log=new RechargeLogs();
        log.setUid(o.getUid());
        log.setAgreeType(o.getAgreeType());
        log.setCoinSymbol(o.getCoinSymbol());

        log.setTransHashCode(code);
        log.setImputedAddr(m.getWalletAddr());
        log.setImputedType(m.getImputedType());
        iRechargeLogsService.updateCustomerImputed(log);
    }

    public static void main(String[] args) {
        List<Integer> we = Lists.newArrayList();
        for (int i = 0; i < 10000; i++) {
            we.add(i);
        }
        List<Integer> wee = Lists.newArrayList();
        we.parallelStream().forEach(w -> {
            if(w.equals(500)){
                throw new BaseException("触发异常");
            }
        });
        System.out.println("大小" + wee.size());
        wee.forEach(w -> System.out.println("数"+w ));
    }
}
