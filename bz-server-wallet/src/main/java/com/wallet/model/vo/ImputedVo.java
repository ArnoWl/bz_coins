package com.wallet.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author arno
 * @version 1.0
 * @date 2020-08-13
 */
@Data
public class ImputedVo implements Serializable {

    private String uid;

    private BigDecimal money;

    /**
     * 货币类型
     */
    private String coinSymbol;

    /**
     *协议类型 OMNI ERC20
     */
    private String agreeType;

    /**
     * 用户钱包地址
     */
    private String walletReceivedAddr;

    private BigDecimal min;

    private String contractAddr;
}
