package com.wallet.consumer;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.common.base.ResultVo;
import com.common.constatns.ThirdConstants;
import com.common.exception.BaseException;
import com.google.common.collect.Lists;
import com.mall.feign.admin.BaseClient;
import com.mall.feign.user.UserInfoClient;
import com.mall.model.admin.vo.SysCoinsQueryVo;
import com.mall.model.wallet.dto.WalletInfoDto;
import com.wallet.core.btc.OmniUtils;
import com.wallet.core.eth.ERC20Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static com.common.enums.wallet.SysCoinsTypeEnums.USDT;

/**
 * @Author: lt
 * @Date: 2020/8/1 18:39
 */
@Slf4j
@Component
public class HandlerWalletConsumer {

    @Resource
    private BaseClient baseClient;
    @Resource
    private ERC20Utils erc20Utils;
    @Resource
    private OmniUtils omniUtils;
    @Resource
    private UserInfoClient userInfoClient;

    /**
     * 生成钱包
     *
     * @return
     */
    @RabbitListener(queues = ThirdConstants.QUEUES_GENERATE_WALLET)
    @RabbitHandler
    public void generateWallet(String uid) {
        try {
            long startTime = System.currentTimeMillis();
            List<SysCoinsQueryVo> sysCoinsQueryVos = baseClient.querySysCoin();
            log.info("已读取平台配置" );
            if(CollectionUtils.isEmpty(sysCoinsQueryVos)){
                throw new BaseException("平台服务中断，获取币种信息失败");
            }
            List<WalletInfoDto> walletLists = Lists.newArrayList();
            sysCoinsQueryVos.forEach(s -> {
                WalletInfoDto infoDto = new WalletInfoDto();
                infoDto.setCoinSymbol(s.getName());
                infoDto.setAccuracy(s.getAccuracy());
                infoDto.setContractAddr(s.getContractAddr());
                // 生成ERC20钱包地址
                Map<String, String> erc20Wallet = erc20Utils.creatAccount(uid);
                infoDto.setWalletEcr20Addr(erc20Wallet.get("walletAddr"));
                infoDto.setWalletEcr20Path(erc20Wallet.get("walletPath"));
                if(USDT.getName().equals(s.getName())){
                    // USDT 需要生成OMNI钱包地址
                    Map<String, String> omniWallet = omniUtils.getNewAddress(uid);
                    infoDto.setWalletOminAddr(omniWallet.get("walletAddr"));
                }
                walletLists.add(infoDto);
            });
            log.info("已生成钱包地址" );
            // 调用用户服务添加钱包账户信息
            ResultVo resultVo = userInfoClient.createWallet(uid,walletLists);
            if(!resultVo.isSuccess()){
                throw new BaseException(resultVo.getErrorMessage());
            }
            log.info("已添加用户账户信息" );
            log.info("总计耗时 : " + (System.currentTimeMillis() - startTime));
        } catch (Exception e) {
            throw new BaseException("生成钱包失败", e);
        }
    }

}
