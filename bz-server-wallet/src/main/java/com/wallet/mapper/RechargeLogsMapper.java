package com.wallet.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.RechargeLogVo;
import com.wallet.entity.RechargeLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wallet.model.vo.ImputedVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 钱包充值记录明细表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-13
 */
public interface RechargeLogsMapper extends BaseMapper<RechargeLogs> {

    Page<RechargeLogVo> queryRecharge(IPage<RechargeLogVo> pageInfo, @Param("param") WalletRechargeListDto rechargeListDto);

    List<ImputedVo> queryImputed(ImputedVo imputedVo);

    void updateCustomerImputed(RechargeLogs log);
}
