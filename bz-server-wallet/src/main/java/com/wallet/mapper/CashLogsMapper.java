package com.wallet.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.wallet.entity.CashLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 钱包提现申请记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
public interface CashLogsMapper extends BaseMapper<CashLogs> {

    IPage<CashLogVo> queryCash(IPage<CashLogVo> pageInfo, @Param("param") WalletCashListDto cashListDto);
}
