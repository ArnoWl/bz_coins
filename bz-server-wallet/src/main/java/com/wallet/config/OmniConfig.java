package com.wallet.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * omni 协议
 * @author arno
 * @version 1.0
 * @date 2020-07-15
 */
@Data
public class OmniConfig {

    @Value("${omni.ip}")
    private String ip;

    @Value("${omni.port}")
    private String port;

    @Value("${omni.user}")
    private String user;

    @Value("${omni.password}")
    private String password;
}
