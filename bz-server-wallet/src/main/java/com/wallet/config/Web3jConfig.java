package com.wallet.config;

import com.common.utils.lang.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
@Slf4j
public class Web3jConfig {


    public static Web3j web3j;//链接以太坊对象


    /**
     * 初始化加载
     * @return
     */
    public static Web3j initLoad() {
        try {
            //连接方式1：使用infura 提供的客户端
            HttpService httpService = new HttpService("https://mainnet.infura.io/v3/56aefbf6198e49998e80a2d1e7ed201a");
            Web3j web3j1 = Web3j.build(httpService);
            //测试是否连接成功
            String web3ClientVersion = web3j1.web3ClientVersion().send().getWeb3ClientVersion();
            if (!StringUtils.isEmpty(web3ClientVersion)) {
                log.info("以太坊链接成功版本号：" + web3ClientVersion);
                web3j = web3j1;
            } else {
                log.error("以太坊连接失败");
            }
        } catch (Exception e) {
            log.error("以太坊连接失败", e);
        }
        return web3j;
    }
}
