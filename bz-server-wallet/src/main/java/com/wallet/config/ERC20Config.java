package com.wallet.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * ERC20 配置
 * @author arno
 * @version 1.0
 * @date 2020-07-15
 */
@Data
public class ERC20Config {


    @Value("${erc20.path}")
    private String erc20FilePath;

    @Value("${erc20.ecr20_url}")
    private String erc20Url;

    @Value("${erc20.ecr20_key}")
    private String erc20Key;

    @Value("${erc20.gas_limit}")
    private String gasLimit;
}
