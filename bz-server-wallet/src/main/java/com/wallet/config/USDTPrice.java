package com.wallet.config;

import com.alibaba.fastjson.JSONObject;
import com.common.exception.BaseException;
import com.common.utils.flashMobile.OkHttpUtil;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * 获取usdt单价
 * @author arno
 * @version 1.0
 * @date 2020-07-20
 */
public class USDTPrice {
    /**
     * 获取1USDT=X CNY
     * @return
     */
    public static BigDecimal getUsdtToCny() {
        BigDecimal money=BigDecimal.ZERO;
        try {
            JSONObject jsonObject= OkHttpUtil.postRequest("https://apiv2.bit-m.com/Market/currencyRate?symbols=usdt_cny",null);
            if(jsonObject==null) {
                throw new BaseException("获取USDT币价错误");
            }
            String status=jsonObject.getString("status");
            if("200".equals(status)) {
                JSONObject data=jsonObject.getJSONObject("data");
                if(data==null) {
                    throw new BaseException("获取USDT币价错误");
                }
                JSONObject obj=data.getJSONObject("usdt_cny");
                if(obj==null) {
                    throw new BaseException("获取USDT币价错误");
                }
                money=obj.getBigDecimal("rate");
            }
            return money;
        }catch (Exception e){
            return money;
        }

    }
    public static void main(String[] args) {
        System.out.println(getUsdtToCny());
    }
}
