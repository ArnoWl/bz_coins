package com.wallet.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包提现申请记录表
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wallet_cash_logs")
@ApiModel(value="CashLogs对象", description="钱包提现申请记录表")
public class CashLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private String uid;

    @ApiModelProperty(value = "货币类型")
    private String coinSymbol;

    @ApiModelProperty(value = "协议类型 OMNI  ERC20")
    private String agreeType;

    @ApiModelProperty(value = "交易hash单号")
    private String hashCode;

    @ApiModelProperty(value = "发送方地址")
    private String walletSendAddr;

    @ApiModelProperty(value = "接收方地址")
    private String walletReceivedAddr;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal money;

    @ApiModelProperty(value = "手续费数量")
    private BigDecimal tax;

    @ApiModelProperty(value = "实际到账数量")
    private BigDecimal realMoney;

    @ApiModelProperty(value = "状态 1待审核  5审核，待转币 10转币，待确认  15已确认     20已驳回")
    private Integer status;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "处理时间")
    private LocalDateTime handleTime;

    @ApiModelProperty(value = "操作人")
    private String handleBy;

    @ApiModelProperty(value = "0是系统内部互转 1不是系统内部互转")
    private Integer sysFlag;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;

    @ApiModelProperty(value = "钱包精度")
    private Integer walletDeclimal;


}
