package com.wallet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 钱包充值记录明细表
 * </p>
 *
 * @author lt
 * @since 2020-07-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wallet_recharge_logs")
@ApiModel(value="RechargeLogs对象", description="钱包充值记录明细表")
public class RechargeLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String uid;

    @ApiModelProperty(value = "货币类型")
    private String coinSymbol;

    @ApiModelProperty(value = "OMNI ERC20")
    private String agreeType;

    @ApiModelProperty(value = "区块充值交易单hash单号")
    private String hashCode;

    @ApiModelProperty(value = "发送方地址")
    private String walletSendAddr;

    @ApiModelProperty(value = "接收地址")
    private String walletReceivedAddr;

    @ApiModelProperty(value = "货币数量")
    private BigDecimal coinNum;

    @ApiModelProperty(value = "0已同步 1未同步")
    private Integer status;

    @ApiModelProperty(value = "同步交易hash单号")
    private String transHashCode;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "同步时间")
    private LocalDateTime synTime;

    @ApiModelProperty(value = "归集的钱包地址")
    private String imputedAddr;

    @ApiModelProperty(value = "归集类型  1系统地址 2场外地址")
    private Integer imputedType;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;


}
