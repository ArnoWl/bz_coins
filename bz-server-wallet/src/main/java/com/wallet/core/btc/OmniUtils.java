package com.wallet.core.btc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.common.exception.BaseException;
import com.common.utils.lang.StringUtils;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.mall.model.wallet.dto.BitDto;
import com.wallet.config.OmniConfig;
import com.wallet.core.btc.dto.Input;
import com.wallet.core.btc.dto.RawTransaction;
import com.wallet.core.btc.dto.SignTransaction;
import com.wallet.core.btc.dto.UtxoInfo;
import com.wallet.core.btc.rpc.HttpRpc;
import lombok.extern.slf4j.Slf4j;
import net.iharder.Base64;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OMNI操作USDT BTC 方法
 * @author arno
 * @version 1.0
 * @date 2020-07-15
 */
@Slf4j
@Configuration
public class OmniUtils {

    // 正式网络usdt=31，测试网络可以用2
    private static final int propertyid =31;
    private final static String RESULT = "result";
    private final static String METHOD_SEND_TO_ADDRESS = "omni_send";
    // 可以指定手续费地址
    private final static String METHOD_OMNI_FUNDED_SEND = "omni_funded_send";
    private final static String METHOD_GET_TRANSACTION = "omni_gettransaction";
    private final static String METHOD_GET_BLOCK_COUNT = "getblockcount";
    private final static String METHOD_NEW_ADDRESS = "getnewaddress";
    private final static String METHOD_GET_BALANCE = "omni_getbalance";
    private final static String METHOD_GET_LISTTRANSACTIONS = "omni_listtransactions";
    private final static BigDecimal usdtBasicFee = new BigDecimal("0.00000546");

    @Resource
    private OmniConfig omniConfig;

    /**
     * 获取整个节点BTC余额
     * @param  account 账号
     * @return
     */
    public String getBTCBalance(String account) {
        try {
            JSONObject json = doRequest("getbalance",account);
            if (isError(json)) {
                return "";
            } else {
                return json.getString(RESULT);
            }
        } catch (Exception e) {
            throw new BaseException("获取BTC节点余额异常");
        }
    }

    /**
      * 验证地址的有效性
      * @param address
      * @return
      * @throws Exception
      */
    public boolean vailedAddress(String address) {
        JSONObject json = doRequest("validateaddress",address);
        if(json==null) {
            return false;
        }
        boolean isvalid=json.getBooleanValue("isvalid");
        return isvalid;
    }


    /**
     * USDT查询余额  * @return  
     */
    public String getBalance(String addr) {
        JSONObject json = doRequest(METHOD_GET_BALANCE,addr, propertyid);
        if (isError(json)) {
            return "0.00";
        }
        return json.getJSONObject(RESULT).getString("balance");
    }

    /**
     *      * 区块高度      * @return      
     */
    public int getBlockCount() {
        JSONObject json = null;
        try {
            json = doRequest(METHOD_GET_BLOCK_COUNT);
            if (!isError(json)) {
                return json.getInteger("result");
            } else {
                return 0;
            }
        } catch (Exception e) {
            throw new BaseException("获取区块高度异常");
        }
    }

    /**
     * USDT产生地址      
     * @return      
     */
    public Map<String,String> getNewAddress(String account) {
        JSONObject json = doRequest(METHOD_NEW_ADDRESS,account);
        if (isError(json)) {
            return null;
        }
        String walletAddr=json.getString(RESULT);
        Map<String,String> map=new HashMap<>();
        map.put("walletAddr",walletAddr);
        map.put("walletAccount",account);
        return map;
    }


    /**
     *      * USDT转帐      
     * * @param toAddr      
     * * @param value      
     * * @return      
     */
    public String send(String fromAddr, String toAddr, String value) {
        if (vailedAddress(toAddr)) {
            JSONObject json = doRequest(METHOD_SEND_TO_ADDRESS, fromAddr, toAddr, propertyid, value);
            if (isError(json)) {
                return "";
            } else {
                return json.getString(RESULT);
            }
        } else {
            return "";
        }
    }

    /**
     *      * USDT转帐      
     * * @param toAddr      
     * * @param value      
     * * @return      
     */
    public String send(String fromAddr, String toAddr, String value,String feeadd) {
        if (vailedAddress(toAddr)) {
            JSONObject json = doRequest(METHOD_OMNI_FUNDED_SEND, fromAddr, toAddr, propertyid, value,feeadd);
            if (isError(json)) {
                return "";
            } else {
                return json.getString(RESULT);
            }
        } else {
            return "";
        }
    }

    /**
     *  获取    交易列表
     * * @param toAddr      
     * * @return      
     */
    public List<BitDto> getTxlist(String addr) {
        if (vailedAddress(addr)) {
            JSONObject json = doRequest(METHOD_GET_LISTTRANSACTIONS, addr);
            if (isError(json)) {
                return null;
            } else {
                return JSONObject.parseArray(json.getString(RESULT), BitDto.class);
            }
        } else {
            return null;
        }
    }

    /**
     *  获取    交易列表
     * @param count 查询条数
     * @param index 下标
     * @return
     */
    public List<BitDto> getTxlist(Integer count, Integer index) {
        JSONObject json = doRequest(METHOD_GET_LISTTRANSACTIONS, "",count,index);
        if (isError(json)) {
            return null;
        } else {
            return JSONObject.parseArray(json.getString(RESULT), BitDto.class);
        }
    }


    /**
     *  获取 根据单号查询交易   
     * * @param toAddr      
     * * @return      
     */
    public BitDto getTx(String txid) {
        try {
            JSONObject json = doRequest(METHOD_GET_TRANSACTION, txid);
            if (isError(json)) {
                return null;
            } else {
                return JSONObject.toJavaObject(json.getJSONObject(RESULT), BitDto.class);
            }
        } catch (Exception e) {
            return null;
        }
    }

    /***************************************请求方法*****************************/
    public JSONObject doRequest(String method, Object... params) {
        JSONObject param = new JSONObject();
        param.put("id", System.currentTimeMillis() + "");
        param.put("jsonrpc", "2.0");
        param.put("method", method);
        if (params != null) {
            param.put("params", params);
        }
        String creb = Base64.encodeBytes((omniConfig.getUser() + ":" + omniConfig.getPassword()).getBytes());
        Map<String, String> headers = new HashMap<>(2);
        headers.put("Authorization", "Basic " + creb);
        headers.put("server", "1");
        String resp = HttpRpc.jsonPost("http://"+omniConfig.getIp()+":"+omniConfig.getPort(), headers, param.toJSONString());
        return JSON.parseObject(resp);
    }

    private boolean isError(JSONObject json) {
        if (json == null || (StringUtils.isNotEmpty(json.getString("error")) && json.get("error") != "null")) {
            return true;
        }
        return false;
    }



    /*************转账**************/

    /**
     * OMNI  USDT 转账
     * @param sender  发送方地址
     * @param feeAddress  手续费低至
     * @param receive  接收方地址
     * @param amountStr  金额
     * @return
     * @throws Throwable
     */
    public String sendrawtransaction(String sender, String feeAddress, String receive, String amountStr) {
        try {
            log.info("Sender:{"+sender+"} Receiver:{"+receive+"} Fee:{"+feeAddress+"}  outAmount:{"+amountStr+"}" );
            String creb = org.apache.commons.codec.binary.Base64.encodeBase64String((omniConfig.getUser() + ":" + omniConfig.getPassword()).getBytes());
            Map<String, String> headers = new HashMap<>(2);
            headers.put("Authorization", "Basic " + creb);
            headers.put("server", "1");
            JsonRpcHttpClient client = new JsonRpcHttpClient(new URL("http://"+omniConfig.getIp()+":"+omniConfig.getPort()), headers);

            List<UtxoInfo> utxoList = new ArrayList<>();
            // 1.
            // 查询Sender地址Utxo
            String senderUtxoResult = JSONArray.toJSONString(client.invoke("listunspent", new Object[]{0, 999999, new String[]{sender}}, Object[].class));
            log.info("Sender utxo result:{}"+senderUtxoResult);
            for (UtxoInfo utxo : JSON.parseArray(senderUtxoResult, UtxoInfo.class)) {
                BigDecimal utxoAmount =utxo.getAmount();
                utxo.setValue(utxoAmount.toPlainString());
                if (utxoAmount.compareTo(usdtBasicFee) >=0) {
                    utxoList.add(utxo);
                    break;
                }
            }
            if (utxoList.isEmpty()) {
                log.info("Sender {} utxo insufficient."+ sender);
                throw new BaseException("Sender utxo insufficient.");
            }

            // 手续费
            BigDecimal fee = new BigDecimal(String.valueOf((2 * 148 + 34 * 3 + 10))).multiply(new BigDecimal(getFeeRate())).multiply(new BigDecimal("0.00000001"));
            log.info("Sender:{"+sender+"} Receiver:{"+receive+"} Fee:{"+feeAddress+"} feeAmount:{"+fee.toPlainString()+"} outAmount:{"+amountStr+"}" );
            // 查询BTC地址Utxo
            // 自己拼接input,USDT地址放在第一个，旷工费地址放在下面就可以了 凌晨3点钟效率最高转账
            String feeUtxoResult = JSONArray.toJSONString(client.invoke("listunspent", new Object[]{0, 999999, new String[]{feeAddress}}, Object[].class));
            log.info("Fee utxo result:{}"+ feeUtxoResult);
            for (UtxoInfo utxo : JSON.parseArray(feeUtxoResult, UtxoInfo.class)) {
                BigDecimal utxoAmount =utxo.getAmount();
                utxo.setValue(utxoAmount.toPlainString());
                if (utxoAmount.compareTo(usdtBasicFee) >=0 && utxoAmount.compareTo(fee) >=0) {
                    utxoList.add(utxo);
                    break;
                }
            }
            if (utxoList.size() != 2) {
                log.info("Fee {} utxo insufficient."+feeAddress);
                throw new BaseException("Fee utxo insufficient.");
            }

            List<RawTransaction> rawTransactionList = new ArrayList<>();
            List<Input> utxoInputList = new ArrayList<>();
            for (UtxoInfo utxo : utxoList) {
                RawTransaction rawTransaction = new RawTransaction();
                rawTransaction.setTxid(utxo.getTxid());
                rawTransaction.setVout(utxo.getVout());
                rawTransactionList.add(rawTransaction);
                Input input = new Input(utxo.getTxid(), utxo.getVout(), utxo.getScriptPubKey(), utxo.getValue());
                utxoInputList.add(input);
            }

            // 2 构造发送代币类型和代币数量数据（payload）
            String payload = client.invoke("omni_createpayload_simplesend", new Object[]{31, amountStr}, String.class);
            log.info("第2步返回:{}"+ payload);
            // 3 构造交易基本数据（transaction base）
            String txBaseStr = createRawTransaction(client, rawTransactionList);
            log.info("第3步返回:{}"+ txBaseStr);
            // 4 在交易数据中加上omni代币数据
            String opreturn = client.invoke("omni_createrawtx_opreturn", new Object[]{txBaseStr, payload}, String.class);
            log.info("第4步返回:{}"+opreturn);
            // 5在交易数据上加上接收地址
            String reference = client.invoke("omni_createrawtx_reference", new Object[]{opreturn, receive}, String.class);
            log.info("第5步返回:{}"+ reference);
            // 6 在交易数据上指定矿工费用
            String createRaw = omniCreateRawTxChange(client, reference, utxoInputList, feeAddress, fee.toPlainString());
            log.info("第6步返回:{}"+ createRaw);

            // 7 签名
            SignTransaction signTransaction = client.invoke("signrawtransaction", new Object[]{createRaw}, SignTransaction.class);
            log.info("第7步返回:{}"+ JSON.toJSONString(signTransaction));
            if (signTransaction.isComplete()) {
                // 8 广播
                try {
                    Object txid = client.invoke("sendrawtransaction", new Object[]{signTransaction.getHex()}, Object.class);
                    log.info("第8步返回:{}"+ JSON.toJSONString(txid));
                    return txid.toString();
                } catch (Exception e) {
                    log.info(e.getMessage());
                    //如果是矿池满了直接返回
                    if("64: too-long-mempool-chain".equals(e.getMessage())) {
                        return "-999";
                    }
                }
            }
        } catch (Throwable throwable) {
            log.error("OMNI -- USDT 转账失败" + sender + "--" + feeAddress +  "--" + receive + "--" + amountStr);
        }
        return null;
    }



    /**
     * 构建交易基础(3)
     *
     * @param params
     * @return
     */
    public static String createRawTransaction(JsonRpcHttpClient client, List<RawTransaction> params) {
        String result = "";
        try {
            Map data = new HashMap();
            result = client.invoke("createrawtransaction", new Object[]{params, data}, String.class);
        } catch (Throwable e) {
            log.error("第三步构建交易异常",e);
        }
        return result;
    }

    /**
     * @param client
     * @param reference 第五步返回的参数
     * @param params    UTXO账本列表
     * @return
     */
    public String omniCreateRawTxChange(JsonRpcHttpClient client, String reference, List<Input> params, String feeAddress, String fee) {
        String result = "";
        try {
            result = client.invoke("omni_createrawtx_change", new Object[]{reference, params, feeAddress, fee}, String.class);
        } catch (Throwable e) {
            log.error("第五步构建交易异常",e);
        }
        return result;
    }

    /**
     * 设置费率  费率越高BTC手续费越高
     * @return
     */
    public String getFeeRate() {
//        String httpResult = "";
//        CloseableHttpClient httpCilent = HttpClients.createDefault();
//        HttpGet httpGet = new HttpGet("https://bitcoinfees.earn.com/api/v1/fees/recommended");
//        try {
//            CloseableHttpResponse response = httpCilent.execute(httpGet);
//            httpResult = convertStreamToString(response.getEntity().getContent());
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                httpCilent.close();//释放资源
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        Fees fees = JSON.parseObject(httpResult, Fees.class);
//        fees.setFastestFee(new BigDecimal(120));
        return "80";
    }
    /**
     * input转String
     *
     * @param is
     * @return
     */
//    public String convertStreamToString(InputStream is) {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
//        StringBuilder sb = new StringBuilder();
//        String line = null;
//        try {
//            while ((line = reader.readLine()) != null) {
//                sb.append(line);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                is.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return sb.toString();
//    }

}
