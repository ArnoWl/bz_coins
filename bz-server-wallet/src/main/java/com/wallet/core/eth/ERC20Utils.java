package com.wallet.core.eth;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.common.exception.BaseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mall.model.wallet.vo.ERC20RechargeVo;
import com.wallet.config.ERC20Config;
import com.wallet.config.Web3jConfig;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.*;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * 以太坊基于ECR20协议 工具类
 * @author arno
 *
 */
@Slf4j
@Configuration
public class ERC20Utils {

	@Resource
	private ERC20Config erc20Config;

	//测试环境 注意生成钱包时候的文件目录地址 上线后钱包文件地址必须保持一致 不然解析出来的钱包地址不一样
//	public static final String path="/Users/arno/Documents/myGitHub/";
//	public static final String ecr20_url="http://api-cn.etherscan.com/api/";
//	public static final String ecr20_key="7QS6PH6GP1K98YJ22TIA8RYTXJ3K9CGGS1";
//
//	public static final String GAS_LIMIT="60000";

	/**
	 * 创建钱包地址
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws InvalidAlgorithmParameterException
	 * @throws CipherException
	 * @throws IOException
	 */
	public Map<String,String> creatAccount(String passWord) {
		try {
			String filename="";//文件名
		    //钱包文件保持路径，请替换位自己的某文件夹路径  生成证书 密码为用户账号
		    filename = WalletUtils.generateNewWalletFile(passWord, new File(erc20Config.getErc20FilePath()),true);
		    String url=erc20Config.getErc20FilePath()+filename;
		    //解析文件返回 钱包路径  钱包地址
			Credentials credentials=loadWallet(url,passWord);
			Map<String,String> map=new HashMap<>();
			map.put("walletAddr",credentials.getAddress());
			map.put("walletPath",url);
		    log.info("生成会员钱包文件信息",map);
		    return map;
		}catch (NoSuchProviderException e) {
			throw new BaseException("未找到服务异常");
		}catch (NoSuchAlgorithmException e) {
			throw new BaseException("加密算法异常");
		}catch (InvalidAlgorithmParameterException e) {
			throw new BaseException("证书异常");
		}catch (CipherException e) {
			throw new BaseException("初始化异常");
		}catch (IOException e) {
			throw new BaseException("文件流异常");
		}
	}

	/**
	 * 根据会员钱包文件获取用户钱包地址 公钥私钥
	 * @param walleFilePath
	 * @return
	 * @throws IOException
	 * @throws CipherException
	 */
	public Credentials loadWallet(String walleFilePath,String passWord) {
		try {
			Credentials credentials = WalletUtils.loadBip39Credentials(passWord, walleFilePath);
		    return credentials;
		} catch (Exception e) {
			throw new BaseException("钱包地址解析失败");
		}
	}

	/**
	 * 校验ETH钱包地址
	 * @param input
	 * @return
	 */
	public boolean isETHValidAddress(String input) {
        if (StringUtils.isBlank(input) || !input.startsWith("0x")) {
        		return false;
        }
        String cleanInput = Numeric.cleanHexPrefix(input);
        try {
            Numeric.toBigIntNoPrefix(cleanInput);
        } catch (NumberFormatException e) {
            return false;
        }
        return cleanInput.length() == 40;
    }

	/**
	 * 获取以太坊余额
	 * @throws IOException
	 */
	public BigDecimal getBlanceOf(String address) {
		try {
			//第二个参数：区块的参数，建议选最新区块
		    BigInteger balance = Web3jConfig.web3j.ethGetBalance(address,DefaultBlockParameterName.LATEST).send().getBalance();
		    //格式转化 wei-ether
		    BigDecimal blanceETH = Convert.fromWei(balance.toString(), Convert.Unit.ETHER);
		    return blanceETH;
		} catch (Exception e) {
			throw new BaseException("获取ETH余额异常");
		}
	}


	/**
	 * 查询代币余额
	 * @param contractAddress 合约地址  合约地址是创建代币的时候有的
	 * @param address 钱包地址
	 * @param decimal 经度
	 * @return
	 */
	public BigDecimal getERC20Balance(String contractAddress,String address,int decimal){
		TreeMap<String,String> treeMap = new TreeMap<>();
		treeMap.put("module", "account");
		treeMap.put("action", "tokenbalance");
		treeMap.put("contractaddress", contractAddress);
		treeMap.put("address", address);
		treeMap.put("tag", "latest");
		treeMap.put("apikey", erc20Config.getErc20Key());
		StringBuffer sbf = new StringBuffer();
		for (String str : treeMap.keySet()) {
			sbf.append(str).append("=").append(treeMap.get(str)).append("&");
		}
		String url = erc20Config.getErc20Url().concat("?").concat(sbf.substring(0, sbf.length() - 1));
		Object send = send(url);
		return toDecimal(decimal,new BigInteger(String.valueOf(send)));
	}


	/**
	 * 转换成符合 decimal 的数值
	 * @param decimal
	 * @return
	 */
	public BigDecimal toDecimal(int decimal,BigInteger integer){
		StringBuffer sbf = new StringBuffer("1");
		for (int i = 0; i < decimal; i++) {
			sbf.append("0");
		}
		BigDecimal balance = new BigDecimal(integer).divide(new BigDecimal(sbf.toString()), decimal, BigDecimal.ROUND_DOWN);
		return balance;
	}

	public Object send(String url){
		 String responseMessageGet =getResponseMessageGet(url);
		 ObjectMapper mapper = new ObjectMapper();
		 Map<String,String> reMap = null;
		 try {
			reMap = mapper.readValue(responseMessageGet, new com.fasterxml.jackson.core.type.TypeReference<Map<String,Object>>(){});
		} catch (Exception e) {
			throw new BaseException("ECR20协议请求错误");
		}
		 return reMap.get("result");
	}

	/**
	 * okhttp get 请求
	 */
	public ResponseBody testGetClient(String url){
		OkHttpClient okClient = new OkHttpClient();
		Request request = new Request.Builder().get().url(url).build();
		ResponseBody resBody = null;
		try {
			Response response = okClient.newCall(request).execute();
			resBody = response.body();
		} catch (IOException e) {
			throw new BaseException("ECR20协议请求错误01");
		}
		return resBody;
	}

	public String getResponseMessageGet(String url) {
		ResponseBody body = null;String string = null;
		body = testGetClient(url);
		if(checkEmpty(body)) return null;
		try {
			string = body.string();
		} catch (IOException e) {
			throw new BaseException("ECR20协议请求错误02");
		}
		return string;
	}

	public boolean checkEmpty(Object obj){
		if(null!=obj && !obj.toString().trim().equals("")){
			return false;
		}
		return true;

	}

	/**
	 * 合约代币转账
	 * @param walletPath 发送方钱包文件地址
	 * @param passWord 发放方钱包密码
	 * @param contractAddress  合约地址
	 * @param toAddress  接收方
	 * @param amount 金额
	 * @param fee 手续费
	 * @param decimal 币种经度
	 * @return
	 */
	public String erc20ContractTrans(String walletPath,String passWord,String contractAddress,String toAddress,BigDecimal amount,BigDecimal fee,int decimal) {
		Credentials credentials = loadWallet(walletPath, passWord);
		String fromAddress = credentials.getAddress();
		log.info("转账金额" + amount + "--- 手续费" + fee + "--- 钱包地址" + fromAddress + ">>" + toAddress);
		if (fee == null || fee.compareTo(BigDecimal.ZERO) <= 0) {
			BigInteger bigInteger=new BigInteger(erc20Config.getGasLimit());
            fee = getMinerFee(bigInteger);
        }
		BigDecimal fromfee=getBlanceOf(fromAddress);
        if(fromfee.compareTo(fee) < 0){
	         throw new BaseException("ETH手续费不足");
        }
        boolean flag=isETHValidAddress(toAddress);
        if(!flag) {
        	throw new BaseException("提币地址错误{"+toAddress+"}");
        }
        try {
			EthGetTransactionCount ethGetTransactionCount = Web3jConfig.web3j.ethGetTransactionCount(credentials.getAddress(), DefaultBlockParameterName.LATEST)
				.sendAsync()
				.get();
            BigInteger nonce = ethGetTransactionCount.getTransactionCount();
            BigInteger gasPrice = getGasPrice();
            //货币转换格式
            BigInteger value=amount.multiply(BigDecimal.TEN.pow(decimal)).toBigInteger();
            Function fn = new Function("transfer", Arrays.asList(new Address(toAddress), new Uint256(value)), Collections.<TypeReference<?>> emptyList());
            String data = FunctionEncoder.encode(fn);
            BigInteger maxGas=Convert.toWei(fee, Convert.Unit.ETHER).toBigInteger().divide(gasPrice);
            log.info("from={"+credentials.getAddress()+"},value={"+value+"},gasPrice={"+gasPrice+"},gasLimit={"+maxGas+"},nonce={"+nonce+"},address={"+toAddress+"}");
            RawTransaction rawTransaction = RawTransaction.createTransaction(
                    nonce, gasPrice, maxGas, contractAddress, data);
            byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
            String hexValue = Numeric.toHexString(signedMessage);
            EthSendTransaction ethSendTransaction =  Web3jConfig.web3j.ethSendRawTransaction(hexValue).sendAsync().get();
            String transactionHash = ethSendTransaction.getTransactionHash();
            if(ethSendTransaction.getError()!=null) {
				log.info("错误信息："+ethSendTransaction.getError().getMessage());
			}
			log.info("ERC20代币交易单号："+transactionHash);
			return transactionHash;
		} catch (InterruptedException e) {
			throw new BaseException("线程异常");
		}catch (ExecutionException e) {
			throw new BaseException("任务异常");
		}
	}

	/**
	 * 计算手续费
	 * @param gasLimit
	 * @return
	 */
	public BigDecimal getMinerFee(BigInteger gasLimit){
        BigDecimal fee = new BigDecimal(getGasPrice().multiply(gasLimit));
        return Convert.fromWei(fee, Convert.Unit.ETHER);
    }
	/**
	 * 计算手续费
	 * @return
	 */
	public BigInteger getGasPrice(){
		try {
			EthGasPrice gasPrice = Web3jConfig.web3j.ethGasPrice().send();
	        return gasPrice.getGasPrice();
		} catch (Exception e) {
			throw new BaseException("链接ETH失败，未获取到费率");
		}
    }

	/**
	 * ETH 基于web3j互转
	 * @param walletPath 发送方钱包文件路径
	 * @param passWord 发送方钱包密码
	 * @param toaddr 接收方
	 * @param num 转账数量
	 * @return
	 * @throws Exception
	 */
	public String transTo(String walletPath,String passWord,String toaddr,BigDecimal num,BigInteger gasprice,BigInteger gaslimit){
		try {
			Credentials credentials =loadWallet(walletPath, passWord);
			BigInteger gas=gasprice;//Convert.toWei(gasprice, Convert.Unit.GWEI).toBigInteger();// DefaultGasProvider.GAS_PRICE;
			BigInteger limit=gaslimit;//Convert.toWei(gaslimit, Convert.Unit.WEI).toBigInteger();
			log.info("发送方："+credentials.getAddress()+">>>>接收方"+toaddr+">>手续费数量："+num);
			EthGetTransactionCount transactionCount = Web3jConfig.web3j.ethGetTransactionCount(credentials.getAddress(), DefaultBlockParameterName.LATEST).sendAsync().get();
	        BigInteger nonce = transactionCount.getTransactionCount();
			log.info("1" + transactionCount.toString());
	        //创建RawTransaction交易对象
			BigInteger eth=Convert.toWei(num, Convert.Unit.ETHER).toBigInteger();//转换ETH单位
			log.info("2" + eth);
	        RawTransaction rawTransaction = RawTransaction.createEtherTransaction(nonce, gas, limit,toaddr, eth);
			log.info("3" + rawTransaction.toString());
	        //签名Transaction，这里要对交易做签名
	        byte[] signMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
	        String hexValue = Numeric.toHexString(signMessage);
	        log.info("4" + hexValue);
	        //发送交易
	        EthSendTransaction ethSendTransaction = Web3jConfig.web3j.ethSendRawTransaction(hexValue).sendAsync().get();
			log.info("5" + ethSendTransaction.toString());
	        String hashcode=ethSendTransaction.getTransactionHash();
	        log.info("ETH交易单号："+hashcode);
	        return hashcode;
		} catch (Exception e) {
			throw new BaseException("转账异常");
		}
    }

	/**
     * 根据hash值获取交易
     *
     * @param hash
     * @return
     * @throws IOException
     */
	public EthGetTransactionReceipt getTransactionByHash(String hash) {
		try {
			EthGetTransactionReceipt request = Web3jConfig.web3j.ethGetTransactionReceipt(hash).send();
			return request;
		} catch (Exception e) {
			throw new BaseException("流异常");
		}
	}

	/**
	 * ERC20充值
	 * @param uid
	 * @param contractAddr
	 * @param walletAddr
	 */
	public List<ERC20RechargeVo> handleERC20Recharge(String uid, String contractAddr, String walletAddr){
		TreeMap<String,Object> treeMap = new TreeMap<>();
		treeMap.put("module", "account");  // 模块类别
		treeMap.put("action", "tokentx");  // 请求接口
		treeMap.put("contractaddress", contractAddr);// 请求参数：代币合约地址
		treeMap.put("address", walletAddr);// 请求参数：钱包地址
		treeMap.put("startblock",0); // 请求参数：查询起始区块
		treeMap.put("endblock",999999999);  // 请求参数：查询结束区块8
		treeMap.put("page", "1");
		treeMap.put("offset", "20");
		treeMap.put("sort", "desc");
		treeMap.put("apikey",erc20Config.getErc20Key());

		StringBuffer sbf = new StringBuffer();
		for (String str : treeMap.keySet()) {
			sbf.append(str).append("=").append(treeMap.get(str)).append("&");
		}

		String url = erc20Config.getErc20Url().concat("?").concat(sbf.substring(0, sbf.length() - 1));
		String send =getResponseMessageGet(url);
		if(StringUtils.isEmpty(send)) {
			return null;
		}

		JSONObject object=JSONObject.parseObject(send);
		if(!"1".equals(object.getString("status"))) {
			return null;
		}
		List<ERC20RechargeVo> list=new ArrayList<>();
		JSONArray array=JSONArray.parseArray(object.getString("result"));
		if(array!=null && array.size()>0) {
			for(int i=0;i<array.size();i++) {

				JSONObject jsonObject=array.getJSONObject(i);
				String hash=jsonObject.getString("hash");
				String blocknumber=jsonObject.getString("blockNumber");
				String timestamp=jsonObject.getString("timeStamp");
				String nonce=jsonObject.getString("nonce");
				String blockhash=jsonObject.getString("blockHash");
				String fromaddr=jsonObject.getString("from");
				String contractaddress=jsonObject.getString("contractAddress");
				String toaddr=jsonObject.getString("to");
				if(!walletAddr.equals(toaddr)){
					continue;
				}
				String valuecoin=jsonObject.getString("value");
				String tokenname=jsonObject.getString("tokenName");
				String tokensymbol=jsonObject.getString("tokenSymbol");
				String tokendecimal=jsonObject.getString("tokenDecimal");
				String gaslimit=jsonObject.getString("gas");
				String gasprice=jsonObject.getString("gasPrice");
				String gasused=jsonObject.getString("gasUsed");

				//将 交易记录的金额 做精度转换
				Integer decimal=Integer.parseInt(tokendecimal);
				BigDecimal num=toDecimal(decimal,new BigInteger(String.valueOf(valuecoin)));

				ERC20RechargeVo rechargeVo=new ERC20RechargeVo();
				rechargeVo.setHashCode(hash);
				rechargeVo.setTimestamp(timestamp);
				rechargeVo.setWalletSendAddr(fromaddr);
				rechargeVo.setContractaddress(contractaddress);
				rechargeVo.setWalletReceivedAddr(toaddr);
				rechargeVo.setCoinNum(num);
				rechargeVo.setTokenname(tokenname);
				rechargeVo.setCoinSymbol(tokensymbol);
				rechargeVo.setTokendecimal(decimal);
				list.add(rechargeVo);
			}
		}
		return list;
	}

}
