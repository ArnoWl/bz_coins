package com.wallet.configuration;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.common.config.RedisUtils;
import com.common.constatns.ThirdConstants;
import com.wallet.config.ERC20Config;
import com.wallet.config.OmniConfig;
import com.wallet.config.USDTPrice;
import com.wallet.config.Web3jConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.web3j.protocol.Web3j;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 初始化自动配置
 * @author lt
 * 2020年3月18日17:07:30
 */
@Configuration
@MapperScan("com.*.mapper")
@ComponentScan({"com.common.config","com.common.handler","com.mall.fallback"})
public class AutoConfiguration {

    @Autowired
    private RedisUtils redisUtils;
    @Resource
    private AmqpTemplate amqpTemplate;
    /**
     * mybatis-plus 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * mybatis-plus 乐观锁插件
     * @return
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }


    /**
     * 初始化web3j
     * @return
     */
    @Bean
    public Web3j getWeb3jConfig(){
        return Web3jConfig.initLoad();
    }


    /**
     * 加载ERC20协议
     * @return
     */
    @Bean
    public ERC20Config getERC20Config(){
        return new ERC20Config();
    }

    /**
     * 加载OMNI协议
     * @return
     */
    @Bean
    public OmniConfig getOmniConfig(){
        return new OmniConfig();
    }

    /**
     * 生产用户钱包消息队列
     * @return
     */
    @Bean
    public Queue generateWallet(){
        return new Queue(ThirdConstants.QUEUES_GENERATE_WALLET);
    }


    /**
     * 初始化usdt价格
     */
    @Bean
    public void setUsdtPrice(){
        BigDecimal price= USDTPrice.getUsdtToCny();
        if(price.compareTo(BigDecimal.ZERO)>0){
            redisUtils.setUSDTPrice(price);
        }
    }
}
