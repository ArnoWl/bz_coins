package com.wallet.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.wallet.entity.CashLogs;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 钱包提现申请记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
public interface ICashLogsService extends IService<CashLogs> {

    IPage<CashLogVo> queryCash(IPage<CashLogVo> pageInfo,WalletCashListDto cashListDto);
}
