package com.wallet.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.RechargeLogVo;
import com.wallet.entity.RechargeLogs;
import com.wallet.mapper.RechargeLogsMapper;
import com.wallet.model.vo.ImputedVo;
import com.wallet.service.IRechargeLogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 钱包充值记录明细表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-13
 */
@Service
public class RechargeLogsServiceImpl extends ServiceImpl<RechargeLogsMapper, RechargeLogs> implements IRechargeLogsService {

    @Override
    public Page<RechargeLogVo> queryRecharge(Page<RechargeLogVo> pageInfo, WalletRechargeListDto rechargeListDto) {
        return baseMapper.queryRecharge(pageInfo,rechargeListDto);
    }

    @Override
    public List<ImputedVo> queryImputed(ImputedVo imputedVo) {
        return baseMapper.queryImputed(imputedVo);
    }

    @Override
    public void updateCustomerImputed(RechargeLogs log) {
        baseMapper.updateCustomerImputed(log);
    }
}
