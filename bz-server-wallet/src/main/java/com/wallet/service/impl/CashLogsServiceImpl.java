package com.wallet.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.wallet.entity.CashLogs;
import com.wallet.mapper.CashLogsMapper;
import com.wallet.service.ICashLogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包提现申请记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
@Service
public class CashLogsServiceImpl extends ServiceImpl<CashLogsMapper, CashLogs> implements ICashLogsService {

    @Override
    public IPage<CashLogVo> queryCash(IPage<CashLogVo> pageInfo, WalletCashListDto cashListDto) {
        return baseMapper.queryCash(pageInfo,cashListDto);
    }
}
