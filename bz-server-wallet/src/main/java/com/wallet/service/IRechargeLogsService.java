package com.wallet.service;

import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.RechargeLogVo;
import com.wallet.entity.RechargeLogs;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wallet.model.vo.ImputedVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 钱包充值记录明细表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-13
 */
public interface IRechargeLogsService extends IService<RechargeLogs> {

    Page<RechargeLogVo> queryRecharge(Page<RechargeLogVo> pageInfo, WalletRechargeListDto rechargeListDto);

    List<ImputedVo> queryImputed(ImputedVo imputedVo);

    void updateCustomerImputed(RechargeLogs log);
}
