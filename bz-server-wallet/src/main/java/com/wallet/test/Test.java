package com.wallet.test;

/**
 *
 * 操作钱包归集 自动审核转账  对账
 * @author arno
 * @version 1.0
 * @date 2020-08-09
 */
public class Test {

    /***************OMNI 部分****************/


    /**
     * OMNI 归集
     * 归集逻辑 循环所有OMNI 的手续费地址，进行轮询，一个OMNI手续费地址只能操作25笔 所以当量比较大的时候 对应手续费地址会增加
     * OMNI 转账需要 BTC的手续费，首先就是平台会往OMNI手续费地址里面充值BTC才可以操作
     */
//    public void omniGJ() {
//        //这里是节点配置  可以使用多个区块节点 ，目前我们平台都只使用一个节点 直接用 omniConfig配置就可以，不需要循环
//        NodeEnums[] nodes = NodeEnums.values();
//        for (NodeEnums node : nodes) {
//            NodeEnums one = NodeEnums.getById(node.getId());
//            String platAddr = one.getAddr();
//            log.info("【自动归集】开始节点" + node.getId());
//            //获取费率地址 每个地址可以操作25次
//            //这是一个 手续费地址枚举，以前我是固定的，现在我们有一个表sys_coin_addr 里面后台自己生成手续费地址
//            for (FeeEnums f : FeeEnums.values()) {
//                // 查询 充值记录表  根据状态未归集，且还没有trancode的，这里查询OMNI协议进来的 USDT记录 进行归集 根据用户uid分组 ，合并金额这样减少旷工费
//                List<TxDto> list = usdtRechargeLogMapper.queryAddr(node.getId());
//                for (TxDto l : list) {
//                    try {
//                        BigDecimal money = l.getCoin();
//                        log.info("【自动归集】充值地址/发送方:" + l.getToaddr()+ " ,接收USDT地址：" + platAddr);
//                        String code = UsdtSend.sendrawtransaction(node, l.getToaddr(), f.getFeeaddr(),
//                                platAddr, money.toPlainString());
//                        if (StringUtils.equals("-999",code)) {
//                            break;
//                        }
//                        if (!StringUtils.isEmpty(code) && !StringUtils.equals("-999",code)) {
//                            // 获取到交易单号 更新状态
//                            l.setTarhash(code);
//                            usdtRechargeLogMapper.updateStatus(l);
//                        }
//                    } catch (Throwable e2) {
//                        System.err.println(e2.getMessage());
//                    }
//                }
//            }
//            log.info("【自动归集】结束节点" + node.getId());
//        }
//    }





    /***************ERC20 部分****************/


    /**
     * 以太坊代币归集  这里我们处理的都是代币 每个代币都有一个自己的合约地址 ，可以认为以太坊 发行的每个币都对应有一个API的key=合约地址
     */
//    public void autoECR20GJ() {
////        //同样道理  这里我根据用户分组 查询 充值记录 还没有归集的  且是ERC20协议
////        List<CposDto> list=txLogsMapper.queryGj();
////        BigInteger price=ECR20Utils.getGasPrice();
////        System.out.println("ECR20归集PRICE:"+price);
////
////        //这里获取到当前ETH的 最优有续费
////        BigInteger limit=Convert.toWei(ECR20Utils.GAS_LIMIT, Convert.Unit.WEI).toBigInteger();
////        BigDecimal minfree=ECR20Utils.getMinerFee(limit);
////        System.out.println("minfree:"+minfree);
////
////        for(CposDto l:list) {
////            try {
////                CCustomer cWallet=cCustomerMapper.getByWalletAddr(l.getToaddr());
////                if(cWallet!=null) {
////                    // ERC20归集 首先发送方必须有ETH，这个 时候我们查询发送方 也就是用户的钱包里面的ETH够不够  不够就先从我们平台的地址给用户转
////                    //转ETH需要时间  等到下次轮询的时候有了ETH 用户再发代币转
////                    BigDecimal balance=ECR20Utils.getBlanceOf(l.getToaddr());
////                    BigDecimal tax=minfree;
////                    //不够手续费 那么先给 用户转一点ETH
////                    if(balance.compareTo(tax)<0) {
////                        tax=FunctionUtils.sub(tax, balance, 6);
////                        BigInteger eth=Convert.toWei(tax, Convert.Unit.ETHER).toBigInteger();//转换ETH单位
////                        //创建转出对象
////                        for(ETHEnums e:ETHEnums.values()) {
////                            Credentials credentials = ECR20Utils.loadWallet(e.getPath(),e.getPassword());
////                            String hashcode=ECR20Utils.transto(credentials, l.getToaddr(),eth,price,limit);
////                            if(!StringUtils.isEmpty(hashcode)) {
////                                System.out.println("给会员："+l.getToaddr()+"转ETH:"+eth+"  交易单号："+hashcode);
////                                break;
////                            }
////                        }
////                    }else {
////                        //这里才是真正的合约转账 吧这个代币转过去
////                        //创建转出对象
////                        Credentials credentials = ECR20Utils.loadWallet(cWallet.getWalletpath(), cWallet.getInvitationcode());
////                        String code = ECR20Utils.ecr20Trans(credentials, ContractEnums.usdt.getContract(),ETHEnums.eth1.getAddr(),l.getCpos(),minfree);
////                        if(!StringUtils.isEmpty(code)) {
////                            l.setTranscode(code);
////                            txLogsMapper.updateStatus(l);
////                        }
////                    }
////                }
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////        }
////    }


    /**
     * 以太坊自动转账
     */
//    public void autoCs() {
//        ETHEnums [] enums=ETHEnums.values();
//        BigInteger limit=Convert.toWei(ECR20Utils.GAS_LIMIT, Convert.Unit.WEI).toBigInteger();
//        BigDecimal minfree=ECR20Utils.getMinerFee(limit);
//        for(ETHEnums e:enums) {
//            CCashUsdtLogs cashCposLogs = cCashUsdtLogsMapper.getOnecs();
//            if (cashCposLogs != null) {
//                try {
//                    handleCposCash(e.getPath(),e.getPassword(),cashCposLogs.getId(),minfree);
//                } catch (Exception e2) {
//                    System.out.println("CPOS提币异常"+e2.getMessage()+"  提币记录id：{"+cashCposLogs.getId()+"}");
//                }
//            }
//        }
//    }
//
//    public void handleCposCash(String mainnb_path,String password,Integer cashid,BigDecimal minfree) {
//        // 主账号钱包信息
//        CCashUsdtLogs cash = cCashUsdtLogsMapper.selectByPrimaryKey(cashid);
//        if (cash != null) {
//            // 表示当前已经转成功了不需要在转了
//            if (!StringUtils.isEmpty(cash.getHashcode())) {
//                EthGetTransactionReceipt transaction = ECR20Utils.getTransactionByHash(cash.getHashcode());
//                if (transaction != null && transaction.getResult() != null
//                        && "0x1".equals(transaction.getResult().getStatus())) {
//                    cash.setDzstatus(StaticUtils.dz_yes);
//                    cash.setDztime(DateUtil.currentDate());
//                    cCashUsdtLogsMapper.updateByPrimaryKeySelective(cash);
//                    return;
//                }
//            }
//
//            Credentials credentials = ECR20Utils.loadWallet(mainnb_path, password);
//            BigDecimal money =cash.getRealmoney();
//            String code = ECR20Utils.ecr20Trans(credentials, ContractEnums.usdt.getContract(), cash.getToaddr(),
//                    money,minfree );
//            if (StringUtils.isEmpty(code)) {
//                throw new ThrowJsonException("交易失败,GAS LIMIT ERRORS");
//            }
//            CCashUsdtLogsExample example = new CCashUsdtLogsExample();
//            example.createCriteria().andHashcodeEqualTo(code);
//            int count = cCashUsdtLogsMapper.countByExample(example);
//            if (count > 0) {
//                throw new ThrowJsonException("当前Hash单号已处理,请稍后重新提交");
//            }
//            cash.setDzstatus(StaticUtils.dz_no);
//            cash.setHashcode(code);
//            cCashUsdtLogsMapper.updateByPrimaryKeySelective(cash);
//        }
//    }


    /**
     * ERC20自动对账
     */
//    public void autoAccount() {
//        List<CCashUsdtLogs> list = cCashUsdtLogsMapper.queryDzlist();
//        for (CCashUsdtLogs l : list) {
//            if (!StringUtils.isEmpty(l.getHashcode())) {
//                EthGetTransactionReceipt transaction = ECR20Utils.getTransactionByHash(l.getHashcode());
//                if (transaction != null && transaction.getResult() != null
//                        && "0x1".equals(transaction.getResult().getStatus())) {
//                    l.setDzstatus(StaticUtils.dz_yes);
//                    l.setDztime(DateUtil.currentDate());
//                    cCashUsdtLogsMapper.updateByPrimaryKeySelective(l);
//                } else {
//                    l.setDzstatus(StaticUtils.dz_reset);
//                    l.setDztime(DateUtil.currentDate());
//                    cCashUsdtLogsMapper.updateByPrimaryKeySelective(l);
//                }
//            }
//        }
//    }
}
