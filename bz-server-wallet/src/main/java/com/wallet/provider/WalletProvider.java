package com.wallet.provider;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.UserConstants;
import com.common.constatns.WalletConstants;
import com.common.context.SysUserContext;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.AgreeEnums;
import com.common.enums.wallet.CoinCashEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.codec.CodeUtils;
import com.mall.feign.admin.BaseClient;
import com.mall.feign.user.UserInfoClient;
import com.mall.feign.wallet.WalletClient;
import com.mall.model.admin.dto.CoinAddrDto;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.dto.WalletRechargeDto;
import com.mall.model.user.vo.WalletInfoVo;
import com.mall.model.wallet.dto.BitDto;
import com.mall.model.wallet.dto.WalletCashDto;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.mall.model.wallet.vo.ERC20RechargeVo;
import com.mall.model.wallet.vo.RechargeLogVo;
import com.wallet.config.ERC20Config;
import com.wallet.core.btc.OmniUtils;
import com.wallet.core.eth.ERC20Utils;
import com.wallet.entity.CashLogs;
import com.wallet.entity.RechargeLogs;
import com.wallet.service.ICashLogsService;
import com.wallet.service.IRechargeLogsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RestController;
import org.web3j.utils.Convert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.common.constatns.WalletConstants.*;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
@RestController
@Slf4j
public class WalletProvider implements WalletClient {

    @Resource
    private IRechargeLogsService iRechargeLogsService;
    @Resource
    private ICashLogsService iCashLogsService;
    @Resource
    private UserInfoClient userInfoClient;
    @Resource
    private ERC20Utils erc20Utils;
    @Resource
    private OmniUtils omniUtils;
    @Resource
    private BaseClient baseClient;
    @Resource
    private ERC20Config erc20Config;

    /**
     * 创建ERC20钱包
     *
     * @param passWord
     * @return map
     */
    @Override
    public ResultVo createERC20Wallet(String passWord) {
        Map<String, String> map = erc20Utils.creatAccount(passWord);
        return ResultVo.success(map);
    }

    /**
     * 创建OMNI钱包地址
     *
     * @param account 账号
     * @return
     */
    @Override
    public ResultVo createOmniWallet(String account) {
        Map<String, String> map = omniUtils.getNewAddress(account);
        return ResultVo.success(map);
    }

    /**
     * 钱包提现申请
     *
     * @param walletCashDto
     */
    @Override
    public ResultVo handleApplyCoinCash(WalletCashDto walletCashDto) {
        if(walletCashDto.getAgreeType().equals(AgreeEnums.OMNI.name())){
            //如果是OMNI判断地址是否正确
            if(!omniUtils.vailedAddress(walletCashDto.getWalletReceivedAddr())){
                return ResultVo.failure("OMNI协议钱包地址错误,请确认");
            }
        }
        if(walletCashDto.getAgreeType().equals(AgreeEnums.ERC20.name())){
            //如果是OMNI判断地址是否正确
            if(!erc20Utils.isETHValidAddress(walletCashDto.getWalletReceivedAddr())){
                return ResultVo.failure("ERC20协议钱包地址错误,请确认");
            }
        }
        //真实到账数量
        BigDecimal realmoney = CalcUtils.sub(walletCashDto.getMoney(), walletCashDto.getTax(), walletCashDto.getDecimal());
        if (realmoney == null || realmoney.compareTo(BigDecimal.ZERO) <= 0) {
            throw new BaseException("提币数量不得低于0");
        }
        CashLogs cashLogs = new CashLogs();
        BeanUtils.copyProperties(walletCashDto, cashLogs);
        cashLogs.setRealMoney(realmoney);
        cashLogs.setWalletDeclimal(walletCashDto.getDecimal());
        boolean flag = iCashLogsService.save(cashLogs);
        if (!flag) {
            return ResultVo.failure("提现申请失败");
        }
        return ResultVo.success();
    }

    /**
     * 修改提现状态
     *
     * @param cashId
     * @param status
     * @return
     */
    @Override
    @Transactional
    public ResultVo updateCashStatus(Integer cashId,Integer status,String handleBy) {
        CashLogs cashLogs = iCashLogsService.getById(cashId);
        if (cashLogs == null) {
            return ResultVo.failure("提现申请记录不存在");
        }
        if (!CoinCashEnums.WAIT_CHECK.getStatus().equals(cashLogs.getStatus())) {
            return ResultVo.failure("当前申请已处理");
        }
        //如果是审核通过
        if (CoinCashEnums.WAIT_TRANSFER_COIN.getStatus().equals(status)) {
            if (cashLogs.getSysFlag().equals(WalletConstants.STATUS_0)) {
                //如果是系统内部互转 直接完成
                cashLogs.setStatus(CoinCashEnums.SUCCESS.getStatus());
                //修改 对方用户钱包余额
                String payCode = CodeUtils.generateOrderNo();
                WalletInfoVo walletInfoVo = null;
                //判断协议
                if (cashLogs.getAgreeType().equals(AgreeEnums.OMNI)) {
                    walletInfoVo = userInfoClient.getOMNIWalletByAddr(cashLogs.getWalletReceivedAddr());//给接收方
                } else {
                    walletInfoVo = userInfoClient.getERC20WalletByAddr(cashLogs.getWalletReceivedAddr());//给接收方
                }
                if (walletInfoVo != null) {
                    WalletLogDto wallLogDto = new WalletLogDto(walletInfoVo.getUid(), cashLogs.getCoinSymbol(),
                            payCode, WalletLogTypeEnums.RECHARGE_IN.getTypeId(), UserConstants.WALLET_IN, cashLogs.getRealMoney(), "接收来至【" + cashLogs.getWalletReceivedAddr() + "】的充值", cashLogs.getUid());
                    ResultVo resultVo = userInfoClient.handleUserWalletMoney(wallLogDto);
                    if (!resultVo.isSuccess()) {
                        throw new BaseException(resultVo.getErrorMessage());
                    }
                }

            } else {
                //处理转账
                handleTrans(cashLogs);
                if(StringUtils.isEmpty(cashLogs.getHashCode())){
                    throw new BaseException("审核失败,请检查系统对应主钱包/手续费钱包是否有足够金额");
                }
            }
        }
        /*String msg = "";
        if (status.equals(CoinCashEnums.WAIT_TRANSFER_COIN.getStatus())) {
            msg = "审核通过";
        } else {
            msg = "审核驳回";
        }
        //发送站内信
        ResultVo resultVo = userInfoClient.saveUserMessage(cashLogs.getUid(), "提现审核通知", "您的提现申请" + msg);
        if (!resultVo.isSuccess()) {
            throw new BaseException(resultVo.getErrorMessage());
        }*/
        cashLogs.setHandleTime(LocalDateTime.now());
        //SysUserContext.getSysUsername()
        cashLogs.setHandleBy(handleBy);
        iCashLogsService.updateById(cashLogs);
        return ResultVo.success("审核完成");
    }

    /**
     * 处理审核转账
     * @param cashLogs
     */
    public void handleTrans(CashLogs cashLogs){
        // 获取平台钱包地址配置
        List<CoinAddrDto> coinAddrList = baseClient.getCoinAddrList();
        //操作转币
        if (cashLogs.getAgreeType().equals(AgreeEnums.OMNI)) {
            //如果是OMNI协议
            // OMNI主地址
            List<CoinAddrDto> mainAddrList = coinAddrList.stream().filter(c ->
                    c.getAgreeType().equals(AGREE_TYPE_OMNI) && c.getType().equals(WALLET_ADDR_MAIN) && c.getImputedType().equals(IMPUTED_TYPE_1)).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(mainAddrList)){
                throw new BaseException("系统未生成OMNI主地址信息");
            }
            List<CoinAddrDto> feeAddrList = coinAddrList.stream().filter(c ->
                    c.getAgreeType().equals(AGREE_TYPE_OMNI) && c.getType().equals(WALLET_ADDR_FEE) && c.getImputedType().equals(IMPUTED_TYPE_1)).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(feeAddrList)){
                throw new BaseException("系统未生成OMNI手续费地址信息");
            }
            String code="";
            for (CoinAddrDto  f: feeAddrList) {
                //如果当前地址手续费不够直接跳过
                BigDecimal fee= NumberUtils.createBigDecimal( omniUtils.getBalance(f.getWalletAddr()));
                if(fee.compareTo(BigDecimal.ZERO)<=0){
                    continue;
                }
                for(CoinAddrDto m:mainAddrList){
                    //判断当前主地址的钱包余额是否大于 提现的金额
                    BigDecimal balance= NumberUtils.createBigDecimal( omniUtils.getBalance(m.getWalletAddr()));
                    if(balance.compareTo(cashLogs.getRealMoney())>=0){
                        log.info("【提现审核OMNI协议】充值地址/发送方:" + m.getWalletAddr() + " ,接收USDT地址：" +cashLogs.getWalletReceivedAddr());
                        code = omniUtils.sendrawtransaction(m.getWalletAddr(),f.getWalletAddr(),
                                cashLogs.getWalletReceivedAddr(),cashLogs.getRealMoney().toPlainString());
                        //-999表示当前这个手续费地址 矿池满了 直接换下一个手续费地址就可以
                        if("-999".equals(code)){
                            break;
                        }
                        if(StringUtils.isNotEmpty(code)){
                            cashLogs.setHashCode(code);
                            cashLogs.setWalletSendAddr(m.getWalletAddr());
                            cashLogs.setStatus(CoinCashEnums.SUCCESS.getStatus());
                            break;
                        }
                    }
                }
                if(StringUtils.isNotEmpty(code)){
                    break;
                }
            }
        }else{
            // ERC20I主地址
            List<CoinAddrDto> mainAddrList = coinAddrList.stream().filter(c ->
                    c.getAgreeType().equals(AGREE_TYPE_ERC20) && c.getType().equals(WALLET_ADDR_MAIN) && c.getImputedType().equals(IMPUTED_TYPE_1)).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(mainAddrList)){
                throw new BaseException("系统未生成ERC20主地址信息");
            }
            // 获取当前ETH的最优手续费
            BigInteger limit= Convert.toWei(erc20Config.getGasLimit(), Convert.Unit.WEI).toBigInteger();
            BigDecimal minFree= erc20Utils.getMinerFee(limit);
            for(CoinAddrDto m:mainAddrList){
                //判断当前主地址上的 代币金额是否大于 需要提现的金额
                BigDecimal balance=erc20Utils.getERC20Balance(cashLogs.getContractAddr(),m.getWalletAddr(),cashLogs.getWalletDeclimal());
                if(balance.compareTo(cashLogs.getRealMoney())>=0){
                    String code = erc20Utils.erc20ContractTrans(m.getWalletPath(),m.getWalletPassword()
                            ,cashLogs.getContractAddr(),cashLogs.getWalletReceivedAddr(),cashLogs.getRealMoney(),minFree,cashLogs.getWalletDeclimal());
                    if(StringUtils.isNotEmpty(code)){
                        cashLogs.setHashCode(code);
                        cashLogs.setWalletSendAddr(m.getWalletAddr());
                        cashLogs.setStatus(CoinCashEnums.SUCCESS.getStatus());
                        break;
                    }
                }
            }
        }
    }

    /**
     * 获取钱包余额
     *
     * @param walletAddr 钱包地址
     * @param type       类型  1获取BTC余额  2获取ETH余额  3获取OMNI USDT余额，4获取ERC20 代币余额
     * @return
     */
    @Override
    public String getWalletBalance(String walletAddr, String contractAddr, int type) {
        String balance = "0.00";
        switch (type) {
            case 1:
                balance = omniUtils.getBTCBalance(walletAddr);
                break;
            case 2:
                balance = erc20Utils.getBlanceOf(walletAddr).toPlainString();
                break;
            case 3:
                balance = omniUtils.getBalance(walletAddr);
                break;
            case 4:
                balance = erc20Utils.getERC20Balance(contractAddr, walletAddr, 6).toPlainString();
                break;
            default:
                break;
        }
        return balance;
    }

    @Override
    public Page<CashLogVo> queryWalletCash(WalletCashListDto cashListDto) {
        try {
            Page<CashLogVo> pageInfo = new Page<>(cashListDto.getPageNo(), cashListDto.getPageSize());
            iCashLogsService.queryCash(pageInfo, cashListDto);
            return pageInfo;
        } catch (Exception e) {
            throw new BaseException("查询提现数据失败", e);
        }
    }

    @Override
    public Page<RechargeLogVo> queryWalletRecharge(WalletRechargeListDto rechargeListDto) {
        try {
            Page<RechargeLogVo> pageInfo = new Page<>(rechargeListDto.getPageNo(), rechargeListDto.getPageSize());
            iRechargeLogsService.queryRecharge(pageInfo, rechargeListDto);
            return pageInfo;
        } catch (Exception e) {
            throw new BaseException("查询充值数据失败", e);
        }
    }
}
