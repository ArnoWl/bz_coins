package com.common.enums.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户钱包明细 操作类型
 *
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
public enum WalletLogTypeEnums {

    SYS(1, "系统操作"),
    RECHARGE_OUT(2, "场外充值"),
    CASH(3, "提现申请"),
    CASH_REFUND(4, "提现驳回"),
    RECHARGE_IN(5, "内部充值"),
    VIDEO_EXAMINE(6, "视频审核"),
    BB_BUY(7, "币币交易购买"),
    BB_SALE(8, "币币交易出售"),
    BUY_UP_AND_DOWN_ORDER(9, "买入涨跌订单"),
    BB_DELEGATION_BUY(10, "币币委托购买"),
    BB_DELEGATION_SALE(11, "币币委托出售"),
    BB_DELEGATION_BUY_CANCEL(12, "发布求购单取消退回"),
    BB_DELEGATION_SALE_CANCEL(13, "发布挂售单取消退回"),
    WALLET_THAW(14, "冻结钱包解冻"),
    REGISTERED_BZW(15, "注册赠送BZW"),
    INDEMNITY_SETTLEMENT(16, "包赔场结算"),
    BZW_BUY(17, "BZW认购"),
    PUSH_REWARD(18, "推广收益"),
    SETTLEMENT_REWARD(19, "收益结算"),
    PUSH_CUSTOMER_ORDER(20, "首次推荐会员下单");


    private Integer typeId;
    private String name;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private WalletLogTypeEnums(Integer typeId, String name) {
        this.typeId = typeId;
        this.name = name;
    }

    public static List<Map<String,Object>> getList(){
        List<Map<String,Object>> list=new ArrayList<>();
        for(WalletLogTypeEnums e:WalletLogTypeEnums.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("typeId",e.getTypeId());
            map.put("name",e.getName());
            list.add(map);
        }
        return list;
    }
}
