package com.common.enums.user;

/**
 * 普通用户账务类型枚举
 * @author lt
 * 2020年3月28日16:34:35
 */
public enum UserLevelInfoEnum {

	USER_LEVEL_1(1, "V0"),
	USER_LEVEL_2(2, "V1"),
	USER_LEVEL_3(3, "V2"),
	USER_LEVEL_4(4, "V3"),
	USER_LEVEL_5(5, "V4"),
	USER_LEVEL_6(6, "V5"),
	USER_LEVEL_7(7, "V6"),
	USER_LEVEL_8(8, "V7"),
	USER_LEVEL_9(9, "V8");

	private Integer key;

	private String value;

	UserLevelInfoEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * 获取账务类型描述
	 * @param key
	 * @return
	 */
	public static String getLevelName(Integer key) {
		for (UserLevelInfoEnum e : UserLevelInfoEnum.values()) {
			if (e.getKey().equals(key)) {
				return e.getValue();
			}
		}
		return null;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
