package com.common.enums.bb;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
public enum TransTypeEnums {

    BUY(1, "求购单"),
    SALE(2, "出售单");

    private Integer type;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    private TransTypeEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
