package com.common.enums.bb;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/28
 */
public enum TransStatusEnums {

    ING(1, "交易中"),
    OVER(2, "已结束"),
    CANCEL(3, "已撤销");

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    TransStatusEnums(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
