package com.common.enums.wallet;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
public enum SysCoinsTypeEnums {

    USDT(1, "USDT"),
    BZW(2, "BZW");

    private Integer type;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    private SysCoinsTypeEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
