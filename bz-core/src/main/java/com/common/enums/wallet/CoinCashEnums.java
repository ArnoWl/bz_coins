package com.common.enums.wallet;

/**
 * 货币提现状态枚举
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
public enum CoinCashEnums {

    WAIT_CHECK(1,"待审核"),
    WAIT_TRANSFER_COIN(5,"待转币"),
    WAIT_CONFIRM(10,"待确认"),
    SUCCESS(15,"已完成"),
    FAIL(20,"已驳回");

    private Integer status;
    private String name;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private CoinCashEnums(Integer status,String name){
        this.status=status;
        this.name=name;
    }
}
