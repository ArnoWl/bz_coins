package com.common.enums.wallet;

/**
 *  类型  1获取BTC余额  2获取ETH余额  3获取OMNI USDT余额，4获取ERC20 代币余额
 * @author arno
 * @version 1.0
 * @date 2020-07-20
 */
public enum  BalanceEnums {
    BALANCE_BTC(1,"BTC余额"),
    BALANCE_ETH(2,"ETH余额"),
    BALANCE_OMNI_USDT(3,"OMNI_USDT余额"),
    BALANCE_ERC20_USDT(4,"ERC20_USDT余额");

    private Integer type;
    private String name;

    private BalanceEnums(Integer type,String name){
        this.type=type;
        this.name=name;
    }
}
