package com.common.jwt;

import com.google.common.collect.Maps;
import io.jsonwebtoken.*;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.util.Date;
import java.util.Map;

import static com.common.constatns.JWTConstants.*;

/**
 * JWT
 * @author lt
 * 2020年3月20日13:28:06
 */
public class JWTHelper {

    private static RsaKeyHelper rsaKeyHelper = new RsaKeyHelper();

    private static JwtBuilder builder;

    /**
     * 密钥加密token
     *
     * @param params        承载参数
     * @param priKeyPath    秘钥
     * @param expire        有效时间（秒）
     * @return
     * @throws Exception
     */
    public static String generateToken(Map<String,Object> params, String priKeyPath, int expire) throws Exception {
        JwtBuilder builder = getJwtBuilder(params);
        builder.setExpiration(DateTime.now().plusSeconds(expire).toDate())
                .signWith(SignatureAlgorithm.RS256, rsaKeyHelper.getPrivateKey(priKeyPath));
        return builder.compact();
    }

    /**
     * 密钥加密token
     *
     * @param params        承载参数
     * @param priKey        秘钥
     * @param expire        有效时间（秒）
     * @return
     * @throws Exception
     */
    public static String generateToken(Map<String,Object> params, byte priKey[], int expire) throws Exception {
        JwtBuilder builder = getJwtBuilder(params);
        builder.setExpiration(DateTime.now().plusSeconds(expire).toDate())
                .signWith(SignatureAlgorithm.RS256, rsaKeyHelper.getPrivateKey(priKey));
        return builder.compact();
    }

    private static JwtBuilder getJwtBuilder(Map<String,Object> params){
        builder =  Jwts.builder()
                .setSubject(params.get(JWT_SUBJECT).toString());
        params.keySet().stream().filter(key ->
                !JWT_SUBJECT.equals(key))
                .forEachOrdered(key -> builder.claim(key, params.get(key)));
        return builder;
    }

    /**
     * 公钥解析token
     *
     * @param token
     * @return
     * @throws Exception
     */
    public static Jws<Claims> parserToken(String token, String pubKeyPath) throws Exception {
        return Jwts.parser().setSigningKey(rsaKeyHelper.getPublicKey(pubKeyPath)).parseClaimsJws(token);
    }

    /**
     * 公钥解析token
     *
     * @param token
     * @return
     * @throws Exception
     */
    public static Jws<Claims> parserToken(String token, byte[] pubKey) throws Exception {
        return Jwts.parser().setSigningKey(rsaKeyHelper.getPublicKey(pubKey)).parseClaimsJws(token);
    }

    /**
     * 获取token中的用户信息
     *
     * @param token
     * @param pubKeyPath
     * @return
     * @throws Exception
     */
    public static Map<String,Object> getInfoFromToken(String token, String pubKeyPath) throws Exception {
        Jws<Claims> claimsJws = parserToken(token, pubKeyPath);
        return getJWTCarryingMap(claimsJws);
    }

    /**
     * 获取token中的用户信息
     *
     * @param token
     * @param pubKey
     * @return
     * @throws Exception
     */
    public static Map<String,Object> getInfoFromToken(String token, byte[] pubKey) throws Exception {
        Jws<Claims> claimsJws = parserToken(token, pubKey);
        return getJWTCarryingMap(claimsJws);
    }

    private static Map<String,Object> getJWTCarryingMap(Jws<Claims> claimsJws){
        Map<String,Object> params = Maps.newHashMap();
        Claims body = claimsJws.getBody();
        params.put(JWT_SUBJECT,body.getSubject());
        body.keySet().forEach(key -> params.put(key,body.get(key)));
        return params;
    }

    /**
     * 根据用户类型获取JWT承载不同客户端的用户ID参数
     * @param params        JWT承载参数
     * @return
     */
    public static String getUserIdBySubject(String subject,Map<String,Object> params){
        // 判断JWT承载用户类型
        switch (subject){
            case JWT_KEY_USER_TYPE_USER:
                return params.get(JWT_KEY_USER_ID).toString();
            case JWT_KEY_USER_TYPE_ADMIN:
                return params.get(JWT_KEY_SYS_USER_ID).toString();
            default:
                return null;
        }
    }

}
