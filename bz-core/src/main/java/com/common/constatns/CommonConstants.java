package com.common.constatns;

/**
 * 公共常量类
 * @author lt
 */
public class CommonConstants {

    /** 公共返回状态码定义 **/
    /** 成功 */
    public static final String E_000 = "000";
    /** 失败 */
    public static final String E_001 = "001";
    /** 用户token异常 */
    public static final String E_002 = "002";
    /** 缺少必要参数 */
    public static final String E_003 = "003";
    /** 登录失效 */
    public static final String E_004 = "004";
    /** 签名验证失败 */
    public static final String E_005 = "005";
    /** 上传文件失败 */
    public static final String E_006 = "006";
    /** 客户端token异常 */
    public static final String E_007 = "007";
    /** 系统繁忙 */
    public static final String E_008 = "008";
    /** 角色无权限访问 */
    public static final String E_009 = "009";
    /** 用户未绑定手机号 */
    public static final String E_010 = "010";

    /** 删除标识 0 正常  1 删除*/
    public static final String DEL_FLAG_0 = "0";
    /** 删除标识 0 正常  1 删除*/
    public static final String DEL_FLAG_1 = "1";

    /** 0-否 1-是 */
    public static final Integer NO = 0;
    /** 0-否 1-是 */
    public static final Integer YES = 1;

    /** 系统类型：0:Android 1:IOS 2:小程序 3:PC/H5 4:第三方 */
    public static final Integer SYS_TYPE_0 = 0;
    /** 系统类型：0:Android 1:IOS 2:小程序 3:PC/H5 4:第三方 */
    public static final Integer SYS_TYPE_1 = 1;
    /** 系统类型：0:Android 1:IOS 2:小程序 3:PC/H5 4:第三方 */
    public static final Integer SYS_TYPE_2 = 2;
    /** 系统类型：0:Android 1:IOS 2:小程序 3:PC/H5 4:第三方 */
    public static final Integer SYS_TYPE_3 = 3;


    /** 短信类型：1-验证码短信 2-短信通知 **/
    public static final Integer SMS_TYPE_1 = 1;
    /** 短信类型：1-验证码短信 2-短信通知 **/
    public static final Integer SMS_TYPE_2 = 2;

    /** 登录客户端类型：1-用户端*/
    public static final Integer CLIENT_TYPE_1 = 1;


    /** 账户收支类型：0-收入 1-支出 */
    public static final Integer ACCOUNT_INCOME = 0;
    /** 账户收支类型：0-收入 1-支出 */
    public static final Integer ACCOUNT_PAY = 1;

    /** 是否已锁定：0-否 1-是 */
    public static final Integer IS_LOCKING_0 = 0;
    /** 是否已锁定：0-否 1-是 */
    public static final Integer IS_LOCKING_1 = 1;

    /** 用户等级信息  游客等级对应*/
    public static final Integer LEVEL_COMMON = 1;
    /** 用户等级信息  V1等级对应id*/
    public static final Integer LEVEL_V1 = 2;
    /** 用户等级信息  V3等级对应id*/
    public static final Integer LEVEL_V3 = 4;

    /** 手续费类型 1按数量 2按比例*/
    public static final Integer TAX_TYPE_1 = 1;
    /** 手续费类型 1按数量 2按比例*/
    public static final Integer TAX_TYPE_2 = 2;


}
