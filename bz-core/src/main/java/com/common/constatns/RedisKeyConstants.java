package com.common.constatns;

import io.swagger.models.auth.In;
import sun.swing.plaf.synth.DefaultSynthStyle;

/**
 * Redis缓存键值存储
 *
 * @Author: lt
 * @Date: 2020/5/13 19:44
 */
public class RedisKeyConstants {

    /**
     * 用户授权RSA公钥/秘钥
     **/
    public static final String REDIS_USER_PRI_KEY = "JWT:PRI";
    public static final String REDIS_USER_PUB_KEY = "JWT:PUB";

    /**
     * 客户端授权RSA公钥/秘钥
     **/
    public static final String REDIS_CLIENT_PRI_KEY = "CLIENT:PRI";
    public static final String REDIS_CLIENT_PUB_KEY = "CLIENT:PUB";

    /**
     * 签名对称加密规则
     */
    public static final String USER_AES_ENCRYPT_PREFIX = "USER:AES:";

    /**
     * 用户单点登录记录版本号
     */
    public static final String USER_SSO_VERSION = "USER:SSO:";

    /**
     * 系统配置：订单盈利比例
     */
    public static final String ORDER_WIN_SCALE = "sys:order_win_scale";
    /**
     * 包赔场盈利比例
     */
    public static final String ORDER_INDEMNITY_SCALE = "sys:order_indemnity_scale";
    /**
     * 系统配置：当日平台盈利比例
     */
    public static final String PLATFORM_PROFIT_RATIO = "sys:platform_profit_ratio";
    /**
     * 系统配置：当日最低盈利要求
     */
    public static final String PLATFORM_MIN_PROFIT_RATIO = "sys:platform_min_profit_ratio";

    /**
     * 系统配置：当日最高盈利要求
     */
    public static final String PLATFORM_MAX_PROFIT_RATIO = "sys:platform_max_profit_ratio";

    /**
     * 系统配置：当日最高盈利要求
     */
    public static final String PLATFORM_MAX_DEFICIT_RATIO = "sys:platform_max_deficit_ratio";

    /**
     * 系统配置：BTC浮动范围
     */
    public static final String BTC_RAND = "sys:btc_rand";

    /**
     * 系统配置：ETH浮动范围
     */
    public static final String ETH_RAND = "sys:eth_rand";

    /**
     * 各大交易所K线订阅配置
     */
    public static final String EXCHANGE_KLINE_WS_CONFIG = "EXCHANGE_KLINE_WS_CONFIG";

    /**
     * 交易所K线数据订阅
     */
    public static final String EXCHANGE_KLINE = "EXCHANGE_KLINE_";

    /**
     * 币种最新行情K线数据
     */
    public static final String LATEST_COIN_MARKET_KLINE = "LATEST_COIN_MARKET_KLINE:";

    /**
     * 用户支付吗输入错误次数
     */
    public static final String USER_PAY_PASSWORD_ERROR_COUNT = "USER_PAY_PASSWORD_ERROR_COUNT:";

    /**
     * redis锁：用户登录 -- 单点唯一
     */
    public static final String LOCK_USER_LOGIN = "LOCK:LOCK_USER_LOGIN:";

    /**
     * redis锁：拼团订单余额支付
     */
    public static final String LOCK_ORDER_BALANCE_PAYMENT = "LOCK:ORDER_BALANCE_PAYMENT:";

    /**
     * redis锁：创建买入涨跌订单
     */
    public static final String LOCK_CREATE_BUY_ORDER = "LOCK:CREATE_BUY_ORDER:";

    /**
     * redis锁：创建认购BZW
     */
    public static final String LOCK_SUB_BZW = "LOCK:LOCK_SUB_BZW:";

    /**
     * redis锁：生成用户钱包
     */
    public static final String GENERATE_WALLET = "LOCK:GENERATE_WALLET:";

    /**
     * redis 系统键值对表key
     */
    public static final String SYS_KEY = "sys:";

    /**
     * redis 系统键值对表参数config_key
     */
    public static final String KEY_REGISTER_BZW = "register_bzw";
    public static final String KEY_DESTORY_BZW_NUM = "destory_bzw_num";
    public static final String KEY_DESTORY_BZW_TOTAL_NUM = "destory_bzw_total_num";
    public static final String PUSH_SEND_BZW = "push_send_bzw";

    /**
     * redis 统计
     */
    //public static final String STATIS_KEY = "statis:";
    /**
     * redis 统计数据有效期
     */
    //public static final long STATIS_VALIDITY = 600;

    /**
     * redis 系统币汇率key
     */
    public static final String SYS_COIN_PRICE = "sys:coin:price:";

    /**
     * 下单扣除BZW数量
     */
    public static final String ORDER_BZW = "sys:order_bzw";

    public static final String COIN_PRICE_BZW = "sys:coin:price:BZW";
    public static final String COIN_PRICE_USDT = "sys:coin:price:USDT";

    public static final String SHARE_DOMAIN="sys:share_domain";

    public static final String WITHDREAW_MIN_AMOUNT="sys:withdraw_min_amount";

    public static final String WITHDREAW_MAX_AMOUNT="sys:withdraw_max_amount";


    public static final String MAX_TRANS_MONEY = "sys:max_trans_money";

    /**
     * 设置USDT 对人民币价格
     */
    public static final String USDT_PRICE = "sys:usdt_price";

    /**
     * 求购减少金额百分比
     */
    public static final String WANT_TO_BUY = "sys:want_to_buy";

    /**
     * 日K最高最低
     */
    public static final String REFRESH_OPEN_PRICE = "sys:refresh_open_price";


    /**
     * 归集最低金额
     */
    public static final String IMPURED_MIN = "sys:imputed_min";


    /**
     * 用户锁定包赔场金额缓存
     */
    public static final String USDT_BUDGET_MONEY = "USER:BUDGET:USDT_BUDGET_MONEY:";

}
