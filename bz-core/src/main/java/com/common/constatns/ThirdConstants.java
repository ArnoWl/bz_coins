package com.common.constatns;

/**
 * 第三方服务常量类
 * @Author: lt
 * @Date: 2020/3/24 14:50
 */
public class ThirdConstants {

    /** 对接第三方请求参数签名参数键值 */
    public static final String AES_ENCRYPTION_RULES = "^ltYln6ReQeZb3N*";

    /** 消息队列：添加调度任务 */
    public static final String QUEUES_ADD_SCHEDULE_JOB = "QUEUES_ADD_SCHEDULE_JOB";

    /** 消息队列：关闭调度任务 */
    public static final String QUEUES_CLOSED_SCHEDULE_JOB = "QUEUES_CLOSED_SCHEDULE_JOB";

    /** 消息队列：生成用户钱包 */
    public static final String QUEUES_GENERATE_WALLET = "QUEUES_GENERATE_WALLET";

    /** 消息队列：处理下单结算奖励 */
    public static final String QUEUES_ORDER_REWARD = "QUEUES_ORDER_REWARD";

    /** 消息队列：创建动态监听 */
    public static final String QUEUES_CREATE_LISTENER = "QUEUES_CREATE_LISTENER";

    /******************** 任务调度-执行器：平台业务任务调度 *********/
    public static final int ADMIN_JOB_EXECUTOR = 1;

    /** 每天统计数据一次 */
    public static final String STATIS_DAY = "statisDay";

    /**  更新开盘价 */
    public static final String UPDATA_COIN_OPEN_PRICE = "updateCoinOpenPrice";

    /**
     * 每5S刷新 日K
     */
    public static final String REFRESH_OPEN_PRICE="refreshOpenPrice";


    /******************** 任务调度-执行器：用户业务任务调度 *********/
    public static final int USER_JOB_EXECUTOR = 2;

    /** 任务调度：处理买入涨/跌下单结算 */
    public static final String BUY_ORDER_SETTLEMENT = "buyOrderSettlement";

    /** 任务调度：OMNI场外充值 */
    public static final String OMNI_OFF_SITE_RECHARGE = "OMNIOffSiteRecharge";

    /** 任务调度：ERC20场外充值 */
    public static final String ERC20_OFF_SITE_RECHARGE = "ERC20OffSiteRecharge";

    /** 任务调度：每隔20秒执行一次计算订单的预结算订单 */
    public static final String ORDER_PER_SETTLEMENT = "orderPreSettlement";

    /** 任务调度：包赔场晚上定时结算 */
    public static final String SETTLEMENT_OF_CLAIMS_SETTLEMENT= "settlementOfClaimsSettlement";

    /** 任务调度：V8级别每日分红 */
    public static final String V8_EVERY_SHARE= "v8EveryShare";

    /** 任务调度：V收益自动划转到USDT */
    public static final String HANDLE_REWARD_CHANGE= "handleRewardChange";

    /******************** 任务调度-执行器：市场业务任务调度 *********/
    public static final int MARKET_JOB_EXECUTOR = 3;

    /** 任务调度：开启订阅各大交易所市场K线数据 */
    public static final String TURN_ON_KLINE_SUBSCRIPTION = "turnOnKlineSubscription";
    /** 任务调度：关闭市场K线数据订阅 */
    public static final String CLOSE_KLINE_SUBSCRIPTION = "closeKlineSubscription";
    /** 任务调度：重启各大交易所市场K线数据订阅 */
    public static final String REBOOT_KLINE_SUBSCRIPTION = "rebootKlineSubscription";
    /** 任务调度：监测缓存市场K线数据 */
    public static final String MONITORING_CACHE_MARKET_KLINE = "monitoringCacheMarketKline";
    /** 任务调度：更新缓存交易对配置信息 */
    public static final String UPDATE_KLINE_CACHE_CONFIG = "updateKlineCacheConfig";

    /******************** 任务调度-执行器：钱包业务任务调度 *********/
    public static final int WALLET_JOB_EXECUTOR = 4;

    /** 更新USDT 实时价 */
    public static final String UPDATE_USDT_PRICE = "updateUsdtPrice";

    /** 用户钱包归集 */
    public static final String USER_WALLET_COLLECTION = "userWalletCollection";



}
