package com.common.constatns;

/**
 * 用户中心服务常量类
 * @Author: lt
 * @Date: 2020/3/18 16:27
 */
public class UserConstants {

    /** 用户状态 0-正常  1-禁用*/
    public static final Integer USER_STATUS_0 = 0;
    /** 用户状态 0-正常  1-禁用*/
    public static final Integer USER_STATUS_1 = 1;

    /** 买入类型: 1-买涨 2-买跌 */
    public static final Integer BUY_TYPE_1 = 1;
    /** 买入类型: 1-买涨 2-买跌 */
    public static final Integer BUY_TYPE_2 = 2;

    /** 买入订单状态：1-进行中 2-已结束 */
    public static final Integer BUY_ORDER_STATUS_1 = 1;
    /** 买入订单状态：1-进行中 2-已结束 */
    public static final Integer BUY_ORDER_STATUS_2 = 2;


    /** 币币交易订单状态：1-进行中 2-已结束 */
    public static final Integer BB_BUY_ORDER_STATUS_1 = 1;
    /** 币币交易订单状态：1-进行中 2-已结束 */
    public static final Integer BB_BUY_ORDER_STATUS_2 = 2;

    /**
     * 1进账 2出账
     */
    public static final Integer WALLET_IN=1;
    public static final Integer WALLET_OUT=2;


    /**
     *1普通场次  2包赔场
     */
    public static final Integer ORDER_TYPE_1=1;
    public static final Integer ORDER_TYPE_2=2;

    /**
     * 1-进行中   2-已结束  3已结算
     */
    public static final Integer ORDER_STATUS_1=1;
    public static final Integer ORDER_STATUS_2=2;
    public static final Integer ORDER_STATUS_3=3;

    /**
     * 1未回复 0已回复
     */
    public static final Integer REPLY_STATUS_1=1;
    public static final Integer REPLY_STATUS_0=0;

    /**
     * 1提问  2回复
     */
    public static final Integer MESSAGE_TYPE_1=1;
    public static final Integer MESSAGE_TYPE_2=2;

    /** 用户升级类型：1-自动升级 2-系统修改 */
    public static final Integer UP_LEVEL_TYPE_1 = 1;
    /** 用户升级类型：1-自动升级 2-系统修改 */
    public static final Integer UP_LEVEL_TYPE_2 = 2;

    /**
     * 是否单个  1是  2不是
     */
    public static final Integer SIGN_FLAG_1=1;
    public static final Integer SIGN_FLAG_2=2;

}
