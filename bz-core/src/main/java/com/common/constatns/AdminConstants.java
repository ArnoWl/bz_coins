package com.common.constatns;

/**
 * 平台后端系统服务常量类
 * @Author: lt
 * @Date: 2020/5/3 14:11
 */
public class AdminConstants {

    /** 平台超级管理员角色编号 */
    public static final String ADMINISTRATOR = "administrator_role";

    /** 启用状态：0-启用 1-禁用 */
    public static final Integer ENABLED_STATUS_0 = 0;
    /** 启用状态：0-启用 1-禁用 */
    public static final Integer ENABLED_STATUS_1 = 1;

    /** 用户类型：1-普通用户*/
    public static final Integer USER_TYPE_1 = 1;

    /** 菜单类型：0-菜单 1-按钮 */
    public static final Integer SYS_MENU_TYPE_0 = 0;
    /** 菜单类型：0-菜单 1-按钮 */
    public static final Integer SYS_MENU_TYPE_1 = 1;

    /** 系统消息推送类型：1-全平台 2-全用户 3-指定用户 4-VIP用户*/
    public static final Integer SYS_MESSAGE_SEND_TYPE_1 = 1;
    /** 系统消息推送类型：1-全平台 2-全用户 3-指定用户 4-VIP用户*/
    public static final Integer SYS_MESSAGE_SEND_TYPE_2 = 2;
    /** 系统消息推送类型：1-全平台 2-全用户 3-指定用户 4-VIP用户*/
    public static final Integer SYS_MESSAGE_SEND_TYPE_3 = 3;
    /** 系统消息推送类型：1-全平台 2-全用户 3-指定用户 4-VIP用户*/
    public static final Integer SYS_MESSAGE_SEND_TYPE_4 = 4;


    /** 系统配置类型：1-提现手续费配置 2-订单配置 3-模式配置 */
    public static final Integer SYS_CONFIG_TYPE_1 = 1;
    /** 系统配置类型：1-提现手续费配置 2-订单配置 3-模式配置 */
    public static final Integer SYS_CONFIG_TYPE_2 = 2;
    /** 系统配置类型：1-提现手续费配置 2-订单配置 3-模式配置 */
    public static final Integer SYS_CONFIG_TYPE_3 = 3;

    /** App版本升级状态：1-提示升级 2-不提示升级 3-强制升级  */
    public static final Integer APP_VERSION_STATUS_3 = 3;

    /** 0系统币 1不是系统币 */
    public static final Integer SYS_COINS_YES = 0;

    public static final Integer SYS_COINS_NO = 1;

    /** 归集类型  1系统地址 2场外地址 */
    public static final Integer SYS_IMPUTED_TYPE_1 = 1;

    public static final Integer SYS_IMPUTED_TYPE_2 = 2;




}
