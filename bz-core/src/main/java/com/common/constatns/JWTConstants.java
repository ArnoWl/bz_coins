package com.common.constatns;

/**
 * jwt常量类
 * @Author: lt
 * @Date: 2020/3/30 10:58
 */
public class JWTConstants {

    /** JWT承载数据 - 主题（客户端类型、用户类型） **/
    public static final String JWT_SUBJECT = "jwtSubject";

    /***************************  客户端微服务调用鉴权   ****/
    /** JWT承载数据-发起微服务调用的客户端信息键值 **/
    public static final String JWT_KEY_CLIENT_CODE = "clientCode";
    public static final String JWT_KEY_CLIENT_NAME = "clientName";

    /** JWT承载数据-客户端类型-微服务调用 **/
    public static final String JWT_KEY_CLIENT_TYPE_REST = "x-client-token";

    /***************************  客户端用户登录鉴权   ****/
    /** JWT承载数据-用户类型-后台系统用户 **/
    public static final String JWT_KEY_USER_TYPE_ADMIN = "admin-token";
    /** JWT承载数据-用户类型-普通用户 **/
    public static final String JWT_KEY_USER_TYPE_USER = "user-token";

    /** JWT承载数据-系统用户信息键值 **/
    public static final String JWT_KEY_SYS_USER_ID = "sysUserId";
    public static final String JWT_KEY_SYS_USER_ACCOUNT = "sysUserAccount";

    /** JWT承载数据-普通用户信息键值 **/
    public static final String JWT_KEY_USER_ID = "userId";
    public static final String JWT_KEY_USER_ACCOUNT = "userAccount";
    public static final String JWT_KEY_USER_SSO = "ssoVersion";

}
