package com.common.constatns;

/**
 * 市场服务常量类
 * @Author: lt
 * @Date: 2020/7/25 18:53
 */
public class MarketConstants {

    /** 交易所K线数据订阅  */
    public static final String EXCHANGE_KLINE = "EXCHANGE_KLINE_";

    /** 订阅通道：比特币/USDT */
    public static final String CHANNEL_BTC_USDT = "BTC/USDT";

    /** 订阅通道：以太坊/USDT */
    public static final String CHANNEL_ETH_USDT = "ETH/USDT";
}
