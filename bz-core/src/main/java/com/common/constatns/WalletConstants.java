package com.common.constatns;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
public class WalletConstants {

    /**
     * 状态0YES 1NO
     */
    public static final Integer STATUS_0=0;
    public static final Integer STATUS_1=1;

    /** 协议类型：OMNI ERC20 */
    public static final String AGREE_TYPE_OMNI = "OMNI";
    /** 协议类型：OMNI ERC20 */
    public static final String AGREE_TYPE_ERC20 = "ERC20";

    /** 钱包地址类型：MAIN-主地址 FEE-手续费 */
    public static final String WALLET_ADDR_MAIN = "MAIN";
    /** 钱包地址类型：MAIN-主地址 FEE-手续费 */
    public static final String WALLET_ADDR_FEE = "FEE";

    /**
     * 归集类型 1内部地址 2场外地址
     */
    public static final Integer IMPUTED_TYPE_1=1;
    public static final Integer IMPUTED_TYPE_2=2;
}
