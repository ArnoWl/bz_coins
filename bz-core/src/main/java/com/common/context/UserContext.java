package com.common.context;

import com.common.utils.StringHelper;

import java.util.Map;

import static com.common.constatns.JWTConstants.*;

/**
 * 当前登录用户信息存储/获取
 * @Author: lt
 * @Date: 2020/3/30 13:33
 */
public class UserContext extends BaseContextHandler{

    public static String getToken(){
        Object value = get(JWT_KEY_USER_TYPE_USER);
        return StringHelper.getObjectValue(value);
    }

    public static String getUserID(){
        Object value = get(JWT_KEY_USER_ID);
        return returnObjectValue(value);
    }

    public static String getUserAccount(){
        Object value = get(JWT_KEY_USER_ACCOUNT);
        return returnObjectValue(value);
    }

    public static void setUserInfo(String token, Map<String,Object> params){
        set(JWT_KEY_USER_TYPE_USER,token);
        params.keySet().forEach(key -> set(key,params.get(key)));
    }

}
