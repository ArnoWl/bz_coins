package com.common.context;

import com.common.utils.StringHelper;

import java.util.Map;

import static com.common.constatns.JWTConstants.*;

/**
 * 后台系统登录用户信息存储/获取
 * @Author: lt
 * @Date: 2020/3/30 13:46
 */
public class SysUserContext extends BaseContextHandler{

    public static String getToken(){
        Object value = get(JWT_KEY_USER_TYPE_ADMIN);
        return StringHelper.getObjectValue(value);
    }

    public static Integer getSysUserID(){
        Object value = get(JWT_KEY_SYS_USER_ID);
        return Integer.valueOf(returnObjectValue(value));
    }

    public static String getSysUsername(){
        Object value = get(JWT_KEY_SYS_USER_ACCOUNT);
        return returnObjectValue(value);
    }

    public static void setSysUserInfo(String token, Map<String,Object> params){
        set(JWT_KEY_USER_TYPE_ADMIN,token);
        params.keySet().forEach(key -> set(key,params.get(key)));
    }

}
