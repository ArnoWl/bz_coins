package com.common.context;

import com.common.exception.UserTokenException;

import java.util.HashMap;
import java.util.Map;

/**
 * 处理信息到本地线程
 * @author lt
 * 2020年3月30日13:49:09
 */
public class BaseContextHandler {
    public static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key){
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<>();
            threadLocal.set(map);
        }
        return map.get(key);
    }

    public static void remove(){
        threadLocal.remove();
    }

    public static String returnObjectValue(Object value) {
        if(value == null){
            throw new UserTokenException();
        }
        return value.toString();
    }

}
