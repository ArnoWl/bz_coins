package com.common.base;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import groovy.lang.Singleton;
import lombok.Data;

import java.io.Serializable;

import static com.common.constatns.CommonConstants.E_000;
import static com.common.constatns.CommonConstants.E_001;

/**
 * 公共返回结果Vo
 * @author lt
 * 2020年3月14日17:07:31
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultVo implements Serializable {

    private static final long serialVersionUID = -1965646749284009585L;

    /**
     * 数据
     */
    public Object data;

    /**
     * 返回码
     */
    public String code;

    /**
     * 描述信息
     */
    public String errorMessage;

    public ResultVo() {
    }

    public ResultVo(String code, String errorMessage, Object data) {
        this.code = code;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    /**
     * 成功
     *
     * @return
     */
    public static ResultVo success() {
        return new ResultVo(E_000,"操作成功",null);
    }

    /**
     * 成功返回携带参数
     * @param data
     * @return
     */
    public static ResultVo success(Object data) {
        return new ResultVo(E_000,"操作成功",data);
    }

    /**
     * 失败
     *
     * @return
     */
    public static ResultVo failure(String errorMessage) {
        return new ResultVo(E_001,errorMessage,null);
    }

    /**
     * 自定义返回
     *
     * @return
     */
    public static ResultVo result(String code,String msg) {
        return new ResultVo(code,msg,null);
    }

    /**
     * 自定义返回
     *
     * @return
     */
    public static ResultVo result(String code,String msg,Object data) {
        return new ResultVo(code,msg,data);
    }

    /**
     * 判断是否成功
     *
     * @return
     */
    public boolean isSuccess() {
        return E_000.equals(this.getCode());
    }

}
