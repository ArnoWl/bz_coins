package com.common.base;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 树形列表模型
 */
@Data
public class TreeNodeVo implements Serializable {

    private static final long serialVersionUID = 4028708592607031566L;
    protected int id;
    protected int parentId;

    List<TreeNodeVo> children = new ArrayList<>();

    public void add(TreeNodeVo node) {
        children.add(node);
    }
}