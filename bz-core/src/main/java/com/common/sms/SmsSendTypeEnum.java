package com.common.sms;

import com.common.constatns.CommonConstants;

/**
 * 短信发送类型枚举
 * @author lt
 * 2020年3月25日16:14:15
 */
public enum SmsSendTypeEnum {

	SMS_TYPE_1(1, CommonConstants.SMS_TYPE_1,"用户注册验证码"),

	SMS_TYPE_2(2, CommonConstants.SMS_TYPE_1,"修改用户登录密码验证码"),

	SMS_TYPE_3(3, CommonConstants.SMS_TYPE_1,"修改用户支付密码验证码");


	/** 标识 */
	private Integer key;

	/** 短信类型：1-验证码短信 2-短信通知 */
	private Integer type;

	/** 描述 */
	private String desc;

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	SmsSendTypeEnum(Integer key, Integer type, String desc) {
		this.key = key;
		this.type = type;
		this.desc = desc;
	}

	public static SmsSendTypeEnum getSendTypeByKey(Integer key) {
		for (SmsSendTypeEnum s : SmsSendTypeEnum.values()) {
			if (key.equals(s.getKey())) {
				return s;
			}
		}
		return null;
	}
}
