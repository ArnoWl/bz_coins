package com.common.sms.kwsms.utils;

import com.common.base.ResultVo;
import com.common.sms.kwsms.yun.SmsSingleSender;
import com.common.sms.kwsms.yun.SmsSingleSenderResult;

/**
 * 短信发送工具类
 * @author arno
 * @version 1.0
 * @date 2020-07-25
 */
public class KwUtils {

    /**
     * https://www.kewail.com/短信发送
     *
     * @param phone
     * @param validcode
     * @return
     */
    public static ResultVo sendValidCode(String phone,String validcode) {
        try {
            // 请根据实际 accesskey 和 secretkey 进行开发，以下只作为演示 sdk 使用
            String accesskey = "5f1c02b9efb9a31981407ffa";
            String secretkey = "5f62dff8dea44b07ac0d47cc2c108294";
            String signName="币指";
            // 初始化单发
            SmsSingleSender singleSender = new SmsSingleSender(accesskey, secretkey);
            SmsSingleSenderResult singleSenderResult;

            // 普通单发,注意前面必须为【】符号包含，置于头或者尾部。
            singleSenderResult = singleSender.send(0, "86", phone, "【"+signName+"】尊敬的用户：您的验证码："+validcode+"，工作人员不会索取，请勿泄漏。",
                    "", "");
            if(singleSenderResult.result!=0) {
                return ResultVo.failure(singleSenderResult.errMsg);
            }
            return ResultVo.success();
        } catch (Exception e) {
            return ResultVo.failure("发送失败");
        }
    }

    public static void main(String[] args) {
        sendValidCode("13611081158","123221");
    }
}
