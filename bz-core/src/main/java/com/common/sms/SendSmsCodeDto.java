package com.common.sms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author lt
 * 2019年5月22日 14:14:39
 */
@Data
@ApiModel(value = "发送短信验证码")
public class SendSmsCodeDto implements Serializable {

    private static final long serialVersionUID = 1161861623386442108L;

    @ApiModelProperty(value = "手机号")
    @NotBlank(message = "手机号不能为空")
    private String phone;

    @ApiModelProperty(value = "发送短信类型: 0 用户登录验证码 1 用户注册验证码 2 修改用户登录密码验证码 3 修改用户支付密码验证码")
    @NotNull(message = "短信类型不能为空")
    private Integer smsType;

}
