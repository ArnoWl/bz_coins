package com.common.sms;

import com.common.base.ResultVo;
import com.common.exception.SmsSendException;
import com.common.utils.SMSUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 发送短信组件(阿里云通信)
 * @author lt
 *	2019年5月22日 14:42:36
 */
@Component
public class SmsComponent {

	@Resource
	private RedisTemplate<String, String> redisTemplate;

	public ResultVo sendSms(SendSmsCodeDto codeDto){
		try {
			//TODO String code =  SMSUtils.createRandom(6);
			String code =  "666666";
			ResultVo resultVo = ResultVo.success();
			//ResultVo resultVo= KwUtils.sendValidCode(phone,code);
			if(resultVo.isSuccess()){
				String key = SMSUtils.getSmsKey(codeDto.getSmsType(), codeDto.getPhone());
				redisTemplate.opsForValue().set(key, code, 5, TimeUnit.MINUTES);
			}
			return resultVo;
		} catch (Exception e) {
			throw new SmsSendException("发送短信失败");
		}
	}

	/**
	 * 发送短信验证码
	 * @param codeDto
	 * @return
	 */
	public ResultVo sendSmsCode(SendSmsCodeDto codeDto){
		if(StringUtils.isBlank(codeDto.getPhone())){
			return ResultVo.failure("手机号不能为空");
		}
		if(codeDto.getSmsType() == null){
			return ResultVo.failure("发送类型不能为空");
		}
		// 校验短信验证码是否已发送，避免重复发送
		Object obj = redisTemplate.opsForValue().get(SMSUtils.getSmsKey(codeDto.getSmsType(),codeDto.getPhone()));
		if(obj == null){
			// 发送短信
			return this.sendSms(codeDto);
		}
		return ResultVo.failure("短信已发送，请稍后再试");
	}

	/**
	 * 校验短信验证码
	 * @param codeDto
	 * @return
	 */
	public ResultVo verifySmsCode(VerifySmsCodeDto codeDto){
		String code = redisTemplate.opsForValue()
				.get(SMSUtils.getSmsKey(codeDto.getSmsType(), codeDto.getPhone()));
		if(StringUtils.isBlank(code)){
			return ResultVo.failure("短信验证码已失效，请重新发送");
		}
		if(!code.equals(codeDto.getVerificationCode())){
			return ResultVo.failure("短信验证有误，登录失败");
		}
		return ResultVo.success();
	}
}
