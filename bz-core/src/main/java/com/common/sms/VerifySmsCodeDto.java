package com.common.sms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 校验短信验证码
 *
 * @Author: lt
 * @Date: 2020/7/3 17:42
 */
@Data
@ApiModel(value = "校验短信验证码")
public class VerifySmsCodeDto implements Serializable {

    private static final long serialVersionUID = 8435689148520953145L;

    @ApiModelProperty(value = "手机号")
    @NotBlank(message = "手机号不能为空")
    private String phone;

    @ApiModelProperty(value = "验证码")
    @NotBlank(message = "验证码不能为空")
    private String verificationCode;

    @ApiModelProperty(value = "短信类型: 1-用户注册验证码 2-修改用户登录密码验证码 3-修改用户支付密码验证码")
    @NotNull(message = "短信类型不能为空")
    private Integer smsType;

}
