package com.common.fileUpload;

import com.baomidou.mybatisplus.extension.api.R;
import com.common.exception.BaseException;
import com.common.utils.lang.DateUtils;
import com.qiniu.common.Zone;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.csource.common.MyException;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Slf4j
public class QiNiuQunClient {

    /** 七牛绑定的域名 */
    private static String domain;
    /** 七牛路径前缀 */
    private static String prefix;
    /** 七牛ACCESS_KEY */
    private static String accessKey;
    /** 七牛SECRET_KEY */
    private static String secretKey;
    /** 七牛存储空间名 */
    private static String bucket;

    /**
     * 云存储配置信息
     */
    private static UploadManager uploadManager;

    private static String token;


    private static Set<String> imgFileTypes = new HashSet<String>(0);

    static {
        try {
            Properties properties = new Properties();
            InputStream in = QiNiuQunClient.class.getClassLoader().getResourceAsStream("qiniuyun_client.properties");
            properties.load(in);

            QiNiuQunClient.accessKey = properties.getProperty("accessKey");
            QiNiuQunClient.domain = properties.getProperty("domain");
            QiNiuQunClient.secretKey = properties.getProperty("secretKey");
            QiNiuQunClient.bucket = properties.getProperty("bucket");

            String fileTypeStr = "bmp|jpg|png|tif|gif|pcx|tga|exif|fpx|svg|psd|cdr|pcd|dxf|ufo|eps|ai|raw|WMF|webp|mp3|mp4";
            String[] fileTypes = fileTypeStr.split("\\|");
            Collections.addAll(imgFileTypes, fileTypes);
            // 初始化
            Zone zone = new Zone.Builder(Zone.zone0())
                    .upHttp("http://up.qiniup.com").build();
            uploadManager = new UploadManager(new Configuration(zone));
        } catch (Exception e) {
            log.error("QINiuYun Client Init Fail!", e);
        }
    }

    public static void init() {
        token = Auth.create(accessKey, secretKey).uploadToken(bucket);
    }

    /**
     * 文件路径
     *
     * @param prefix 前缀
     * @param suffix 后缀
     * @return 返回上传路径
     */
    public static String getPath(String prefix, String suffix) {
        //生成uuid
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        //文件路径
        String path = DateUtils.formatDate(new Date(), "yyyyMMdd") + "/" + uuid;

        if (StringUtils.isNotBlank(prefix)) {
            path = prefix + "/" + path;
        }

        return path + suffix;
    }

    /**
     * 文件上传
     *
     * @param data 文件字节数组
     * @param path 文件路径，包含文件名
     * @return 返回http地址
     */
    public static String upload(byte[] data, String path) {
        try {
            QiNiuQunClient.init();
            Response res = uploadManager.put(data, path, token);
            if (!res.isOK()) {
                throw new RuntimeException("上传七牛出错：" + res.toString());
            }
        } catch (Exception e) {
            throw new BaseException("上传文件失败，请核对七牛配置信息", e);
        }

        return domain + "/" + path;
    }

    /**
     * 文件上传
     *
     * @param inputStream 字节流
     * @param path        文件路径，包含文件名
     * @return 返回http地址
     */
    public static String upload(InputStream inputStream, String path) {
        try {
            byte[] data = IOUtils.toByteArray(inputStream);
            return upload(data, path);
        } catch (IOException e) {
            throw new BaseException("上传文件失败", e);
        }
    }

    public static String uploadFile(MultipartFile file) throws IOException {
        String type = FileType.getFileType(file.getInputStream());
        if (!imgFileTypes.contains(type)) {
            throw new BaseException("图片格式有误，上传文件失败");
        }
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        return upload(file.getBytes(), getPath(prefix, suffix));
    }

    /**
     * 文件上传
     *
     * @param data   文件字节数组
     * @param suffix 后缀
     * @return 返回http地址
     */
    public static String uploadSuffix(byte[] data, String suffix) {
        return upload(data, getPath(prefix, suffix));
    }

    /**
     * 文件上传
     *
     * @param inputStream 字节流
     * @param suffix      后缀
     * @return 返回http地址
     */
    public static String uploadSuffix(InputStream inputStream, String suffix) {
        return upload(inputStream, getPath(prefix, suffix));
    }

    public static String getToken(){
        return Auth.create(accessKey, secretKey).uploadToken(bucket);
    }
}