package com.common.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Redis分布式锁
 * @author tps
 */
@Component
public class RedisLock {

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    /**
     * 默认设置锁超时
     */
    private static final int MILLI_SECOND = 20000;

    public String getDefaultOutTime(){
        return String.valueOf(System.currentTimeMillis() + MILLI_SECOND);
    }

    /**
     * 加锁
     * @param key
     * @param outTime 当前时间+超时时间
     * @return
     */
    public boolean lock(String key, String outTime) {
        //相当于SETNX，setIfAbsent方法设置了为true,没有设置为false
        if(redisTemplate.opsForValue().setIfAbsent(key, outTime)) {
            return true;
        }
        String currentValue = String.valueOf(redisTemplate.opsForValue().get(key));
        //如果锁过期  解决死锁
        if (!StringUtils.isEmpty(currentValue) && Long.parseLong(currentValue) < System.currentTimeMillis()) {
            // 获取上一个锁的时间，锁过期后，GETSET将原来的锁替换成新锁
            String oldValue = String.valueOf(redisTemplate.opsForValue().getAndSet(key, outTime));
            return !StringUtils.isEmpty(oldValue) && oldValue.equals(currentValue);
        }
        return false;
    }

    /**
     * 解锁
     * @param key
     * @param outTime
     */
    public void unlock(String key, String outTime) {
        try {
            String currentValue = String.valueOf(redisTemplate.opsForValue().get(key));
            if (!StringUtils.isEmpty(currentValue) && currentValue.equals(outTime)) {
                redisTemplate.opsForValue().getOperations().delete(key);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}