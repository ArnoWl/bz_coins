package com.common.config;

import com.alibaba.fastjson.JSONArray;
import com.common.constatns.RedisKeyConstants;
import com.common.utils.lang.NumberUtils;
import com.common.utils.lang.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-21
 */
@Component
public class RedisUtils {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 设置USDT 对人民币价格
     *
     * @param usdtPrice
     */
    public void setUSDTPrice(BigDecimal usdtPrice) {
        redisTemplate.opsForValue().set(RedisKeyConstants.USDT_PRICE, usdtPrice.toPlainString());
    }

    /**
     * 获取USDT价格
     *
     * @return
     */
    public BigDecimal getUSDTPrice() {
        String price = redisTemplate.opsForValue().get(RedisKeyConstants.USDT_PRICE);
        BigDecimal usdtPrice = new BigDecimal(price);
        return usdtPrice;
    }



    /**
     * 设置日K
     *
     */
    public void setRefreshOpenPrice(JSONArray list) {
        redisTemplate.opsForValue().set(RedisKeyConstants.REFRESH_OPEN_PRICE, list.toJSONString());
    }

    /**
     * 获取日K
     *
     */
    public JSONArray getRefreshOpenPrice() {
        String result = redisTemplate.opsForValue().get(RedisKeyConstants.REFRESH_OPEN_PRICE);
        if(!StringUtils.isEmpty(result)){
            return JSONArray.parseArray(result);
        }
        return  null;
    }


    /**
     * 设置下单价
     */
    public void setOrderPrice(String coinSymbol,BigDecimal price){
        redisTemplate.opsForValue().set("order:close_price:"+coinSymbol,price.toPlainString());
    }

    /**
     * 获取下单价
     */
    public BigDecimal getOrderPrice(String coinSymbol){
        String key = "order:close_price:"+coinSymbol;
        String result = redisTemplate.opsForValue().get(key);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        BigDecimal orderPrice = NumberUtils.createBigDecimal(result);
        redisTemplate.delete(key);
        return orderPrice;
    }
}
