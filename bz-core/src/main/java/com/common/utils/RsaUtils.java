package com.common.utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * RSA - 解密和加密方法
 * @Author: lt
 * @Date: 2020/3/17 14:24
 */
public class RsaUtils {

    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;
    /**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;


    /**
     * 公钥加密
     *
     * @param publicKey     公钥
     * @param plainTextData 明文数据
     * @return
     * @throws Exception 加密过程中的异常信息
     */
    public static String encrypt(RSAPublicKey publicKey, byte[] plainTextData) throws Exception {
        if (publicKey == null) {
            throw new Exception("加密公钥为空, 请设置");
        }
        Cipher cipher = null;
        try {
            // 使用默认RSA
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] output = cipher.doFinal(plainTextData);
            return base64ToStr(output);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此加密算法");
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e) {
            throw new Exception("加密公钥非法,请检查");
        } catch (IllegalBlockSizeException e) {
            throw new Exception("明文长度非法");
        } catch (BadPaddingException e) {
            throw new Exception("明文数据已损坏");
        }
    }

    /**
     * 公钥加密 -- 分段加密
     *
     * @param publicKey     公钥
     * @param plainTextData 明文数据
     * @return
     * @throws Exception 加密过程中的异常信息
     */
    public static String decryptEncrypt(RSAPublicKey publicKey, byte[] plainTextData) throws Exception {
        if (publicKey == null) {
            throw new Exception("加密公钥为空, 请设置");
        }
        Cipher cipher = null;
        try {
            // 使用默认RSA
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            int inputLen = plainTextData.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段加密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(plainTextData, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(plainTextData, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();
            return base64ToStr(encryptedData);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此加密算法");
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e) {
            throw new Exception("加密公钥非法,请检查");
        } catch (IllegalBlockSizeException e) {
            throw new Exception("明文长度非法");
        } catch (BadPaddingException e) {
            throw new Exception("明文数据已损坏");
        }
    }

    /**
     * 私钥解密
     *
     * @param privateKey 私钥
     * @param cipherData 密文数据
     * @return 明文
     * @throws Exception 解密过程中的异常信息
     */
    public static String decrypt(RSAPrivateKey privateKey, byte[] cipherData) throws Exception {
        if (privateKey == null) {
            throw new Exception("解密私钥为空, 请设置");
        }
        Cipher cipher = null;
        try {
            // 使用默认RSA
            byte[] sa = RsaUtils.strToBase64("w3ZTv3LxFHM30tKvWA43TbvjBhtPmFsoxVrBDcvZ7cQoCrMIEHJGV3voVPfEeyXQwRhKz8sTNBJh/9rEZOq4d9AuahHOWpG0ZwIn+3Ii7ivJIX9/vkp1cahu1IrbRb4SGNZgv+Tn/mX6D5Wx9rXAo8dkkCQlNJTCImwl7cZ9fxd1oWisfSdbVvV1X+CQnFsgwdDkW5nTMyz9J9kV2s2mTFLVdEnCEMXhxIX+Dv1Spl7WWTBIBDhOjALUwK2tdmqStHGDpeae9aAvgbbdr8WkRBhOQTeqJ0E1SblKmkrpzK+iawx5NlI794ar+F8jpeaem3oF1d/G82/IGvxltcXm2irV7iriVprqXNN9NdAUToMpkh8XyNA269zZyFzUhIRRXVQuoshMVPNaXetiJ4VMCSV9QizKTIqWvgfN7NebpEyK5ewuLizFlaI0zh80G7D65XsbRlbFquOdpexfy43HOOva+PWdhPbD75VVfoKivInU1JnyRI90rlTSNuovbKdhzM+gOXhtd0tF/mhL9oFn7GtKU+Y3D4UJIHnieF2MUmFg6xhsLg6m+dasqS6x7TqvQ7IcQZt9G04XWJ30W7p/3B9jr4Va+sHhdr93e8cdvXSfWAWTRZDrcjl/GzTXY4tQNOYY9h5fMHSneczYR87JvFi4DrCWvXu+A2h6vCLXx91ahi/rISdL2cwwjUKV1qCjOrE/dO4ps8FVTug61nJKdfw7fTibmdbZ1PhtvU3jTwEpWMNqgmksxEQsFgl8PDdnZAWv81JfEXJF+DyGpKNSNt6GPzsLW/rpzW9fTqdP357sLJ/qH9UwNtRBdQk++2mGFnQuIT2Do3jWpsUJfryHehNaXX9v3gz6DIbTtp2/fgRpRZcbJQitUqPZej08UxNLBz2Dsn2PXqcYf6anY5GtyQWRqtXv5f+kcKkzz48e3UtaaxVC+Jt8a7mIbHtrAHcsETcPkh4wjKlTU6jUuC1Ibk2ASItqnpAvbLiGflvk40z7rot59tw5asYt9mbhIq0zuYwhJqkWnUbyrNLkIyZIiJQbbhHxnmmdECZxjMi3QStIamdMnnjaye31i7hzR+i9n3Lh6qGn+ynjFsLQb8UrB4jtdn3kqjN5f25X80DXmHh1o/IiuFgrSPW9rCNDlOg+YlDAZ3HXn5wmC7fNGC+WWEJn8tok4fkQrZIY6h1cp9Khs5+4M4SRrJvjgnKYtYj1OEubp7sVaFIUTZGauTFWNOhkpVdf9eTnnkL2pHOhAGrH/03D4OF3otaxMYomG9alIvGCJ5lXecuSe473uNPaSqCGufP12tseDG9PujDSdTJNhO6MKoemIsKadgSr3/0vQ6WMAoXFscgMLy5Ft7xveEUB7V43qJis4fS2dotUehAlAS2ajoSybONfEJPm87SGtPxoXk1eFX/biL0/IzMYAwO0qevYn6dsanjpLsy7BobC1LbyhN11cDlNX1WnC48m9PTgmt95/6aDIdHeRZLzohNSbe79jHLD0HYnSRb49cxvK6GmLVyxrv0dsScwAD0K");
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] output = cipher.doFinal(cipherData);
            return new String(output);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此解密算法");
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e) {
            throw new Exception("解密私钥非法,请检查");
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new Exception("密文长度非法");
        } catch (BadPaddingException e) {
            e.printStackTrace();
            throw new Exception("密文数据已损坏");
        }
    }

    /**
     * 私钥解密  ---- 分段解密
     *
     * @param privateKey 私钥
     * @param cipherData 密文数据
     * @return 明文
     * @throws Exception 解密过程中的异常信息
     */
    public static String segmentDecrypt(RSAPrivateKey privateKey, byte[] cipherData) throws Exception {
        if (privateKey == null) {
            throw new Exception("解密私钥为空, 请设置");
        }
        Cipher cipher = null;
        try {
            // 使用默认RSA
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            int inputLen = cipherData.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段解密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                    cache = cipher.doFinal(cipherData, offSet, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(cipherData, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            out.close();
            return new String(decryptedData);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此解密算法");
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e) {
            throw new Exception("解密私钥非法,请检查");
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new Exception("密文长度非法");
        } catch (BadPaddingException e) {
            e.printStackTrace();
            throw new Exception("密文数据已损坏");
        }
    }

    public static String base64ToStr(byte[] b) {
        return javax.xml.bind.DatatypeConverter.printBase64Binary(b);
    }

    public static byte[] strToBase64(String str) {
        return javax.xml.bind.DatatypeConverter.parseBase64Binary(str);
    }

    public static void main(String[] args) throws Exception {
       /* Map<String, byte[]> keyMap = RsaKeyHelper.generateKey("123456");
        System.out.println("初始化私钥为："+keyMap.get("pri"));
        System.out.println("初始化公钥为："+keyMap.get("pub"));

        String originData="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDPRfc4A5SAmpi3sSWqRBk/QL2UQ/uvn/BsbaNoa5mz1AaWJS3eP7lSBFezPWbCskIhq6WFThKLtGDlofO+QVb7i3h0hRL5Z+4zuAbOSY4iMv1vYryHAyjPGX2vubGlr3yrrjt3QDYwbTuj853g8zgZ0e5G0LhdclWZLHEPaFpFIwIDAQAB";
        System.out.println(originData.getBytes());
        RsaKeyHelper keyHelper = new RsaKeyHelper();
        String encryptData = RsaUtils.encrypt((RSAPublicKey) keyHelper.getPublicKey(keyMap.get("pub")),RsaUtils.strToBase64(originData));
        System.out.println("加密后："+encryptData);
        String decryptData=RsaUtils.decrypt((RSAPrivateKey) keyHelper.getPrivateKey(keyMap.get("pri")),RsaUtils.strToBase64(encryptData));
        System.out.println("解密后："+decryptData);*/
        /*String sa = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDPRfc4A5SAmpi3sSWqRBk/QL2UQ/uvn/BsbaNoa5mz1AaWJS3eP7lSBFezPWbCskIhq6WFThKLtGDlofO+QVb7i3h0hRL5Z+4zuAbOSY4iMv1vYryHAyjPGX2vubGlr3yrrjt3QDYwbTuj853g8zgZ0e5G0LhdclWZLHEPaFpFIwIDAQAB";
        System.out.println(RsaUtils.strToBase64(sa));
        byte[] as = RsaUtils.strToBase64(sa);
        System.out.println(RsaUtils.base64ToStr(as));*/
    }
}

