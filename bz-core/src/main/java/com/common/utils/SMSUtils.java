package com.common.utils;

/**
 * 短信工具类
 * 
 */
public final class SMSUtils {
	
	public static final String NUMBER_STR = "1234567890";
	
	public static final String STR = "1234567890abcdefghijkmnpqrstuvwxyz";

	/** 缓存key分割符 */
	public static final String COLON = ":";

	/** 短信 */
	public static final String MODEL_SMS = "sms:";
	
	/**
	 * 生成指定长度的短信验证码
	 * @param length 
	 * @return
	 */
	public static String createRandom(int length) {
		return createRandom(true, length);
	}
	
	/**
	 * 创建指定长度的随机字符串
	 * 
	 * @param numberFlag 是否是数字
	 * @param length 验证码长度
	 * @return
	 */
	public static String createRandom(boolean numberFlag, int length) {
		StringBuilder sb = new StringBuilder();
		String strTable = numberFlag ? NUMBER_STR : STR;
		int len = strTable.length();
		boolean bDone = true;
		do {
			int count = 0;
			for (int i = 0; i < length; i++) {
				double dblR = Math.random() * len;
				int intR = (int) Math.floor(dblR);
				char c = strTable.charAt(intR);
				if (('0' <= c) && (c <= '9')) {
					count++;
				}
				sb.append(strTable.charAt(intR));
			}
			if (count >= 2) {
				bDone = false;
			}
		} while (bDone);
		return sb.toString();
	}

	/**
	 * 创建短信验证码缓存Key
	 * @param smsType 短信类型
	 * @param phone 手机号码
	 * @return
	 */
	public static String getSmsKey(Integer smsType, String phone) {
		StringBuilder sb = new StringBuilder();
		sb.append(COLON).append(MODEL_SMS);
		sb.append(smsType).append(COLON).append(phone);
		return sb.toString();
	}



}
