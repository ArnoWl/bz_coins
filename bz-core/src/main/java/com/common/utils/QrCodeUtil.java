package com.common.utils;

import com.common.exception.BaseException;
import com.common.fileUpload.QiNiuQunClient;
import org.iherus.codegen.qrcode.SimpleQrcodeGenerator;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 二维码生成工具类 -- FastDFS上传
 * @author lt
 */
public class QrCodeUtil {

    public static String generalQRCode(String content) {
        try {
            BufferedImage image = new SimpleQrcodeGenerator().generate(content).getImage();
            return uploadImage(image);
        } catch (Exception e) {
            throw new BaseException("生成二维码失败",e);
        }
    }

    public static String generalLogoQRCode(String content,String logoUrl) {
        try {
            BufferedImage image = new SimpleQrcodeGenerator().setRemoteLogo(logoUrl).generate(content).getImage();
            return uploadImage(image);
        } catch (Exception e) {
            throw new BaseException("生成二维码失败",e);
        }
    }

    private static String uploadImage(BufferedImage image) throws Exception{
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //文件转换为字节数组
        ImageIO.write(image, "png", out);
        byte[] bytes = out.toByteArray();
        // 转换成文件
        InputStream inputStream = new ByteArrayInputStream(bytes);
        MultipartFile multipartFile = new MockMultipartFile("qrCode","qrCode.png","png",inputStream);
        // 上传二维码图片文件
        return QiNiuQunClient.uploadFile(multipartFile);
    }

    public static void main(String[] args) throws IOException {
        String content = "www.baidu.com";
        String logoUrl = "http://img.shidaizhu.com/group1/M00/00/27/tzznF17fLYKAd6UMAAF-3xOx1Zg535.png";
        System.out.println(generalQRCode(content));
//        System.out.println(generalLogoQRCode(content,logoUrl));
    }
}
