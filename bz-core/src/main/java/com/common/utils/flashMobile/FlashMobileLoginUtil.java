package com.common.utils.flashMobile;

import com.alibaba.fastjson.JSONObject;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.exception.BaseException;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: lt
 * @Date: 2020bz_coins/28 18:45
 */
public class FlashMobileLoginUtil {

    // 免密登录后台url
    public static final String FLASH_LOGIN_URL = "https://api.253.com/open/flashsdk/mobile-query";
    // 本机号校验url
    public static final String FLASH_VALIDATE_URL = "https://api.253.com/open/flashsdk/mobile-validate";


    //创建应用时填入的rsa公钥对应的私钥字符串
    public static final String privateKey = "";
    // 安卓应用闪验APPID
    private static final String ANDROID_APP_ID = "qTa58IVH";
    // 安卓应用闪验APPKEY
    private static final String ANDROID_APP_KEY = "x4j7PkZx";
    // IOS应用闪验APPID
    private static final String IOS_APP_ID = "qTa58IVH";
    // IOS应用闪验APPKEY
    private static final String IOS_APP_KEY = "x4j7PkZx";
    //手机号加解密方式 0 AES 1 RSA , 可以不传，不传则手机号解密直接使用AES解密
    private static final String encryptType = "0";

    public static ResultVo getFlashMobile(String token, Integer sysType){
        try {
            String key;
            Map<String, String> params = new HashMap<String, String>();
            params.put("token", token);
            params.put("encryptType", encryptType);//可以不传，不传则解密直接使用AES解密
            if(CommonConstants.SYS_TYPE_0.equals(sysType)){
                params.put("appId", ANDROID_APP_ID);
                params.put("sign", SignUtils.getSign(params, ANDROID_APP_KEY));
                key = MD5.getMD5Code(ANDROID_APP_KEY);
            }else if(CommonConstants.SYS_TYPE_1.equals(sysType)){
                params.put("appId", IOS_APP_ID);
                params.put("sign", SignUtils.getSign(params, IOS_APP_KEY));
                key = MD5.getMD5Code(IOS_APP_KEY);
            }else{
                return ResultVo.failure("当前终端不支持手机号一键登录");
            }
            JSONObject jsonObject = OkHttpUtil.postRequest(FLASH_LOGIN_URL, params);
            if (null != jsonObject) {
                String code = jsonObject.getString("code");     //返回码 200000为成功
                String message = jsonObject.getString("message"); //返回消息
                if ("200000".equals(code)) {
                    String dataStr = jsonObject.getString("data");
                    JSONObject dataObj = JSONObject.parseObject(dataStr);
                    String mobile = dataObj.getString("mobileName");
                    if ("0".equals(encryptType)) {
                        mobile = AESUtils.decrypt(mobile, key.substring(0, 16), key.substring(16));
                    } else if ("1".equals(encryptType)) {
                        mobile = RSAUtils.decryptByPrivateKeyForLongStr(mobile, privateKey);
                    }
                    return ResultVo.success(mobile);
                }
                return ResultVo.failure(message);
            }
            return ResultVo.failure("手机号一键登录验证失败");
        } catch (Exception e) {
            throw new BaseException("获取闪验手机号信息失败",e);
        }
    }

    public static ResultVo verifyMobile(String token, String mobile, Integer sysType) {
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put("token", token);
            params.put("mobile", mobile);
            if(CommonConstants.SYS_TYPE_0.equals(sysType)){
                params.put("appId", ANDROID_APP_ID);
                params.put("sign", SignUtils.getSign(params, ANDROID_APP_KEY));
            }else if(CommonConstants.SYS_TYPE_1.equals(sysType)){
                params.put("appId", IOS_APP_ID);
                params.put("sign", SignUtils.getSign(params, IOS_APP_KEY));
            }else{
                return ResultVo.failure("当前终端不支持手机号一键登录");
            }
            JSONObject jsonObject = OkHttpUtil.postRequest(FLASH_VALIDATE_URL, params);
            if (null != jsonObject) {
                System.out.println("response:"+jsonObject.toJSONString());
                String code = jsonObject.getString("code");     //返回码 200000为成功
                String message = jsonObject.getString("message");//返回消息
                if ("200000".equals(code)) {
                    String dataStr = jsonObject.getString("data");
                    JSONObject dataObj = JSONObject.parseObject(dataStr);
                    String isVerify = dataObj.getString("isVerify");
                    String tradeNo = dataObj.getString("tradeNo");//交易流水号
                    if ("1".equals(isVerify)) {
                        return ResultVo.success();
                    } else if ("0".equals(isVerify)) {
                        return ResultVo.failure("手机号码校验不通过");
                    }
                }
                return ResultVo.failure(message);
            }
            return ResultVo.failure("账户认证失败");
        } catch (Exception e) {
            throw new BaseException("账户认证失败",e);
        }
    }
}
