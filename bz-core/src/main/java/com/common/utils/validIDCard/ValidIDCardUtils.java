package com.common.utils.validIDCard;

import com.alibaba.fastjson.JSONObject;
import com.common.base.ResultVo;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Encoder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-28
 */
@Slf4j
public class ValidIDCardUtils {

    /**
     * 图片识别身份证
     *
     * @param file 身份证 正面
     * @return
     */
    public static ResultVo validIDCardFile(MultipartFile file) {
        try {
            OkHttpClient client = new OkHttpClient();
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
//                    .addFormDataPart("file", file.getName(),RequestBody.create(MediaType.parse("image/jpeg"), file))
                    .addFormDataPart("image", multipartFileToBASE64(file))
                    .addFormDataPart("key", "4f3a0d117b19005e8f60c40c33318066")
                    .addFormDataPart("side", "front")
                    .build();

            Request request = new Request.Builder()
                    .url("http://apis.juhe.cn/idimage/verify")//需修改为对应产品接口url
                    .post(body)
                    .build();

            Response response = client.newCall(request).execute();
            String result = response.body().string();
            JSONObject jsonObject = JSONObject.parseObject(result);
            if (!"0".equals(jsonObject.getString("error_code"))) {
                return ResultVo.failure("证件识别失败");
            }
            JSONObject resultObj = jsonObject.getJSONObject("result");
            if (resultObj == null) {
                return ResultVo.failure("证件识别失败");
            }
            Map<String, String> map = new HashMap<>();
            String realName = resultObj.getString("realname");
            map.put("realName", realName);
            String idcard = resultObj.getString("idcard");
            map.put("idCard", idcard);
            if (StringUtils.isEmpty(map.get("idCard"))) {
                return ResultVo.failure("证件识别失败");
            }
            return ResultVo.success(map);
        } catch (Exception e) {
            return ResultVo.failure("图片识别失败");
        }

    }

    // MultipartFile转BASE64字符串
    public static String multipartFileToBASE64(MultipartFile mFile) throws Exception {
        BASE64Encoder bEncoder = new BASE64Encoder();
        String base64EncoderImg = bEncoder.encode(mFile.getBytes()).replaceAll("[\\s*\t\n\r]", "");;
        return base64EncoderImg;
    }

    public static void main(String[] args) {
        try {
            byte[] bytes = Files.readAllBytes(Paths.get("/Users/arno/Downloads/idcard/front.jpg"));
            BASE64Encoder bEncoder = new BASE64Encoder();
            String base64EncoderImg = bEncoder.encode(bytes).replaceAll("[\\s*\t\n\r]", "");;
            System.out.println(base64EncoderImg);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
