package com.common.utils;

import java.math.BigDecimal;

/**
 * 计算方法工具类
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
public class CalcUtils {
    /**
     * 加
     *
     * @param a1
     *            加数
     * @param a2
     *            被加数
     * @param index
     *            保留位数
     * @return 四舍五入 保留两位小数
     */
    public static BigDecimal add(BigDecimal a1, BigDecimal a2, int index) {
        return a1.add(a2).setScale(index, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 减
     *
     * @param a1
     *            减数
     * @param a2
     *            被减数
     * @param index
     *            保留位数
     * @return 四舍五入 保留两位小数
     */
    public static BigDecimal sub(BigDecimal a1, BigDecimal a2, int index) {
        return a1.subtract(a2).setScale(index, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 乘
     *
     * @param a1
     *            乘数
     * @param a2
     *            被乘数
     * @param index
     *            保留位数
     * @return 四舍五入 保留两位小数
     */
    public static BigDecimal mul(BigDecimal a1, BigDecimal a2, int index) {
        return a1.multiply(a2).setScale(index, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 除
     *
     * @param d1 除数
     * @param d2
     *            被除数
     * @param index
     *            保留位数
     * @return 四舍五入 保留两位小数
     */
    public static BigDecimal div(BigDecimal d1, BigDecimal d2, int index) {
        if(d2.compareTo(BigDecimal.ZERO)==0) {
            return BigDecimal.ZERO;
        }
        return d1.divide(d2, index, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 获取一个随机数
     * @param number
     * @return
     */
    public static int getRandom(int number){
        int num = (int)(0+Math.random()*(number));
        return num;
    }

    /**
     * 获取两个金额之间的随机数
     * @param min
     * @param max
     * @return
     */
    public static BigDecimal getRandomRedPacketBetweenMinAndMax(BigDecimal min, BigDecimal max){
        float minF = min.floatValue();
        float maxF = max.floatValue();
        //生成随机数
        BigDecimal db = new BigDecimal(Math.random() * (maxF - minF) + minF);

        //返回保留两位小数的随机数。不进行四舍五入
        return db.setScale(6,BigDecimal.ROUND_DOWN);
    }

    public static void main(String[] args) {
        for(int i=0;i<10;i++){
            System.out.println(getRandomRedPacketBetweenMinAndMax(new BigDecimal(0.001),new BigDecimal(0.0015)));
        }
    }
}
