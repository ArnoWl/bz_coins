package com.common.utils;

import java.math.BigDecimal;

public class RedEnvelope {

    public static BigDecimal getRandomMoney(int remainSize,BigDecimal remainMoney) {
        if (remainSize == 1) {
            return remainMoney.setScale(2, BigDecimal.ROUND_DOWN);
        }
        BigDecimal random = BigDecimal.valueOf(Math.random());
        BigDecimal min = BigDecimal.valueOf(0.01);
        BigDecimal halfRemainSize = BigDecimal.valueOf(remainSize).divide(new BigDecimal(2), BigDecimal.ROUND_UP);
        BigDecimal max1 = remainMoney.divide(halfRemainSize, BigDecimal.ROUND_DOWN);
        BigDecimal minRemainAmount = min.multiply(BigDecimal.valueOf(remainSize - 1)).setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal max2 = remainMoney.subtract(minRemainAmount);
        BigDecimal max = (max1.compareTo(max2) < 0) ? max1 : max2;
        BigDecimal money = random.multiply(max).setScale(2, BigDecimal.ROUND_DOWN);
        return money.compareTo(min) < 0 ? min: money;
    }

    public static void main(String[] args) {
        System.out.println(getRandomMoney(10,new BigDecimal(50)));
    }

}