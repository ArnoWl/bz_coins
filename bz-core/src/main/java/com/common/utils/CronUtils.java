package com.common.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 时间转换为cron表达式
 */
public class CronUtils {

    /***
     * convert Date to cron, eg "0 07 10 15 1 ?"
     * @param dateTime  : 时间点
     * @return
     */
    public static String getCron(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern("ss mm HH dd MM ? yyyy"));
    }

    public static void main(String[] args) {
        System.out.println(getCron(LocalDateTime.now()));
    }
}
