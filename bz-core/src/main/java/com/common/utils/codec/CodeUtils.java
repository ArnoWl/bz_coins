package com.common.utils.codec;

import com.sun.org.apache.bcel.internal.classfile.Code;

import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 编号获取工具类
 * @author lt
 *	2017年12月26日09:57:38
 */
public class CodeUtils {

	private static Random random = new Random();

	/** 拼团订单兑换码标识 */
	private static int ORDER_CODE_FLAG = 1;
	/** 优惠券兑换码标识 */
	private static int COUPON_CODE_FLAG = 2;

	/***
	 * 生成系统内唯一的产品SKU编码
	 * @return
	 */
	public static  String maskProductSkuCode(Integer productId){
		return getLetterCode(2) + addZeroForNum(productId.toString(),4);
	}

	/**
	 * 获取随机码 -- 两位大写字母 + 数字
	 * @param length	编号字节长度
	 * @return
	 */
	public static String getDutyDasCode(int length){
	      StringBuilder val = new StringBuilder();
	      for(int i = 0; i < length; i++)   
	      {
	        if(i<2){
		        val.append(getLetterCode(1));
	        }else{
	        	val.append(getNumCode(1));
	        }
	      }
	      //字母转为大写
	      val = new StringBuilder(val.toString().toUpperCase());
	      return val.toString();
	}

	/**
	 * 位数不足左侧补位0
	 * @param str
	 * @param strLength
	 * @return
	 */
	public static String addZeroForNum(String str, int strLength) {
		int strLen = str.length();
		StringBuilder strBuilder = new StringBuilder(str);
		while (strLen < strLength) {
			strBuilder.insert(0, "0");
			strLen = strBuilder.length();
		}
		str = strBuilder.toString();
		return str;
	}
	
	/**
	 * 生成随机数
	 * @param length	长度
	 * @return
	 */
	public static String getNumCode(int length){
		return IntStream.range(0, length).mapToObj(i ->
				String.valueOf(random.nextInt(10)))
				.collect(Collectors.joining());
	}

	/**
	 * 生成随机字母
	 * @param length
	 * @return
	 */
	public static String getLetterCode(int length){
		return IntStream.range(0, length).map(i ->
				random.nextInt(2) % 2 == 0 ? 65 : 97)
				.mapToObj(choice -> String.valueOf((char) (choice + random.nextInt(26))))
				.collect(Collectors.joining());
	}

	/**
	 * 生成订单号
	 * @return
	 */
	public static synchronized String generateOrderNo() {
		int machineId = 1;//最大支持1-9个集群机器部署
		int hashCodev = UUID.randomUUID().toString().hashCode();
		if(hashCodev < 0){
			//有可能是负数
			hashCodev = -hashCodev;
		}
		return machineId + String.format("%015d", hashCodev);
	}

	/**
	 * 生成拼团订单兑换码
	 * @return
	 */
	public static String generateOrderCode(){
		String code = GenSerial.generateNewCode(ORDER_CODE_FLAG,10);
		return String.valueOf(GenSerial.convertBase32CharToNum(code));
	}

	/**
	 * 验证是否为拼团订单码
	 * @return
	 */
	public static boolean isOrderCode(String orderCode){
		orderCode = GenSerial.convertToBase32SerialCode(Long.parseLong(orderCode),9);
		Long acId = GenSerial.getFlagFromCode(orderCode,10);
		if(acId.intValue() == ORDER_CODE_FLAG){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 生成优惠券兑换码
	 * @return
	 */
	public static String generateCouponCode(){
		String code = GenSerial.generateNewCode(COUPON_CODE_FLAG,10);
		return String.valueOf(GenSerial.convertBase32CharToNum(code));
	}

	/**
	 * 验证是否为优惠券兑换码
	 * @return
	 */
	public static boolean isCouponCode(String couponCode){
		couponCode = GenSerial.convertToBase32SerialCode(Long.parseLong(couponCode),9);
		Long acId = GenSerial.getFlagFromCode(couponCode,10);
		if(acId.intValue() == COUPON_CODE_FLAG){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 随机指定范围内N个不重复的数
	 * 在初始化的无重复待选数组中随机产生一个数放入结果中，
	 * 将待选数组被随机到的数，用待选数组(len-1)下标对应的数替换
	 * 然后从len-2里随机产生下一个随机数，如此类推
	 * @param max  指定范围最大值
	 * @param min  指定范围最小值
	 * @param n  随机数个数
	 * @return int[] 随机数结果集
	 */
	public static int[] randomArray(int min,int max,int n){
		int len = max-min+1;

		if(max < min || n > len){
			return null;
		}

		//初始化给定范围的待选数组
		int[] source = new int[len];
		for (int i = min; i < min+len; i++){
			source[i-min] = i;
		}

		int[] result = new int[n];
		Random rd = new Random();
		int index = 0;
		for (int i = 0; i < result.length; i++) {
			//待选数组0到(len-2)随机一个下标
			index = Math.abs(rd.nextInt() % len--);
			//将随机到的数放入结果集
			result[i] = source[index];
			//将待选数组中被随机到的数，用待选数组(len-1)下标对应的数替换
			source[index] = source[len];
		}
		return result;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {
			System.out.println(CodeUtils.generateOrderNo());
		}
	}

	

}
