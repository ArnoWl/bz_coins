package com.common.utils.codec;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;

/**
 * AES加密工具  模式：ECB  补码方式：PKCS5Padding
 */
@Slf4j
public class AESUtils {

    private static String Algorithm = "AES";
    private static String AlgorithmProvider = "AES/ECB/PKCS5Padding"; // 算法/模式/补码方式

    /**
     * 得到转换为16进制字符串的加密字符串
     * @param src
     * @param key  必须为16个字符（1234567887654321）
     * @return
     */
    public static String getHexStr(String src, byte[] key) {
        return parseByte2HexStr(encrypt(src, key));
    }

    /**
     * 转换16进制加密字符串变为未加密字符串
     * @param src
     * @param key
     * @return
     * @throws Exception
     */
    public static String getSrcStr(String src, byte[] key) throws Exception {
        return new String(decrypt(src, key), "utf-8");
    }

    public static byte[] encrypt(String src, byte[] key){
        byte[] cipherBytes = new byte[0];
        try {
            SecretKey secretKey = new SecretKeySpec(key, Algorithm);
            Cipher cipher = Cipher.getInstance(AlgorithmProvider);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            cipherBytes = cipher.doFinal(src.getBytes(Charset.forName("utf-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cipherBytes;
    }

    public static byte[] decrypt(String src, byte[] key) throws Exception {
        SecretKey secretKey = new SecretKeySpec(key, Algorithm);
        Cipher cipher = Cipher.getInstance(AlgorithmProvider);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] hexBytes = parseHexStr2Byte(src);
        return cipher.doFinal(hexBytes);
    }

    /**
     * 将二进制转换成十六进制
     * 
     * @param buf
     * @return
     */
    private static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将十六进制转换为二进制
     * 
     * @param hexStr
     * @return
     */
    private static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1) {
            return null;
        } else {
            byte[] result = new byte[hexStr.length() / 2];
            for (int i = 0; i < hexStr.length() / 2; i++) {
                int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
                int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
                result[i] = (byte) (high * 16 + low);
            }
            return result;
        }
    }

    public static void main(String[] args){
        String da  = "{\"merchantId\":1,\"pageNo\":1,\"pageSize\":20,\"sysType\":3}";
        String params ="{\"merchantId\":1,\"pageNo\":1,\"pageSize\":20,\"sysType\":3,}";
        String str = AESUtils.getHexStr(params,"^ltYln6ReQeZb3N*".getBytes());
        System.out.println(str);
    }
}
