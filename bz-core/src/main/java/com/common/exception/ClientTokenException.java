package com.common.exception;


import com.common.constatns.CommonConstants;

public class ClientTokenException extends BaseException {
    public ClientTokenException(String message) {
        super(message, CommonConstants.E_007);
    }
}