package com.common.exception;


import com.common.constatns.CommonConstants;

public class SmsSendException extends BaseException {
    public SmsSendException(String message) {
        super(message, CommonConstants.E_006);
    }
}