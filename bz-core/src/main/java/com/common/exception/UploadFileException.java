package com.common.exception;


import com.common.constatns.CommonConstants;

public class UploadFileException extends BaseException {
    public UploadFileException(String message) {
        super(message, CommonConstants.E_006);
    }
}