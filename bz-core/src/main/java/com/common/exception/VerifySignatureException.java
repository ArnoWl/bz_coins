package com.common.exception;

import com.common.constatns.CommonConstants;

public class VerifySignatureException extends BaseException {
    public VerifySignatureException(String message) {
        super(message, CommonConstants.E_005);
    }
}