package com.common.exception;


import com.common.constatns.CommonConstants;

public class SystemBusyException extends BaseException {

    public SystemBusyException(Throwable cause) {
        super("系统繁忙，请稍后再试", cause);
        setStatus(CommonConstants.E_008);
    }


}