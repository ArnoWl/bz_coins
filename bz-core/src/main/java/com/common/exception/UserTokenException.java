package com.common.exception;


import com.common.constatns.CommonConstants;

public class UserTokenException extends BaseException {

    public UserTokenException() {
        super("用户登录信息有误，操作失败", CommonConstants.E_002);
    }

    public UserTokenException(String message) {
        super(message, CommonConstants.E_002);
    }


}