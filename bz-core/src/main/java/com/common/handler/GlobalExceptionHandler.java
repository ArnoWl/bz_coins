package com.common.handler;

import com.common.base.ResultVo;
import com.common.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * 全局异常处理
 * @author lt
 * 2020年3月23日13:59:17
 */
@ResponseBody
@ControllerAdvice
public class GlobalExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    public void handleCORS(HttpServletResponse response){
        response.setStatus(200);
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type,token");
        response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");
    }

    @ExceptionHandler(BaseException.class)
    public ResultVo baseExceptionHandler(HttpServletResponse response, BaseException ex) {
        this.handleCORS(response);
        logger.error(ex.getMessage(),ex);
        return ResultVo.result(ex.getStatus(),ex.getMessage());
    }

    @ExceptionHandler(ClientTokenException.class)
    public ResultVo clientTokenExceptionHandler(HttpServletResponse response, ClientTokenException ex) {
        this.handleCORS(response);
        logger.error(ex.getMessage(),ex);
        return ResultVo.result(ex.getStatus(),ex.getMessage());
    }

    @ExceptionHandler(UserTokenException.class)
    public ResultVo userTokenExceptionHandler(HttpServletResponse response, UserTokenException ex) {
        this.handleCORS(response);
        logger.error(ex.getMessage(),ex);
        return ResultVo.result(ex.getStatus(),ex.getMessage());
    }

    @ExceptionHandler(UploadFileException.class)
    public ResultVo uploadFileExceptionHandler(HttpServletResponse response, UploadFileException ex) {
        this.handleCORS(response);
        logger.error(ex.getMessage(),ex);
        return ResultVo.result(ex.getStatus(),ex.getMessage());
    }

    @ExceptionHandler(VerifySignatureException.class)
    public ResultVo verifySignatureException(HttpServletResponse response, VerifySignatureException ex) {
        this.handleCORS(response);
        logger.error(ex.getMessage(),ex);
        return ResultVo.result(ex.getStatus(),ex.getMessage());
    }

    @ExceptionHandler(SystemBusyException.class)
    public ResultVo systemBusyExceptionHandler(HttpServletResponse response, SystemBusyException ex) {
        this.handleCORS(response);
        logger.error(ex.getMessage(),ex);
        return ResultVo.result(ex.getStatus(),ex.getMessage());
    }

}
