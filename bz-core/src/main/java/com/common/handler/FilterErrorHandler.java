package com.common.handler;

import com.common.base.ResultVo;
import com.common.exception.BaseException;
import com.google.common.base.Strings;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * filter异常处理
 * @author lt
 * 2020年6月22日16:23:07
 */
@RestController
public class FilterErrorHandler extends BasicErrorController {

    public FilterErrorHandler() {
        super(new DefaultErrorAttributes(), new ErrorProperties());
    }

    @Override
    @RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        Map<String, Object> body = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.ALL));
        HttpStatus status = getStatus(request);
        String msg = body.get("message").toString();
        // 自定义异常基础类
        if (!Strings.isNullOrEmpty((String) body.get("exception")) && body.get("exception").equals(BaseException.class.getName())) {
            body.put("status", HttpStatus.FORBIDDEN.value());
            status = HttpStatus.FORBIDDEN;
        }
        return new ResponseEntity(ResultVo.failure(msg), status);
    }


    @Override
    public String getErrorPath() {
        return super.getErrorPath();
    }
}