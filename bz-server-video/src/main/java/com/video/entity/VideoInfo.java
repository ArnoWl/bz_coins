package com.video.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 视频信息记录表
 * </p>
 *
 * @author lt
 * @since 2020-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="VideoInfo对象", description="视频信息记录表")
public class VideoInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户uid")
    private String uid;

    @ApiModelProperty(value = "描述内容")
    private String descVal;

    @ApiModelProperty(value = "视频封面图")
    private String coverImgUrl;

    @ApiModelProperty(value = "视频url链接")
    private String videoUrl;

    @ApiModelProperty(value = "1待审核   2已审核   3已驳回")
    private Integer status;

    @ApiModelProperty(value = "发布时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "审核时间")
    private LocalDateTime handle_time;


}
