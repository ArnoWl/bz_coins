package com.video.provider;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.AdminConstants;
import com.common.constatns.CommonConstants;
import com.common.constatns.UserConstants;
import com.common.constatns.VideoConstants;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.ExtBeanUtils;
import com.common.utils.codec.CodeUtils;
import com.common.utils.lang.StringUtils;
import com.mall.feign.user.UserInfoClient;
import com.mall.feign.video.VideoClient;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.video.dto.*;
import com.mall.model.video.vo.VideoDetailVo;
import com.mall.model.video.vo.VideoPlatQueryVo;
import com.mall.model.video.vo.VideoUserQueryVo;
import com.mall.model.video.vo.VideoUserVo;
import com.video.entity.VideoInfo;
import com.video.entity.VideoThumbs;
import com.video.service.IVideoInfoService;
import com.video.service.IVideoThumbsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chenxing
 * @version 1.0
 * @date 2020-07-16
 */
@RestController
public class VideoProvider implements VideoClient {
    @Resource
    private IVideoInfoService videoInfoService;
    @Resource
    private IVideoThumbsService videoThumbsService;
    @Resource
    private UserInfoClient userInfoClient;

    /**
     * 用户查询列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResultVo queryUserVideo(VideoUserQueryDto dto) {
        Map<String, Object> map = new HashMap<>();
        map.put("desc_val", dto.getKey());
        Page<VideoUserQueryVo> page = new Page<>(dto.getPageNo(), dto.getPageSize());
        videoInfoService.queryUserVideo(page, map);
        if (StringUtils.isNotBlank(dto.getUid())) {
            page.getRecords().forEach(video -> {
                QueryWrapper<VideoThumbs> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("uid", dto.getUid());
                queryWrapper.eq("video_id", video.getId());
                VideoThumbs thumbs = videoThumbsService.getOne(queryWrapper);
                if (thumbs != null) {
                    video.setThumbs(VideoConstants.THUMB_YES);
                }
            });
        }
        return ResultVo.success(page);
    }

    /**
     * 视频详情
     *
     * @param id 视频id
     * @return
     */
    @Override
    public ResultVo getVideoDetail(Integer id, String uid) {
        VideoInfo videoInfo = videoInfoService.getById(id);
        if(videoInfo == null){
            return ResultVo.failure("视频已删除");
        }
        VideoDetailVo vo=new VideoDetailVo();
        BeanUtils.copyProperties(videoInfo,vo);
        if(StringUtils.isNotBlank(uid)){
            VideoThumbs thumbs = videoThumbsService.getByIdParamUid(id,uid);
            if(thumbs!=null){
                vo.setThumbs(VideoConstants.THUMB_YES);
            }
            QueryWrapper<VideoThumbs> queryWrapper=new QueryWrapper<>();
            queryWrapper.eq("video_id",id);
            int count = videoThumbsService.count(queryWrapper);
            vo.setThumbsNum(count);
        }
        return ResultVo.success(vo);
    }

    /**
     * 用户发布视频列表
     *
     * @param dto 视频
     * @return
     */
    @Override
    public ResultVo queryVideoRelease(VideoUserDto dto) {
        IPage<VideoInfo> pageInfo=new Page<>(dto.getPageNo(),dto.getPageSize());
        QueryWrapper<VideoInfo> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("uid",dto.getUid());
        videoInfoService.page(pageInfo,queryWrapper);
        IPage<VideoUserVo> pageInfoOut=new Page<>();
        BeanUtils.copyProperties(pageInfo,pageInfoOut);
        List<VideoUserVo> listOut = ExtBeanUtils.copyList(pageInfo.getRecords(), VideoUserVo.class);
        pageInfoOut.setRecords(listOut);
        return ResultVo.success(pageInfoOut);
    }

    /**
     * 用户视频点赞
     *
     * @param id  视频id
     * @param uid 用户id
     * @return
     */
    @Override
    public ResultVo handleThumb(String uid, Integer id) {
        VideoInfo videoInfo = videoInfoService.getById(id);
        if (videoInfo == null) {
            return ResultVo.failure("未查询到视频数据");
        }
        QueryWrapper<VideoThumbs> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uid", uid);
        queryWrapper.eq("video_id", id);
        VideoThumbs one = videoThumbsService.getOne(queryWrapper);
        if (one == null) {
            one = new VideoThumbs();
            one.setUid(uid);
            one.setVideoId(id);
            videoThumbsService.save(one);
        } else {
            videoThumbsService.remove(queryWrapper);
        }
        return ResultVo.success();
    }

    /**
     * 用户发布视频
     * @param releaseDto
     * @return
     */
    @Override
    public ResultVo handleVideoRelease(VideoReleaseDto releaseDto) {
        try {
            VideoInfo info = new VideoInfo();
            BeanUtils.copyProperties(releaseDto, info);
            videoInfoService.save(info);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("发布视频失败",e);
        }
    }

    /**
     * 平台查询视频列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResultVo queryPlatVideo(VideoPlatQueryDto dto) {
        try {
            Map<Object, Object> map = new HashMap<>();
            map.put("desc_val", dto.getKey());
            map.put("status", dto.getStatus());
            map.put("nickName", dto.getNickName());
            map.put("realName", dto.getRealName());
            map.put("phone", dto.getPhone());
            if(StringUtils.isNotBlank(dto.getQueryTime())){
                String[] split = dto.getQueryTime().split(",");
                if(split.length==2){
                    map.put("startTime", split[0]);
                    map.put("endTime", split[1]);
                }
            }
            if(StringUtils.isNotBlank(dto.getHandleTime())){
                String[] split = dto.getHandleTime().split(",");
                if(split.length==2){
                    map.put("startHandleTime", split[0]);
                    map.put("endHandleTime", split[1]);
                }
            }
            map.put("status", dto.getQueryTime());
            Page<VideoPlatQueryVo> page = new Page<>(dto.getPageNo(), dto.getPageSize());
            videoInfoService.queryPlatVideo(page, map);
            return ResultVo.success(page);
        }catch (Exception e){
            e.printStackTrace();
            throw new BaseException("平台视频查询失败",e);
        }
    }

    /**
     * 平台审核视频
     *
     * @return
     */
    @Override
    @Transactional
    public ResultVo handleExamine(VideoExamineDto examineDto) {
        VideoInfo videoInfo = videoInfoService.getById(examineDto.getId());
        if (videoInfo == null) {
            return ResultVo.failure("未查询到视频数据");
        }
        if (!videoInfo.getStatus().equals(VideoConstants.STATUS_STAY)) {
            return ResultVo.failure("该视频已审核，请忽重复处理");
        }
        try {
            videoInfo.setStatus(examineDto.getStatus());
            videoInfoService.updateById(videoInfo);
            if(examineDto.getStatus().equals(VideoConstants.STATUS_YES)) {
                if(examineDto.getQty().compareTo(BigDecimal.ZERO)>0) {
                    String payCode = CodeUtils.generateOrderNo();
                    WalletLogDto dto = new WalletLogDto();
                    dto.setCost(examineDto.getQty());
                    dto.setUid(videoInfo.getUid());
                    dto.setCoinSymbol(examineDto.getCoinSymbol());
                    dto.setPayCode(payCode);
                    dto.setRemark("视频审核赠送");
                    dto.setInOut(UserConstants.WALLET_IN);
                    dto.setTypeId(WalletLogTypeEnums.VIDEO_EXAMINE.getTypeId());
                    ResultVo resultVo = userInfoClient.handleUserWalletMoney(dto);
                    if (!resultVo.isSuccess()) {
                        throw new BaseException("审核失败");
                    }
                }
            }
        }catch (Exception e){
            throw new BaseException("审核视频失败");
        }
        return ResultVo.success();
    }

    /**
     * 平台删除视频
     *
     * @param id 视频id
     * @return
     */
    @Override
    public ResultVo delVideo(Integer id) {
        VideoInfo videoInfo = videoInfoService.getById(id);
        if (videoInfo == null) {
            return ResultVo.failure("视频已删除,请重新选择");
        }
        videoInfo.setStatus(0);
        videoInfoService.updateById(videoInfo);
        return ResultVo.success();
    }

    @Override
    public ResultVo updateVideo(VideoUpdateDto updateDto) {
        VideoInfo videoInfo = videoInfoService.getById(updateDto.getId());
        if(videoInfo==null){
            return ResultVo.failure("视频已删除,请重新选择");
        }
        videoInfo.setDescVal(updateDto.getDescVal());
        videoInfoService.updateById(videoInfo);
        return ResultVo.success();
    }


}
