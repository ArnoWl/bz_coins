package com.video.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.video.vo.VideoPlatQueryVo;
import com.mall.model.video.vo.VideoUserQueryVo;
import com.video.entity.VideoInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 视频信息记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-16
 */
public interface IVideoInfoService extends IService<VideoInfo> {

    Page<VideoUserQueryVo> queryUserVideo(Page<VideoUserQueryVo> page, Map<String, Object> map);

    Page<VideoPlatQueryVo> queryPlatVideo(Page<VideoPlatQueryVo> page, Map<Object, Object> map);
}
