package com.video.service;

import com.video.entity.VideoThumbs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 视频点赞记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-16
 */
public interface IVideoThumbsService extends IService<VideoThumbs> {

    VideoThumbs getByIdParamUid(Integer id,String uid);
}
