package com.video.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.video.entity.VideoThumbs;
import com.video.mapper.VideoThumbsMapper;
import com.video.service.IVideoThumbsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 视频点赞记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-16
 */
@Service
public class VideoThumbsServiceImpl extends ServiceImpl<VideoThumbsMapper, VideoThumbs> implements IVideoThumbsService {

    @Override
    public VideoThumbs getByIdParamUid(Integer id, String uid) {
        QueryWrapper<VideoThumbs> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uid", uid);
        queryWrapper.eq("video_id", id);
        return this.getOne(queryWrapper);
    }
}
