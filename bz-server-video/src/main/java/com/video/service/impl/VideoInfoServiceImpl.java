package com.video.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.video.vo.VideoPlatQueryVo;
import com.mall.model.video.vo.VideoUserQueryVo;
import com.video.entity.VideoInfo;
import com.video.mapper.VideoInfoMapper;
import com.video.service.IVideoInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 视频信息记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-16
 */
@Service
public class VideoInfoServiceImpl extends ServiceImpl<VideoInfoMapper, VideoInfo> implements IVideoInfoService {

    @Override
    public Page<VideoUserQueryVo> queryUserVideo(Page<VideoUserQueryVo> page, Map<String, Object> map) {
        return baseMapper.queryUserVideo(page,map);
    }

    @Override
    public Page<VideoPlatQueryVo> queryPlatVideo(Page<VideoPlatQueryVo> page, Map<Object, Object> map) {
        return baseMapper.queryPlatVideo(page,map);
    }
}
