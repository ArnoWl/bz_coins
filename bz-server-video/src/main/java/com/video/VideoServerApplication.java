package com.video;

import com.auth.client.EnableAuthClient;
import com.mall.aspect.WebLogAspect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

/**
 * @author tps
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.mall.feign")
@EnableAuthClient
@Import({WebLogAspect.class})
public class VideoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideoServerApplication.class, args);
	}

}