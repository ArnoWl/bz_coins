package com.video.mapper;

import com.video.entity.VideoThumbs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 视频点赞记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-16
 */
public interface VideoThumbsMapper extends BaseMapper<VideoThumbs> {

}
