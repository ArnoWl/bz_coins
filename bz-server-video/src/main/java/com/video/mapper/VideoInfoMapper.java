package com.video.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.video.vo.VideoPlatQueryVo;
import com.mall.model.video.vo.VideoUserQueryVo;
import com.video.entity.VideoInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 视频信息记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-16
 */
public interface VideoInfoMapper extends BaseMapper<VideoInfo> {

    Page<VideoUserQueryVo> queryUserVideo(Page<VideoUserQueryVo> page, @Param("param") Map<String, Object> map);

    Page<VideoPlatQueryVo> queryPlatVideo(Page<VideoPlatQueryVo> page, @Param("param") Map<Object, Object> map);
}
