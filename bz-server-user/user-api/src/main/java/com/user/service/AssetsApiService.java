package com.user.service;

import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.model.wallet.dto.WalletCashDto;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.user.model.in.WalletCashIn;
import com.user.model.in.WalletLogsIn;

/**
 * 用户信息Api业务接口类
 * @Author: lt
 * @Date: 2020/5/23 13:33
 */
public interface AssetsApiService {

    /**
     * 查询钱包记录
     * @param walletLogsIn
     * @return
     */
    ResultVo queryWalletLogs(WalletLogsIn walletLogsIn);

    /**
     * 提币数据展示
     * @param coinSymbol
     * @return
     */
    ResultVo walleCashData(String coinSymbol);

    /**
     * 用户提币
     * @param walletCashIn
     * @return
     */
    ResultVo handleWalletCash(WalletCashIn walletCashIn);

    /**
     * 查询充值记录
     * @param dto
     * @return
     */
    ResultVo queryWalletRecharge(WalletRechargeListDto dto);

    /**
     * 查询提现记录
     * @param dto
     * @return
     */
    ResultVo queryWalletCash(WalletCashListDto dto);


}
