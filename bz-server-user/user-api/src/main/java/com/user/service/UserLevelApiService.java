package com.user.service;

import com.common.base.ResultVo;

/**
 * <p>
 *  用户等级信息Api业务接口类
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
public interface UserLevelApiService {

    ResultVo queryUserupgradeRule();
}
