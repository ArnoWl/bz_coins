package com.user.service;

import com.common.base.ResultVo;
import com.user.model.in.CreateOrderIn;
import com.mall.model.market.vo.GetBuyOrderListDto;

/**
 * 买涨/跌订单业务接口类
 * @Author: lt
 * @Date: 2020/7/25 16:12
 */
public interface OrderApiService {

    /**
     * 创建订单
     * @param orderIn
     * @return
     */
    ResultVo createOrder(CreateOrderIn orderIn);

    /**
     * 获取BZW销毁数量
     * @return
     */
    ResultVo getBZWDestoryNum();

    /**
     *
     * @return
     */
    ResultVo getUSDTData();



}
