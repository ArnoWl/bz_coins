package com.user.service;

import com.common.base.ResultVo;
import com.user.model.in.MessageIn;
import com.user.model.in.SaveMessageIn;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
public interface MessageApiService {


    /**
     * 获取用户站内信列表
     * @return
     */
    ResultVo queryUserMessage(MessageIn paramIn);

    /**
     * 已读站内信
     * @param id
     * @return
     */
    ResultVo readUserMessage(Integer id);

    /**
     * 已读所有站内信
     * @return
     */
    ResultVo readAllUserMessage();

    /**
     * 提问
     * @param paramIn
     * @return
     */
    ResultVo saveMessage(SaveMessageIn paramIn);
}
