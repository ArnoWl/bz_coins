package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.constatns.UserConstants;
import com.common.context.UserContext;
import com.common.exception.BaseException;
import com.common.utils.ExtBeanUtils;
import com.mall.model.user.vo.UserMessageQueryVo;
import com.user.entity.Info;
import com.user.entity.Message;
import com.user.model.in.MessageIn;
import com.user.model.in.SaveMessageIn;
import com.user.model.out.MessageOut;
import com.user.service.IInfoService;
import com.user.service.IMessageService;
import com.user.service.MessageApiService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@Service
public class MessageApiServiceImpl implements MessageApiService {

    @Resource
    private IMessageService iMessageService;
    @Resource
    private IInfoService iInfoService;

    @Override
    public ResultVo queryUserMessage(MessageIn paramIn) {
        try {
            Page<Message> page = new Page<>(paramIn.getPageNo(), paramIn.getPageSize());
            QueryWrapper<Message> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", UserContext.getUserID()).eq("type",paramIn.getType()).orderByDesc("id");
            iMessageService.page(page, wrapper);

            //组装出参
            IPage<UserMessageQueryVo> outIPage = new Page<>();
            BeanUtils.copyProperties(page, outIPage);
            if (!CollectionUtils.isEmpty(outIPage.getRecords())) {
                outIPage.setRecords(ExtBeanUtils.copyList(page.getRecords(), UserMessageQueryVo.class));
            }
            return ResultVo.success(outIPage);
        } catch (Exception e) {
            throw new BaseException("获取系统消息列表失败", e);
        }
    }

    @Override
    public ResultVo readUserMessage(Integer id) {
        try {
            Message message = iMessageService.getById(id);
            if (message == null) {
                return ResultVo.failure("未找到该记录");
            }
            if (!StringUtils.equals(message.getUid(), UserContext.getUserID())) {
                return ResultVo.failure("无权限操作");
            }
            //只能将平台回复的内容标记已读
            if(!message.getType().equals(1)){
                message.setReadFlag(CommonConstants.NO);
                iMessageService.updateById(message);
            }

            MessageOut out = new MessageOut();
            BeanUtils.copyProperties(message, out);
            return ResultVo.success(out);
        } catch (Exception e) {
            throw new BaseException("设置为已读消息失败", e);
        }
    }

    @Override
    public ResultVo readAllUserMessage() {
        try {
            //将系统回复的都标记已读
            QueryWrapper<Message> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", UserContext.getUserID()).eq("type",2);

            Message message = new Message();
            message.setReadFlag(CommonConstants.NO);

            iMessageService.update(message, wrapper);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("设置所有为已读消息失败", e);
        }
    }

    @Override
    public ResultVo saveMessage(SaveMessageIn paramIn) {
        String uid=UserContext.getUserID();
        Info info= iInfoService.getById(uid);
        if(info==null){
            return ResultVo.failure("用户信息不存在");
        }
        if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
            return ResultVo.failure("账户已被冻结");
        }
        Message message=new Message();
        BeanUtils.copyProperties(paramIn,message);
        message.setUid(uid);
        message.setType(1);
        iMessageService.save(message);
        return ResultVo.success();
    }
}
