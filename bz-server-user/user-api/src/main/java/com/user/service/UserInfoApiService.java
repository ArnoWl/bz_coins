package com.user.service;

import com.common.base.ResultVo;
import com.mall.base.AppBaseParamIn;
import com.user.model.in.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 用户信息Api业务接口类
 *
 * @Author: lt
 * @Date: 2020/5/23 13:33
 */
public interface UserInfoApiService {

    /**
     * 获取用户个人信息
     *
     * @param paramIn
     * @return
     */
    ResultVo getUserInfo(AppBaseParamIn paramIn);

    /**
     * 修改用户基础信息
     *
     * @param paramIn
     * @return
     */
    ResultVo updateUserInfo(UpdateUserInfoIn paramIn);

    /**
     * 实名认证 -- 个人认证
     *
     * @param
     * @return
     */
    ResultVo verified(MultipartFile frontImage,MultipartFile reverseImage);

    /**
     * 设置支付密码
     *
     * @param userPayPassWordIn
     * @return
     */
    ResultVo setPayPassword(UserPayPassWordIn userPayPassWordIn);


}
