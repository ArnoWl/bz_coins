package com.user.service;

import com.common.base.ResultVo;
import com.mall.model.video.dto.VideoReleaseDto;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/27
 */
public interface VideoApiService {

    ResultVo handleVideoRelease(VideoReleaseDto dto);
}
