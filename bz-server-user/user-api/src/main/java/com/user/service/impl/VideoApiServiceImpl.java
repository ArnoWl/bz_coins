package com.user.service.impl;

import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.constatns.UserConstants;
import com.mall.feign.video.VideoClient;
import com.mall.model.video.dto.VideoReleaseDto;
import com.user.entity.Info;
import com.user.service.IInfoService;
import com.user.service.VideoApiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/27
 */
@Service
public class VideoApiServiceImpl implements VideoApiService {

    @Resource
    private IInfoService iInfoService;

    @Resource
    private VideoClient videoClient;

    @Override
    public ResultVo handleVideoRelease(VideoReleaseDto dto) {
        Info info = iInfoService.getById(dto.getUid());
        if (info == null) {
            return ResultVo.failure("用户信息丢失");
        }
        if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
            return ResultVo.failure("账户已被冻结");
        }
        if (info.getLevelId().compareTo(CommonConstants.LEVEL_V1) <= 0) {
            return ResultVo.failure("用户等级大于VIP1才能发布视频");
        }
        return videoClient.handleVideoRelease(dto);

    }
}
