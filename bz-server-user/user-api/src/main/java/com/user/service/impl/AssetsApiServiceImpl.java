package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.context.UserContext;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.AgreeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.ExtBeanUtils;
import com.common.utils.codec.CodeUtils;
import com.common.utils.lang.NumberUtils;
import com.mall.feign.admin.BaseClient;
import com.mall.feign.wallet.WalletClient;
import com.mall.model.admin.vo.SysCoinsQueryVo;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.wallet.dto.WalletCashDto;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.mall.model.wallet.vo.RechargeLogVo;
import com.user.entity.*;
import com.user.model.in.WalletCashIn;
import com.user.model.in.WalletLogsIn;
import com.user.model.out.CashLogOut;
import com.user.model.out.RechargeLogOut;
import com.user.model.out.WalleCashDataOut;
import com.user.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@Service
public class AssetsApiServiceImpl implements AssetsApiService {

    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private IWalletLogsService iWalletLogsService;
    @Resource
    private BaseClient baseClient;
    @Resource
    private WalletClient walletClient;
    @Resource
    private TransactionApiService transactionApiService;
    @Resource
    private IWalletFrozenLogsService iWalletFrozenLogsService;
    @Resource
    private IInfoService iInfoService;
    @Resource
    private IRewardLogsService iRewardLogsService;

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public ResultVo queryWalletLogs(WalletLogsIn walletLogsIn) {
        String beginTime="";
        String endTime="";
        if(StringUtils.isNotEmpty(walletLogsIn.getTimes())){
            beginTime=walletLogsIn.getTimes().split("~")[0];
            endTime=walletLogsIn.getTimes().split("~")[1];
        }
        if(1 == walletLogsIn.getType()){
            QueryWrapper<WalletLogs> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", UserContext.getUserID()).eq("coin_symbol", walletLogsIn.getCoinSymbol());
            if(walletLogsIn.getTypeId()!=null){
                wrapper.eq("type_id", walletLogsIn.getTypeId());
            }
            if(StringUtils.isNotEmpty(walletLogsIn.getTimes())){
                wrapper.apply("DATE_FORMAT(create_time,'%Y%m%d')>=DATE_FORMAT({0},'%Y%m%d') ",beginTime)
                        .apply("DATE_FORMAT(create_time,'%Y%m%d')<=DATE_FORMAT({0},'%Y%m%d')",endTime);
            }
            wrapper.orderByDesc("create_time");
            Page<WalletLogs> page = new Page<>(walletLogsIn.getPageNo(), walletLogsIn.getPageSize());
            iWalletLogsService.page(page, wrapper);
            return ResultVo.success(page);
        }else{
            if(SysCoinsTypeEnums.USDT.getName().equals(walletLogsIn.getCoinSymbol())){
                //usdt收益明细表
                QueryWrapper<RewardLogs> wrapper = new QueryWrapper<>();
                wrapper.eq("uid", UserContext.getUserID());
                if(walletLogsIn.getTypeId()!=null){
                    wrapper.eq("type_id", walletLogsIn.getTypeId());
                }
                if(StringUtils.isNotEmpty(walletLogsIn.getTimes())){
                    wrapper.apply("DATE_FORMAT(create_time,'%Y%m%d')>=DATE_FORMAT({0},'%Y%m%d') ",beginTime)
                            .apply("DATE_FORMAT(create_time,'%Y%m%d')<=DATE_FORMAT({0},'%Y%m%d')",endTime);
                }
                wrapper.orderByDesc("create_time");
                Page<RewardLogs> page = new Page<>(walletLogsIn.getPageNo(), walletLogsIn.getPageSize());
                iRewardLogsService.page(page, wrapper);
                return ResultVo.success(page);
            }else{
                //其他币种的冻结明细表
                QueryWrapper<WalletFrozenLogs> wrapper = new QueryWrapper<>();
                wrapper.eq("uid", UserContext.getUserID()).eq("coin_symbol", walletLogsIn.getCoinSymbol());
                if(walletLogsIn.getTypeId()!=null){
                    wrapper.eq("type_id", walletLogsIn.getTypeId());
                }
                if(StringUtils.isNotEmpty(walletLogsIn.getTimes())){
                    wrapper.apply("DATE_FORMAT(create_time,'%Y%m%d')>=DATE_FORMAT({0},'%Y%m%d') ",beginTime)
                            .apply("DATE_FORMAT(create_time,'%Y%m%d')<=DATE_FORMAT({0},'%Y%m%d')",endTime);
                }
                wrapper.orderByDesc("create_time");
                Page<WalletFrozenLogs> page = new Page<>(walletLogsIn.getPageNo(), walletLogsIn.getPageSize());
                iWalletFrozenLogsService.page(page, wrapper);
                return ResultVo.success(page);
            }
        }
    }

    @Override
    public ResultVo walleCashData(String coinSymbol) {
        // 获取钱包信息
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", UserContext.getUserID()).eq("coin_symbol", coinSymbol);
        WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
        if (walletInfo == null) {
            throw new BaseException("钱包信息不存在");
        }

        List<SysCoinsQueryVo> sysCoinsList = baseClient.querySysCoin();
        WalleCashDataOut cashDataOut = new WalleCashDataOut();
        if (!CollectionUtils.isEmpty(sysCoinsList)) {
            for (SysCoinsQueryVo item : sysCoinsList) {
                if (StringUtils.equals(coinSymbol, item.getName())) {
                    cashDataOut.setTax(item.getTax());
                    cashDataOut.setTaxType(item.getTaxType());
                    break;
                }
            }
        }
        BigDecimal minTax= NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.WITHDREAW_MIN_AMOUNT));
        BigDecimal maxTax= NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.WITHDREAW_MAX_AMOUNT));

        cashDataOut.setMinTax(minTax);
        cashDataOut.setMaxTax(maxTax);
        cashDataOut.setBalanceMoney(walletInfo.getWalletBalance());
        return ResultVo.success(cashDataOut);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo handleWalletCash(WalletCashIn walletCashIn) {

        if (walletCashIn.getMoney().compareTo(BigDecimal.ZERO) <= 0) {
            return ResultVo.failure("提币数量不得小于0");
        }
        BigDecimal minTax= NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.WITHDREAW_MIN_AMOUNT));
        BigDecimal maxTax= NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.WITHDREAW_MAX_AMOUNT));
        if(walletCashIn.getMoney().compareTo(minTax)<0 || walletCashIn.getMoney().compareTo(maxTax)>0){
            return ResultVo.failure("提币数量范围:"+minTax+"~"+maxTax);
        }

        ResultVo resultVo = transactionApiService.verifyPayPassWord(walletCashIn.getPayPassWord());
        if (!resultVo.isSuccess()) {
            return resultVo;
        }
        String uid=UserContext.getUserID();
        Info info=iInfoService.getById(uid);
        if(info==null){
            return ResultVo.failure("会员不存在");
        }
        if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
            return ResultVo.failure("账户已被冻结");
        }
        if(info.getIsVerified().equals(UserConstants.USER_STATUS_0)){
            return ResultVo.failure("请先前往实名认证");
        }
        WalletCashDto walletCashDto = new WalletCashDto();
        BeanUtils.copyProperties(walletCashIn, walletCashDto);
        walletCashDto.setUid(uid);
        walletCashDto.setSysFlag(UserConstants.USER_STATUS_1);

        //判断接收方的钱包地址是不是系统内部的
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("wallet_addr", walletCashIn.getWalletReceivedAddr());
        WalletInfo walletInfoERC20 = iWalletInfoService.getOne(wrapper);
        if (walletInfoERC20 != null) {
            walletCashDto.setSysFlag(UserConstants.USER_STATUS_0);
        }
        if (SysCoinsTypeEnums.USDT.getName().equals(walletCashIn.getCoinSymbol())) {
            if (AgreeEnums.OMNI.name().equals(walletCashIn.getAgreeType())) {
                wrapper.clear();
                wrapper.eq("wallet_omin_addr", walletCashIn.getWalletReceivedAddr());
                WalletInfo walletInfoOMNI = iWalletInfoService.getOne(wrapper);
                if (walletInfoOMNI != null) {
                    walletCashDto.setSysFlag(UserConstants.USER_STATUS_0);
                }
            }
        }
        //查询当前币种用户的钱包信息 获取到合约地址
        wrapper.clear();
        wrapper.eq("uid", uid).eq("coin_symbol",walletCashIn.getCoinSymbol());
        WalletInfo myWallet = iWalletInfoService.getOne(wrapper);
        walletCashDto.setContractAddr(myWallet.getContractAddr());
        walletCashDto.setDecimal(myWallet.getWalletDeclimal());
//        walletCashDto.setWalletSendAddr(walletAddr.getWalletAddr());
        walletCashDto.setWalletReceivedAddr(walletCashIn.getWalletReceivedAddr());
        //计算手续费、获取币种精度
        List<SysCoinsQueryVo> sysCoinsList = baseClient.querySysCoin();
        if (!CollectionUtils.isEmpty(sysCoinsList)) {
            for (SysCoinsQueryVo item : sysCoinsList) {
                if (StringUtils.equals(walletCashIn.getCoinSymbol(), item.getName())) {
                    if (CommonConstants.TAX_TYPE_1.equals(item.getTaxType())) {
                        walletCashDto.setTax(item.getTax());
                        walletCashDto.setDecimal(item.getAccuracy());
                    } else if (CommonConstants.TAX_TYPE_2.equals(item.getTaxType())) {
                        walletCashDto.setTax(CalcUtils.mul(item.getTax(), walletCashIn.getMoney(), 4));
                        walletCashDto.setDecimal(item.getAccuracy());
                    }
                    break;
                }
            }
        }
        resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(walletCashDto.getUid(), SysCoinsTypeEnums.USDT.getName(), CodeUtils.generateOrderNo(),
                WalletLogTypeEnums.CASH.getTypeId(), UserConstants.WALLET_OUT, walletCashDto.getMoney(), "提现申请"));
        if (!resultVo.isSuccess()) {
            throw new BaseException(resultVo.getErrorMessage());
        }
        resultVo = walletClient.handleApplyCoinCash(walletCashDto);
        if (!resultVo.isSuccess()) {
            throw new BaseException(resultVo.getErrorMessage());
        }
        return resultVo;
    }

    @Override
    public ResultVo queryWalletRecharge(WalletRechargeListDto dto) {
        Page<RechargeLogVo> page = walletClient.queryWalletRecharge(dto);
        // 组装出参
        IPage<RechargeLogOut> outIPage = new Page<>();
        BeanUtils.copyProperties(page, outIPage);
        if (!CollectionUtils.isEmpty(outIPage.getRecords())) {
            outIPage.setRecords(ExtBeanUtils.copyList(page.getRecords(), RechargeLogOut.class));
        }
        return ResultVo.success(outIPage);
    }

    @Override
    public ResultVo queryWalletCash(WalletCashListDto dto) {
        Page<CashLogVo> page = walletClient.queryWalletCash(dto);

        // 组装出参
        IPage<CashLogOut> outIPage = new Page<>();
        BeanUtils.copyProperties(page, outIPage);
        if (!CollectionUtils.isEmpty(outIPage.getRecords())) {
            outIPage.setRecords(ExtBeanUtils.copyList(page.getRecords(), CashLogOut.class));
        }
        return ResultVo.success(outIPage);
    }

}
