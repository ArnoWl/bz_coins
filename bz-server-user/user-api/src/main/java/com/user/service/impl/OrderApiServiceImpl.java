package com.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.common.base.ResultVo;
import com.common.config.RedisLock;
import com.common.config.RedisUtils;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.context.UserContext;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.CronUtils;
import com.common.utils.codec.CodeUtils;
import com.common.utils.lang.NumberUtils;
import com.mall.feign.admin.BaseClient;
import com.mall.model.admin.vo.CoinSpreadVo;
import com.mall.model.admin.vo.IndemnityRuleVo;
import com.mall.model.admin.vo.OrderRuleVo;
import com.mall.model.market.dto.KlineInfo;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.third.dto.AddScheduleJobDto;
import com.mall.model.user.dto.WalletLogDto;
import com.user.entity.Info;
import com.user.entity.Invitation;
import com.user.entity.OrderInfo;
import com.user.entity.WalletInfo;
import com.user.model.OrderRewardDto;
import com.user.model.in.CreateOrderIn;
import com.user.model.out.BZWDestoryOut;
import com.user.model.out.USDTDataOut;
import com.user.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.common.constatns.CommonConstants.E_000;
import static com.common.constatns.ThirdConstants.*;
import static com.common.constatns.UserConstants.*;

/**
 * @Author: lt
 * @Date: 2020/7/25 16:30
 */
@Service
public class OrderApiServiceImpl implements OrderApiService {

    @Resource
    private RedisLock redisLock;
    @Resource
    private IOrderInfoService iOrderInfoService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private IInvitationService iInvitationService;
    @Resource
    private AmqpTemplate amqpTemplate;
    @Resource
    private IInfoService iInfoService;
    @Resource
    private BaseClient baseClient;
    @Resource
    private RedisUtils redisUtils;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo createOrder(CreateOrderIn orderIn) {
        String uid = UserContext.getUserID();
        String lockKey = RedisKeyConstants.LOCK_CREATE_BUY_ORDER + uid;
        String value = redisLock.getDefaultOutTime();
        if (!redisLock.lock(lockKey, value)) {
            return ResultVo.failure("订单提交频繁,请稍后重试");
        }
        try {
            // 校验下单用户信息
            Info info = iInfoService.getById(UserContext.getUserID());
            ResultVo resultVo = this.handleVerifyOrderUser(info,orderIn);
            if(!resultVo.isSuccess()){
                return resultVo;
            }
            // 创建买入订单信息
            OrderInfo orderInfo = new OrderInfo();
            BeanUtils.copyProperties(orderIn, orderInfo);
            orderInfo.setUid(uid);
            orderInfo.setOrderCode(CodeUtils.generateOrderNo());
            orderInfo.setCreateTime(LocalDateTime.now());
            orderInfo.setOverTime(orderInfo.getCreateTime().plusSeconds(60));
            orderInfo.setStatus(ORDER_STATUS_1);
            // 包赔场订单
            if (ORDER_TYPE_2.equals(orderInfo.getOrderType())) {
                resultVo = this.handleCoverOrder(orderInfo);
                if(!resultVo.isSuccess()){
                    return resultVo;
                }
            }
            BigDecimal max_trans_money = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.MAX_TRANS_MONEY));
            if(orderIn.getMoney().compareTo(max_trans_money)>0){
                return ResultVo.failure("最大下单金额:"+max_trans_money);
            }

            // 从缓存中读取当前市场买入价
            String latestMarketStr = redisTemplate.opsForValue().get(RedisKeyConstants.LATEST_COIN_MARKET_KLINE + orderIn.getCoinSymbol());
            if(StringUtils.isBlank(latestMarketStr)){
                return ResultVo.failure("最新行情信息丢失，下单失败");
            }
            //计算点差
            CoinSpreadVo coinSpreadVo = baseClient.getSpread(orderIn.getCoinSymbol());
            if(coinSpreadVo==null){
                return ResultVo.failure("下单币种信息不存在");
            }
            KlineInfo newKline = JSONObject.toJavaObject(JSON.parseObject(latestMarketStr), KlineInfo.class);
            BigDecimal price = newKline.getClosePrice();
            BigDecimal randomScale = CalcUtils.getRandomRedPacketBetweenMinAndMax(coinSpreadVo.getSpreadMin(),coinSpreadVo.getSpreadMax());
            BigDecimal spreadMoney = CalcUtils.mul(price,randomScale,4);
            //如果是买涨
            if(BUY_TYPE_1.equals(orderIn.getBuyType())){
                price = CalcUtils.add(price,spreadMoney,4);
            }
            //如果是买跌
            if(BUY_TYPE_2.equals(orderIn.getBuyType())){
                price = CalcUtils.sub(price,spreadMoney,4);
            }
            // 按照实时价买入
            orderInfo.setOpenPrice(price);

            //设置下单价
            redisUtils.setOrderPrice(orderIn.getCoinSymbol(),orderInfo.getOpenPrice());
            // 扣除用户钱包
            resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(uid, SysCoinsTypeEnums.USDT.getName(), orderInfo.getOrderCode(),
                    WalletLogTypeEnums.BUY_UP_AND_DOWN_ORDER.getTypeId(), WALLET_OUT, orderInfo.getMoney(), "币指交易下单"));
            if (!resultVo.isSuccess()) {
                return resultVo;
            }
            //赠送推荐人 BZW
            if(!StringUtils.isEmpty(info.getInvitationCode())){
                //当前用户是下第一单
                QueryWrapper<OrderInfo> orderInfoQueryWrapper=new QueryWrapper<>();
                orderInfoQueryWrapper.eq("uid",info.getUid());
                int count=iOrderInfoService.count(orderInfoQueryWrapper);
                if(count<1){
                    //如果当前用户还没有下单  那么给直推人赠送BZW
                    QueryWrapper<Info> infoQueryWrapper=new QueryWrapper<>();
                    infoQueryWrapper.eq("code",info.getInvitationCode());
                    Info parent=iInfoService.getOne(infoQueryWrapper);

                    BigDecimal bzw = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.SYS_KEY + RedisKeyConstants.PUSH_SEND_BZW));
                    if (bzw.compareTo(BigDecimal.ZERO)>0){
                        resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(parent.getUid(), SysCoinsTypeEnums.BZW.getName(), orderInfo.getOrderCode(),
                                WalletLogTypeEnums.PUSH_CUSTOMER_ORDER.getTypeId(), UserConstants.WALLET_IN,bzw, "直推会员奖励"));
                        if (!resultVo.isSuccess()) {
                            return resultVo;
                        }
                    }
                }
            }
            // 扣除冻结金额明细表
            resultVo = this.deductUserOrderBzw(orderInfo);
            if(!resultVo.isSuccess()){
                throw new BaseException(resultVo.getErrorMessage());
            }
            if (!iOrderInfoService.save(orderInfo)) {
                throw new BaseException("执行添加买入订单信息失败");
            }
            //累积个人业绩
            QueryWrapper<Invitation> invitationQueryWrapper=new QueryWrapper<>();
            invitationQueryWrapper.eq("be_invited_uid",uid);
            Invitation invitation = iInvitationService.getOne(invitationQueryWrapper);
            invitation.setMoney(CalcUtils.add(invitation.getMoney(),orderIn.getMoney(),6));
            if(!iInvitationService.updateById(invitation)){
                throw new BaseException("个人业绩累积失败");
            }
            // 异步结算
            this.handleSettlement(orderInfo);
            //返回下单结果
            BuyOrderListVo buyOrderListVo=new BuyOrderListVo();
            BeanUtils.copyProperties(orderInfo,buyOrderListVo);
            return ResultVo.result(E_000,"下单成功",buyOrderListVo);
        } catch (Exception e) {
            throw new BaseException(e.getMessage(), e);
        } finally {
            redisLock.unlock(lockKey, value);
        }
    }

    /**
     * 处理校验下单
     * @param info  下单用户
     * @return
     */
    private ResultVo handleVerifyOrderUser(Info info,CreateOrderIn orderInfo){
        if(info == null){
            return ResultVo.failure("用户信息丢失，下单失败");
        }
        if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
            return ResultVo.failure("账户已被冻结");
        }
        if (USER_STATUS_0.equals(info.getIsVerified())) {
            return ResultVo.failure("请先进行实名认证");
        }
        // 查询用户是否有在进行中的订单
        QueryWrapper<OrderInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", info.getUid()).eq("coin_symbol",orderInfo.getCoinSymbol()).eq("status", ORDER_STATUS_1);
        int rows = iOrderInfoService.count(wrapper);
        if (rows > 0) {
            return ResultVo.failure("下单失败，已存在进行中的订单");
        }
        return ResultVo.success();
    }

    /**
     * 处理包赔场下单
     * @param order 订单
     * @return
     */
    private ResultVo handleCoverOrder(OrderInfo order){
        OrderRuleVo orderRuleVo = baseClient.getNewlast();
        if (orderRuleVo == null) {
            return ResultVo.failure("包赔场还未开始,请进入其他场次下单");
        }
        order.setOrderSessions(orderRuleVo.getId());
        //如果是当天第一次下包赔场才设置包赔场金额  整天的包赔场都按照这个金额来判断他的权限
        int count=iOrderInfoService.getNowBudgetNum(order.getUid());

        //获取包赔场当天的金额 如果没有设置当前额
        String budget=redisTemplate.opsForValue().get(RedisKeyConstants.USDT_BUDGET_MONEY + order.getUid());
        if(count<1 || StringUtils.isEmpty(budget)){
            //设置一个用户的包赔场当日金额
            QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", order.getUid()).eq("coin_symbol", SysCoinsTypeEnums.USDT.getName());
            WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
            budget=walletInfo.getWalletBalance().toPlainString();
            redisTemplate.opsForValue().set(RedisKeyConstants.USDT_BUDGET_MONEY + order.getUid(),budget);
        }
        BigDecimal budgetMoney=NumberUtils.createBigDecimal(budget);

        IndemnityRuleVo indemnityRuleVo = baseClient.getRuleByUSDT(budgetMoney);
        if (indemnityRuleVo == null) {
            return ResultVo.failure("钱包金额不满足包赔场下单规则");
        }
        if (order.getMoney().compareTo(indemnityRuleVo.getIndemnityMaxMoney()) > 0) {
            return ResultVo.failure("包赔场下单金额不得超过" + indemnityRuleVo.getIndemnityMaxMoney()+"USDT");
        }

        int nowIndemnityNum = iOrderInfoService.getNowIndemnityNum(order);
        if (nowIndemnityNum > indemnityRuleVo.getIndemnityNum() - 1) {
            return ResultVo.failure("当日本次包赔场已达到下单限制次数");
        }
        return ResultVo.success();
    }

    /**
     * 扣除用户下单BZW
     * @param order 订单
     * @return
     */
    private ResultVo deductUserOrderBzw(OrderInfo order){
        order.setBzwNum(NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.ORDER_BZW)));
        if(order.getBzwNum() == null || order.getBzwNum().compareTo(BigDecimal.ZERO) <= 0){
            return ResultVo.success();
        }
        // 先扣除冻结BZW  在扣除可用BZW
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", order.getUid()).eq("coin_symbol", SysCoinsTypeEnums.BZW.getName());
        WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
        if(walletInfo == null){
            return ResultVo.failure("用户钱包信息丢失，扣除用户下单BZW失败");
        }
        //如果冻结的金额还有 优先扣除冻结的
        BigDecimal frozenBalance = walletInfo.getWalletFrozenBalance();
        if (frozenBalance != null && frozenBalance.compareTo(BigDecimal.ZERO) > 0) {
            ResultVo resultVo = iWalletInfoService.handleUserFrozenWalletMoney(new WalletLogDto(order.getUid(), SysCoinsTypeEnums.BZW.getName(), order.getOrderCode(),
                    WalletLogTypeEnums.BUY_UP_AND_DOWN_ORDER.getTypeId(), WALLET_OUT, frozenBalance, "币指交易下单冻结金额解冻"));
            if (!resultVo.isSuccess()) {
                return resultVo;
            }
            //剩下的冻结部分转换到可用
            BigDecimal orderBzw = CalcUtils.sub(frozenBalance, order.getBzwNum(), 4);
            if (orderBzw.compareTo(BigDecimal.ZERO) != 0) {
                resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(order.getUid(), SysCoinsTypeEnums.BZW.getName(), order.getOrderCode(),
                        WalletLogTypeEnums.WALLET_THAW.getTypeId(), WALLET_IN, orderBzw, "冻结解冻"));
                if (!resultVo.isSuccess()) {
                    throw new BaseException(resultVo.getErrorMessage());
                }
            }
        } else {
            // 如果没有冻结金额
            ResultVo resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(order.getUid(), SysCoinsTypeEnums.BZW.getName(), order.getOrderCode(),
                    WalletLogTypeEnums.BUY_UP_AND_DOWN_ORDER.getTypeId(), WALLET_OUT, order.getBzwNum(), "币指交易下单"));
            if (!resultVo.isSuccess()) {
                return resultVo;
            }
        }
        return ResultVo.success();
    }

    /**
     * 处理异步结算
     * @param orderInfo
     */
    private void handleSettlement(OrderInfo orderInfo){
        //处理订单 奖励结算
        OrderRewardDto rewardDto = new OrderRewardDto();
        rewardDto.setUid(orderInfo.getUid());
        rewardDto.setOrderCode(orderInfo.getOrderCode());
        rewardDto.setMoney(orderInfo.getMoney());
        amqpTemplate.convertAndSend(QUEUES_ORDER_REWARD,rewardDto);
        // 创建结算订单任务调度
        AddScheduleJobDto jobDto = new AddScheduleJobDto();
        jobDto.setJobGroup(USER_JOB_EXECUTOR);
        jobDto.setExecutorHandler(BUY_ORDER_SETTLEMENT);
        jobDto.setExecutorParam(orderInfo.getOrderCode());
        jobDto.setJobDesc("处理买入涨/跌下单结算" + jobDto.getExecutorParam());
        jobDto.setJobCron(CronUtils.getCron(orderInfo.getOverTime()));
        amqpTemplate.convertAndSend(QUEUES_ADD_SCHEDULE_JOB, jobDto);
    }


    @Override
    public ResultVo getBZWDestoryNum() {
        BigDecimal out = BigDecimal.ZERO;
        BigDecimal bigDecimal = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.SYS_KEY + RedisKeyConstants.KEY_DESTORY_BZW_NUM));
        BigDecimal destoryNum = iOrderInfoService.getBZWDestoryNum();
        if (bigDecimal != null) {
            out = CalcUtils.add(destoryNum, bigDecimal, 2);
        }
        BigDecimal totalDestoryNum = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.SYS_KEY + RedisKeyConstants.KEY_DESTORY_BZW_TOTAL_NUM));

        BZWDestoryOut destoryOut = new BZWDestoryOut();
        destoryOut.setDestoryNum(out);
        destoryOut.setTotalDestoryNum(totalDestoryNum);
        return ResultVo.success(destoryOut);
    }

    @Override
    public ResultVo getUSDTData() {
        LocalDateTime now = LocalDateTime.now();

        OrderInfo info = new OrderInfo();
        info.setUid(UserContext.getUserID());
        info.setCreateTime(now);
        BigDecimal todayNum = iOrderInfoService.getUSDTNumByDay(info);

        info.setCreateTime(now.plusDays(-1));
        BigDecimal yesterdayNum = iOrderInfoService.getUSDTNumByDay(info);

        info.setCreateTime(now.plusDays(-2));
        BigDecimal beforeNum = iOrderInfoService.getUSDTNumByDay(info);

        USDTDataOut usdtDataOut = new USDTDataOut();
        usdtDataOut.setTodayNum(todayNum);
        usdtDataOut.setTodayNum(yesterdayNum);
        usdtDataOut.setTodayNum(beforeNum);

        BigDecimal num = CalcUtils.add(todayNum, yesterdayNum, 4);
        usdtDataOut.setTotalNum(CalcUtils.add(num, beforeNum, 4));
        return ResultVo.success(usdtDataOut);
    }


}
