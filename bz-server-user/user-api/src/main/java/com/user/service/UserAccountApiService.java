package com.user.service;

import com.common.base.ResultVo;
import com.user.model.in.UpdatePayPasswordIn;

/**
 * 用户账户Api业务接口类
 * @Author: lt
 * @Date: 2020/5/23 13:37
 */
public interface UserAccountApiService {

    /**
     * 获取用户账户信息
     * @return
     */
    ResultVo getUserAccount();

    /**
     * 修改用户支付密码
     * @param paramIn
     * @return
     */
    ResultVo updatePayPassword(UpdatePayPasswordIn paramIn);

}
