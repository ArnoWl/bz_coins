package com.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.config.RedisLock;
import com.common.constatns.CommonConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.context.UserContext;
import com.common.enums.bb.TransStatusEnums;
import com.common.enums.bb.TransTypeEnums;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.codec.CodeUtils;
import com.google.common.collect.Maps;
import com.mall.base.PageParamIn;
import com.mall.feign.admin.BaseClient;
import com.mall.model.admin.vo.SubscriptionVo;
import com.mall.model.market.dto.KlineInfo;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.vo.*;
import com.user.entity.*;
import com.user.model.in.BBBuyInfoIn;
import com.user.model.in.BBOrderIn;
import com.user.model.in.BBSaleInfoIn;
import com.user.model.in.SubscriptionIn;
import com.user.service.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.common.constatns.CommonConstants.IS_LOCKING_1;
import static com.common.constatns.UserConstants.USER_STATUS_0;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Service
public class TransactionApiServiceImpl implements TransactionApiService {

    @Resource
    private IBbBuyService iBbBuyService;
    @Resource
    private IBbBuyDetailService iBbBuyDetailService;
    @Resource
    private IBbSaleService iBbSaleService;
    @Resource
    private IBbSaleDetailService iBbSaleDetailService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private RedisLock redisLock;
    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private IInfoService iInfoService;
    @Resource
    private BaseClient baseClient;
    @Resource
    private ISubLogsService iSubLogsService;

    @Override
    public ResultVo getPrice() {
        //获取BZW和USTD的换算比例  1BZW=x USTD
        //出售
        BigDecimal price = NumberUtils.createBigDecimal((String) redisTemplate.opsForHash().get(RedisKeyConstants.COIN_PRICE_BZW, "price"));
        //购买
        BigDecimal rate = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.WANT_TO_BUY));
        BigDecimal sub = CalcUtils.sub(BigDecimal.ONE, rate, 4);
        BigDecimal cost = CalcUtils.mul(sub, price, 4);
        //出参
        HashMap<String, BigDecimal> map = new HashMap<>();
        map.put("buy_price", cost);
        map.put("sale_price", price);
        return ResultVo.success(map);
    }

    @Override
    public ResultVo queryWantBuyList(PageParamIn paramIn) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("uid", UserContext.getUserID());
            Page<BBSaleQueryVo> page = new Page<>(paramIn.getPageNo(), paramIn.getPageSize());
            iBbSaleService.querySaleList(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取币币交易我要买列表失败", e);
        }
    }

    @Override
    public ResultVo queryWantSaleList(PageParamIn paramIn) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("uid", UserContext.getUserID());
            Page<BBBuyQueryVo> page = new Page<>(paramIn.getPageNo(), paramIn.getPageSize());
            iBbBuyService.queryBuyList(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取币币交易我要卖列表失败", e);
        }
    }

    @Override
    public ResultVo showDate(Integer id, Integer type) {
        String coinSymbol = "";
        BBDataVo vo = null;
        //出售
        if (TransTypeEnums.BUY.getType().equals(type)) {
            vo = iBbBuyService.getBuyInfo(id);
            coinSymbol = SysCoinsTypeEnums.BZW.getName();
            //购买
        } else if (TransTypeEnums.SALE.getType().equals(type)) {
            vo = iBbSaleService.getSaleInfo(id);
            coinSymbol = SysCoinsTypeEnums.USDT.getName();
        }
        //获取钱包余额
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", UserContext.getUserID()).eq("coin_symbol", coinSymbol);
        WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
        if (walletInfo == null) {
            throw new BaseException("钱包信息不存在");
        }
        vo.setBalanceNum(walletInfo.getWalletBalance());

        //获取BZW和USTD的换算比例  1BZW=x USTD
        String price = (String) redisTemplate.opsForHash().get(RedisKeyConstants.COIN_PRICE_BZW, "price");
        vo.setConversionRatio(NumberUtils.createBigDecimal(price));
        return ResultVo.success(vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo handleBuy(BBBuyInfoIn in) {
        String uid = UserContext.getUserID();
        Info info=iInfoService.getById(uid);
        if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
            return ResultVo.failure("账户已被冻结");
        }
        // 校验支付密码
        ResultVo resultVo = this.verifyPayPassWord(in.getPayPassword());
        if (!resultVo.isSuccess()) {
            return resultVo;
        }
        BbSale bbSale = iBbSaleService.getById(in.getId());
        if (bbSale == null) {
            return ResultVo.failure("订单不存在");
        }
        //****  加锁  ****//
        String timeout = String.valueOf(System.currentTimeMillis() + 30000);
        String lockKey = RedisKeyConstants.LOCK_ORDER_BALANCE_PAYMENT + bbSale.getPayCode();
        if (!redisLock.lock(lockKey, timeout)) {
            return ResultVo.failure("处理支付中，请勿重复提交");
        }
        try {
            //校验

            if (StringUtils.equals(bbSale.getUid(), uid)) {
                return ResultVo.failure("不能购买自己发布的委托单");
            }
            if (bbSale.getBalanceNum().compareTo(in.getNum()) < 0) {
                return ResultVo.failure("购买数量超额");
            }
            if (in.getNum().compareTo(bbSale.getMinPrice()) < 0 || in.getNum().compareTo(bbSale.getMaxPrice()) > 0) {
                return ResultVo.failure("购买限额"+bbSale.getMinPrice()+"~"+bbSale.getMaxPrice());
            }

            //获取BZW和USTD的换算比例  1BZW=x USTD
            //String price = (String) redisTemplate.opsForHash().get(RedisKeyConstants.COIN_PRICE_BZW, "price");
            // BigDecimal cost = CalcUtils.mul(NumberUtils.createBigDecimal(price), in.getBZCNum(), 4);
            BigDecimal cost = CalcUtils.mul(bbSale.getPrice(), in.getNum(), 4);
            //购买人:扣除自己的USDT 增加系统币
            WalletLogDto wallLogDto = new WalletLogDto(uid, SysCoinsTypeEnums.USDT.getName(), bbSale.getPayCode(), WalletLogTypeEnums.BB_BUY.getTypeId(), UserConstants.WALLET_OUT, cost, "BZW购买成功扣除");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }
            wallLogDto = new WalletLogDto(uid, SysCoinsTypeEnums.BZW.getName(), bbSale.getPayCode(), WalletLogTypeEnums.BB_BUY.getTypeId(), UserConstants.WALLET_IN, in.getNum(),"BZW购买成功");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            //发布委托单人:增加USDT
            wallLogDto = new WalletLogDto(bbSale.getUid(), SysCoinsTypeEnums.USDT.getName(), bbSale.getPayCode(), WalletLogTypeEnums.BB_BUY.getTypeId(), UserConstants.WALLET_IN, cost,"BZW出售成功");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            //增加出售明细记录
            BbSaleDetail saleDetail = new BbSaleDetail();
            saleDetail.setUid(uid);
            saleDetail.setCoinSymbol(SysCoinsTypeEnums.BZW.getName());
            saleDetail.setBbSaleId(bbSale.getId());
            saleDetail.setPayCode(bbSale.getPayCode());
            saleDetail.setNum(in.getNum());
            saleDetail.setTotalMoney(cost);
            boolean flag = iBbSaleDetailService.save(saleDetail);
            if (!flag) {
                throw new BaseException("币币交易购买失败");
            }
            //通过版本号更新出售记录
            QueryWrapper<BbSale> updateWrapper = new QueryWrapper<>();
            updateWrapper.eq("id", bbSale.getId()).eq("version", bbSale.getVersion());

            BbSale sale = new BbSale();
            sale.setVersion(bbSale.getVersion() + 1);
            sale.setBalanceNum(CalcUtils.sub(bbSale.getBalanceNum(), in.getNum(), 4));
            if (BigDecimal.ZERO.compareTo(sale.getBalanceNum()) == 0) {
                sale.setStatus(UserConstants.BB_BUY_ORDER_STATUS_2);
            }
            iBbSaleService.update(sale, updateWrapper);
            if (!flag) {
                throw new BaseException("币币交易购买失败");
            }
        }catch (Exception e){
            throw new BaseException(e.getMessage(),e);
        }finally {
            // 处理完成：释放锁
            redisLock.unlock(lockKey, timeout);
        }
        return ResultVo.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo handleSale(BBSaleInfoIn in) {
        String uid = UserContext.getUserID();
        Info info=iInfoService.getById(uid);
        if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
            return ResultVo.failure("账户已被冻结");
        }
        // 校验支付密码
        ResultVo resultVo = this.verifyPayPassWord(in.getPayPassword());
        if (!resultVo.isSuccess()) {
            return resultVo;
        }
        BbBuy bbBuy = iBbBuyService.getById(in.getId());
        if (bbBuy == null) {
            return ResultVo.failure("订单不存在");
        }
        //****  加锁  ****//
        String timeout = String.valueOf(System.currentTimeMillis() + 30000);
        String lockKey = RedisKeyConstants.LOCK_ORDER_BALANCE_PAYMENT + bbBuy.getPayCode();
        if (!redisLock.lock(lockKey, timeout)) {
            return ResultVo.failure("处理支付中，请勿重复提交");
        }
        try {
            //校验

            if (StringUtils.equals(bbBuy.getUid(), uid)) {
                return ResultVo.failure("不能出售给自己的委托单");
            }
            if (bbBuy.getBalanceNum().compareTo(in.getNum()) < 0) {
                return ResultVo.failure("出售数量超额");
            }
            if (in.getNum().compareTo(bbBuy.getMinPrice()) < 0 || in.getNum().compareTo(bbBuy.getMaxPrice()) > 0) {
                return ResultVo.failure("出售限额"+bbBuy.getMinPrice()+"~"+bbBuy.getMaxPrice());
            }

            //获取BZW和USTD的换算比例  1BZW=x USTD
//        String price = (String) redisTemplate.opsForHash().get(RedisKeyConstants.COIN_PRICE_BZW, "price");
//        BigDecimal cost = CalcUtils.mul(NumberUtils.createBigDecimal(price), in.getBZCNum(), 4);
            BigDecimal cost = CalcUtils.mul(bbBuy.getPrice(), in.getNum(), 4);
            //购买人:扣除自己的系统币增加USDT
            WalletLogDto wallLogDto = new WalletLogDto(uid, SysCoinsTypeEnums.USDT.getName(), bbBuy.getPayCode(), WalletLogTypeEnums.BB_SALE.getTypeId(), UserConstants.WALLET_IN, cost,"BZW出售成功收入");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            wallLogDto = new WalletLogDto(uid, SysCoinsTypeEnums.BZW.getName(), bbBuy.getPayCode(), WalletLogTypeEnums.BB_SALE.getTypeId(), UserConstants.WALLET_OUT, in.getNum(),"BZW出售成功");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            //发布委托单人:增加系统币
            wallLogDto = new WalletLogDto(bbBuy.getUid(), SysCoinsTypeEnums.BZW.getName(), bbBuy.getPayCode(), WalletLogTypeEnums.BB_SALE.getTypeId(), UserConstants.WALLET_IN, in.getNum(),"BZW求购成功");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            //增加购买明细记录
            BbBuyDetail buyDetail = new BbBuyDetail();
            buyDetail.setUid(uid);
            buyDetail.setCoinSymbol(SysCoinsTypeEnums.USDT.getName());
            buyDetail.setBbBuyId(bbBuy.getId());
            buyDetail.setPayCode(bbBuy.getPayCode());
            buyDetail.setNum(in.getNum());
            buyDetail.setTotalMoney(cost);
            boolean flag = iBbBuyDetailService.save(buyDetail);
            if (!flag) {
                throw new BaseException("币币交易出售失败");
            }
            //通过版本号更新出售记录
            QueryWrapper<BbBuy> updateWrapper = new QueryWrapper<>();
            updateWrapper.eq("id", bbBuy.getId()).eq("version", bbBuy.getVersion());

            BbBuy buy = new BbBuy();
            buy.setVersion(bbBuy.getVersion() + 1);
            buy.setBalanceNum(CalcUtils.sub(bbBuy.getBalanceNum(), in.getNum(), 4));
            if (BigDecimal.ZERO.compareTo(buy.getBalanceNum()) == 0) {
                buy.setStatus(UserConstants.BB_BUY_ORDER_STATUS_2);
            }
            iBbBuyService.update(buy, updateWrapper);
            if (!flag) {
                throw new BaseException("币币交易出售失败");
            }
        }catch (Exception e){
            throw new BaseException(e.getMessage(),e);
        }finally {
            // 处理完成：释放锁
            redisLock.unlock(lockKey, timeout);
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo queryOrderList(PageParamIn paramIn) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("uid", UserContext.getUserID());
        Page<BBDetailQueryVo> page = new Page<>(paramIn.getPageNo(), paramIn.getPageSize());
        iBbBuyDetailService.queryOrderList(params, page);
        return ResultVo.success(page);
    }

    @Override
    public ResultVo getOrderDetail(Integer id, Integer type) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("uid", UserContext.getUserID());
        params.put("id", id);
        BBDetailVo vo = null;
        if (TransTypeEnums.BUY.getType().equals(type)) {
            vo = iBbSaleDetailService.getOrderDetail(params);
        } else if (TransTypeEnums.SALE.getType().equals(type)) {
            vo = iBbBuyDetailService.getOrderDetail(params);
        }
        return ResultVo.success(vo);
    }

    @Override
    public ResultVo publishDelegation(BBOrderIn in) {
        String uid = UserContext.getUserID();
        String payCode = CodeUtils.generateOrderNo();
        ResultVo resultVo = null;
        //获取BZW和USTD的换算比例  1BZW=x USTD
        String price = (String) redisTemplate.opsForHash().get(RedisKeyConstants.COIN_PRICE_BZW, "price");
        BigDecimal cost = NumberUtils.createBigDecimal(price);

        Info info = iInfoService.getById(uid);
        if (info == null) {
            return ResultVo.failure("用户信息丢失");
        }
        if (info.getLevelId().compareTo(CommonConstants.LEVEL_V3) < 0) {
            return ResultVo.failure("用户等级大于VIP3才能发布委托单");
        }
        if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
            return ResultVo.failure("账户已被冻结");
        }

        //求购单
        if (TransTypeEnums.BUY.getType().equals(in.getType())) {
            BigDecimal rate = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.WANT_TO_BUY));
            BigDecimal sub = CalcUtils.sub(BigDecimal.ONE, rate, 4);
            cost = CalcUtils.mul(sub, cost, 4);
            String coinSymbol = SysCoinsTypeEnums.USDT.getName();

            //扣除发布人的USDT
            BigDecimal usdtNum=CalcUtils.mul(cost,in.getNum(),4);//单价*数量
            WalletLogDto wallLogDto = new WalletLogDto(uid, coinSymbol, payCode, WalletLogTypeEnums.BB_DELEGATION_BUY.getTypeId(), UserConstants.WALLET_OUT,usdtNum, "发布BZW求购单");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            BbBuy bbBuy = new BbBuy();
            bbBuy.setUid(uid);
            bbBuy.setCoinSymbol(coinSymbol);
            bbBuy.setPayCode(payCode);
            bbBuy.setPrice(cost);
            bbBuy.setTotalNum(in.getNum());
            bbBuy.setBalanceNum(in.getNum());
            bbBuy.setMinPrice(in.getMinPrice());
            bbBuy.setMaxPrice(in.getMaxPrice());
            boolean flag = iBbBuyService.save(bbBuy);
            if (!flag) {
                throw new BaseException("发布委托单失败");
            }
            //出售单
        } else if (TransTypeEnums.SALE.getType().equals(in.getType())) {
            String coinSymbol = SysCoinsTypeEnums.BZW.getName();

            //扣除发布人的BZW
            WalletLogDto wallLogDto = new WalletLogDto(uid, coinSymbol, payCode, WalletLogTypeEnums.BB_DELEGATION_SALE.getTypeId(), UserConstants.WALLET_OUT, in.getNum(), "发布BZW挂售单");
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            BbSale bbSale = new BbSale();
            bbSale.setUid(uid);
            bbSale.setCoinSymbol(coinSymbol);
            bbSale.setPayCode(payCode);
            bbSale.setPrice(cost);
            bbSale.setTotalNum(in.getNum());
            bbSale.setBalanceNum(in.getNum());
            bbSale.setMinPrice(in.getMinPrice());
            bbSale.setMaxPrice(in.getMaxPrice());
            boolean flag = iBbSaleService.save(bbSale);
            if (!flag) {
                throw new BaseException("发布委托单失败");
            }
        }
        return ResultVo.success();
    }

    /**
     * 检验用户支付密码-
     *
     * @param payPassWord
     * @return
     */
    @Override
    public ResultVo verifyPayPassWord(String payPassWord) {
        if (StringUtils.isBlank(payPassWord)) {
            return ResultVo.failure("支付密码不能为空,请先设置支付密码");
        }
        Info info = iInfoService.getById(UserContext.getUserID());
        if (StringUtils.isBlank(info.getPayPassword())) {
            return ResultVo.failure("当前用户未设置支付密码，无法使用余额支付");
        }
        if (CommonConstants.IS_LOCKING_1.equals(info.getPayIsLocking())) {
            return ResultVo.failure("支付密码输入错误次数过多，已被锁定");
        }
        String redisKey = RedisKeyConstants.USER_PAY_PASSWORD_ERROR_COUNT + info.getUid();
        if (!payPassWord.equals(info.getPayPassword())) {
            // 记录错误次数
            int errorNum;
            if (redisTemplate.hasKey(redisKey)) {
                errorNum = Integer.parseInt(redisTemplate.opsForValue().get(redisKey));
                if (errorNum < 5) {   // 5次容错
                    errorNum++;
                    redisTemplate.opsForValue().set(redisKey, String.valueOf(errorNum));
                } else {
                    // 锁定用户账户支付密码
                    info.setPayIsLocking(IS_LOCKING_1);
                    iInfoService.updateById(info);
                    redisTemplate.delete(redisKey);
                }
            } else {
                errorNum = 1;
                redisTemplate.opsForValue().set(redisKey, String.valueOf(errorNum));
            }
            return ResultVo.failure("支付密码输入错误");
        }
        redisTemplate.delete(redisKey);
        return ResultVo.success();
    }

    @Override
    public ResultVo queryDelegationList(PageParamIn paramIn) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("uid", UserContext.getUserID());
        Page<BBDelegationQueryVo> page = new Page<>(paramIn.getPageNo(), paramIn.getPageSize());
        iBbBuyService.queryDelegationList(params, page);
        return ResultVo.success(page);
    }

    @Override
    public ResultVo getDelegation(Integer id, Integer type) {
        BBDelegationQueryVo vo = new BBDelegationQueryVo();
        if (TransTypeEnums.BUY.getType().equals(type)) {
            QueryWrapper<BbBuy> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", UserContext.getUserID()).eq("id", id);
            BbBuy bbBuy = iBbBuyService.getOne(wrapper);
            if (bbBuy != null) {
                BeanUtils.copyProperties(bbBuy, vo);
            }
        } else if (TransTypeEnums.SALE.getType().equals(type)) {
            QueryWrapper<BbSale> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", UserContext.getUserID()).eq("id", id);
            BbSale bbSale = iBbSaleService.getOne(wrapper);
            if (bbSale != null) {
                BeanUtils.copyProperties(bbSale, vo);
            }
        }
        return ResultVo.success(vo);
    }

    @Override
    public ResultVo cancelDelegation(Integer id, Integer type) {
        try {
            ResultVo resultVo = null;
            if (TransTypeEnums.BUY.getType().equals(type)) {
                QueryWrapper<BbBuy> wrapper = new QueryWrapper<>();
                wrapper.eq("uid", UserContext.getUserID()).eq("id", id);
                BbBuy bbBuy = iBbBuyService.getOne(wrapper);
                if (bbBuy == null) {
                    return ResultVo.failure("未找到该记录");
                }
                //增加发布人的USDT
                //TODO  钱包余额
                BigDecimal usdtNum=CalcUtils.mul(bbBuy.getPrice(),bbBuy.getBalanceNum(),4);
                WalletLogDto wallLogDto = new WalletLogDto(bbBuy.getUid(), bbBuy.getCoinSymbol(), bbBuy.getPayCode(), WalletLogTypeEnums.BB_DELEGATION_BUY_CANCEL.getTypeId(), UserConstants.WALLET_IN,usdtNum , WalletLogTypeEnums.BB_DELEGATION_BUY_CANCEL.getName());
                resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
                if (!resultVo.isSuccess()) {
                    return resultVo;
                }

                bbBuy.setStatus(TransStatusEnums.CANCEL.getId());
                boolean flag = iBbBuyService.updateById(bbBuy);
                if (!flag) {
                    throw new BaseException("撤销委托单失败");
                }

            } else if (TransTypeEnums.SALE.getType().equals(type)) {
                QueryWrapper<BbSale> wrapper = new QueryWrapper<>();
                wrapper.eq("uid", UserContext.getUserID()).eq("id", id);
                BbSale bbSale = iBbSaleService.getOne(wrapper);
                if (bbSale == null) {
                    return ResultVo.failure("未找到该记录");
                }
                //增加发布人的BZW
                //TODO  钱包余额
                WalletLogDto wallLogDto = new WalletLogDto(bbSale.getUid(), bbSale.getCoinSymbol(), bbSale.getPayCode(), WalletLogTypeEnums.BB_DELEGATION_SALE_CANCEL.getTypeId(), UserConstants.WALLET_IN, bbSale.getBalanceNum(), WalletLogTypeEnums.BB_DELEGATION_SALE_CANCEL.getName());
                resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
                if (!resultVo.isSuccess()) {
                    return resultVo;
                }

                bbSale.setStatus(TransStatusEnums.CANCEL.getId());
                boolean flag = iBbSaleService.updateById(bbSale);
                if (!flag) {
                    throw new BaseException("撤销委托单失败");
                }
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("撤销委托单失败失败", e);
        }
    }

    @Override
    @Transactional
    public ResultVo handleSubscription(SubscriptionIn in) {
        String uid = UserContext.getUserID();
        String payCode = CodeUtils.generateOrderCode();

        String lockKey = RedisKeyConstants.LOCK_SUB_BZW + uid;
        String value = redisLock.getDefaultOutTime();
        if (!redisLock.lock(lockKey, value)) {
            return ResultVo.failure("认购频繁,请重试...");
        }
        try {
            Info info = iInfoService.getById(UserContext.getUserID());
            if(UserConstants.USER_STATUS_1.equals(info.getStatus())){
                return ResultVo.failure("账户已被冻结");
            }
            if (USER_STATUS_0.equals(info.getIsVerified())) {
                return ResultVo.failure("请先进行实名认证");
            }

            //获取单价
            SubscriptionVo subscriptionVo = baseClient.getSysSubscriptionPrice(in.getId());
            if (subscriptionVo == null) {
                return ResultVo.failure("BZW认购记录不存在");
            }
            if(in.getQty().compareTo(subscriptionVo.getMaxNum())>0){
                return ResultVo.failure("本场可认购最大数量:"+subscriptionVo.getMaxNum());
            }
            BigDecimal price=subscriptionVo.getPrice();
            BigDecimal cost = CalcUtils.mul(price, in.getQty(), 4);

            //获取当前用户本次认购累积金额 判断当前是否可以认购这么多金额
            SubLogs subLogs=new SubLogs();
            subLogs.setUid(uid);
            subLogs.setSubscriptionId(in.getId());
            BigDecimal totalMoney=iSubLogsService.getTotalMoney(subLogs);
            if(totalMoney!=null && totalMoney.compareTo(BigDecimal.ZERO)>0){
                if(totalMoney.compareTo(subscriptionVo.getMaxNum())>=0){
                    return ResultVo.failure("本场认购数量已达到上线");
                }
                BigDecimal sub=CalcUtils.sub(subscriptionVo.getMaxNum(),totalMoney,4);
                if(sub.compareTo(in.getQty())<0){
                    return ResultVo.failure("本场可认购数量剩余:"+sub);
                }
            }
            subLogs.setMoney(in.getQty());
            if(!iSubLogsService.save(subLogs)){
                throw new BaseException("保存认购记录失败");
            }
            //增加购买人的的BZW
            WalletLogDto wallLogDto = new WalletLogDto(uid, SysCoinsTypeEnums.BZW.getName(), payCode, WalletLogTypeEnums.BZW_BUY.getTypeId(), UserConstants.WALLET_IN, in.getQty(), WalletLogTypeEnums.BZW_BUY.getName());
            ResultVo resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }
            //扣除购买人的的USDT
            wallLogDto = new WalletLogDto(uid, SysCoinsTypeEnums.USDT.getName(), payCode, WalletLogTypeEnums.BZW_BUY.getTypeId(), UserConstants.WALLET_OUT, cost, WalletLogTypeEnums.BZW_BUY.getName());
            resultVo = iWalletInfoService.handleUserWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

            //更新认购总数
            resultVo = baseClient.handleSubtractQty(in.getId(), in.getQty());
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }

        }catch (Exception e){
            throw new BaseException(e.getMessage(),e);
        }finally {
            //解锁
            redisLock.unlock(lockKey, value);
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo queryIncrease() {
        ArrayList<KlineInfo> klineInfos = new ArrayList<>();
        Set<String> keys = redisTemplate.keys(RedisKeyConstants.LATEST_COIN_MARKET_KLINE + "*");
        String content = null;
        for (String key : keys) {
            content = redisTemplate.opsForValue().get(key);
            KlineInfo klineInfo = JSON.parseObject(content, KlineInfo.class);
            klineInfos.add(klineInfo);
        }
        return ResultVo.success(klineInfos);
    }
}
