package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.context.UserContext;
import com.common.exception.BaseException;
import com.common.fileUpload.QiNiuQunClient;
import com.common.sms.SmsComponent;
import com.common.sms.SmsSendTypeEnum;
import com.common.sms.VerifySmsCodeDto;
import com.common.utils.DesensitizationUtil;
import com.common.utils.validIDCard.ValidIDCardUtils;
import com.mall.base.AppBaseParamIn;
import com.mall.feign.auth.LoginAccountClient;
import com.mall.model.auth.vo.UserLoginAccountVo;
import com.user.entity.Info;
import com.user.model.in.*;
import com.user.model.out.UserInfoOut;
import com.user.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * 用户信息Api业务实现类
 *
 * @Author: lt
 * @Date: 2020/5/23 13:33
 */
@Service
public class UserInfoApiServiceImpl implements UserInfoApiService {

    @Resource
    private IInfoService iInfoService;
    @Resource
    private LoginAccountClient loginAccountClient;
    @Resource
    private SmsComponent smsComponent;

    @Override
    public ResultVo getUserInfo(AppBaseParamIn paramIn) {
        try {
            Info info = iInfoService.getById(UserContext.getUserID());
            if (info == null) {
                return ResultVo.failure("用户信息丢失");
            }
            if (StringUtils.isNotBlank(info.getIdentityCard())) {
                info.setIdentityCard(DesensitizationUtil.around(info.getIdentityCard(), 3, 4));
            }
            UserInfoOut infoOut = new UserInfoOut();
            BeanUtils.copyProperties(info, infoOut);
            UserLoginAccountVo accountVo = loginAccountClient.getUserLoginAccount(info.getUid());
            infoOut.setLevelId(info.getLevelId());
            infoOut.setPhoneAccount(accountVo.getPhoneAccount());
            return ResultVo.success(infoOut);
        } catch (Exception e) {
            throw new BaseException("获取用户个人信息失败", e);
        }
    }

    @Override
    public ResultVo updateUserInfo(UpdateUserInfoIn paramIn) {
        try {
            Info info = new Info();
            BeanUtils.copyProperties(paramIn, info);
            info.setUid(UserContext.getUserID());
            iInfoService.updateById(info);
        } catch (BeansException e) {
            throw new BaseException("修改用户个人信息失败", e);
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo verified(MultipartFile frontImage, MultipartFile reverseImage) {
        try {
            if (frontImage == null || reverseImage == null) {
                return ResultVo.failure("请上传身份证正反照");
            }
            UserLoginAccountVo userLoginAccount = loginAccountClient.getUserLoginAccount(UserContext.getUserID());
            if (userLoginAccount == null) {
                return ResultVo.failure("用户信息不存在，认证失败");
            }
            Info info = iInfoService.getById(UserContext.getUserID());
            if (info == null) {
                return ResultVo.failure("用户信息不存在，认证失败");
            }
            if (CommonConstants.YES.equals(info.getIsVerified())) {
                return ResultVo.failure("用户已通过实名认证，请勿重复认证");
            }
            ResultVo resultVo = ValidIDCardUtils.validIDCardFile(frontImage);
            if (!resultVo.isSuccess()) {
                return resultVo;
            }
            HashMap<String, String> verifiedInfo = (HashMap<String, String>) resultVo.getData();
            // 验证身份证信息
            QueryWrapper<Info> wrapper = new QueryWrapper<>();
            wrapper.eq("identity_card", verifiedInfo.get("idCard"));
            int rows = iInfoService.count(wrapper);
            if (rows > 0) {
                return ResultVo.failure("当前身份证已在平台被使用，认证失败");
            }
            //正面照片
            String url = QiNiuQunClient.uploadFile(frontImage);
            if (StringUtils.isBlank(url)) {
                return ResultVo.failure("识别失败,请重新上传");
            }
            info.setPositivePicture(url);
            //反面照片
            url = QiNiuQunClient.uploadFile(reverseImage);
            if (StringUtils.isBlank(url)) {
                return ResultVo.failure("识别失败,请重新上传");
            }
            info.setBackPicture(url);
            info.setIdentityCard(verifiedInfo.get("idCard"));
            info.setRealName(verifiedInfo.get("realName"));
            info.setIsVerified(CommonConstants.YES);
            if (!iInfoService.updateById(info)) {
                return ResultVo.failure("数据异常，认证失败");
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("处理用户个人实名认证失败", e);
        }
    }

    @Override
    public ResultVo setPayPassword(UserPayPassWordIn userPayPassWordIn) {
        try {
            UserLoginAccountVo userLoginAccount = loginAccountClient.getUserLoginAccount(UserContext.getUserID());
            if (userLoginAccount == null) {
                return ResultVo.failure("用户信息不存在，认证失败");
            }
            if (!StringUtils.equals(userPayPassWordIn.getPayPassword(), userPayPassWordIn.getConfirmPayPassword())) {
                return ResultVo.failure("两次密码输入不一致");
            }
            // 校验手机验证码
            VerifySmsCodeDto codeDto = new VerifySmsCodeDto();
            codeDto.setPhone(userLoginAccount.getPhoneAccount());
            codeDto.setSmsType(SmsSendTypeEnum.SMS_TYPE_3.getKey());
            codeDto.setVerificationCode(userPayPassWordIn.getVerificationCode());
            ResultVo resultVo = smsComponent.verifySmsCode(codeDto);
            if (!resultVo.isSuccess()) {
                return resultVo;
            }
            //设置支付密码
            QueryWrapper<Info> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", UserContext.getUserID());
            Info info = new Info();
            info.setPayPassword(userPayPassWordIn.getPayPassword());
            iInfoService.update(info, wrapper);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("设置支付密码失败", e);
        }
    }

}
