package com.user.service;

import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.user.model.in.BBBuyInfoIn;
import com.user.model.in.BBOrderIn;
import com.user.model.in.BBSaleInfoIn;
import com.user.model.in.SubscriptionIn;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
public interface TransactionApiService {

    /**
     * 查询盘口价格
     *
     * @return
     */
    ResultVo getPrice();

    /**
     * 交易-我要买列表查询
     *
     * @return
     */
    ResultVo queryWantBuyList(PageParamIn paramIn);

    /**
     * 交易-我要卖列表查询
     *
     * @return
     */
    ResultVo queryWantSaleList(PageParamIn paramIn);

    /**
     * 交易-出售前数据展示
     *
     * @return
     */
    ResultVo showDate(Integer id, Integer type);

    /**
     * 交易-处理购买
     *
     * @return
     */
    ResultVo handleBuy(BBBuyInfoIn in);

    /**
     * 交易-处理出售
     *
     * @return
     */
    ResultVo handleSale(BBSaleInfoIn in);

    /**
     * 交易-订单列表
     *
     * @param paramIn
     * @return
     */
    ResultVo queryOrderList(PageParamIn paramIn);

    /**
     * 交易-订单详情
     *
     * @param id
     * @return
     */
    ResultVo getOrderDetail(Integer id, Integer type);

    /**
     * 发布委托
     *
     * @param in
     * @return
     */
    ResultVo publishDelegation(BBOrderIn in);

    /**
     * 查询我的委托列表
     *
     * @param paramIn
     * @return
     */
    ResultVo queryDelegationList(PageParamIn paramIn);

    /**
     * 查询我的委托单
     *
     * @param id
     * @return
     */
    ResultVo getDelegation(Integer id, Integer type);

    /**
     * 取消委托
     *
     * @param id
     * @return
     */
    ResultVo cancelDelegation(Integer id, Integer type);

    /**
     * 处理BZW认购
     *
     * @param in
     * @return
     */
    ResultVo handleSubscription(SubscriptionIn in);

    /**
     * 查询涨幅榜
     *
     * @return
     */
    ResultVo queryIncrease();

    /**
     * 校验支付密码
     *
     * @param payPassWord
     * @return
     */
    ResultVo verifyPayPassWord(String payPassWord);
}
