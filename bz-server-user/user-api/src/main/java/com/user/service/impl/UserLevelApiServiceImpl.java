package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.utils.ExtBeanUtils;
import com.common.utils.Query;
import com.user.entity.LevelInfo;
import com.user.model.out.UserLevelOut;
import com.user.service.ILevelInfoService;
import com.user.service.UserLevelApiService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Service
public class UserLevelApiServiceImpl implements UserLevelApiService {

    @Resource
    private ILevelInfoService iLevelInfoService;

    @Override
    public ResultVo queryUserupgradeRule() {
        QueryWrapper<LevelInfo> wrapper = new QueryWrapper<>();
        wrapper.ne("id", CommonConstants.LEVEL_COMMON);
        List<LevelInfo> list = iLevelInfoService.list(wrapper);

        // 组装出参
        List<UserLevelOut> listOut = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)) {
            listOut = ExtBeanUtils.copyList(list, UserLevelOut.class);
        }
        return ResultVo.success(listOut);
    }
}
