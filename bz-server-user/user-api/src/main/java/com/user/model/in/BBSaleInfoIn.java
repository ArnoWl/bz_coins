package com.user.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Data
@ApiModel("币币出售入参")
public class BBSaleInfoIn {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "BZC出售数量")
    private BigDecimal num;

    @ApiModelProperty(value = "支付密码（MD5）")
    private String payPassword;
}
