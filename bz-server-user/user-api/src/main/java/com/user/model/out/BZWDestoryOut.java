package com.user.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/29
 */
@Data
@ApiModel("BZW销毁实体出参")
public class BZWDestoryOut {

    @ApiModelProperty("销毁数量")
    private BigDecimal destoryNum;

    @ApiModelProperty("销毁总数量")
    private BigDecimal totalDestoryNum;
}
