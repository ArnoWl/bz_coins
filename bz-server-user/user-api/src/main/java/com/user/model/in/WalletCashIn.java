package com.user.model.in;

import com.mall.base.AppBaseParamIn;
import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/28
 */
@ApiModel("钱包记录实体信息")
@Data
public class WalletCashIn extends BaseParamIn {

    @ApiModelProperty("货币名称")
    @NotBlank(message = "请选择币种")
    private String coinSymbol;

    @ApiModelProperty("协议类型 OMNI ERC20")
    @NotBlank(message = "请选择协议")
    private String agreeType;

    @ApiModelProperty("接收方地址不能为空")
    @NotBlank(message = "请填写接收方钱包地址")
    private String walletReceivedAddr;

    @ApiModelProperty("金额")
    @NotNull(message = "请填写金额")
    private BigDecimal money;

    @ApiModelProperty("交易密码")
    @NotBlank(message = "请填写交易密码")
    private String payPassWord;

}
