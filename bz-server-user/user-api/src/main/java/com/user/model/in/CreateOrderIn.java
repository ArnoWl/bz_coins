package com.user.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: lt
 * @Date: 2020/7/25 16:13
 */
@Data
@ApiModel(value = "买入涨/跌订单入参")
public class CreateOrderIn implements Serializable {

    private static final long serialVersionUID = -5893382761923352608L;

    @ApiModelProperty(value = "买入币种")
    @NotBlank(message = "买入币种不能为空")
    private String coinSymbol;

    @ApiModelProperty(value = "下单类型：1-买涨 2-买跌")
    @NotNull(message = "下单类型不能为空")
    private Integer buyType;

    @ApiModelProperty(value = "下单金额")
    @NotNull(message = "下单金额不能为空")
    @Min(value = 1,message = "下单金额不可小于1")
    private BigDecimal money;

    @ApiModelProperty(value = "1普通场次  2包赔场")
    @NotNull(message = "1普通场次  2包赔场")
    private Integer orderType;



}
