package com.user.model.in;

import com.mall.base.AppPageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author arno
 * @version 1.0
 * @date 2020-08-03
 */
@Data
@ApiModel(value = "查询客服工单消息入参")
public class MessageIn extends AppPageParamIn {

    @ApiModelProperty(value = "消息类型 1提问 2是回复")
    @NotNull(message = "请传输类型")
    private Integer type;
}
