package com.user.model.in;

import com.mall.base.AppBaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/28
 */
@Data
@ApiModel("设置支付密码实体")
public class UserPayPassWordIn extends AppBaseParamIn {

    private static final long serialVersionUID = 4711418268816270475L;

    @ApiModelProperty(value = "验证码")
    private String verificationCode;

    @ApiModelProperty(value = "支付密码")
    private String payPassword;

    @ApiModelProperty(value = "确认支付密码")
    private String confirmPayPassword;

}
