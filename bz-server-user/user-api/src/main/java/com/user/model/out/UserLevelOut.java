package com.user.model.out;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Data
public class UserLevelOut {

    @ApiModelProperty(value = "等级id")
    private String id;

    @ApiModelProperty(value = "升级需要满足个人业绩")
    private BigDecimal personalMoney;

    @ApiModelProperty(value = "升级需要满足团队业绩")
    private BigDecimal teamMoney;

}
