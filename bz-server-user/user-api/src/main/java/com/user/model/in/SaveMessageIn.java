package com.user.model.in;

import com.mall.base.AppBaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author arno
 * @version 1.0
 * @date 2020-08-04
 */
@Data
@ApiModel(value = "新增客服工单消息入参")
public class SaveMessageIn extends AppBaseParamIn {

    @ApiModelProperty(value = "标题")
    @NotBlank(message = "请填写标题信息")
    private String title;

    @ApiModelProperty(value = "内容")
    @NotNull(message = "请填写内容")
    private String content;

}
