package com.user.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/27
 */
@ApiModel("钱包记录实体信息")
@Data
public class WalletLogsIn extends PageParamIn {

    @NotBlank(message = "货币名称")
    private String coinSymbol;

    @NotNull(message = "钱包类型 1可用，2冻结")
    private int type;

    @ApiModelProperty("操作类型")
    private Integer typeId;

    @ApiModelProperty("时间yyyy-MM-dd HH:mm:ss~yyyy-MM-dd HH:mm:ss")
    private String times;
}
