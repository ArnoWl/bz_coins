package com.user.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/30
 */
@ApiModel("提现记录出参")
@Data
public class CashLogOut {

    @ApiModelProperty(value = "货币类型")
    private String coinSymbol;

    @ApiModelProperty(value = "接收方地址")
    private String walletReceivedAddr;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal money;

    @ApiModelProperty(value = "状态 1待审核  5审核，待转币 10转币，待确认  15已确认 20已驳回")
    private Integer status;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime createTime;

}
