package com.user.model.out;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/29
 */
@Data
public class USDTDataOut {

    @ApiModelProperty(value = "今天")
    private BigDecimal todayNum;

    @ApiModelProperty(value = "昨日")
    private BigDecimal yesterdayNum;

    @ApiModelProperty(value = "前天")
    private BigDecimal beforeNum;

    @ApiModelProperty(value = "总计")
    private BigDecimal totalNum;

}
