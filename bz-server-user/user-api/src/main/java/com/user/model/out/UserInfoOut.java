package com.user.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息出参实体
 *
 * @Author: lt
 * @Date: 2020/3/18 15:32
 */
@ApiModel(value = "用户信息出参")
@Data
public class UserInfoOut implements Serializable {

    private static final long serialVersionUID = -1707427768729649895L;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "会员等级ID")
    private Integer levelId;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "头像")
    private String portrait;

    @ApiModelProperty(value = "ID")
    private String code;

    @ApiModelProperty(value = "邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "手机账号")
    private String phoneAccount;

    @ApiModelProperty(value = "身份证号")
    private String identityCard;

    @ApiModelProperty(value = "是否实名认证：0-否 1-是")
    private Integer isVerified;

    @ApiModelProperty(value = "二维码信息")
    private String qrCodeInfo;
}
