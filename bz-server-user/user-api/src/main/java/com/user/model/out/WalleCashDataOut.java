package com.user.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/29
 */
@Data
@ApiModel("提币数据展示实体")
public class WalleCashDataOut {

    @ApiModelProperty(value = "手续费数量")
    private BigDecimal tax;

    @ApiModelProperty(value = "手续费类型 1按数量 2按比例")
    private Integer taxType;

    @ApiModelProperty(value = "账号余额")
    private BigDecimal balanceMoney;

    @ApiModelProperty(value = "最低提币数量")
    private BigDecimal minTax=BigDecimal.ZERO;

    @ApiModelProperty(value = "最大提币数量")
    private BigDecimal maxTax=BigDecimal.ZERO;

}
