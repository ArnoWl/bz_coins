package com.user.model.in;

import com.mall.base.AppBaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 修改用户基础信信息入参实体
 * @Author: lt
 * @Date: 2020/3/23 17:11
 */
@Data
@ApiModel(value = "修改用户基础信息")
public class UpdateUserInfoIn extends AppBaseParamIn {

    private static final long serialVersionUID = 4711418268816270475L;

    @ApiModelProperty(value = "头像")
    private String portrait;

    @ApiModelProperty(value = "昵称")
    private String nickName;

}
