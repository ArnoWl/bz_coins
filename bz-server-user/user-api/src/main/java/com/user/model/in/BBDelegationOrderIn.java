package com.user.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
@Data
@ApiModel("币币交易发布委托入参")
public class BBDelegationOrderIn extends PageParamIn {

    @ApiModelProperty(value = "uid", hidden = true)
    private String uid;

}
