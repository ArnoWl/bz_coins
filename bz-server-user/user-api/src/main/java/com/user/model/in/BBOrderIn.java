package com.user.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
@Data
@ApiModel("币币交易发布委托入参")
public class BBOrderIn {

    @ApiModelProperty(value = "数量")
    private BigDecimal num;

    @ApiModelProperty(value = "发布委托类型 1:求购单 2:出售单")
    private Integer type;

    @ApiModelProperty(value = "最低购买金额")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最高购买金额")
    private BigDecimal maxPrice;

}
