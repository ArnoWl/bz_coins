package com.user.model.out;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/30
 */
@ApiModel("充值记录出参")
@Data
public class RechargeLogOut {

    @ApiModelProperty(value = "货币类型")
    private String coinSymbol;

    @ApiModelProperty(value = "发送方地址")
    private String walletSendAddr;

    @ApiModelProperty(value = "货币数量")
    private BigDecimal coinNum;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime createTime;

}
