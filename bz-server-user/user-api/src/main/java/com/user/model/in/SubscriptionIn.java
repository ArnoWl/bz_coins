package com.user.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/30
 */
@Data
@ApiModel("BZW认购")
public class SubscriptionIn {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "数量")
    private BigDecimal qty;
}
