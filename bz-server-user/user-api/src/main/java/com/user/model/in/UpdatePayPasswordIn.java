package com.user.model.in;

import com.mall.base.AppBaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 修改用户支付密码参数实体
 * @Author: lt
 * @Date: 2020/3/25 16:04
 */
@Data
@ApiModel(value = "修改用户支付密码参数")
public class UpdatePayPasswordIn extends AppBaseParamIn {

    private static final long serialVersionUID = -7264158400307048545L;

    @ApiModelProperty(value = "短信验证码")
    private String verificationCode;

    @ApiModelProperty(value = "支付密码（MD5）")
    @NotBlank(message = "支付密码不能为空")
    private String newPayPassword;

}
