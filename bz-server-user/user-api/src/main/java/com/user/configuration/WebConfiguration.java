package com.user.configuration;

import com.auth.client.interceptor.UserAuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Collections;

/**
 * web拦截器配置
 *
 * @author lt
 * 2020年3月18日14:09:26
 */
@Configuration("merchantWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * 用户鉴权拦截处理
     *
     * @return
     */
    @Bean
    UserAuthInterceptor getUserAuthRestInterceptor() {
        return new UserAuthInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getUserAuthRestInterceptor()).
                addPathPatterns(getIncludePathPatterns());
    }

    private ArrayList<String> getIncludePathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/info/**",
                "/account/**",
                "/message/**",
                "/transaction/**",
                "/order/**",
                "/video/**",
                "/assets/**"
        };
        Collections.addAll(list, urls);
        return list;
    }

}
