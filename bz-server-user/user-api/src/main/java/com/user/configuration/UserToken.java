package com.user.configuration;

import com.auth.client.config.UserAuthConfig;
import com.common.constatns.JWTConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.jwt.JWTHelper;
import com.common.jwt.RsaKeyHelper;
import com.common.utils.lang.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author: lt
 * @Date: 2020/7/7 15:49
 */
@Slf4j
@Component
public class UserToken {

    @Resource
    private UserAuthConfig userAuthConfig;

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    public String getUID(){
        // 登录用户token信息
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert requestAttributes != null;
        HttpServletRequest request = requestAttributes.getRequest();
        String token = request.getHeader(userAuthConfig.getTokenHeader());
        if(StringUtils.isNotBlank(token)){
            try {
                Map<String,Object> userInfo =  JWTHelper.getInfoFromToken(token,
                        RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(RedisKeyConstants.REDIS_USER_PUB_KEY)));
                return userInfo.get(JWTConstants.JWT_KEY_USER_ID).toString();
            } catch (Exception e) {
                log.error("用户token无效：" + token);
            }
        }
        return null;
    }

}
