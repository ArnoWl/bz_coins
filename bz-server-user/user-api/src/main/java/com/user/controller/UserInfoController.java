package com.user.controller;

import com.common.base.ResultVo;
import com.common.context.UserContext;
import com.common.fileUpload.QiNiuQunClient;
import com.mall.base.AppBaseParamIn;
import com.mall.model.user.vo.TeamOrderVo;
import com.mall.model.user.vo.TeamPerformanceVo;
import com.user.entity.Info;
import com.user.model.in.UpdateUserInfoIn;
import com.user.model.in.UserPayPassWordIn;
import com.user.model.out.UserInfoOut;
import com.user.service.IInvitationService;
import com.user.service.IOrderInfoService;
import com.user.service.UserInfoApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 用户个人信息控制器
 *
 * @Author: lt
 * @Date: 2020/3/18 14:49
 */
@CrossOrigin
@Api(description = "个人信息模块")
@RestController
@RequestMapping(value = "info")
public class UserInfoController {

    @Resource
    private UserInfoApiService userInfoApiService;
    @Resource
    private IOrderInfoService iOrderInfoService;
    @Resource
    private IInvitationService iInvitationService;

    @ApiOperation(value = "设置支付密码")
    @RequestMapping(value = "setPayPassword", method = RequestMethod.POST)
    public ResultVo setPayPassword(@RequestBody UserPayPassWordIn userPayPassWordIn) {
        return userInfoApiService.setPayPassword(userPayPassWordIn);
    }

    @ApiOperation(value = "获取用户个人信息", response = UserInfoOut.class)
    @RequestMapping(value = "personal", method = RequestMethod.POST)
    public ResultVo getPersonalInfo(@RequestBody AppBaseParamIn paramIn) {
        return userInfoApiService.getUserInfo(paramIn);
    }

    @ApiOperation(value = "修改用户基础信息")
    @RequestMapping(value = "updateInfo", method = RequestMethod.POST)
    public ResultVo updateInfo(@RequestBody UpdateUserInfoIn infoIn) {
        return userInfoApiService.updateUserInfo(infoIn);
    }

    @ApiOperation(value = "用户实名认证")
    @RequestMapping(value = "verified", method = RequestMethod.POST)
    public ResultVo verified(@RequestParam("frontImage") MultipartFile frontImage, @RequestParam("reverseImage") MultipartFile reverseImage) {
        return userInfoApiService.verified(frontImage, reverseImage);
    }

    @ApiOperation(value = "我的团队业绩", response = TeamPerformanceVo.class)
    @RequestMapping(value = "getMyTeamPerformance", method = RequestMethod.POST)
    public ResultVo getMyTeamPerformance() {
        return iOrderInfoService.getMyTeamPerformance(UserContext.getUserID());
    }

    @ApiOperation(value = "我的团对每个等级对应人数")
    @RequestMapping(value = "getMyTeamLevel", method = RequestMethod.GET)
    public ResultVo getMyTeamLevel(@RequestParam("id") Integer id) {
        return ResultVo.success(iInvitationService.getTeamLevel(id));
    }

    @ApiOperation(value = "我的团队成员", response = TeamOrderVo.class)
    @RequestMapping(value = "getMyTeamMember", method = RequestMethod.GET)
    public ResultVo getMyTeamMember() {
        return ResultVo.success(iInvitationService.getTeamMemberOrders(UserContext.getUserID()));
    }

}
