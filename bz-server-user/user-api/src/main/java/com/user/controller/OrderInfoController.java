package com.user.controller;

import com.common.base.ResultVo;
import com.common.context.UserContext;
import com.mall.model.admin.vo.SubscriptionVo;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import com.mall.model.user.vo.BZTotalVo;
import com.user.entity.OrderOverview;
import com.user.model.in.CreateOrderIn;
import com.user.model.out.BZWDestoryOut;
import com.user.model.out.USDTDataOut;
import com.user.service.IOrderInfoService;
import com.user.service.OrderApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 买入订单模块控制器
 *
 * @Author: lt
 * @Date: 2020/7/25 22:50
 */
@CrossOrigin
@Api(description = "买入订单模块")
@RestController
@RequestMapping(value = "order")
public class OrderInfoController {

    @Resource
    private OrderApiService orderApiService;
    @Resource
    private IOrderInfoService iOrderInfoService;

    @ApiOperation(value = "创建买入订单",response =BuyOrderListVo.class)
    @RequestMapping(value = "create", method = RequestMethod.POST)
    public ResultVo createOrder(@RequestBody CreateOrderIn orderIn) {
        return orderApiService.createOrder(orderIn);
    }

    @ApiOperation(value = "买入订单信息列表", response = BuyOrderListVo.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo orderList(@RequestBody GetBuyOrderListDto listDto) {
        listDto.setUid(UserContext.getUserID());
        return ResultVo.success(iOrderInfoService.getBuyOrderPage(listDto));
    }

    @ApiOperation(value = "获取BZW销毁数量", response = BZWDestoryOut.class)
    @RequestMapping(value = "getBZWDestoryNum", method = RequestMethod.GET)
    public ResultVo getBZWDestoryNum() {
        return orderApiService.getBZWDestoryNum();
    }

    @ApiOperation(value = "USDT交易量", response = USDTDataOut.class)
    @RequestMapping(value = "getUSDTData", method = RequestMethod.POST)
    public ResultVo getUSDTData() {
        return orderApiService.getUSDTData();
    }

    @ApiOperation(value = "订单数据总览", response = OrderOverview.class)
    @RequestMapping(value = "getOrderOverviewData", method = RequestMethod.POST)
    public ResultVo getOrderOverviewData(@RequestBody GetBuyOrderListDto listDto) {
        return ResultVo.success(iOrderInfoService.getOrderOverviewData(listDto));
    }

    @ApiOperation(value = "获取包赔场结束时间于对人民币价格")
    @RequestMapping(value = "getOverTime", method = RequestMethod.GET)
    public ResultVo getOverTime() {
        return iOrderInfoService.getOverTime();
    }


    @ApiOperation(value = "获取币指交易统计数据", response = BZTotalVo.class)
    @RequestMapping(value = "getOrderTotal", method = RequestMethod.GET)
    public ResultVo getOrderTotal(@RequestParam("coinSymbol") String coinSymbol) {
        return iOrderInfoService.getOrderTotal(coinSymbol);
    }

}
