package com.user.controller;

import com.auth.client.annotation.IgnoreUserToken;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.fileUpload.QiNiuQunClient;
import com.common.sms.*;
import com.common.utils.SMSUtils;
import com.common.utils.lang.StringUtils;
import com.mall.base.AppBaseParamIn;
import com.mall.base.PageParamIn;
import com.mall.feign.admin.AppVersionClient;
import com.mall.feign.admin.BaseClient;
import com.mall.feign.auth.LoginAccountClient;
import com.mall.model.admin.vo.*;
import com.common.sms.SendSmsCodeDto;
import com.user.model.out.UserLevelOut;
import com.user.service.UserLevelApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@CrossOrigin
@Api(description = "基础业务模块")
@RestController
@RequestMapping(value = "base")
@Slf4j
public class BaseController {

    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private SmsComponent smsComponent;
    @Resource
    private AppVersionClient appVersionClient;
    @Resource
    private BaseClient baseClient;
    @Resource
    private LoginAccountClient loginAccountClient;
    @Resource
    private UserLevelApiService userLevelApiService;

    @ApiOperation(value = "获取客服信息", response = ContactOut.class)
    @RequestMapping(value = "getSysContact", method = RequestMethod.GET)
    @IgnoreUserToken
    public ResultVo getSysContact() {
        try {
            return baseClient.getSysContact();
        } catch (Exception e) {
            log.error("获取客服信息失败", e);
            return ResultVo.failure("获取客服信息失败");
        }
    }

    @ApiOperation(value = "会员升级规则", response = UserLevelOut.class)
    @RequestMapping(value = "queryUserupgradeRule", method = RequestMethod.GET)
    public ResultVo queryUserupgradeRule() {
        try {
            return userLevelApiService.queryUserupgradeRule();
        } catch (Exception e) {
            log.error("获取会员升级规则失败", e);
            return ResultVo.failure("获取会员升级规则失败");
        }
    }

    @ApiOperation(value = "帮助中心", response = HelpCenterVo.class)
    @RequestMapping(value = "queryHelp", method = RequestMethod.GET)
    public ResultVo queryHelp() {
        try {
            return baseClient.queryHelp();
        } catch (Exception e) {
            log.error("帮助中心失败", e);
            return ResultVo.failure("帮助中心失败");
        }
    }

    @ApiOperation(value = "获取banner", response = BannerVo.class)
    @ApiImplicitParam(name = "type", required = true, value = "1首页 2首页中部", paramType = "query", dataType = "Integer")
    @RequestMapping(value = "queryBanner", method = RequestMethod.GET)
    public ResultVo queryBanner(@RequestParam("type") Integer type) {
        try {
            return baseClient.queryBanner(type);
        } catch (Exception e) {
            log.error("获取banner失败", e);
            return ResultVo.failure("获取banner失败");
        }
    }

    @ApiOperation(value = "获取公告", response = NoticeInfoVo.class)
    @RequestMapping(value = "queryNotice", method = RequestMethod.POST)
    public ResultVo queryNotice(@RequestBody PageParamIn pageParamIn) {
        try {
            return baseClient.queryNotice(pageParamIn);
        } catch (Exception e) {
            log.error("获取公告失败", e);
            return ResultVo.failure("获取公告失败");
        }
    }

    @ApiOperation(value = "获取公告详情", response = NoticeDetailVo.class)
    @RequestMapping(value = "getNoticeDetail", method = RequestMethod.GET)
    public ResultVo getNoticeDetail(@RequestParam("id") Integer id) {
        try {
            return baseClient.getNoticeDetail(id);
        } catch (Exception e) {
            log.error("获取公告详情失败", e);
            return ResultVo.failure("获取公告详情失败");
        }
    }

    @ApiOperation(value = "获取规则", response = SysRuleVo.class)
    @ApiImplicitParam(name = "type", required = true, value = "1 视频有关规则、2 订单有关规则、3 用户协议", paramType = "query", dataType = "Integer")
    @RequestMapping(value = "getSysRule", method = RequestMethod.GET)
    public ResultVo getSysRule(@RequestParam("type") Integer type) {
        try {
            return baseClient.getSysRule(type);
        } catch (Exception e) {
            log.error("获取公告详情失败", e);
            return ResultVo.failure("获取公告详情失败");
        }
    }

    @ApiOperation(value = "版本升级 -- 获取最新版本信息", response = AppVersionVo.class)
    @RequestMapping(value = "getLatestVersion", method = RequestMethod.POST)
    public ResultVo getLatestVersion(@RequestBody AppBaseParamIn paramIn) {
        paramIn.setAppType(CommonConstants.CLIENT_TYPE_1);
        AppVersionVo versionVo = appVersionClient.getLatestAppVersion(paramIn);
        if (versionVo != null) {
            return ResultVo.success(versionVo);
        }
        return ResultVo.success("平台服务中断，获取最新版本信息失败");
    }

    @ApiOperation(value = "上传图片")
    @RequestMapping(value = "uploadImg", method = RequestMethod.POST)
    public ResultVo uploadImg(@RequestParam("file") MultipartFile file) {
        try {
            //上传文件
            return ResultVo.success(QiNiuQunClient.uploadFile(file));
        } catch (Exception e) {
            log.error("上传图片失败", e);
            return ResultVo.failure("上传图片失败");
        }
    }

    @ApiOperation(value = "发送短信验证码")
    @RequestMapping(value = "sendSmsCode", method = RequestMethod.POST)
    public ResultVo sendSmsCode(@RequestBody SendSmsCodeDto codeDto) {
        // 获取短信发送类型信息
        SmsSendTypeEnum typeEnum = SmsSendTypeEnum.getSendTypeByKey(codeDto.getSmsType());
        if(typeEnum == null){
            return ResultVo.failure("短信类型有误");
        }
        if(typeEnum.getKey().equals(SmsSendTypeEnum.SMS_TYPE_1.getKey())){
            String uid = loginAccountClient.getUidByPhoneAccount(codeDto.getPhone());
            if(StringUtils.isNotBlank(uid)){
                return ResultVo.failure("当前手机号已在平台注册，请前往登录");
            }
        }
        // 校验短信验证码是否已发送，避免重复发送
        Object obj = redisTemplate.opsForValue().get(SMSUtils.getSmsKey(codeDto.getSmsType(), codeDto.getPhone()));
        if (obj == null) {
            return smsComponent.sendSms(codeDto);
        }
        return ResultVo.failure("短信已发送，请稍后重试");
    }

    @ApiOperation(value = "校验短信验证码")
    @RequestMapping(value = "verifySmsCode", method = RequestMethod.POST)
    public ResultVo sendSmsCode(@RequestBody VerifySmsCodeDto codeDto) {
        return smsComponent.verifySmsCode(codeDto);
    }

}
