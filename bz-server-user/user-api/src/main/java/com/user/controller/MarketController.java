package com.user.controller;

import com.common.base.ResultVo;
import com.common.constatns.MarketConstants;
import com.google.common.collect.Lists;
import com.mall.model.market.dto.KlineInfo;
import com.user.model.in.CreateOrderIn;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.List;

import static com.common.constatns.MarketConstants.EXCHANGE_KLINE;

/**
 * @Author: lt
 * @Date: 2020/7/26 18:14
 */
@CrossOrigin
@Api(description = "市场行情信息模块")
@RestController
@RequestMapping(value = "market")
public class MarketController {

    @Resource
    private MongoTemplate mongoTemplate;

    @ApiOperation(value = "获取行情K线信息",response = KlineInfo.class)
    @RequestMapping(value = "kline", method = RequestMethod.GET)
    public ResultVo marketKline(@RequestParam("channelType") String channelType,
                                @RequestParam("minute") Integer minute,@RequestParam("limit") Integer limit) {
        String collectionName = EXCHANGE_KLINE + channelType + "_" + minute;
        Query query = new Query().with((Sort.by(Sort.Order.desc("time")))).limit(limit);
        List<KlineInfo> klineInfos = mongoTemplate.find(query,KlineInfo.class,collectionName);
        if(CollectionUtils.isEmpty(klineInfos)){
            klineInfos = Lists.newArrayList();
        }
        return ResultVo.success(klineInfos);
    }

}
