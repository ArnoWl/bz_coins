package com.user.controller;

import com.common.base.ResultVo;
import com.mall.base.AppPageParamIn;
import com.mall.model.user.vo.UserMessageQueryVo;
import com.user.model.in.MessageIn;
import com.user.model.in.SaveMessageIn;
import com.user.model.out.MessageOut;
import com.user.service.MessageApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@CrossOrigin
@Api(description = "消息模块")
@RestController
@RequestMapping(value = "message")
@Slf4j
public class MessageController {

    @Resource
    private MessageApiService messageApiService;

    @ApiOperation(value = "查询用户站内信消息", response = UserMessageQueryVo.class)
    @RequestMapping(value = "queryUserMessage", method = RequestMethod.POST)
    public ResultVo queryUserMessage(@RequestBody MessageIn paramIn) {
        return messageApiService.queryUserMessage(paramIn);
    }

    @ApiOperation(value = "设置为已读消息", response = MessageOut.class)
    @RequestMapping(value = "readUserMessage", method = RequestMethod.GET)
    public ResultVo readUserMessage(@RequestParam("id") Integer id) {
        return messageApiService.readUserMessage(id);
    }

    @ApiOperation(value = "设置所有为已读消息")
    @RequestMapping(value = "readAllUserMessage", method = RequestMethod.GET)
    public ResultVo readAllUserMessage() {
        return messageApiService.readAllUserMessage();
    }


    @ApiOperation(value = "提交客服工单问题")
    @RequestMapping(value = "saveMessage", method = RequestMethod.POST)
    public ResultVo saveMessage(@RequestBody SaveMessageIn paramIn){
        return messageApiService.saveMessage(paramIn);
    }
}
