package com.user.controller;

import com.common.base.ResultVo;
import com.common.context.UserContext;
import com.common.fileUpload.QiNiuQunClient;
import com.mall.feign.video.VideoClient;
import com.mall.model.video.dto.VideoReleaseDto;
import com.mall.model.video.dto.VideoUserDto;
import com.mall.model.video.dto.VideoUserQueryDto;
import com.mall.model.video.vo.VideoDetailVo;
import com.mall.model.video.vo.VideoUserQueryVo;
import com.mall.model.video.vo.VideoUserVo;
import com.user.entity.Info;
import com.user.service.IInfoService;
import com.user.service.VideoApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 视频控制器
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@CrossOrigin
@Api(description = "视频模块")
@RestController
@RequestMapping(value = "video")
public class VideoController {

    @Resource
    private VideoClient videoClient;

    @Resource
    private VideoApiService videoApiService;

    @ApiOperation(value = "七牛云视频上传token")
    @RequestMapping(value = "uploadToken", method = RequestMethod.GET)
    public ResultVo uploadImg() {
        return ResultVo.success(QiNiuQunClient.getToken());
    }

    @ApiOperation(value = "查询视频", response = VideoUserQueryVo.class)
    @PostMapping(value = "queryUserVideo")
    public ResultVo queryUserVideo(@RequestBody VideoUserQueryDto dto) {
        dto.setUid(UserContext.getUserID());
        return videoClient.queryUserVideo(dto);
    }

    @ApiOperation(value = "视频详情", response = VideoDetailVo.class)
    @GetMapping(value = "getVideoDetail")
    public ResultVo getVideoDetail(@RequestParam("id") Integer id) {
        String uid = UserContext.getUserID();
        return videoClient.getVideoDetail(id, uid);
    }

    @ApiOperation(value = "用户视频点赞")
    @GetMapping(value = "handleThumb")
    public ResultVo handleThumb(@RequestParam("id") Integer id) {
        String uid = UserContext.getUserID();
        return videoClient.handleThumb(uid, id);
    }

    @ApiOperation(value = "用户发布视频列表", response = VideoUserVo.class)
    @PostMapping(value = "queryVideoRelease")
    public ResultVo queryVideoRelease(@RequestBody VideoUserDto dto) {
        dto.setUid(UserContext.getUserID());
        return videoClient.queryVideoRelease(dto);
    }

    @ApiOperation(value = "用户发布视频")
    @PostMapping(value = "handleVideoRelease")
    public ResultVo handleVideoRelease(@RequestBody VideoReleaseDto dto) {
        dto.setUid(UserContext.getUserID());
        return videoApiService.handleVideoRelease(dto);
    }

}
