package com.user.controller;

import com.common.base.ResultVo;
import com.common.context.UserContext;
import com.common.enums.user.WalletLogTypeEnums;
import com.mall.base.PageParamIn;
import com.mall.feign.admin.BaseClient;
import com.mall.model.wallet.dto.WalletCashDto;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.RechargeLogVo;
import com.user.entity.WalletLogs;
import com.user.model.AssetsItem;
import com.user.model.UserAssets;
import com.user.model.WalletAddr;
import com.user.model.in.WalletCashIn;
import com.user.model.in.WalletLogsIn;
import com.user.model.out.CashLogOut;
import com.user.model.out.MessageOut;
import com.user.model.out.RechargeLogOut;
import com.user.model.out.WalleCashDataOut;
import com.user.service.AssetsApiService;
import com.user.service.IWalletInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/27
 */
@CrossOrigin
@Api(description = "资金业务模块")
@RestController
@RequestMapping(value = "assets")
@Slf4j
public class AssetsController {

    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private BaseClient baseClient;
    @Resource
    private AssetsApiService assetsApiService;

    @ApiOperation(value = "获取我的资产", response = UserAssets.class)
    @RequestMapping(value = "getMyAssets", method = RequestMethod.GET)
    public ResultVo getMyAssets() {
        return iWalletInfoService.getMyAssets();
    }

    @ApiOperation(value = "获取单项我的资产", response = AssetsItem.class)
    @RequestMapping(value = "getMyAssetsItem", method = RequestMethod.GET)
    public ResultVo getMyAssetsItem(@RequestParam String coinSymbol) {
        return iWalletInfoService.getMyAssetsItem(coinSymbol);
    }

    @ApiOperation(value = "提币数据展示", response = WalleCashDataOut.class)
    @RequestMapping(value = "walletData", method = RequestMethod.GET)
    @ApiImplicitParam(name = "coinSymbol", required = true, value = "币种名称:BZW,USDT", paramType = "query", dataType = "String")
    public ResultVo walleCashData(@RequestParam("coinSymbol") String coinSymbol) {
        return assetsApiService.walleCashData(coinSymbol);
    }

    @ApiOperation(value = "用户提币")
    @RequestMapping(value = "walletCash", method = RequestMethod.POST)
    public ResultVo handleWalletCash(@RequestBody WalletCashIn walletCashIn) {
        return assetsApiService.handleWalletCash(walletCashIn);
    }

    @ApiOperation(value = "查询充值记录", response = RechargeLogOut.class)
    @RequestMapping(value = "queryWalletRecharge", method = RequestMethod.POST)
    public ResultVo queryWalletRecharge(@RequestBody PageParamIn in) {
        WalletRechargeListDto dto = new WalletRechargeListDto();
        dto.setUid(UserContext.getUserID());
        dto.setPageNo(in.getPageNo());
        dto.setPageSize(in.getPageSize());
        return assetsApiService.queryWalletRecharge(dto);
    }

    @ApiOperation(value = "查询提现记录", response = CashLogOut.class)
    @RequestMapping(value = "queryWalletCash", method = RequestMethod.POST)
    public ResultVo queryWalletCash(@RequestBody PageParamIn in) {
        WalletCashListDto dto = new WalletCashListDto();
        dto.setUid(UserContext.getUserID());
        dto.setPageNo(in.getPageNo());
        dto.setPageSize(in.getPageSize());
        return assetsApiService.queryWalletCash(dto);
    }

    @ApiOperation(value = "查询财务记录", response = WalletLogs.class)
    @RequestMapping(value = "queryWalletLogs", method = RequestMethod.POST)
    public ResultVo queryWalletLogs(@RequestBody WalletLogsIn walletLogsIn) {
        return assetsApiService.queryWalletLogs(walletLogsIn);
    }

    @ApiOperation(value = "查询币种", response = String.class)
    @RequestMapping(value = "querySysCoinCash", method = RequestMethod.GET)
    public ResultVo querySysCoinCash() {
        return ResultVo.success(baseClient.querySysCoinCash());
    }

    @ApiOperation(value = "获取钱包地址", response = WalletAddr.class)
    @RequestMapping(value = "getWalletAddr", method = RequestMethod.GET)
    @ApiImplicitParams({@ApiImplicitParam(name = "crotocol", required = true, value = "协议名称:OMNI,ERC20", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "coinSymbol", required = true, value = "币种名称:BZW,USDT", paramType = "query", dataType = "String")})
    public ResultVo getWalletAddr(@RequestParam("coinSymbol") String coinSymbol, @RequestParam("crotocol") String crotocol) {
        return ResultVo.success(iWalletInfoService.getWalletAddr(coinSymbol, crotocol));
    }


    @ApiOperation(value = "查询操作类型", response = String.class)
    @RequestMapping(value = "queryHandleType", method = RequestMethod.GET)
    public ResultVo queryHandleType() {
        return ResultVo.success(WalletLogTypeEnums.getList());
    }

}
