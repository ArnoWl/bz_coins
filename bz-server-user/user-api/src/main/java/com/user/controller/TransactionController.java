package com.user.controller;

import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.feign.admin.BaseClient;
import com.mall.model.admin.vo.SubscriptionVo;
import com.mall.model.user.vo.*;
import com.user.model.in.BBBuyInfoIn;
import com.user.model.in.BBOrderIn;
import com.user.model.in.BBSaleInfoIn;
import com.user.model.in.SubscriptionIn;
import com.user.service.TransactionApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * <p>
 * 交易信息控制器
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@CrossOrigin
@Api(description = "交易信息模块")
@RestController
@RequestMapping(value = "transaction")
public class TransactionController {

    @Resource
    private TransactionApiService transactionApiService;

    @Resource
    private BaseClient client;


    @ApiOperation(value = "查询涨幅榜")
    @RequestMapping(value = "queryIncrease", method = RequestMethod.GET)
    public ResultVo queryIncrease() {
        return transactionApiService.queryIncrease();
    }

    @ApiOperation(value = "查询盘口价格")
    @RequestMapping(value = "getPrice", method = RequestMethod.GET)
    public ResultVo getPrice() {
        return transactionApiService.getPrice();
    }

    @ApiOperation(value = "我要买列表查询", response = BBSaleQueryVo.class)
    @RequestMapping(value = "queryBuyList", method = RequestMethod.POST)
    public ResultVo queryWantBuyList(@RequestBody PageParamIn paramIn) {
        return transactionApiService.queryWantBuyList(paramIn);
    }

    @ApiOperation(value = "我要卖列表查询", response = BBBuyQueryVo.class)
    @RequestMapping(value = "querySellList", method = RequestMethod.POST)
    public ResultVo queryWantSaleList(@RequestBody PageParamIn paramIn) {
        return transactionApiService.queryWantSaleList(paramIn);
    }

    @ApiOperation(value = "购买前数据展示", response = BBBuyVo.class)
    @RequestMapping(value = "showBuyDate", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", required = true, value = "1:出售 2:购买", paramType = "query", dataType = "Integer")
    public ResultVo showDate(@RequestParam("id") Integer id, Integer type) {
        return transactionApiService.showDate(id, type);
    }

    @ApiOperation(value = "处理购买")
    @RequestMapping(value = "handleBuy", method = RequestMethod.POST)
    public ResultVo handleBuy(@RequestBody BBBuyInfoIn in) {
        return transactionApiService.handleBuy(in);
    }

    @ApiOperation(value = "处理出售")
    @RequestMapping(value = "handleSale", method = RequestMethod.POST)
    public ResultVo handleSale(@RequestBody BBSaleInfoIn in) {
        return transactionApiService.handleSale(in);
    }

    @ApiOperation(value = "订单列表", response = BBDetailQueryVo.class)
    @RequestMapping(value = "queryOrderList", method = RequestMethod.POST)
    public ResultVo queryOrderList(@RequestBody PageParamIn paramIn) {
        return transactionApiService.queryOrderList(paramIn);
    }

    @ApiOperation(value = "订单详情", response = BBDetailVo.class)
    @RequestMapping(value = "getOrderDetail", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", required = true, value = "1:购买单 2:出售单", paramType = "query", dataType = "Integer")
    public ResultVo getOrderDetail(@RequestParam("id") Integer id, @RequestParam("type") Integer type) {
        return transactionApiService.getOrderDetail(id, type);
    }

    @ApiOperation(value = "发布委托")
    @RequestMapping(value = "publishDelegation", method = RequestMethod.POST)
    public ResultVo publishDelegation(@RequestBody BBOrderIn in) {
        return transactionApiService.publishDelegation(in);
    }

    @ApiOperation(value = "查询我的委托列表", response = BBDelegationQueryVo.class)
    @RequestMapping(value = "queryDelegationList", method = RequestMethod.POST)
    public ResultVo queryDelegationList(@RequestBody PageParamIn paramIn) {
        return transactionApiService.queryDelegationList(paramIn);
    }

    @ApiOperation(value = "查询我的委托单", response = BBDelegationQueryVo.class)
    @RequestMapping(value = "getDelegation", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", required = true, value = "1:求购单 2:出售单", paramType = "query", dataType = "Integer")
    public ResultVo getDelegation(@RequestParam("id") Integer id, @RequestParam("type") Integer type) {
        return transactionApiService.getDelegation(id, type);
    }

    @ApiOperation(value = "撤销委托", response = BBDelegationQueryVo.class)
    @RequestMapping(value = "cancelDelegation", method = RequestMethod.GET)
    public ResultVo cancelDelegation(@RequestParam("id") Integer id, @RequestParam("type") Integer type) {
        return transactionApiService.cancelDelegation(id, type);
    }

    @ApiOperation(value = "BZW认购列表", response = SubscriptionVo.class)
    @RequestMapping(value = "querySubscription", method = RequestMethod.GET)
    public ResultVo querySubscription() {
        return client.querySysSubscription();
    }

    @ApiOperation(value = "处理BZW认购", response = SubscriptionVo.class)
    @RequestMapping(value = "handleSubscription", method = RequestMethod.POST)
    public ResultVo handleSubscription(@RequestBody SubscriptionIn in) {
        return transactionApiService.handleSubscription(in);
    }

}
