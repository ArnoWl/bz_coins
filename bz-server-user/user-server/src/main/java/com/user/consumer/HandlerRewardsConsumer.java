package com.user.consumer;

import com.common.exception.BaseException;
import com.user.model.OrderRewardDto;
import com.user.service.IInvitationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.common.constatns.ThirdConstants.QUEUES_ORDER_REWARD;

/**
 * @Author: lt
 * @Date: 2020/8/4 21:52
 */
@Slf4j
@Component
public class HandlerRewardsConsumer {

    @Resource
    private IInvitationService iInvitationService;

    /**
     * 下单返送奖励
     *
     * @return
     */
    @RabbitListener(queues = QUEUES_ORDER_REWARD)
    @RabbitHandler
    public void orderReward(OrderRewardDto rewardDto) {
        try {
            long startTime = System.currentTimeMillis();
            log.info("收到订单返送奖励消息:" + rewardDto.getOrderCode());
            iInvitationService.buySuccessReferralRewards(rewardDto);
            log.info("处理完成订单 : "+ rewardDto.getOrderCode() + "耗时：" + (System.currentTimeMillis() - startTime));
        } catch (Exception e) {
            throw new BaseException("处理下单返送奖励失败",e);
        }
    }

}
