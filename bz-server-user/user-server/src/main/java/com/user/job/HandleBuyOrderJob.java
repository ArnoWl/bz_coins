package com.user.job;

import com.common.base.ResultVo;
import com.common.exception.BaseException;
import com.mall.feign.admin.BaseClient;
import com.mall.model.admin.vo.CoinKlineVo;
import com.user.service.IInfoService;
import com.user.service.IOrderInfoService;
import com.user.service.IWalletInfoService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

import static com.common.constatns.ThirdConstants.*;

/**
 * 任务调度：处理买入订单
 *
 * @Author: lt
 * @Date: 2020/7/18 14:29
 */
@Slf4j
@Component
public class HandleBuyOrderJob {

    @Resource
    private IOrderInfoService iOrderInfoService;
    @Resource
    private BaseClient baseClient;
    @Resource
    private IInfoService iInfoService;

    /**
     * 买入订单结算  60S后
     *
     * @return
     */
    @XxlJob(BUY_ORDER_SETTLEMENT)
    public ReturnT<String> buyOrderSettlement(String param) {
        ResultVo resultVo = iOrderInfoService.settlementOrder(param);
        if (!resultVo.isSuccess()) {
            throw new BaseException(resultVo.getErrorMessage());
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 定时更新订单预测价格 20S每次
     *
     * @param param
     * @return
     */
    @XxlJob(ORDER_PER_SETTLEMENT)
    @Transactional(rollbackFor = Exception.class)
    public ReturnT<String> orderPreSettlement(String param) {
        try {
            List<CoinKlineVo> coinKlineVos = baseClient.queryCoinkline();
            if (coinKlineVos != null) {
                for (CoinKlineVo c : coinKlineVos) {
                    iOrderInfoService.handleBudgetOrder(c.getCoinSymbol());
                }
            }
        } catch (Exception e) {
            throw new BaseException("处理定时更新订单预测价格失败",e);
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 包赔场晚上定时结算  11：30
     *
     * @param param
     * @return
     */
    @XxlJob(SETTLEMENT_OF_CLAIMS_SETTLEMENT)
    public ReturnT<String> settlementOfClaimsSettlement(String param) {
        iOrderInfoService.handleIndemnityOrder();
        return ReturnT.SUCCESS;
    }

    /**
     * v8级别会员 每日分红  1：30
     *
     * @param param
     * @return
     */
    @XxlJob(V8_EVERY_SHARE)
    public ReturnT<String> v8EveryShare(String param) {
        iInfoService.handleV8EveryShare();
        return ReturnT.SUCCESS;
    }
}
