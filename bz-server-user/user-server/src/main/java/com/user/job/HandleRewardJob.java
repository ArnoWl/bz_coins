package com.user.job;

import com.common.base.ResultVo;
import com.common.exception.BaseException;
import com.user.service.IRewardAccountService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.common.constatns.ThirdConstants.HANDLE_REWARD_CHANGE;

/**
 * 任务调度：处理买入订单
 *
 * @Author: lt
 * @Date: 2020/7/18 14:29
 */
@Slf4j
@Component
public class HandleRewardJob {

    @Resource
    private IRewardAccountService iRewardAccountService;

    /**
     * 处理收益划转到USDT 晚上12点整
     *
     * @return
     */
    @XxlJob(HANDLE_REWARD_CHANGE)
    public ReturnT<String> handleRewardChange(String param) {
        ResultVo resultVo = iRewardAccountService.handleRewardChange();
        if (!resultVo.isSuccess()) {
            throw new BaseException(resultVo.getErrorMessage());
        }
        return ReturnT.SUCCESS;
    }

}
