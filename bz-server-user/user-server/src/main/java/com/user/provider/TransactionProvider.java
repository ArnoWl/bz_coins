package com.user.provider;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.lang.StringUtils;
import com.google.common.collect.Maps;
import com.mall.feign.user.TransactionClient;
import com.mall.model.user.dto.*;
import com.mall.model.user.vo.*;
import com.user.service.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 用户信息服务提供
 *
 * @Author: lt
 * @Date: 2020/3/18 14:37
 */
@RestController
@RequestMapping(value = "trans")
public class TransactionProvider implements TransactionClient {

    @Resource
    private IBbBuyService iBbBuyService;
    @Resource
    private IBbSaleService iBbSaleService;
    @Resource
    private IBbSaleDetailService iBbSaleDetailService;
    @Resource
    private IBbBuyDetailService iBbBuyDetailService;
    @Resource
    private ISubLogsService subLogsService;

    @Override
    public ResultVo queryBBBuyOrder(BBOrderDto bbOrderDto) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("payCode", bbOrderDto.getPayCode());
            params.put("nickName", bbOrderDto.getNickName());
            params.put("realName", bbOrderDto.getRealName());
            params.put("phone", bbOrderDto.getPhone());
            params.put("invitationCode", bbOrderDto.getInvitationCode());
            if (StringUtils.isNoneBlank(bbOrderDto.getQueryTime())) {
                String[] split = bbOrderDto.getQueryTime().split("~");
                if (split != null && split.length == 2) {
                    params.put("startTime", split[0]);
                    params.put("endTime", split[1]);
                }
            }
            Page<BBOrderPlatQueryVo> page = new Page<>(bbOrderDto.getPageNo(), bbOrderDto.getPageSize());
            iBbSaleService.queryPlatBbSalePageByParams(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取币币交易我要买列表失败", e);
        }
    }

    @Override
    public ResultVo queryBBSaleOrder(BBOrderDto bbOrderDto) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("payCode", bbOrderDto.getPayCode());
            params.put("nickName", bbOrderDto.getNickName());
            params.put("realName", bbOrderDto.getRealName());
            params.put("phone", bbOrderDto.getPhone());
            params.put("invitationCode", bbOrderDto.getInvitationCode());
            if (StringUtils.isNoneBlank(bbOrderDto.getQueryTime())) {
                String[] split = bbOrderDto.getQueryTime().split("~");
                if (split != null && split.length == 2) {
                    params.put("startTime", split[0]);
                    params.put("endTime", split[1]);
                }
            }
            Page<BBOrderPlatQueryVo> page = new Page<>(bbOrderDto.getPageNo(), bbOrderDto.getPageSize());
            iBbBuyService.queryPlatBbBuyPageByParams(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取币币交易我要卖列表失败", e);
        }
    }

    @Override
    public ResultVo queryBBBuyOrderDetails(BBOrderDetailsDto bbOrderDetailsDto) {

        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("payCode", bbOrderDetailsDto.getPayCode());
            params.put("nickName", bbOrderDetailsDto.getNickName());
            params.put("phone", bbOrderDetailsDto.getPhone());
            params.put("realName", bbOrderDetailsDto.getRealName());
            if (StringUtils.isNotBlank(bbOrderDetailsDto.getQueryTime())) {
                String[] split = bbOrderDetailsDto.getQueryTime().split("~");
                if (split != null && split.length == 2) {
                    params.put("startTime", split[0]);
                    params.put("endTime", split[1]);
                }
            }
            Page<BBOrderDetailPlatQueryVo> page = new Page<>(bbOrderDetailsDto.getPageNo(), bbOrderDetailsDto.getPageSize());
            iBbSaleDetailService.queryPlatBbSaleDetailsPageByParams(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取币币交易我要买列表详情失败", e);
        }

    }

    @Override
    public ResultVo queryBBSaleOrderDetails(BBOrderDetailsDto bbOrderDetailsDto) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("payCode", bbOrderDetailsDto.getPayCode());
            params.put("nickName", bbOrderDetailsDto.getNickName());
            params.put("phone", bbOrderDetailsDto.getPhone());
            params.put("realName", bbOrderDetailsDto.getRealName());
            if (StringUtils.isNotBlank(bbOrderDetailsDto.getQueryTime())) {
                String[] split = bbOrderDetailsDto.getQueryTime().split("~");
                if (split != null && split.length == 2) {
                    params.put("startTime", split[0]);
                    params.put("endTime", split[1]);
                }
            }
            Page<BBOrderDetailPlatQueryVo> page = new Page<>(bbOrderDetailsDto.getPageNo(), bbOrderDetailsDto.getPageSize());
            iBbBuyDetailService.queryPlatBbBuyDetailsPageByParams(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取币币交易我要卖列表详情失败", e);
        }
    }

    @Override
    public ResultVo queryUserSubLogs(SubscriptionDetailDto detailDto) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("invitationCode", detailDto.getInvitationCode());
        params.put("realName", detailDto.getRealName());
        params.put("phone", detailDto.getPhone());
        Page<SubscriptionDetailVo> page=new Page<>(detailDto.getPageNo(),detailDto.getPageSize());
        subLogsService.queryUserSubLogs(page,params);
        page.getRecords().forEach(log->{
            log.setNum(CalcUtils.div(log.getMoney(),log.getPrice(),2));
        });
        return ResultVo.success(page);
    }
}
