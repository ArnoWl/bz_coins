package com.user.provider;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.ExtBeanUtils;
import com.common.utils.QrCodeUtil;
import com.common.utils.codec.CodeUtils;
import com.common.utils.lang.NumberUtils;
import com.common.utils.lang.StringUtils;
import com.google.common.collect.Maps;
import com.mall.feign.auth.LoginAccountClient;
import com.mall.feign.user.UserInfoClient;
import com.mall.model.user.dto.*;
import com.mall.model.user.vo.*;
import com.mall.model.wallet.dto.WalletInfoDto;
import com.user.entity.*;
import com.user.service.*;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.common.constatns.ThirdConstants.QUEUES_GENERATE_WALLET;
import static com.common.constatns.UserConstants.UP_LEVEL_TYPE_2;

/**
 * 用户信息服务提供
 *
 * @Author: lt
 * @Date: 2020/3/18 14:37
 */
@RestController
@RequestMapping(value = "info")
public class UserInfoProvider implements UserInfoClient {

    @Resource
    private IInfoService iInfoService;
    @Resource
    private IInvitationService iInvitationService;
    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private IWalletLogsService iWalletLogsService;
    @Resource
    private IWalletFrozenLogsService iWalletFrozenLogsService;
    @Resource
    private IMessageService iMessageService;
    @Resource
    private ILevelInfoService iLevelInfoService;
    @Resource
    private LoginAccountClient loginAccountClient;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private AmqpTemplate amqpTemplate;
    @Resource
    private IRewardAccountService iRewardAccountService;
    @Resource
    private IWalletInfoService walletInfoService;
    @Resource
    private IInvitationService invitationService;
    @Resource
    private IOrderInfoService orderInfoService;
    @Resource
    private IUpLevelLogsService iUpLevelLogsService;
    @Resource
    private IRewardLogsService rewardLogsService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo createWallet(String uid, List<WalletInfoDto> walletInfoDtoList) {
        try {
            QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", uid);
            if (iWalletInfoService.count(wrapper) > 0) {
                return ResultVo.success();
            }
            // 遍历已经生成的钱包地址信息添加用户钱包账户
            walletInfoDtoList.forEach(w -> {
                WalletInfo wallet = new WalletInfo();
                BeanUtils.copyProperties(w, wallet);
                wallet.setUid(uid);
                wallet.setWalletPassWord(uid);
                wallet.setWalletDeclimal(w.getAccuracy());
                if (StringUtils.isNotBlank(w.getWalletEcr20Addr())) {
                    wallet.setWalletAddr(w.getWalletEcr20Addr());
                    wallet.setWalletPath(w.getWalletEcr20Path());
                    wallet.setWalletQrUrl(QrCodeUtil.generalQRCode(wallet.getWalletAddr()));
                }
                if (StringUtils.isNotBlank(w.getWalletOminAddr())) {
                    wallet.setWalletAccount(uid);
                    wallet.setWalletOmniQrUrl(QrCodeUtil.generalQRCode(w.getWalletOminAddr()));
                }
                iWalletInfoService.save(wallet);
            });
            // 生成钱包后赠送用户BZW
            BigDecimal bzw = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.SYS_KEY + RedisKeyConstants.KEY_REGISTER_BZW));
            WalletLogDto wallLogDto = new WalletLogDto(uid, SysCoinsTypeEnums.BZW.getName(), CodeUtils.generateOrderNo(), WalletLogTypeEnums.REGISTERED_BZW.getTypeId(), UserConstants.WALLET_IN, bzw, WalletLogTypeEnums.REGISTERED_BZW.getName());
            ResultVo resultVo = iWalletInfoService.handleUserFrozenWalletMoney(wallLogDto);
            if (!resultVo.isSuccess()) {
                throw new BaseException("赠送BZW失败");
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException(e.getMessage(), e);
        }
    }

    @Override
    public ResultVo queryUserLevel() {
        return iLevelInfoService.queryUserLevel();
    }

    @Override
    public ResultVo updateUserLevel(UserLevelDto userLevelDto) {
        try {
            LevelInfo info = new LevelInfo();
            BeanUtils.copyProperties(userLevelDto, info);
            iLevelInfoService.updateById(info);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("更新用户等级失败", e);
        }
    }

    @Override
    public ResultVo getUserLevel(Integer id) {
        LevelInfo levelInfo = iLevelInfoService.getById(id);
        UserLevelVo levelVo = new UserLevelVo();
        BeanUtils.copyProperties(levelInfo, levelVo);
        return ResultVo.success(levelVo);
    }

    @Override
    public ResultVo updateUserInfo(UpdateUserDto updateUserDto) {
        try {
            QueryWrapper<Info> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", updateUserDto.getUid());
            Info info = iInfoService.getOne(wrapper);
            if (info == null) {
                return ResultVo.failure("当前用户系统中不存在");
            }
            if(updateUserDto.getLevelId() != null && !updateUserDto.getLevelId().equals(info.getLevelId())){
                UpLevelLogs levelLogs = new UpLevelLogs();
                levelLogs.setUid(info.getUid());
                levelLogs.setOldLevel(info.getLevelId());
                levelLogs.setNewLevel(updateUserDto.getLevelId());
                levelLogs.setType(UP_LEVEL_TYPE_2);
                iUpLevelLogsService.save(levelLogs);
            }
            BeanUtils.copyProperties(updateUserDto, info);
            if (!iInfoService.updateById(info)) {
                throw new BaseException("修改用户信息失败");
            }
            if (StringUtils.isNotBlank(updateUserDto.getLoginPassword())) {
                ResultVo resultVo = loginAccountClient.updateUserPassword(updateUserDto.getUid(), updateUserDto.getLoginPassword());
                if (!resultVo.isSuccess()) {
                    throw new BaseException(resultVo.getErrorMessage());
                }
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("修改用户信息失败", e);
        }
    }

    @Override
    public ResultVo queryUserInfo(UserInfoQueryDto userInfoQueryDto) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("phone", userInfoQueryDto.getPhone());
            params.put("levelId", userInfoQueryDto.getLevelId());
            params.put("nickName", userInfoQueryDto.getNickName());
            params.put("realName", userInfoQueryDto.getRealName());
            params.put("invitationCode", userInfoQueryDto.getInvitationCode());
            params.put("status", userInfoQueryDto.getStatus());
            if (StringUtils.isNoneBlank(userInfoQueryDto.getQueryTime())) {
                String[] split = userInfoQueryDto.getQueryTime().split("~");
                if (split.length == 2) {
                    params.put("startTime", split[0]);
                    params.put("endTime", split[1]);
                }
            }
            if (StringUtils.isNoneBlank(userInfoQueryDto.getVerifiedTime())) {
                String[] split = userInfoQueryDto.getVerifiedTime().split("~");
                if (split.length == 2) {
                    params.put("startVerTime", split[0]);
                    params.put("endVerTime", split[1]);
                }
            }
            Page<UserVo> page = new Page<>(userInfoQueryDto.getPageNo(), userInfoQueryDto.getPageSize());
            iInfoService.queryUserInfoPageByParams(params, page);
            page.getRecords().forEach(info -> {
                QueryWrapper<WalletInfo> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("uid", info.getUid());
                List<WalletInfo> list = walletInfoService.list(queryWrapper);
                List<WalletVo> walletVoList = ExtBeanUtils.copyList(list, WalletVo.class);
                info.setList(walletVoList);
            });
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取用户列表失败", e);
        }
    }

    @Override
    public ResultVo queryUserWallet(UserWalletQueryDto userWalletQueryDto) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("realName", userWalletQueryDto.getRealName());
            params.put("nickName", userWalletQueryDto.getNickName());
            params.put("phoneAccount", userWalletQueryDto.getPhoneAccount());
            params.put("levelId", userWalletQueryDto.getLevelId());
            params.put("coinSymbol", userWalletQueryDto.getCoinSymbol());
            if (StringUtils.isNoneBlank(userWalletQueryDto.getQueryMoney())) {
                String[] split = userWalletQueryDto.getQueryMoney().split("~");
                if (split.length == 2) {
                    params.put("startMoney", split[0]);
                    params.put("endMoney", split[1]);
                }
            }
            Page<UserWalletQueryVo> page = new Page<>(userWalletQueryDto.getPageNo(), userWalletQueryDto.getPageSize());
            iWalletInfoService.queryUserWalletPageByParams(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取用户钱包列表失败", e);
        }
    }

    @Override
    public ResultVo queryUserWalletLog(UserWalletLogQueryDto userWalletLogQueryDto) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("uid", userWalletLogQueryDto.getUid());
            params.put("inOut", userWalletLogQueryDto.getInOut());
            params.put("coinSymbol", userWalletLogQueryDto.getCoinSymbol());
            params.put("payCode", userWalletLogQueryDto.getPayCode());
            params.put("phone", userWalletLogQueryDto.getPhone());
            params.put("nickName", userWalletLogQueryDto.getNickName());
            params.put("realName", userWalletLogQueryDto.getRealName());
            params.put("typeId", userWalletLogQueryDto.getTypeId());
            if (StringUtils.isNoneBlank(userWalletLogQueryDto.getQueryTime())) {
                String[] split = userWalletLogQueryDto.getQueryTime().split("~");
                if (split.length == 2) {
                    params.put("startTime", split[0]);
                    params.put("endTime", split[1]);
                }
            }
            if (StringUtils.isNoneBlank(userWalletLogQueryDto.getQueryMoney())) {
                String[] split = userWalletLogQueryDto.getQueryMoney().split("~");
                if (split.length == 2) {
                    params.put("startMoney", split[0]);
                    params.put("endMoney", split[1]);
                }
            }
            Page<UserWalletLogQueryVo> page = new Page<>(userWalletLogQueryDto.getPageNo(), userWalletLogQueryDto.getPageSize());
            iWalletLogsService.queryUserWalletLogPageByParams(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("获取用户钱包记录失败", e);
        }
    }

    @Override
    public ResultVo queryUserWalletFrozenLog(UserWalletFrozenLogQueryDto userWalletFrozenLogQueryDto) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("uid", userWalletFrozenLogQueryDto.getUid());
            params.put("nickName", userWalletFrozenLogQueryDto.getNickName());
            params.put("realName", userWalletFrozenLogQueryDto.getRealName());
            params.put("phone", userWalletFrozenLogQueryDto.getPhone());
            if (StringUtils.isNoneBlank(userWalletFrozenLogQueryDto.getQueryTime())) {
                String[] split = userWalletFrozenLogQueryDto.getQueryTime().split("~");
                if (split != null && split.length == 2) {
                    params.put("startTime", split[0]);
                    params.put("endTime", split[1]);
                }
            }
            Page<UserWalletFrozenLogQueryVo> page = new Page<>(userWalletFrozenLogQueryDto.getPageNo(), userWalletFrozenLogQueryDto.getPageSize());
            iWalletFrozenLogsService.queryUserWalletFrozenLogPageByParams(params, page);
            return ResultVo.success(page);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException("获取用户钱包冻结记录操作明细失败", e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo createUser(CreateUserDto userDto) {
        try {
            // 创建用户信息
            Info info = new Info();
            info.setUid(userDto.getUid());
            info.setPayPassword(userDto.getPayPassword());
            info.setCode("BZ" + CodeUtils.getNumCode(6));
            info.setNickName("用户" + info.getCode());
            String shareDomain = redisTemplate.opsForValue().get(RedisKeyConstants.SHARE_DOMAIN);
            String shareUrl = shareDomain + "/#/pages/loginReg/registration?invitationCode=" + info.getCode();
            info.setQrCodeInfo(QrCodeUtil.generalQRCode(shareUrl));
            // 添加邀请关系链
            ResultVo resultVo = iInvitationService.addUserInvitation(userDto.getInvitationCode(), info.getUid());
            if (!resultVo.isSuccess()) {
                return resultVo;
            }
            info.setInvitationCode(userDto.getInvitationCode());
            if (!iInfoService.save(info)) {
                throw new BaseException("执行添加用户基础信息失败");
            }
            //添加用户收益钱包
            RewardAccount rewardAccount = new RewardAccount();
            rewardAccount.setUid(userDto.getUid());
            if (!iRewardAccountService.save(rewardAccount)) {
                throw new BaseException("添加用户收益钱包失败");
            }
            // 创建用户钱包信息
            amqpTemplate.convertAndSend(QUEUES_GENERATE_WALLET, info.getUid());
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("用户注册失败", e);
        }
    }

    @Override
    public ResultVo validateUserByUid(String uid) {
        QueryWrapper<Info> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", uid);
        Info info = iInfoService.getOne(wrapper);
        if (info == null) {
            return ResultVo.failure("用户信息不存在或已被删除");
        }
        if (!info.getDelFlag().equals(CommonConstants.DEL_FLAG_0)) {
            return ResultVo.failure("用户信息已失效");
        }
        if (info.getStatus().equals(UserConstants.USER_STATUS_1)) {
            return ResultVo.failure("用户被禁用");
        }
        return ResultVo.success();
    }

    /**
     * 处理保存用户钱包金额
     *
     * @param wallLogDto
     * @return
     */
    @Override
    public ResultVo handleUserWalletMoney(WalletLogDto wallLogDto) {
        return iWalletInfoService.handleUserWalletMoney(wallLogDto);
    }


    /**
     * 处理保存用户钱包金额
     *
     * @param wallLogDto
     * @return
     */
    @Override
    public ResultVo handleUserWalletMoneyMore(List<WalletLogDto> wallLogDto) {
        for(WalletLogDto w:wallLogDto){
           ResultVo r=  iWalletInfoService.handleUserWalletMoney(w);
           if(!r.isSuccess()){
               throw new BaseException(r.errorMessage);
           }
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo handleUserFrozenWalletMoney(WalletLogDto wallLogDto) {
        return iWalletInfoService.handleUserFrozenWalletMoney(wallLogDto);
    }

    /**
     * 根据ERC20钱包地址获取用户钱包账户信息
     *
     * @param walletErc20Addr
     * @return
     */
    @Override
    public WalletInfoVo getERC20WalletByAddr(String walletErc20Addr) {
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("wallet_addr", walletErc20Addr);
        WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
        WalletInfoVo walletInfoVo = null;
        if (walletInfo != null) {
            walletInfoVo = new WalletInfoVo();
            BeanUtils.copyProperties(walletInfo, walletInfoVo);
        }
        return walletInfoVo;
    }

    /**
     * 根据OMNI钱包地址获取用户钱包账户信息
     *
     * @param walletOmniAddr
     * @return
     */
    @Override
    public WalletInfoVo getOMNIWalletByAddr(String walletOmniAddr) {
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("wallet_omin_addr", walletOmniAddr);
        WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
        WalletInfoVo walletInfoVo = null;
        if (walletInfo != null) {
            walletInfoVo = new WalletInfoVo();
            BeanUtils.copyProperties(walletInfo, walletInfoVo);
        }
        return walletInfoVo;
    }

    @Override
    public ResultVo saveUserMessage(String uid, String title, String content) {
        try {
            Message message = new Message();
            message.setUid(uid);
            message.setTitle(title);
            message.setContent(content);
            iMessageService.save(message);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("发送用户站内信失败", e);
        }
    }

    @Override
    public ResultVo queryUserMessage(UserMessageQueryDto userMessageQueryDto) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("nickName", userMessageQueryDto.getNickName());
        params.put("realName", userMessageQueryDto.getRealName());
        params.put("phone", userMessageQueryDto.getPhone());
        params.put("reply", userMessageQueryDto.getReply());
        if (StringUtils.isNoneBlank(userMessageQueryDto.getQueryTime())) {
            String[] split = userMessageQueryDto.getQueryTime().split("~");
            if (split.length == 2) {
                params.put("startTime", split[0]);
                params.put("endTime", split[1]);
            }
        }
        Page<UserMessageQueryVo> page = new Page<>(userMessageQueryDto.getPageNo(), userMessageQueryDto.getPageSize());
        iMessageService.queryUserMessagePageByParams(params, page);
        return ResultVo.success(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo handleUserMessageReply(UserMessageReplyDto replyDto) {
        Message message = iMessageService.getById(replyDto.getId());
        if (message == null) {
            return ResultVo.failure("信息已删除");
        }
        if (!message.getReply().equals(UserConstants.REPLY_STATUS_1)) {
            return ResultVo.failure("信息已回复,不可重复回复");
        }
        try {
            message.setReply(UserConstants.REPLY_STATUS_0);
            iMessageService.updateById(message);

            Message replyMessage = new Message();
            replyMessage.setUid(message.getUid());
            replyMessage.setTitle(message.getTitle());
            replyMessage.setType(UserConstants.MESSAGE_TYPE_2);
            replyMessage.setTargetId(message.getId());
            replyMessage.setContent(replyDto.getContent());
            iMessageService.save(replyMessage);
            return ResultVo.result(CommonConstants.E_000, "回复成功");
        } catch (Exception e) {
            throw new BaseException("回复失败");
        }
    }

    @Override
    public ResultVo getMessageContent(Integer id) {
        QueryWrapper<Message> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("target_id", id);
        Message message = iMessageService.getOne(queryWrapper);
        if (message == null) {
            return ResultVo.failure("该信件还未回复无法查看");
        }
        MessageReplyVo vo = new MessageReplyVo();
        BeanUtils.copyProperties(message, vo);
        return ResultVo.success(vo);
    }

    @Override
    public ResultVo queryUserTeam(UserTeamDto dto) {
        try {
            Page<TeamOrderVo> page = new Page<>(dto.getPageNo(), dto.getPageSize());
            invitationService.queryTeamMemberOrders(page, dto.getUid());
            return ResultVo.success(page);
        } catch (Exception e) {
            throw new BaseException("查询会员团队异常");
        }

    }

    @Override
    public ResultVo getMyTeamPerformance(String uid) {
        return orderInfoService.getMyTeamPerformance(uid);
    }

    @Override
    public ResultVo getTeamLevel(Integer id) {
        List<Map<String, Object>> teamLevel = invitationService.getTeamLevel(id);
        return ResultVo.success(teamLevel);
    }

    @Override
    public ResultVo queryTeamLevel(Integer id, Integer levelid) {
        List<UserVo> list = invitationService.queryTeamLevel(id, levelid);
        list.forEach(info -> {
            QueryWrapper<WalletInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("uid", info.getUid());
            List<WalletInfo> walletInfos = walletInfoService.list(queryWrapper);
            List<WalletVo> walletVoList = ExtBeanUtils.copyList(walletInfos, WalletVo.class);
            info.setList(walletVoList);
        });
        return ResultVo.success(list);
    }

    @Override
    public ResultVo queryUserUpLog(UserUpLogDto dto) {
        Map<String,String> map =new HashMap<>();
        map.put("nickName",dto.getNickName());
        map.put("realName",dto.getRealName());
        map.put("phone",dto.getPhone());
        map.put("uid",dto.getUid());
        if(StringUtils.isNotBlank(dto.getQueryTime())){
            String[] split = dto.getQueryTime().split("~");
            if(split.length==2){
                map.put("startTime",split[0]);
                map.put("endTime",split[1]);
            }
        }
        Page<UserUpLogVo> page=new Page<>(dto.getPageNo(),dto.getPageSize());
        iUpLevelLogsService.queryList(page,map);
        return ResultVo.success(page);
    }

    @Override
    public List<WalletRechargeDto> getRechargeByCoinSymbol(String coinSymbol) {
        return iWalletInfoService.getRechargeByCoinSymbol(coinSymbol);
    }

    @Override
    public ResultVo queryReward(UserRewardDto rewardDto) {
        Map<String,String> map =new HashMap<>();
        map.put("nickName",rewardDto.getNickName());
        map.put("realName",rewardDto.getRealName());
        map.put("phone",rewardDto.getPhone());
        map.put("typeId",rewardDto.getTypeId());
        if(StringUtils.isNotBlank(rewardDto.getQueryTime())){
            String[] split = rewardDto.getQueryTime().split("~");
            if(split.length==2){
                map.put("startTime",split[0]);
                map.put("endTime",split[1]);
            }
        }
        Page<UserRewardVo> page=new Page<>(rewardDto.getPageNo(),rewardDto.getPageSize());
        rewardLogsService.queryList(page,map);
        return ResultVo.success(page);
    }

}
