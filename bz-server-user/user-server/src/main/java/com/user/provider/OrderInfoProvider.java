package com.user.provider;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.feign.user.OrderInfoClient;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.BuyOrderSumVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import com.user.service.IOrderInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;

/**
 * 买入订单服务提供者
 * @Author: lt
 * @Date: 2020/7/26 15:46
 */
@RestController
@RequestMapping("order")
public class OrderInfoProvider implements OrderInfoClient {

    @Resource
    private IOrderInfoService iOrderInfoService;

    @Override
    public Page<BuyOrderListVo> getOrderListPage(GetBuyOrderListDto listDto) {
        return iOrderInfoService.getBuyOrderPage(listDto);
    }

}
