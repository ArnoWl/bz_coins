package com.user;

import com.auth.client.EnableAuthClient;
import com.common.job.XxlJobConfig;
import com.mall.aspect.WebLogAspect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.mall.feign")
@EnableAuthClient
@Import({WebLogAspect.class,XxlJobConfig.class})
public class UserServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServerApplication.class, args);
	}

}