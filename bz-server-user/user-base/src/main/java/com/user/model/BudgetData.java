package com.user.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by admin on 2020/8/6.
 */
@Data
public class BudgetData {

    /**
     * 结算方式1每一秒一个订单2每一秒订单数量大于一
     */
    private Integer settlementType;

    /**
     * 预计盈利率
     */
    private BigDecimal estimatedProfitability;
    /**
     * 预计亏损率
     */
    private BigDecimal estimatedLossRate;
    /**
     * 本组合的盈亏数据
     */
    private ToDayOrderAmount toDayOrderAmount;
    /**
     * 预结算价格 settlementType = 1时有值
     */
    private BigDecimal budgetPrice;

    /**
     * 组合的订单下标集合
     */
    private List<String> orderIndexList;

    private int situation;

    public BudgetData(Integer settlementType, BigDecimal estimatedProfitability, BigDecimal estimatedLossRate,
                      ToDayOrderAmount toDayOrderAmount, BigDecimal budgetPrice, List<String> orderIndexList, int situation) {
        super();
        this.settlementType = settlementType;
        this.estimatedProfitability = estimatedProfitability;
        this.estimatedLossRate = estimatedLossRate;
        this.toDayOrderAmount = toDayOrderAmount;
        this.budgetPrice = budgetPrice;
        this.orderIndexList = orderIndexList;
        this.situation = situation;
    }
}
