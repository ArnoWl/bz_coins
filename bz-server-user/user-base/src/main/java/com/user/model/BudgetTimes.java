package com.user.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 查询预处理的时间端
 * @author arno
 * @version 1.0
 * @date 2020-07-26
 */
@Data
public class BudgetTimes {

    private String uid;
    /**
     * 下单时间
     */
    private LocalDateTime createTime;

    /**
     * 订单数量
     */
    private Integer orderNum;

    /**
     * 下单金额
     */
    private BigDecimal money;

    /**
     * 下单类型1买涨 2买跌
     */
    private Integer buyType;

    /**
     * 买入点位
     */
    private BigDecimal openPrice;

    private Integer orderType;
}
