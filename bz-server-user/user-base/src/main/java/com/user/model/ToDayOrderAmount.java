package com.user.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 今日订单金额信息
 * @Author: lt
 * @Date: 2020/7/25 16:56
 */
@Data
public class ToDayOrderAmount implements Serializable {

    private static final long serialVersionUID = -2612701913457387623L;

    /** 今日买入订单金额 */
    private BigDecimal toDayBuyMoney;

    /** 今日订单收益(平台)=用户订单总额-订单结算总额 */
    private BigDecimal toDayIncome;

    /** 今日包赔场订单买入金额 */
    private BigDecimal toDayBuyMoneyBpc;

    /** 今日包赔场订单亏损金额 */
    private BigDecimal lossToday;

    public void setBudgetData(BigDecimal toDayBuyMoney, BigDecimal toDayIncome, BigDecimal toDayBuyMoneyBpc,BigDecimal lossToday){
        this.toDayBuyMoney = toDayBuyMoney == null ? BigDecimal.ZERO : toDayBuyMoney;
        this.toDayIncome = toDayIncome == null ? BigDecimal.ZERO : toDayIncome;
        this.toDayBuyMoneyBpc = toDayBuyMoneyBpc == null ? BigDecimal.ZERO : toDayBuyMoneyBpc;
        this.lossToday = lossToday == null ? BigDecimal.ZERO : lossToday;
    }

}
