package com.user.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 未结算的包赔场信息
 * @Author: lt
 * @Date: 2020/7/25 16:56
 */
@Data
public class SumBudgetAmount implements Serializable {

    private static final long serialVersionUID = -2612701913457387623L;

    private String uid;
    /** 未结算的包赔场总金额 */
    private BigDecimal totalMoney=BigDecimal.ZERO;

    /** 未结算的包赔场总盈亏 */
    private BigDecimal totalProfit=BigDecimal.ZERO;

}
