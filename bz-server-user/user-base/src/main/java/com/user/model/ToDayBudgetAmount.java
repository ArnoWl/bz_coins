package com.user.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 今日订单金额信息
 * @Author: lt
 * @Date: 2020/7/25 16:56
 */
@Data
public class ToDayBudgetAmount implements Serializable {

    private static final long serialVersionUID = -2612701913457387623L;

    private String uid;

    /** 今日包赔场订单买入金额 */
    private BigDecimal toDayBuyMoneyBpc;

    /** 今日包赔场订单亏损金额 */
    private BigDecimal lossToday;


}
