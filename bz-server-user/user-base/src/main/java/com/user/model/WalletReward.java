package com.user.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/30
 */
@Data
public class WalletReward {
    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "操作类型")
    private Integer typeId;

    @ApiModelProperty(value = "1 :今日")
    private Integer flag;

    private LocalDateTime createTime;

}
