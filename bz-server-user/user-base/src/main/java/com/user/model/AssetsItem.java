package com.user.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AssetsItem {

    @ApiModelProperty(value = "货币代码 类似USDT ETH BTC等")
    private String coinSymbol;

    @ApiModelProperty(value = "钱包余额")
    private BigDecimal walletBalance;

    @ApiModelProperty(value = "冻结金额")
    private BigDecimal walletFrozenBalance;

    @ApiModelProperty(value = "折合后金额")
    private BigDecimal convertMoney;
}