package com.user.model;

import com.user.entity.LevelInfo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 推荐人信息
 * @Author: lt
 * @Date: 2020/7/15 10:56
 */
@Data
public class ReferralRewardsInfo extends LevelInfo {

    private static final long serialVersionUID = -2828019134902371307L;

    /** 推荐人UID */
    private String uid;

    /** 个人业绩 */
    private BigDecimal money;

}
