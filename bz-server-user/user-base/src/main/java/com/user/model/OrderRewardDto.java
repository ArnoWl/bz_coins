package com.user.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: lt
 * @Date: 2020/8/4 21:55
 */
@Data
public class OrderRewardDto implements Serializable {

    private static final long serialVersionUID = -200163870663338308L;

    /** 下单用户UID */
    private String uid;

    /** 下单金额 */
    private BigDecimal money;

    /** 订单号 */
    private String orderCode;

}
