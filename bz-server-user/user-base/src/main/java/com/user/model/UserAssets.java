package com.user.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/27
 */
@Data
public class UserAssets implements Serializable {

    private static final long serialVersionUID = -2612701913457387623L;

    @ApiModelProperty(value = "总资产折合（USDT）")
    private BigDecimal totalUSDT;

    @ApiModelProperty(value = "总资产折合(CNY)")
    private BigDecimal totalMoney;

    @ApiModelProperty(value = "钱包列表")
    private List<AssetsItem> list;

}
