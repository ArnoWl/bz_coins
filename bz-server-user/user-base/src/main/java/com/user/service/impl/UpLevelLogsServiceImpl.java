package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserUpLogVo;
import com.user.entity.UpLevelLogs;
import com.user.mapper.UpLevelLogsMapper;
import com.user.service.IUpLevelLogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 用户升级记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-08-09
 */
@Service
public class UpLevelLogsServiceImpl extends ServiceImpl<UpLevelLogsMapper, UpLevelLogs> implements IUpLevelLogsService {

    @Override
    public Page<UserUpLogVo> queryList(Page<UserUpLogVo> page, Map<String, String> map) {
        return baseMapper.queryList(page,map);
    }
}
