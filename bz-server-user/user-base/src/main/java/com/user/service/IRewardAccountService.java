package com.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;
import com.mall.model.user.dto.WalletLogDto;
import com.user.entity.RewardAccount;

/**
 * <p>
 * 用户收益账户表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
public interface IRewardAccountService extends IService<RewardAccount> {

    /**
     * 处理用户奖励钱包
     *
     * @param wallLogDto
     * @return
     */
    ResultVo handleUserUsdtRewardMoney(WalletLogDto wallLogDto);

    /**
     * 将收益自动划转到 USDT钱包
     * @return
     */
    ResultVo handleRewardChange();
}
