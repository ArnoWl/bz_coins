package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.SubscriptionDetailVo;
import com.user.entity.SubLogs;
import com.user.mapper.SubLogsMapper;
import com.user.service.ISubLogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户认购记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-08-02
 */
@Service
public class SubLogsServiceImpl extends ServiceImpl<SubLogsMapper, SubLogs> implements ISubLogsService {

    @Override
    public BigDecimal getTotalMoney(SubLogs subLogs) {
        return baseMapper.getTotalMoney(subLogs);
    }

    @Override
    public Page<SubscriptionDetailVo> queryUserSubLogs(Page<SubscriptionDetailVo> page, Map<String, Object> params) {
        return baseMapper.queryUserSubLogs(page,params);
    }
}
