package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserMessageQueryVo;
import com.user.entity.Message;
import com.user.mapper.MessageMapper;
import com.user.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 用户站内信 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-23
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {

    @Override
    public Page<UserMessageQueryVo> queryUserMessagePageByParams(Map<String, Object> params, Page<UserMessageQueryVo> page) {
        return baseMapper.queryUserMessagePageByParams(params,page);
    }
}
