package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.*;
import com.user.entity.BbBuyDetail;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户币币求购明细记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface IBbBuyDetailService extends IService<BbBuyDetail> {

    Page<BBDetailQueryVo> queryOrderList(Map<String, Object> params, Page<BBDetailQueryVo> page);

    BBDetailVo getOrderDetail(Map<String, Object> params);

    Page<BBOrderDetailPlatQueryVo> queryPlatBbBuyDetailsPageByParams(Map<String, Object> params, Page<BBOrderDetailPlatQueryVo> page);

}
