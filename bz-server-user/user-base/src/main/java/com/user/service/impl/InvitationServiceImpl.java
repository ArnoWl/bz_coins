package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.constatns.UserConstants;
import com.common.enums.user.UserLevelInfoEnum;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.codec.CodeUtils;
import com.common.utils.lang.StringUtils;
import com.google.common.collect.Maps;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.vo.TeamOrderVo;
import com.mall.model.user.vo.UserVo;
import com.user.entity.*;
import com.user.mapper.InvitationMapper;
import com.user.model.OrderRewardDto;
import com.user.model.ReferralRewardsInfo;
import com.user.service.*;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static com.common.constatns.UserConstants.*;

/**
 * <p>
 * 用户邀请关系表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-12
 */
@Slf4j
@Service
public class InvitationServiceImpl extends ServiceImpl<InvitationMapper, Invitation> implements IInvitationService {

    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private IInfoService iInfoService;
    @Resource
    private IOrderInfoService iOrderInfoService;
    @Resource
    private ILevelInfoService iLevelInfoService;
    @Resource
    private  IRewardAccountService iRewardAccountService;
    @Resource
    private IUpLevelLogsService iUpLevelLogsService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo addUserInvitation(String invitationCode, String beInvitedUid) {
        // 添加当前用户的邀请关系链
        Invitation invitation = new Invitation();
        invitation.setBeInvitedUid(beInvitedUid);
        if(StringUtils.isNotBlank(invitationCode)){
            // 根据邀请码查询邀请人信息
            QueryWrapper<Info> wrapper = new QueryWrapper<>();
            wrapper.eq("code",invitationCode);
            Info invitePeople = iInfoService.getOne(wrapper);
            if(invitePeople == null){
                return ResultVo.failure("邀请码有误");
            }
            // 查询邀请人邀请关系链
            QueryWrapper<Invitation> invitationWrapper = new QueryWrapper<>();
            invitationWrapper.eq("be_invited_uid", invitePeople.getUid());
            Invitation invitationChain = this.getOne(invitationWrapper);
            if(invitationChain == null){
                return ResultVo.failure("邀请人关系链信息丢失，请联系系统管理员");
            }
            invitation.setInvitationUid(invitePeople.getUid());
            invitation.setLevel(invitationChain.getLevel() + 1);
            invitation.setInvitationRelationship(invitationChain.getId() + "," + invitationChain.getInvitationRelationship());
        }else{
            invitation.setInvitationRelationship("0");
        }
        if(!this.save(invitation)){
            return ResultVo.failure("执行添加邀请关系链信息失败");
        }
        return ResultVo.success();
    }

    @Override
    public int getTeamMembers(String uid) {
        return baseMapper.countTeamMembers(uid);
    }

    @Override
    public List<TeamOrderVo> getTeamMemberOrders(String uid) {
        List<TeamOrderVo> list=baseMapper.selectTeamMemberOrders(uid);
        return list;
    }

    @Override
    public List<Map<String, Object>> getTeamLevel(Integer id){
        return baseMapper.getTeamLevel(id);
    }

    @Override
    public BigDecimal getTeamPerformanceByDay(String uid) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("uid",uid);
        params.put("dateStr","day");
        return baseMapper.countTeamPerformance(params);
    }

    @Override
    public BigDecimal getTeamPerformanceByMonth(String uid) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("uid",uid);
        params.put("dateStr","month");
        return baseMapper.countTeamPerformance(params);
    }

    @Override
    public void buySuccessReferralRewards(OrderRewardDto rewardDto) {
        // 触发用户自己升级
        this.userUpgrade(rewardDto.getUid());
        // 查询用户推荐上级信息
        List<ReferralRewardsInfo> rewardsInfos = baseMapper.selectRecommendList(rewardDto.getUid());
        // 团队奖励极差比例
        BigDecimal rangeRatio = BigDecimal.ZERO;
        // 团队奖励极差返还金额
        BigDecimal veryBadAmount = BigDecimal.ZERO;
        // 当前平级奖励等级
        Integer peerRewardLevel = null;
        for (ReferralRewardsInfo r : rewardsInfos) {
            if (r.getTeamScale() != null && r.getTeamScale().compareTo(BigDecimal.ZERO) > 0) {
                // 团队极差奖励
                if (rangeRatio.compareTo(r.getTeamScale()) != 0) {
                    if(r.getTeamScale().compareTo(rangeRatio)>0){
                        rangeRatio = r.getTeamScale().subtract(rangeRatio);
                        if (rangeRatio.compareTo(BigDecimal.ZERO) > 0) {
                            veryBadAmount = rewardDto.getMoney().multiply(rangeRatio);
                            ResultVo resultVo = iRewardAccountService.handleUserUsdtRewardMoney(new WalletLogDto(r.getUid(), SysCoinsTypeEnums.USDT.getName(), rewardDto.getOrderCode(),
                                    WalletLogTypeEnums.PUSH_REWARD.getTypeId(), WALLET_IN, veryBadAmount, "极差奖励"));
                            if (!resultVo.isSuccess()) {
                                throw new BaseException(resultVo.getErrorMessage());
                            }
                        }
                        rangeRatio = r.getTeamScale();
                    }
                } else {
                    if(peerRewardLevel != null && peerRewardLevel.equals(r.getId())){
                        continue;
                    }
                    // 触发平级奖励
                    if (r.getTeamCommonScale() != null && r.getTeamCommonScale().compareTo(BigDecimal.ZERO) > 0){
                        ResultVo resultVo = iRewardAccountService.handleUserUsdtRewardMoney(new WalletLogDto(r.getUid(), SysCoinsTypeEnums.USDT.getName(), rewardDto.getOrderCode(),
                                WalletLogTypeEnums.PUSH_REWARD.getTypeId(), WALLET_IN, veryBadAmount.multiply(r.getTeamCommonScale()), "平级奖励"));
                        if (!resultVo.isSuccess()) {
                            throw new BaseException(resultVo.getErrorMessage());
                        }
                        peerRewardLevel= r.getId();
                    }
                }
            }
            // 团队成员升级
            this.teamUpgrade(r.getUid(),r.getMoney(),r.getId());
        }
    }

    @Override
    public List<UserVo> queryTeamLevel(Integer id, Integer levelid) {
        return baseMapper.queryTeamLevel(id,levelid);
    }

    @Override
    public Page<TeamOrderVo> queryTeamMemberOrders(Page<TeamOrderVo> page, String uid) {
        return baseMapper.queryTeamMemberOrders(page,uid);
    }

    /**
     * 当前下单用户升级
     * @param uid  下单用户UID
     */
    private void userUpgrade(String uid){
        Info info = iInfoService.getById(uid);
        if(info == null){
            return;
        }
        //处理当前用户升级 判断用户如果是第一次下单就升级到V1 级别等级2
        if (info.getLevelId() == null || UserLevelInfoEnum.USER_LEVEL_1.getKey().equals(info.getLevelId())) {
            info.setLevelId(UserLevelInfoEnum.USER_LEVEL_2.getKey());
            if (!iInfoService.updateById(info)) {
                throw new BaseException("执行变更用户等级信息失败");
            }
            LevelInfo levelInfo = iLevelInfoService.getById(info.getLevelId());
            //处理赠送BZW
            handleSendBZW(levelInfo,info);
            return;
        }
        QueryWrapper<Invitation> wrapper = new QueryWrapper<>();
        wrapper.eq("be_invited_uid",uid);
        Invitation invitation = this.getOne(wrapper);
        this.teamUpgrade(info.getUid(),invitation.getMoney(),info.getLevelId());
    }

    /**
     * 团队升级
     * @param uid           团队成员UID
     * @param levelId       团队成员级别ID
     * @param money         个人业绩
     */
    private void teamUpgrade(String uid,BigDecimal money, Integer levelId){
        // 查询升级到下一级别需要满足的条件
        LevelInfo levelInfo = iLevelInfoService.getById(levelId + 1);
        if(levelInfo == null) {
            // 未能获取到更高等级信息
            return;
        }
        //判断当前持有USDT数量
        if(levelInfo.getMinMoney() != null && levelInfo.getMinMoney().compareTo(BigDecimal.ZERO) > 0){
            QueryWrapper<WalletInfo> wrapper=new QueryWrapper<>();
            wrapper.eq("uid",uid).eq("coin_symbol",SysCoinsTypeEnums.USDT.getName());
            WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
            if(walletInfo.getWalletBalance().compareTo(levelInfo.getMinMoney()) < 0){
                return;
            }
        }
        // 判断个人业绩
        if(levelInfo.getPersonalMoney() != null && levelInfo.getPersonalMoney().compareTo(BigDecimal.ZERO) > 0){
            if(money == null || money.compareTo(levelInfo.getPersonalMoney()) < 0){
                // 个人业绩不满足升级条件
                return;
            }
        }
        Map<String,Object> params = Maps.newHashMap();
        params.put("uid",uid);
        // 判断团队业绩
        if(levelInfo.getTeamMoney() != null && levelInfo.getTeamMoney().compareTo(BigDecimal.ZERO) > 0){
            BigDecimal teamPerformance = baseMapper.countTeamPerformance(params);
            if(teamPerformance == null || teamPerformance.compareTo(levelInfo.getTeamMoney()) < 0){
                // 团队业绩不满足升级条件
                return;
            }
        }
        // 判断直推V1人数
        if(levelInfo.getPushNum() != null && levelInfo.getPushNum() > 0){
            QueryWrapper<Invitation> wrapper = new QueryWrapper<>();
            wrapper.eq("invitation_uid",uid)
                    .exists("select id from user_info where user_info.uid = user_invitation.be_invited_uid and user_info.level_id > 1");
            int rows = this.count(wrapper);
            if(rows < levelInfo.getPushNum()){
                return;
            }
        }
        // 判断团队平级数量
        if(levelInfo.getTeamNum() != null && levelInfo.getTeamNum() > 0){
            // 再查询伞下平级数量
            params.put("levelId",levelId);
            Integer levelNumber = baseMapper.countTeamSameLevel(params);
            // 团队平级数量不满足升级条件
            if(levelNumber == null || levelNumber < levelInfo.getTeamNum()) {
                return;
            }
        }
        // 条件满足：提升用户等级
        Info info = new Info();
        info.setUid(uid);
        info.setLevelId(levelInfo.getId());
        if(!iInfoService.updateById(info)){
            throw new BaseException("执行变更用户等级信息失败");
        }
        // 添加用户级别变更记录
        UpLevelLogs levelLogs = new UpLevelLogs();
        levelLogs.setUid(info.getUid());
        levelLogs.setOldLevel(levelId);
        levelLogs.setNewLevel(info.getLevelId());
        levelLogs.setType(UP_LEVEL_TYPE_1);
        iUpLevelLogsService.save(levelLogs);
       //处理赠送BZW
        handleSendBZW(levelInfo,info);
    }

    /**
     * 处理赠送
     * @param levelInfo
     * @param info
     */
    public void handleSendBZW(LevelInfo levelInfo,Info info){
        if(levelInfo.getCoinNum().compareTo(BigDecimal.ZERO)>0){
            // 升级成功：赠送用户BZW
            ResultVo resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(info.getUid(), SysCoinsTypeEnums.BZW.getName(), CodeUtils.generateOrderNo(),
                    WalletLogTypeEnums.PUSH_REWARD.getTypeId(), WALLET_IN, levelInfo.getCoinNum(), "升级赠送"));
            if(!resultVo.isSuccess()){
                throw new BaseException(resultVo.getErrorMessage());
            }
        }
    }

}
