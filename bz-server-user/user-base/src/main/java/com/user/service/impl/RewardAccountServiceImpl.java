package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.constatns.UserConstants;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.codec.CodeUtils;
import com.mall.model.user.dto.WalletLogDto;
import com.user.entity.*;
import com.user.mapper.RewardAccountMapper;
import com.user.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 用户收益账户表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
@Service
public class RewardAccountServiceImpl extends ServiceImpl<RewardAccountMapper, RewardAccount> implements IRewardAccountService {


    @Resource
    private IRewardLogsService iRewardLogsService;
    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private IInfoService iInfoService;
    @Resource
    private ILevelInfoService iLevelInfoService;
    /**
     * 保存USDT收益明细
     * @param wallLogDto
     * @return
     */
    @Override
    public ResultVo handleUserUsdtRewardMoney(WalletLogDto wallLogDto) {
        try {
            // 获取钱包信息
            RewardAccount rewardAccount = getById( wallLogDto.getUid());
            if (rewardAccount == null) {
                rewardAccount=new RewardAccount();
                rewardAccount.setUid(wallLogDto.getUid());
                this.save(rewardAccount);
            }
            // 计算剩余额度
            if (UserConstants.WALLET_IN.equals(wallLogDto.getInOut())) {
                //如果是进账
                BigDecimal money = CalcUtils.add(rewardAccount.getUsdtReward(), wallLogDto.getCost(),4);
                rewardAccount.setUsdtReward(money);
            } else {
                BigDecimal money = CalcUtils.sub(rewardAccount.getUsdtReward(), wallLogDto.getCost(), 4);
                rewardAccount.setUsdtReward(money);
            }
            if (rewardAccount.getUsdtReward().compareTo(BigDecimal.ZERO) < 0) {
                return ResultVo.failure("收益金额不足");
            }
            if (!this.updateById(rewardAccount)) {
                throw new BaseException("更新收益钱包失败,频繁操作");
            }
            // 保存资金明细
            RewardLogs rewardLogs = new RewardLogs();
            BeanUtils.copyProperties(wallLogDto, rewardLogs);
            iRewardLogsService.save(rewardLogs);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional
    public ResultVo handleRewardChange() {
        QueryWrapper<RewardAccount> wrapper=new QueryWrapper<>();
        wrapper.gt("usdt_reward",BigDecimal.ZERO);
        List<RewardAccount> list = list(wrapper);
        for(RewardAccount l:list){
            //如果当前用户持币 没有满足当前会员级别的金额 不参与释放
            Info info=iInfoService.getById(l.getUid());
            LevelInfo levelInfo=iLevelInfoService.getById(info.getLevelId());
            QueryWrapper<WalletInfo> walletInfoQueryWrapper = new QueryWrapper<>();
            walletInfoQueryWrapper.eq("uid",l.getUid()).eq("coin_symbol",SysCoinsTypeEnums.USDT.name());
            WalletInfo walletInfo = iWalletInfoService.getOne(walletInfoQueryWrapper);
            if(walletInfo.getWalletBalance().compareTo(levelInfo.getMinMoney())<0){
                continue;
            }

            BigDecimal reward=l.getUsdtReward();
            //扣除收益 增加USDT钱包
            ResultVo result=handleUserUsdtRewardMoney(new WalletLogDto(l.getUid(), SysCoinsTypeEnums.USDT.getName(), CodeUtils.generateOrderNo(),
                    WalletLogTypeEnums.SETTLEMENT_REWARD.getTypeId(), UserConstants.WALLET_OUT, reward, "每日收益结算"));
            if(!result.isSuccess()){
                throw new BaseException(result.getErrorMessage());
            }

            result=iWalletInfoService.handleUserWalletMoney(new WalletLogDto(l.getUid(), SysCoinsTypeEnums.USDT.getName(), CodeUtils.generateOrderNo(),
                    WalletLogTypeEnums.SETTLEMENT_REWARD.getTypeId(), UserConstants.WALLET_IN, reward, "每日收益结算"));
            if(!result.isSuccess()){
                throw new BaseException(result.getErrorMessage());
            }
        }
        return ResultVo.success();
    }
}
