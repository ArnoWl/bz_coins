package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.BBDelegationQueryVo;
import com.mall.model.user.vo.BBDetailQueryVo;
import com.mall.model.user.vo.BBDetailVo;
import com.mall.model.user.vo.BBOrderDetailPlatQueryVo;
import com.user.entity.BbBuyDetail;
import com.user.mapper.BbBuyDetailMapper;
import com.user.service.IBbBuyDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 用户币币求购明细记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Service
public

class BbBuyDetailServiceImpl extends ServiceImpl<BbBuyDetailMapper, BbBuyDetail> implements IBbBuyDetailService {

    @Override
    public Page<BBDetailQueryVo> queryOrderList(Map<String, Object> params, Page<BBDetailQueryVo> page) {
        return baseMapper.queryOrderList(params,page);
    }

    @Override
    public BBDetailVo getOrderDetail(Map<String, Object> params) {
        return baseMapper.getOrderDetail(params);
    }

    @Override
    public Page<BBOrderDetailPlatQueryVo> queryPlatBbBuyDetailsPageByParams(Map<String, Object> params, Page<BBOrderDetailPlatQueryVo> page) {
        return baseMapper.queryPlatBbBuyDetailsPageByParams(params,page);
    }

}
