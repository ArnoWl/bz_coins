package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserWalletFrozenLogQueryVo;
import com.user.entity.WalletFrozenLogs;
import com.user.mapper.WalletFrozenLogsMapper;
import com.user.service.IWalletFrozenLogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 冻结钱包操作明细记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-28
 */
@Service
public class WalletFrozenLogsServiceImpl extends ServiceImpl<WalletFrozenLogsMapper, WalletFrozenLogs> implements IWalletFrozenLogsService {

    @Override
    public Page<UserWalletFrozenLogQueryVo> queryUserWalletFrozenLogPageByParams(Map<String, Object> params, Page<UserWalletFrozenLogQueryVo> page) {
        return baseMapper.queryUserWalletFrozenLogPageByParams(params,page);
    }
}
