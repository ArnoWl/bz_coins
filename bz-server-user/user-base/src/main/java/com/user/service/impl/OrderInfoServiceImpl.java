package com.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.config.RedisUtils;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.context.UserContext;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.ExtBeanUtils;
import com.common.utils.codec.CodeUtils;
import com.common.utils.lang.NumberUtils;
import com.google.common.collect.Maps;
import com.mall.feign.admin.BaseClient;
import com.mall.model.admin.vo.CoinSpreadVo;
import com.mall.model.admin.vo.OrderRuleVo;
import com.mall.model.market.dto.KlineInfo;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.vo.BZTotalVo;
import com.mall.model.user.vo.TeamPerformanceVo;
import com.user.entity.Invitation;
import com.user.entity.OrderInfo;
import com.user.entity.OrderOverview;
import com.user.entity.WalletInfo;
import com.user.mapper.OrderInfoMapper;
import com.user.model.*;
import com.user.service.IInvitationService;
import com.user.service.IOrderInfoService;
import com.user.service.IRewardLogsService;
import com.user.service.IWalletInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;

import static com.common.constatns.RedisKeyConstants.*;

/**
 * <p>
 * 订单信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
@Service
@Slf4j
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {

    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private IWalletInfoService iWalletInfoService;
    @Resource
    private BaseClient baseClient;
    @Resource
    private IInvitationService iInvitationService;
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private IRewardLogsService iRewardLogsService;

    @Override
    public ToDayOrderAmount getCountTodayOrderMoney() {
        //查询普通场
        ToDayOrderAmount orderAmount = baseMapper.sumToDayOrderAmount(UserConstants.ORDER_TYPE_1);
        //查询包赔场
        ToDayOrderAmount budgetOrderAmount = baseMapper.sumToDayBudgetOrderAmount(UserConstants.ORDER_TYPE_2);
        //包赔场盈利比例
        BigDecimal orderIndemnityScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_INDEMNITY_SCALE));

        orderAmount.setBudgetData(orderAmount.getToDayBuyMoney(),
                orderAmount.getToDayIncome(),budgetOrderAmount.getToDayBuyMoneyBpc(),CalcUtils.mul(budgetOrderAmount.getLossToday(),orderIndemnityScale,4));

        return orderAmount;
    }

    @Override
    public Page<BuyOrderListVo> getBuyOrderPage(GetBuyOrderListDto listDto) {
        /*QueryWrapper<OrderInfo> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(listDto.getUid()), "uid", listDto.getUid())
                .eq(StringUtils.isNotBlank(listDto.getCoinSymbol()), "coin_symbol", listDto.getCoinSymbol())
                .eq(listDto.getStatus() != null, "status", listDto.getStatus())
                .ge(listDto.getQueryTime() != null, "create_time", listDto.getQueryTime())
                .eq(listDto.getOrderType() != null, "order_type", listDto.getOrderType()).orderByDesc("id");*/
        Map<String, Object> params = Maps.newHashMap();
        params.put("coinSymbol", listDto.getCoinSymbol());
        params.put("invitationCode", listDto.getInvitationCode());
        params.put("orderType", listDto.getOrderType());
        params.put("phone", listDto.getPhone());
        params.put("queryTime", listDto.getQueryTime());
        params.put("realName",listDto.getRealName());
        params.put("status",listDto.getStatus());
        params.put("uid",listDto.getUid());
        Page<BuyOrderListVo> infoPage = new Page<>(listDto.getPageNo(), listDto.getPageSize());
        baseMapper.getBuyOrderPage(infoPage, params);
        // 组装出参
        Page<BuyOrderListVo> voPage = new Page<>();
        BeanUtils.copyProperties(infoPage, voPage);
        if (!CollectionUtils.isEmpty(infoPage.getRecords())) {
            voPage.setRecords(ExtBeanUtils.copyList(infoPage.getRecords(), BuyOrderListVo.class));
            for(BuyOrderListVo v: voPage.getRecords()){
                long endTime = v.getOverTime().toEpochSecond(ZoneOffset.of("+8"));
                long second=endTime - LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
                v.setSeconds(second <0L?0L:second);
            }
        }
        return voPage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo settlementOrder(String orderSn) {
        QueryWrapper<OrderInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("order_code", orderSn);
        OrderInfo orderInfo = this.getOne(wrapper);
        if (orderInfo == null) {
            return ResultVo.failure("订单不存在");
        }
        if (!UserConstants.BUY_ORDER_STATUS_1.equals(orderInfo.getStatus())) {
            return ResultVo.success();
        }
        // 从缓存中读取当前市场买入价
        String latestMarketStr = redisTemplate.opsForValue().get(RedisKeyConstants.LATEST_COIN_MARKET_KLINE + orderInfo.getCoinSymbol());
        if(StringUtils.isBlank(latestMarketStr)){
            return ResultVo.failure("最新行情信息丢失，结算失败");
        }
        KlineInfo newKline = JSONObject.toJavaObject(JSON.parseObject(latestMarketStr), KlineInfo.class);

        //如果订单已经有预测价格
        if (orderInfo.getBudgetPrice().compareTo(BigDecimal.ZERO) > 0) {
            //重新获取预测价
            if(orderInfo.getSignFlag().equals(UserConstants.SIGN_FLAG_1)){
                if(orderInfo.getOrderType() == 1)
                    orderInfo=getNewBudgetPrice(orderInfo,newKline);
                else
                    orderInfo=getNewBudgetPriceBp(orderInfo,newKline);
            }
            orderInfo.setClosePrice(orderInfo.getBudgetPrice());
        } else {
            orderInfo.setClosePrice(newKline.getClosePrice());
        }
        //设置下单价
        redisUtils.setOrderPrice(orderInfo.getCoinSymbol(),orderInfo.getClosePrice());
        if (UserConstants.ORDER_TYPE_1.equals(orderInfo.getOrderType())) {
            orderInfo.setStatus(UserConstants.ORDER_STATUS_3);
        }else{
            orderInfo.setStatus(UserConstants.ORDER_STATUS_2);
        }
        // 判断订单是否成功买中
        if (this.isBuying(orderInfo)) {
            // 订单盈利比例
            BigDecimal orderWinScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_WIN_SCALE));
            if(orderWinScale != null && orderWinScale.compareTo(BigDecimal.ZERO) > 0){
                // 订单盈利金额
                BigDecimal profit = orderInfo.getMoney().multiply(orderWinScale);
                orderInfo.setProfit(profit);
                // 订单结算金额 = 下单金额 + 盈利金额
                orderInfo.setSettleMoney(orderInfo.getMoney().add(profit));
            }else{
                // 未设置盈利比例
                orderInfo.setSettleMoney(orderInfo.getMoney());
            }
            // 普通场订单直接结算到用户钱包
            if(UserConstants.ORDER_TYPE_1.equals(orderInfo.getOrderType())){
                ResultVo resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(orderInfo.getUid(), SysCoinsTypeEnums.USDT.getName(), orderInfo.getOrderCode(),
                        WalletLogTypeEnums.BUY_UP_AND_DOWN_ORDER.getTypeId(), UserConstants.WALLET_IN, orderInfo.getSettleMoney(), "订单号[" + orderInfo.getOrderCode() + "]结算盈利"));
                if (!resultVo.isSuccess()) {
                    return resultVo;
                }
            }
        }else {
            orderInfo.setProfit(BigDecimal.ZERO.subtract(orderInfo.getMoney()));
            orderInfo.setSettleMoney(BigDecimal.ZERO);
        }
        if (!this.updateById(orderInfo)) {
            throw new BaseException("执行修改订单信息失败");
        }
        return ResultVo.success();
    }
    /**
     * 包赔场重新获取预测价
     * @param orderInfo
     * @param newKline
     * @return
     */
    private OrderInfo getNewBudgetPriceBp(OrderInfo orderInfo, KlineInfo newKline){
        //BTC浮动范围
        BigDecimal btcRand=NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.BTC_RAND));
        //ETH浮动范围
        BigDecimal ethRand=NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.ETH_RAND));
        BigDecimal rand="BTC/USDT".equals(orderInfo.getCoinSymbol())?btcRand:ethRand;
        BigDecimal sub1=BigDecimal.ZERO;
        //计算预测价与当前价差值
        if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
            //如果预测价大于当前价
            sub1=CalcUtils.sub(orderInfo.getBudgetPrice(),newKline.getClosePrice(),4);
        }else{
            sub1=CalcUtils.sub(newKline.getClosePrice(),orderInfo.getBudgetPrice(),4);
        }
        BigDecimal add1=BigDecimal.ZERO;
        //判断预测价与当前价差值是否大于设置的差值
        if(sub1.compareTo(rand)>=0){
            add1=CalcUtils.getRandomRedPacketBetweenMinAndMax(new BigDecimal(0.004),rand);
        }
        //判断预测价与下单价谁大谁小
        if(orderInfo.getBudgetPrice().compareTo(orderInfo.getOpenPrice())>0){
            if(newKline.getClosePrice().compareTo(orderInfo.getOpenPrice())>0){
                //预测价与当前价都大于下单价
                orderInfo.setBudgetPrice(CalcUtils.add(add1,newKline.getClosePrice(),4));
            }else{
                //预测价大于下单价,当前价小于下单价
                orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfo.getBudgetPrice(),orderInfo.getOpenPrice()));
            }
        }else{
            if(newKline.getClosePrice().compareTo(orderInfo.getOpenPrice())>0){
                //预测价小于下单价,当前价大于下单价
                orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfo.getBudgetPrice(),orderInfo.getOpenPrice()));
            }else{
                //预测价与当前价都小于下单价
                orderInfo.setBudgetPrice(CalcUtils.sub(newKline.getClosePrice(),add1,4));
            }
        }
        /**
         * 重新计算预测价是否大于当前价
         * 重新计算 预测价是否超过平台定义差额
         */
        BigDecimal sub=BigDecimal.ZERO;
        if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
            sub=CalcUtils.sub(orderInfo.getBudgetPrice(),newKline.getClosePrice(),4);

        }else{
            sub=CalcUtils.sub(newKline.getClosePrice(),orderInfo.getBudgetPrice(),4);
        }
        if(sub.compareTo(rand)>=0){
            if(!isBudgetWin(orderInfo)){
                //取当日平台盈亏
                ToDayOrderAmount toDayOrderAmount = getCountTodayOrderMoney();
                //包赔场盈利比例
                BigDecimal orderIndemnityScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_INDEMNITY_SCALE));
                //预设亏损率
                BigDecimal defaultLossRate = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_MAX_DEFICIT_RATIO));
                //用户当前中奖订单金额*0.85-未中奖订单金额
                BigDecimal profitAmount = baseMapper.getUserBpcProfitAmount(orderInfo.getUid());
                BigDecimal orderWinScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_WIN_SCALE));
                BigDecimal orderBouns = CalcUtils.mul(orderInfo.getMoney(),orderWinScale,2);
                profitAmount = CalcUtils.add(profitAmount,orderBouns,2);
                if(profitAmount.compareTo(BigDecimal.ZERO) > 0){
                    //计算用户中奖后平台亏损率变化
                    BigDecimal magin = BigDecimal.ZERO;
                    if(profitAmount.compareTo(orderBouns)>0){
                        //用户计算奖金的订单不止当前一个订单
                        magin = CalcUtils.mul(orderBouns,orderIndemnityScale,2);
                    }
                    else{
                        //当前订单中奖了用户才有奖金
                        magin = CalcUtils.mul(profitAmount,orderIndemnityScale,2);
                    }
                    BigDecimal lossToday = CalcUtils.add(magin,toDayOrderAmount.getLossToday(),2);
                    if(CalcUtils.div(lossToday,toDayOrderAmount.getToDayBuyMoneyBpc(),9).compareTo(defaultLossRate)<=0){
                        CoinSpreadVo coinSpreadVo = baseClient.getSpread(orderInfo.getCoinSymbol());
                        BigDecimal randomScale =BigDecimal.ZERO;
                        if(coinSpreadVo!=null){
                            randomScale= CalcUtils.getRandomRedPacketBetweenMinAndMax(coinSpreadVo.getSpreadMin(),coinSpreadVo.getSpreadMax());
                        }
                        BigDecimal spreadMoney = CalcUtils.mul(newKline.getClosePrice(),randomScale,4);
                        //先在我让他赢
                        if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
                            BigDecimal max=CalcUtils.add(newKline.getClosePrice(),spreadMoney,4) ;
                            orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                        }else{
                            BigDecimal max=CalcUtils.sub(newKline.getClosePrice(),spreadMoney,4) ;
                            orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                        }
                    }
                }
                else {
                    //用户依然未盈利,直接修改预测价为中奖
                    CoinSpreadVo coinSpreadVo = baseClient.getSpread(orderInfo.getCoinSymbol());
                    BigDecimal randomScale =BigDecimal.ZERO;
                    if(coinSpreadVo!=null){
                        randomScale= CalcUtils.getRandomRedPacketBetweenMinAndMax(coinSpreadVo.getSpreadMin(),coinSpreadVo.getSpreadMax());
                    }
                    BigDecimal spreadMoney = CalcUtils.mul(newKline.getClosePrice(),randomScale,4);
                    //先在我让他赢
                    if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
                        BigDecimal max=CalcUtils.add(newKline.getClosePrice(),spreadMoney,4) ;
                        orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                    }else{
                        BigDecimal max=CalcUtils.sub(newKline.getClosePrice(),spreadMoney,4) ;
                        orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                    }
                }
            }
            else{
                //计算点差
                CoinSpreadVo coinSpreadVo = baseClient.getSpread(orderInfo.getCoinSymbol());
                BigDecimal randomScale = BigDecimal.ZERO;
                if(coinSpreadVo!=null){
                    randomScale= CalcUtils.getRandomRedPacketBetweenMinAndMax(coinSpreadVo.getSpreadMin(),coinSpreadVo.getSpreadMax());
                }
                BigDecimal spreadMoney = CalcUtils.mul(newKline.getClosePrice(),randomScale,4);
                if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
                    BigDecimal max=CalcUtils.add(newKline.getClosePrice(),spreadMoney,4) ;
                    orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                }
                else{
                    BigDecimal max=CalcUtils.sub(newKline.getClosePrice(),spreadMoney,4) ;
                    orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                }
            }
        }
        return orderInfo;
    }
    /**
     * 重新获取预测价
     * @param orderInfo
     * @return
     */
    private OrderInfo getNewBudgetPrice(OrderInfo orderInfo, KlineInfo newKline){

        //BTC浮动范围
        BigDecimal btcRand=NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.BTC_RAND));
        //ETH浮动范围
        BigDecimal ethRand=NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(RedisKeyConstants.ETH_RAND));
        BigDecimal rand="BTC/USDT".equals(orderInfo.getCoinSymbol())?btcRand:ethRand;
        BigDecimal sub1=BigDecimal.ZERO;
        if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
            sub1=CalcUtils.sub(orderInfo.getBudgetPrice(),newKline.getClosePrice(),4);
        }else{
            sub1=CalcUtils.sub(newKline.getClosePrice(),orderInfo.getBudgetPrice(),4);
        }
        BigDecimal add1=BigDecimal.ZERO;

        if(sub1.compareTo(rand)>=0){
            add1=CalcUtils.getRandomRedPacketBetweenMinAndMax(new BigDecimal(0.004),rand);
        }
        if(orderInfo.getBudgetPrice().compareTo(orderInfo.getOpenPrice())>0){
            if(newKline.getClosePrice().compareTo(orderInfo.getOpenPrice())>0){
                orderInfo.setBudgetPrice(CalcUtils.add(add1,newKline.getClosePrice(),4));
            }else{
                orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfo.getBudgetPrice(),orderInfo.getOpenPrice()));
            }
        }else{
            if(newKline.getClosePrice().compareTo(orderInfo.getOpenPrice())>0){
                orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfo.getBudgetPrice(),orderInfo.getOpenPrice()));
            }else{
                orderInfo.setBudgetPrice(CalcUtils.add(add1,newKline.getClosePrice(),4));
            }
        }
        /**
         * 重新计算预测价是否大于当前价
         * 重新计算 预测价是否超过平台定义差额
         */
        BigDecimal sub=BigDecimal.ZERO;
        if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
            sub=CalcUtils.sub(orderInfo.getBudgetPrice(),newKline.getClosePrice(),4);

        }else{
            sub=CalcUtils.sub(newKline.getClosePrice(),orderInfo.getBudgetPrice(),4);
        }
        if(sub.compareTo(rand)>=0){
            //根据预测价判断输赢
            if(!isBudgetWin(orderInfo)){
                //取当日平台盈亏
                ToDayOrderAmount toDayOrderAmount = getCountTodayOrderMoney();
                //预设平台最低盈利率
                BigDecimal platMinRatio = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_MIN_PROFIT_RATIO));
                BigDecimal total=CalcUtils.add(toDayOrderAmount.getToDayBuyMoney(),orderInfo.getMoney(),4);

                BigDecimal orderWinScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_WIN_SCALE));
                BigDecimal reward=CalcUtils.add(orderWinScale,orderInfo.getMoney(),4);
                reward=CalcUtils.sub(toDayOrderAmount.getToDayIncome(),reward,4);
                //
                if(CalcUtils.div(reward,total,9).compareTo(platMinRatio)>0){
                    //计算点差
                    CoinSpreadVo coinSpreadVo = baseClient.getSpread(orderInfo.getCoinSymbol());
                    BigDecimal randomScale =BigDecimal.ZERO;
                    if(coinSpreadVo!=null){
                        randomScale= CalcUtils.getRandomRedPacketBetweenMinAndMax(coinSpreadVo.getSpreadMin(),coinSpreadVo.getSpreadMax());
                    }
                    BigDecimal spreadMoney = CalcUtils.mul(newKline.getClosePrice(),randomScale,4);
                    //先在我让他赢
                    if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
                        BigDecimal max=CalcUtils.add(newKline.getClosePrice(),spreadMoney,4) ;
                        orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                    }else{
                        BigDecimal max=CalcUtils.sub(newKline.getClosePrice(),spreadMoney,4) ;
                        orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                    }
                }
            }
            else{
                //计算点差
                CoinSpreadVo coinSpreadVo = baseClient.getSpread(orderInfo.getCoinSymbol());
                BigDecimal randomScale = BigDecimal.ZERO;
                if(coinSpreadVo!=null){
                    randomScale= CalcUtils.getRandomRedPacketBetweenMinAndMax(coinSpreadVo.getSpreadMin(),coinSpreadVo.getSpreadMax());
                }
                BigDecimal spreadMoney = CalcUtils.mul(newKline.getClosePrice(),randomScale,4);
                //先在我让他赢
                if(orderInfo.getBudgetPrice().compareTo(newKline.getClosePrice())>0){
                    BigDecimal max=CalcUtils.add(newKline.getClosePrice(),spreadMoney,4) ;
                    orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                }else{
                    BigDecimal max=CalcUtils.sub(newKline.getClosePrice(),spreadMoney,4) ;
                    orderInfo.setBudgetPrice(CalcUtils.getRandomRedPacketBetweenMinAndMax(newKline.getClosePrice(),max));
                }
            }
        }
        return orderInfo;
    }

    @Override
    @Transactional
    public void handleIndemnityOrder() {
        //统计 还未结算的包赔场订单 本金总额 以及收益总额
        List<SumBudgetAmount> sumBudgetAmount=baseMapper.sumBudgetMoney();
        for(SumBudgetAmount s:sumBudgetAmount){
            BigDecimal money=s.getTotalMoney();
            //加上包赔场盈利
            if(s.getTotalProfit().compareTo(BigDecimal.ZERO)>0){
                BigDecimal orderIndemnityScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_INDEMNITY_SCALE));
                BigDecimal profit=CalcUtils.mul(s.getTotalProfit(),orderIndemnityScale,4);
                money=CalcUtils.add(money,profit,4);
            }
            ResultVo resultVo =iWalletInfoService.handleUserWalletMoney(new WalletLogDto(s.getUid(), SysCoinsTypeEnums.USDT.getName(), CodeUtils.generateOrderNo(),
                    WalletLogTypeEnums.INDEMNITY_SETTLEMENT.getTypeId(), UserConstants.WALLET_IN,money, "包赔场每日结算"));
            if (!resultVo.isSuccess()) {
                throw new BaseException(resultVo.getErrorMessage());
            }
            QueryWrapper<OrderInfo> orderInfoQueryWrapper = new QueryWrapper<>();
            orderInfoQueryWrapper.eq("uid",s.getUid()).eq("order_type", UserConstants.ORDER_TYPE_2).eq("status", UserConstants.ORDER_STATUS_2);

            OrderInfo orderInfo=new OrderInfo();
            orderInfo.setStatus(UserConstants.ORDER_STATUS_3);
            if(!update(orderInfo,orderInfoQueryWrapper)){
                throw new BaseException("修改包赔场订单状态失败");
            }
        }
    }

    /**
     * 判断买入订单是否中奖
     *
     * @param orderInfo
     * @return
     */
    private boolean isBuying(OrderInfo orderInfo) {
        // 买涨
        if (UserConstants.BUY_TYPE_1.equals(orderInfo.getBuyType())
                && orderInfo.getClosePrice().compareTo(orderInfo.getOpenPrice()) > 0) {
            // 成功
            return true;
        }
        // 买跌
        if (UserConstants.BUY_TYPE_2.equals(orderInfo.getBuyType())
                && orderInfo.getClosePrice().compareTo(orderInfo.getOpenPrice()) < 0) {
            // 成功
            return true;
        }
        return false;
    }


    /**
     * 判断预测是否中奖
     *
     * @param orderInfo
     * @return
     */
    private boolean isBudgetWin(OrderInfo orderInfo) {
        // 买涨
        if (UserConstants.BUY_TYPE_1.equals(orderInfo.getBuyType())
                && orderInfo.getBudgetPrice().compareTo(orderInfo.getOpenPrice()) > 0) {
            // 成功
            return true;
        }
        // 买跌
        if (UserConstants.BUY_TYPE_2.equals(orderInfo.getBuyType())
                && orderInfo.getBudgetPrice().compareTo(orderInfo.getOpenPrice()) < 0) {
            // 成功
            return true;
        }
        return false;
    }


    public Map<String,BigDecimal>  getMap(List<ToDayBudgetAmount> amounts){
        //
        Map<String,BigDecimal> map = new HashMap<>();
        //时间段内包赔场订单用户包赔场未中奖订单总金额
        Map<String,BigDecimal> userBpcUnWinnerOrderMoneyMap = new HashMap<>();
        for(ToDayBudgetAmount l:amounts){
            map.put(l.getUid(),l.getToDayBuyMoneyBpc());
        }
        return map;
    }

    /**
     * 每分钟的10s 30s 50s 的时候执行，每次结算的时间为往前推55s到30s的时间段订单
     * @param coinSymbol 币种  比特币   以太坊
     */
    @Override
    public void handleBudgetOrder(String coinSymbol) {
        //当前时间往前推55秒的时间，防止定时延迟执行容错   需要赋值
        LocalDateTime beforeTime = LocalDateTime.now().minusSeconds(55);
        //当前时间往后推30秒的时间    需要赋值
        LocalDateTime afterTime = LocalDateTime.now().minusSeconds(30);

        ToDayOrderAmount toDayOrderAmount = getCountTodayOrderMoney();

        //如果咵天的情况下
        if(beforeTime.getDayOfYear() != afterTime.getDayOfYear()){
            toDayOrderAmount.setBudgetData(BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO);
        }

        //预设平台最合理盈利率
        BigDecimal platHopeMinRatio = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_PROFIT_RATIO));
        //预设平台最低盈利率
        BigDecimal platMinRatio = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_MIN_PROFIT_RATIO));
        //预设平台最高盈利率   需要赋值
        BigDecimal platMaxRatio = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_MAX_PROFIT_RATIO));
        //预设亏损率
        BigDecimal defaultLossRate = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_MAX_DEFICIT_RATIO));
        //设置中奖订单返奖比例 0.85
        BigDecimal winScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_WIN_SCALE));
        //中将订单应反金额赔率(本金+奖金)1.85
        BigDecimal capitalWinScale = CalcUtils.add(winScale, BigDecimal.ONE, 4);

        //包赔场盈利比例
        BigDecimal orderIndemnityScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_INDEMNITY_SCALE));


        //查询应算时间段内是否有订单需要结算  需要赋值
        Map<String,Object> params=new HashMap<>();
        params.put("coinSymbol",coinSymbol);
        params.put("beforeTime",beforeTime);
        params.put("afterTime",afterTime);
        Map<String,BigDecimal> totalMap = baseMapper.getNotBudgetOrderMoney(params);
        BigDecimal totalMoney= totalMap.get("notBudgetTotalMoney");
        //结算订单所处分钟段的最高指数值   需要赋值2
        // 从缓存中读取当前市场买入价
        String latestMarketStr = redisTemplate.opsForValue().get(RedisKeyConstants.LATEST_COIN_MARKET_KLINE + coinSymbol);
        if(StringUtils.isBlank(latestMarketStr)){
            return;
        }
        KlineInfo newKline = JSONObject.toJavaObject(JSON.parseObject(latestMarketStr), KlineInfo.class);
        BigDecimal highestPrice = newKline.getLowestPrice();
        //结算订单所处分钟段的最小指数值   需要赋值
        BigDecimal hHighestPrice = newKline.getHighestPrice();

        //时间段内包赔场订单用户包赔场中奖订单总金额
        Map<String,BigDecimal> userBpcWinnerOrderMoneyMap=getMap(baseMapper.toDayBudgetMoneyGt());
        //时间段内包赔场订单用户包赔场未中奖订单总金额
        Map<String,BigDecimal> userBpcUnWinnerOrderMoneyMap =getMap(baseMapper.toDayBudgetMoneylt());
        log.info("预设数据最优盈利率:" + platHopeMinRatio + "最小盈利率:" + platMinRatio + "最大盈利率:" + platMaxRatio + "亏损率:" + defaultLossRate
                + "中奖订单返奖比例:" + winScale + "包赔场盈利比例:" + orderIndemnityScale + "最高价:" + highestPrice + "最低价:" + hHighestPrice);
        log.info("sqltotalMap查询返回:" + totalMap + "包赔场中奖:" + userBpcWinnerOrderMoneyMap + "未中奖:" + userBpcUnWinnerOrderMoneyMap);
        //判断当前时间是否有未结算订单totalMoney未结算订单金额
        if (totalMoney.compareTo(BigDecimal.ZERO) > 0) {
            //取出当前时间往前推55s-30s时间段的每秒订单数为一的订单
            List<BudgetTimes> budgetTimesLessOne = baseMapper.getBudgetTimesLessOne(params);
            if (budgetTimesLessOne != null && budgetTimesLessOne.size() > 0) {
                BigDecimal allMoney = BigDecimal.ZERO;//当前时间段内订单总金额
                BigDecimal bpcAllMoney = BigDecimal.ZERO;//当前时间段内包赔场订单总金额
                Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();
                List<String> indexList = new ArrayList<String>();
                Map<String, Integer> orderTypeMap = new HashMap();
                BudgetTimes budgetTimes;
                for(int j = 0;j<budgetTimesLessOne.size();j++){
                    budgetTimes = budgetTimesLessOne.get(j);
                    indexList.add("" + j);
                    orderTypeMap.put("" + j,budgetTimes.getOrderType());
                    if(budgetTimes.getOrderType() == 1){
                        map.put("" + j, CalcUtils.add(budgetTimes.getMoney(),CalcUtils.mul(budgetTimes.getMoney(),winScale,2),2));
                        allMoney = CalcUtils.add(allMoney,budgetTimes.getMoney(),2);
                    }
                    else{
                        BigDecimal userBpcWinnerOrderMoney = userBpcWinnerOrderMoneyMap.get(budgetTimes.getUid()) == null ? BigDecimal.ZERO : userBpcWinnerOrderMoneyMap.get(budgetTimes.getUid());
                        BigDecimal userBpcUnWinnerOrderMoney = userBpcUnWinnerOrderMoneyMap.get(budgetTimes.getUid()) == null ? BigDecimal.ZERO : userBpcUnWinnerOrderMoneyMap.get(budgetTimes.getUid());
                        if(CalcUtils.mul(CalcUtils.add(budgetTimes.getMoney(),userBpcWinnerOrderMoney,2),winScale,2).compareTo(userBpcUnWinnerOrderMoney) > 0){
                            //用户盈利
                            map.put("" + j,CalcUtils.sub(CalcUtils.mul(CalcUtils.add(budgetTimes.getMoney(),userBpcWinnerOrderMoney,2),winScale,2),userBpcUnWinnerOrderMoney,2).compareTo(CalcUtils.mul(budgetTimes.getMoney(),winScale,2))>= 0 ?
                                    CalcUtils.mul(CalcUtils.mul(budgetTimes.getMoney(),winScale,2),orderIndemnityScale,2) :
                                    CalcUtils.mul(CalcUtils.sub(CalcUtils.mul(CalcUtils.add(budgetTimes.getMoney(),userBpcWinnerOrderMoney,2),winScale,2),userBpcUnWinnerOrderMoney,2),orderIndemnityScale,2));
                        }
                        else{
                            map.put("" + j,BigDecimal.ZERO);
                        }
                        bpcAllMoney = CalcUtils.add(bpcAllMoney,budgetTimes.getMoney(),2);
                    }
                }
                BudgetData budgetData = this.calculateTheOptimalResult( indexList, map, toDayOrderAmount, allMoney, bpcAllMoney,orderTypeMap, platMinRatio, platHopeMinRatio, platMaxRatio,defaultLossRate);
                List<String> combinationList = budgetData.getOrderIndexList();
                toDayOrderAmount = budgetData.getToDayOrderAmount();
                log.info("返回中奖订单的数量:" + combinationList.size() + "盈利率:" + budgetData.getEstimatedProfitability() + "亏损率:" + budgetData.getEstimatedLossRate());
                for(int i = 0;i<budgetTimesLessOne.size();i++){
                    budgetTimes = budgetTimesLessOne.get(i);
                    BigDecimal randomPrice = BigDecimal.ZERO;
                    BigDecimal min = CalcUtils.add(budgetTimes.getOpenPrice(),new BigDecimal(0.005),4);
                    BigDecimal max = CalcUtils.sub(budgetTimes.getOpenPrice(),new BigDecimal(0.005),4);
                    if(combinationList.contains(String.valueOf(i))){
                        //中奖订单
                        if (UserConstants.BUY_TYPE_1.equals(budgetTimes.getBuyType())) {
                            //如果是买涨 向上取价格
                            log.info(budgetTimes.getUid() + "中奖订单,买涨");
                            if(highestPrice.compareTo(min) <= 0){
                                highestPrice = CalcUtils.add(min,new BigDecimal(0.01),4);
                            }
                            randomPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(min, highestPrice);
                        } else if (UserConstants.BUY_TYPE_2.equals(budgetTimes.getBuyType())) {
                            log.info(budgetTimes.getUid() + "中奖订单,买跌");
                            if(hHighestPrice.compareTo(max) >= 0){
                                hHighestPrice = CalcUtils.sub(max,new BigDecimal(0.01),4);
                            }
                            randomPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(hHighestPrice, max);
                        }
                    }
                    else{
                        if (UserConstants.BUY_TYPE_1.equals(budgetTimes.getBuyType())) {
                            //如果是买涨 向下取价格
                            log.info(budgetTimes.getUid() + "未中奖订单,买涨");
                            if(hHighestPrice.compareTo(max) >= 0){
                                hHighestPrice = CalcUtils.sub(max,new BigDecimal(0.01),4);
                            }
                            randomPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(hHighestPrice, max);
                        } else if (UserConstants.BUY_TYPE_2.equals(budgetTimes.getBuyType())) {
                            log.info(budgetTimes.getUid() + "未中奖订单,买跌");
                            if(highestPrice.compareTo(min) <= 0){
                                highestPrice = CalcUtils.add(min,new BigDecimal(0.01),4);
                            }
                            randomPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(min, highestPrice);
                        }
                    }

                    OrderInfo orderInfo = new OrderInfo();
                    orderInfo.setBudgetPrice(randomPrice);
                    orderInfo.setBudgetFlag(UserConstants.USER_STATUS_0);
                    QueryWrapper<OrderInfo> orderInfoQueryWrapper = new QueryWrapper<>();
                    orderInfoQueryWrapper.eq("create_time", budgetTimes.getCreateTime()).eq("coin_symbol", coinSymbol)
                            .eq("status", UserConstants.BUY_ORDER_STATUS_1).eq("budget_flag", UserConstants.USER_STATUS_1);
                    this.update(orderInfo, orderInfoQueryWrapper);
                }
            }
            //时间段内单独一秒的订单数量大于一结算
            //sql有问题 select count(1) as orderNum,create_time as createTime from order_info where status=3
            //and budget_flag=1 and coin_symbol='BTC/USDT' group by create_time having orderNum >1
            //需要赋值
            List<LocalDateTime> budgetTimesMore = baseMapper.getBudgetTimesMore(params);
            for (LocalDateTime b : budgetTimesMore) {
                QueryWrapper<OrderInfo> orderInfoQueryWrapper = new QueryWrapper<>();
                orderInfoQueryWrapper.eq("create_time", b).eq("coin_symbol", coinSymbol)
                        .eq("status", UserConstants.BUY_ORDER_STATUS_1).eq("budget_flag", UserConstants.USER_STATUS_1);
                orderInfoQueryWrapper.orderByDesc("open_price");
                List<OrderInfo> orderInfoS = list(orderInfoQueryWrapper);
                if(orderInfoS.size() == 0) continue;
                //根据投注指数升序排序
                orderInfoS.sort(Comparator.comparing(OrderInfo::getOpenPrice));
                BudgetData budgetData = nodeCalculateTheOptimalResult(userBpcUnWinnerOrderMoneyMap,userBpcWinnerOrderMoneyMap,winScale, highestPrice, hHighestPrice, orderInfoS, toDayOrderAmount, platMinRatio, platHopeMinRatio, platMaxRatio,defaultLossRate);
                List<String> combinationList = budgetData.getOrderIndexList();
                BigDecimal randomPrice = budgetData.getBudgetPrice();
                log.info("返回中奖订单的数量:" + combinationList.size() + "盈利率:" + budgetData.getEstimatedProfitability() + "亏损率:" + budgetData.getEstimatedLossRate() + "预测价格:" + randomPrice);
                for(OrderInfo orderInfo : orderInfoS){
                    orderInfo.setBudgetPrice(randomPrice);
                    orderInfo.setBudgetFlag(UserConstants.USER_STATUS_0);
                    orderInfo.setSignFlag(UserConstants.SIGN_FLAG_2);
                    this.updateById(orderInfo);
                }
            }
        }
    }
    /**
     *
     * @param list 时间节点list集合下标集合
     * @param map 每一个下标对应订单的本金+奖金 包赔场的为订单中奖后用户的奖金
     * @param toDayOrderAmount  普通场于包赔场今日订单金额与盈利金额，亏损金额
     * @param allMoney   本批订单普通场订单的总金额
     * @param bpcAllMoney  本批订单的包赔场订单的总金额
     * @param orderTypeMap   下标对应订单的类型1普通场2包赔场
     * @param platMinRatio 平台最小盈利率
     * @param platHopeMinRatio 预设平台最合理盈利率
     * @param platMaxRatio  预设平台最大盈利率
     * @param defaultLossRate  平台预设亏损率
     * @return
     */
    public BudgetData calculateTheOptimalResult(List<String> list, Map<String, BigDecimal> map, ToDayOrderAmount toDayOrderAmount, BigDecimal allMoney, BigDecimal bpcAllMoney, Map<String, Integer> orderTypeMap, BigDecimal platMinRatio, BigDecimal platHopeMinRatio, BigDecimal platMaxRatio, BigDecimal defaultLossRate){
        long n = (long) Math.pow(2, list.size());
        List<String> combine ;
        BudgetData budgetData = null;
        for (int l = 0; l < n; l++) {
            combine = new ArrayList<String>();
            //本批次普通场订单本金+奖金
            BigDecimal sum = BigDecimal.ZERO;
            //本批次包赔场订单中奖后订单对应用户应反奖金(平台包赔场亏损金额)
            BigDecimal ksSum = BigDecimal.ZERO;
            for (int i = 0; i < list.size(); i++) {
                if ((l >>> i & 1) == 1){
                    if(orderTypeMap.get(list.get(i)) == 1) sum = sum.add(map.get(list.get(i)));
                    else ksSum = CalcUtils.add(ksSum, map.get(list.get(i)), 2);
                    combine.add(list.get(i));
                }
            }
            //本组合情况下平台总亏损金额
            BigDecimal lossAmount = CalcUtils.add(ksSum, toDayOrderAmount.getLossToday(), 2);
            //本组合情况下平台盈利金额
            BigDecimal currentPortfolioProfit = CalcUtils.add(CalcUtils.sub(allMoney,sum,2),toDayOrderAmount.getToDayIncome(),2);
            budgetData = optimalCombination(1, allMoney, bpcAllMoney, toDayOrderAmount, defaultLossRate, combine, budgetData, lossAmount, currentPortfolioProfit, platMinRatio, platHopeMinRatio, platMaxRatio, BigDecimal.ZERO);
            log.info("本组合判断后利率变化:" + budgetData.getSituation() + "-" + budgetData.getEstimatedProfitability() + "-" + budgetData.getEstimatedLossRate());
            if(budgetData.getSituation() == 1){
                log.info("本组合为最优利率直接返回" + budgetData.getEstimatedProfitability() + "-" + budgetData.getEstimatedLossRate());
                return budgetData;
            }
        }
        return budgetData;
    }
    /**
     *
     * @param userBpcUnWinnerOrderMoneyMap
     * @param userBpcWinnerOrderMoneyMap
     * @param winScale
     * @param highestPrice
     * @param hHighestPrice
     * @param orderInfoS
     * @param toDayOrderAmount
     * @param platMinRatio
     * @param platHopeMinRatio
     * @param platMaxRatio
     * @param defaultLossRate
     * @return
     */
    public BudgetData nodeCalculateTheOptimalResult(Map<String,BigDecimal> userBpcUnWinnerOrderMoneyMap,Map<String,BigDecimal> userBpcWinnerOrderMoneyMap,BigDecimal winScale,BigDecimal highestPrice,BigDecimal hHighestPrice,List<OrderInfo> orderInfoS, ToDayOrderAmount toDayOrderAmount, BigDecimal platMinRatio, BigDecimal platHopeMinRatio, BigDecimal platMaxRatio,BigDecimal defaultLossRate){
        BigDecimal allMoney = BigDecimal.ZERO;
        //包赔场盈利比例
        BigDecimal orderIndemnityScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_INDEMNITY_SCALE));

        BigDecimal bpcAllMoney = BigDecimal.ZERO;
        for(OrderInfo o : orderInfoS){
            BigDecimal min = CalcUtils.add(o.getOpenPrice(),new BigDecimal(0.005),4);
            BigDecimal max = CalcUtils.sub(o.getOpenPrice(),new BigDecimal(0.005),4);
            if(o.getBuyType() == 1 && highestPrice.compareTo(min) <= 0){
                highestPrice = CalcUtils.add(min,new BigDecimal(0.01),4);
            }
            if(o.getBuyType() == 2 && hHighestPrice.compareTo(max) >= 0){
                hHighestPrice = CalcUtils.sub(max,new BigDecimal(0.01),4);
            }
            if(o.getOrderType() == 1) allMoney = CalcUtils.add(allMoney,o.getMoney(),2);
            else bpcAllMoney = CalcUtils.add(bpcAllMoney,o.getMoney(),2);
        }
        List<BigDecimal> provisionalIndexList = new ArrayList<BigDecimal>();
        for(int i =0;i<orderInfoS.size();i++){
            if(i == 0){
                BigDecimal max = CalcUtils.sub(orderInfoS.get(i).getOpenPrice(),new BigDecimal(0.005),4);
                BigDecimal provisionalIndex = CalcUtils.getRandomRedPacketBetweenMinAndMax(hHighestPrice, max);
                provisionalIndexList.add(provisionalIndex);
            }
            else{
                BigDecimal provisionalIndex = CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfoS.get(i-1).getOpenPrice(), orderInfoS.get(i).getOpenPrice());
                provisionalIndexList.add(provisionalIndex);
                if(i == (orderInfoS.size() - 1)){
                    BigDecimal min = CalcUtils.add(orderInfoS.get(i).getOpenPrice(),new BigDecimal(0.005),4);
                    provisionalIndexList.add(CalcUtils.getRandomRedPacketBetweenMinAndMax(min,highestPrice));
                }
            }
        }
        OrderInfo order;
        BudgetData budgetData = null;
        for (BigDecimal provisionalIndex : provisionalIndexList) {
            List<String> orderIndexList = new ArrayList<String>();
            BigDecimal sum = BigDecimal.ZERO;
            BigDecimal ksSum = BigDecimal.ZERO;
            for (int i = 0; i < orderInfoS.size(); i++) {
                order = orderInfoS.get(i);
                if ((order.getBuyType() == 1 && order.getOpenPrice().compareTo(provisionalIndex) < 0) ||
                        (order.getBuyType() == 2 && order.getOpenPrice().compareTo(provisionalIndex) > 0)) {
                    orderIndexList.add("" + i);
                    if(order.getOrderType() == 1){
                        sum = CalcUtils.add(sum, CalcUtils.add(order.getMoney(), CalcUtils.mul(order.getMoney(), winScale, 2), 2), 2);
                    }
                    else{
                        BigDecimal userBpcWinnerOrderMoney = userBpcWinnerOrderMoneyMap.get(order.getUid()) == null ? BigDecimal.ZERO : userBpcWinnerOrderMoneyMap.get(order.getUid());
                        BigDecimal userBpcUnWinnerOrderMoney = userBpcUnWinnerOrderMoneyMap.get(order.getUid()) == null ? BigDecimal.ZERO : userBpcUnWinnerOrderMoneyMap.get(order.getUid());
                        if(CalcUtils.mul(CalcUtils.add(order.getMoney(),userBpcWinnerOrderMoney,2),winScale,2).compareTo(userBpcUnWinnerOrderMoney) > 0) {
                            //用户盈利
                            ksSum = CalcUtils.add(ksSum, (CalcUtils.sub(CalcUtils.mul(CalcUtils.add(order.getMoney(), userBpcWinnerOrderMoney, 2), winScale, 2), userBpcUnWinnerOrderMoney, 2).compareTo(CalcUtils.mul(order.getMoney(), winScale, 2)) >= 0 ?
                                    CalcUtils.mul(CalcUtils.mul(order.getMoney(), winScale, 2),orderIndemnityScale, 2) :
                                    CalcUtils.mul(CalcUtils.sub(CalcUtils.mul(CalcUtils.add(order.getMoney(), userBpcWinnerOrderMoney, 2), winScale, 2), userBpcUnWinnerOrderMoney, 2),orderIndemnityScale, 2)), 2);
                        }
                    }
                }
            }
            //本组合情况下平台总亏损金额
            BigDecimal lossAmount = CalcUtils.add(ksSum, toDayOrderAmount.getLossToday(), 2);
            //本组合情况下平台盈利金额
            BigDecimal currentPortfolioProfit = CalcUtils.add(CalcUtils.sub(allMoney,sum,2), toDayOrderAmount.getToDayIncome(), 2);
            budgetData = optimalCombination(2, allMoney, bpcAllMoney, toDayOrderAmount, defaultLossRate, orderIndexList, budgetData, lossAmount, currentPortfolioProfit, platMinRatio, platHopeMinRatio, platMaxRatio, provisionalIndex);
            log.info("本组合判断后利率变化:" + budgetData.getSituation() + "-" + budgetData.getEstimatedProfitability() + "-" + budgetData.getEstimatedLossRate());
        }
        return budgetData;
    }
    /**
     *
     * @param type
     * @param allMoney
     * @param bpcAllMoney
     * @param toDayOrderAmount
     * @param defaultLossRate
     * @param orderIndexList
     * @param budgetData
     * @param lossAmount
     * @param currentPortfolioProfit
     * @param platMinRatio
     * @param platHopeMinRatio
     * @param platMaxRatio
     * @param budgetPrice
     * @return
     */
    public BudgetData optimalCombination(int type, BigDecimal allMoney,BigDecimal bpcAllMoney,ToDayOrderAmount toDayOrderAmount,BigDecimal defaultLossRate,List<String> orderIndexList,BudgetData budgetData,BigDecimal lossAmount,BigDecimal currentPortfolioProfit,BigDecimal platMinRatio, BigDecimal platHopeMinRatio, BigDecimal platMaxRatio, BigDecimal budgetPrice ){
        //0包含普通场订单及包赔场订单1只有普通场订单2只有包赔场订单
        if(allMoney.compareTo(BigDecimal.ZERO) == 0 && bpcAllMoney.compareTo(BigDecimal.ZERO) == 0) return null;
        int situation = 0;
        if(allMoney.compareTo(BigDecimal.ZERO) == 0){
            //普通场没有订单
            situation = 2;
        }
        if(bpcAllMoney.compareTo(BigDecimal.ZERO) == 0){
            situation = 1;
        }
        //加上本批次订单普通场总金额
        BigDecimal orderAllMoney = CalcUtils.add(toDayOrderAmount.getToDayBuyMoney(),allMoney,2);
        //加上本批次订单包赔场总金额
        BigDecimal bpcOrderAllMoney = CalcUtils.add(toDayOrderAmount.getToDayBuyMoneyBpc(),bpcAllMoney,2);

        //本组合中奖下平台亏损率
        BigDecimal estimatedLossRate = BigDecimal.ZERO;
        //本组合中奖下平台盈利率
        BigDecimal estimatedProfitability = BigDecimal.ZERO;
        ToDayOrderAmount t = new ToDayOrderAmount();
        t.setBudgetData(
                orderAllMoney,
                currentPortfolioProfit,
                bpcOrderAllMoney,
                lossAmount
        );
        if(situation == 1){
            //只有普通场订单
            estimatedProfitability = CalcUtils.div(currentPortfolioProfit,orderAllMoney,9);
            int judgePriority = judgePriority(situation, estimatedLossRate, estimatedProfitability, defaultLossRate, platMinRatio, platHopeMinRatio, platMaxRatio);
            log.info("预结算组合数据:" + (budgetData == null) + "组合盈利率:" + estimatedProfitability
                    + "优先等级:" + judgePriority);
            if(budgetData == null){
                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                return budgetData;
            }
            else{
                //本组合优先预选组合
                if(judgePriority < budgetData.getSituation()){
                    budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                }
                else if(judgePriority == budgetData.getSituation()){
                    if(judgePriority == 2 || judgePriority == 4){
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                    }
                    else{
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                    }
                }
            }
        }
        else if(situation == 2){
            //只有包赔场订单
            //计算出本组合的亏损率
            estimatedLossRate = CalcUtils.div(lossAmount, bpcOrderAllMoney, 9);
            int judgePriority = judgePriority(situation, estimatedLossRate, estimatedProfitability, defaultLossRate, platMinRatio, platHopeMinRatio, platMaxRatio);
            log.info("预结算组合数据:" + (budgetData == null) + "组合亏损率:" + estimatedLossRate
                    + "优先等级:" + judgePriority);
            if(budgetData == null){
                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                return budgetData;
            }
            else{
                if(judgePriority < budgetData.getSituation()){
                    //本组合比预选组合优先
                    budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                }
                else if(judgePriority == budgetData.getSituation()){
                    //都等于预设亏损率
                    if(judgePriority == 1){
                        //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                        if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                    }
                    //都小于预设亏损率
                    else if(judgePriority == 2){
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0){
                            //当前组合亏损率大于预选亏损率
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                            }
                        }
                    }
                    //都大于预设亏损率
                    else{
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0){
                            //当前组合亏损率小于预选亏损率
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                            }
                        }
                    }
                }
            }
        }
        else{
            //混合订单
            //普通场订单盈利率
            estimatedProfitability = CalcUtils.div(currentPortfolioProfit,orderAllMoney,9);
            //亏损率
            estimatedLossRate = CalcUtils.div(lossAmount, bpcOrderAllMoney, 9);
            int judgePriority = judgePriority(situation, estimatedLossRate, estimatedProfitability, defaultLossRate, platMinRatio, platHopeMinRatio, platMaxRatio);
            log.info("预结算组合数据:" + (budgetData == null) + "组合亏损率:" + estimatedLossRate
                    + "盈利率:" + estimatedProfitability + "优先等级:" + judgePriority);
            if(budgetData == null){
                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                return budgetData;
            }
            else{
                //本组合优先级高于预选组合
                if(judgePriority < budgetData.getSituation()){
                    budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                }
                //本组合优先级等于预选组合
                else if(judgePriority == budgetData.getSituation()){
                    if(judgePriority == 2){
                        //等于预选亏损率 大于最优小于最大
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                            }
                        }
                    }
                    else if(judgePriority == 3){
                        //等于预选亏损率 小于最优大于最低
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                            }
                        }
                    }
                    else if(judgePriority == 4){
                        //小于亏损率 等于最优盈利率
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                            }
                        }
                    }
                    else if(judgePriority == 5){
                        //小于亏损率 大于最优小于最大
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 6){
                        //小于亏损率 小于最优大于最低
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 7){
                        //小于亏损率 大于最大
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,BigDecimal.ZERO,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 8){
                        //等于预选亏损率 大于最大
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0
                                && estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                            }
                        }
                    }
                    else if(judgePriority == 9){
                        //亏损大于预设 等于最优盈利率
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability())== 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 10){
                        //亏损大于预设 大于最优小于最大
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 11){
                        //亏损大于预设 小于最优大于最低
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 12){
                        //亏损大于预设 大于最大
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 13){
                        //小于亏损率 小于最小大于0
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability())== 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 14){
                        //小于亏损率 小于0
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) > 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability())== 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else if(judgePriority == 15){
                        //等于预选亏损率 小于最小大于0
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                            }
                        }
                    }
                    else if(judgePriority == 16){
                        //等于预选亏损率 小于0
                        if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) == 0){
                            //如果两个组合亏损率相同，哪个中奖订单多，哪个优先
                            if(orderIndexList.size() > budgetData.getOrderIndexList().size()){
                                budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                            }
                        }
                    }
                    else if(judgePriority == 17){
                        //亏损大于预设 小于最小大于0
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                    else{
                        //亏损大于预设 小于0
                        if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) < 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability())== 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                        else if(estimatedLossRate.compareTo(budgetData.getEstimatedLossRate()) == 0
                                && estimatedProfitability.compareTo(budgetData.getEstimatedProfitability()) > 0){
                            budgetData = new BudgetData(type,estimatedProfitability,estimatedLossRate,t,budgetPrice,orderIndexList,judgePriority);
                        }
                    }
                }
            }
        }
        return budgetData;
    }
    /**
     *
     * @param situation
     * @param estimatedLossRate
     * @param estimatedProfitability
     * @param platMinRatio 平台最小盈利率
     * @param platHopeMinRatio 预设平台最合理盈利率
     * @param platMaxRatio  预设平台最大盈利率
     * @param defaultLossRate  平台预设亏损率
     */
    public int judgePriority(int situation, BigDecimal estimatedLossRate, BigDecimal estimatedProfitability,BigDecimal defaultLossRate,BigDecimal platMinRatio, BigDecimal platHopeMinRatio, BigDecimal platMaxRatio){
        if(situation == 1){
            //只有普通场
            if(estimatedProfitability.compareTo(platHopeMinRatio) == 0){
                return 1;
            }
            else if(estimatedProfitability.compareTo(platHopeMinRatio)>0 && estimatedProfitability.compareTo(platMaxRatio) < 0){
                return 2;
            }
            else if(estimatedProfitability.compareTo(platHopeMinRatio)<0 && estimatedProfitability.compareTo(platMinRatio) > 0){
                return 3;
            }
            else if(estimatedProfitability.compareTo(platMaxRatio)>0){
                return 4;
            }
            else if(estimatedProfitability.compareTo(platMinRatio) < 0){
                return 5;
            }
            else{
                return 6;
            }
        }
        else if(situation == 2){
            //包赔场订单
            if(estimatedLossRate.compareTo(defaultLossRate) < 0){
                return 2;
            }
            else if(estimatedLossRate.compareTo(defaultLossRate) == 0){
                return 1;
            }
            else{
                return 3;
            }
        }
        else{
            if(estimatedLossRate.compareTo(defaultLossRate) == 0){
                //本次亏损率等于预选亏损率
                if(estimatedProfitability.compareTo(platHopeMinRatio) == 0){
                    return 1;
                }
                else if(estimatedProfitability.compareTo(platHopeMinRatio)>0 && estimatedProfitability.compareTo(platMaxRatio) < 0){
                    return 2;
                }
                else if(estimatedProfitability.compareTo(platHopeMinRatio)<0 && estimatedProfitability.compareTo(platMinRatio) > 0){
                    return 3;
                }
                else if(estimatedProfitability.compareTo(platMaxRatio)>0){
                    return 8;
                }
                else if(estimatedProfitability.compareTo(platMinRatio) < 0){
                    return 15;
                }
                else{
                    return 16;
                }
            }
            //本次亏损小于预设亏损
            else if(estimatedLossRate.compareTo(defaultLossRate) < 0){
                if(estimatedProfitability.compareTo(platHopeMinRatio) == 0){
                    return 4;
                }
                else if(estimatedProfitability.compareTo(platHopeMinRatio)>0 && estimatedProfitability.compareTo(platMaxRatio) < 0){
                    return 5;
                }
                else if(estimatedProfitability.compareTo(platHopeMinRatio)<0 && estimatedProfitability.compareTo(platMinRatio) > 0){
                    return 6;
                }
                else if(estimatedProfitability.compareTo(platMaxRatio)>0){
                    return 7;
                }
                else if(estimatedProfitability.compareTo(platMinRatio) < 0){
                    return 13;
                }
                else{
                    return 14;
                }
            }
            else{
                if(estimatedProfitability.compareTo(platHopeMinRatio) == 0){
                    return 9;
                }
                else if(estimatedProfitability.compareTo(platHopeMinRatio)>0 && estimatedProfitability.compareTo(platMaxRatio) < 0){
                    return 10;
                }
                else if(estimatedProfitability.compareTo(platHopeMinRatio)<0 && estimatedProfitability.compareTo(platMinRatio) > 0){
                    return 11;
                }
                else if(estimatedProfitability.compareTo(platMaxRatio)>0){
                    return 12;
                }
                else if(estimatedProfitability.compareTo(platMinRatio) < 0){
                    return 17;
                }
                else{
                    return 18;
                }
            }
        }

    }
//    @Override
//    public void handleBudgetOrder(String coinSymbol) {
//        //第一步先获取到 平台当前盈利比例  从sql计算出来
//        ToDayOrderAmount toDayOrderAmount = getCountTodayOrderMoney();
//        //第二获取到 平台希望盈利的比例 从缓存取 0.15
//        BigDecimal platHopeMinRatio = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_PROFIT_RATIO));
//        //第三部获取到 平台最低盈利比例 从缓存取 0.08
//        BigDecimal platMinRatio = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(PLATFORM_MIN_PROFIT_RATIO));
//        //获取到平台 每单盈利的百分比  从缓存去  0.85
//        BigDecimal winScale = NumberUtils.createBigDecimal(redisTemplate.opsForValue().get(ORDER_WIN_SCALE));
//        //连本带息比例
//        BigDecimal capitalWinScale = CalcUtils.add(winScale, BigDecimal.ONE, 4);
//        // 获取市场最新行情价格
//        String marketPriceStr = redisTemplate.opsForValue().get(LATEST_COIN_MARKET_PRICE + coinSymbol);
//        MarketRealPrice realPrice = JSONObject.toJavaObject(JSON.parseObject(marketPriceStr), MarketRealPrice.class);
//        //获取下一分钟的最高价 和最低价
//        BigDecimal minPirce = realPrice.getOneMinuteLowestPrice();
//        BigDecimal maxPirce = realPrice.getOneMinuteHighestPrice();
//        //计算出当前这个时间范围还未预结算的订单总额  根据币种查询
//        BigDecimal totalMoney = baseMapper.getNotBudgetOrderMoney(coinSymbol);
//        if (totalMoney.compareTo(BigDecimal.ZERO) > 0) {
//            //计算出当前这个时间范围类所有订单 平台要盈利的最低金额
//            BigDecimal platMinMoney = CalcUtils.mul(totalMoney, platMinRatio, 4);
//            //计算出当前这个时间范围类所有订单 平台要盈利的希望金额
//            BigDecimal platHopeMinMoney = CalcUtils.mul(totalMoney, platHopeMinRatio, 4);
//
//            //只要 下面的订单我预设的行情价格 ，需要我们平台赚这么多钱，就要计算出平台奖励出去的金额在一个范围内就可以
//            //只要控制好我们让哪些订单亏 平台保证赢就可以
//            //计算出所有订单 用户要亏损多少本金 其他的 用户亏不亏都没关系
//            //用户需要亏损总额 =当前订单总额-((当前订单总额-平台预计要盈利的金额)/1.85)
//            BigDecimal lossTotalMoney = CalcUtils.sub(totalMoney, platMinMoney, 4);
//            lossTotalMoney = CalcUtils.div(lossTotalMoney, capitalWinScale, 4);
//            lossTotalMoney = CalcUtils.sub(totalMoney, lossTotalMoney, 4);
//            //现在得出了用户需要亏损的本金数量
//            //计算权重比例 计算每个时间点让用户亏多少
//            BigDecimal lossScale = CalcUtils.div(lossTotalMoney, totalMoney, 4);
//
//            //1：计算 每秒订单数量是小于1的
//            //根据时间分组查询 还未预结算的 时间分段，根据币种查询
//            List<BudgetTimes> budgetTimesLessOne = baseMapper.getBudgetTimesLessOne(coinSymbol);
//            if (budgetTimesLessOne != null && budgetTimesLessOne.size() > 0) {
//                //计算这个订单里面让用户亏的本金总额
//                BigDecimal budgetTimesLessOneTotalMoney = baseMapper.getBudgetTimesLessOneTotalMoney(coinSymbol);
//                BigDecimal lossLessOneMoney = CalcUtils.mul(budgetTimesLessOneTotalMoney, lossScale, 4);
//                for (int i = 0; i < budgetTimesLessOne.size(); i++) {
//                    //按照顺序往下取 如果当前订单 亏损的情况下 平台盈利超过了 希望盈利 而且不是最后一单就跳过
//                    BudgetTimes budgetTimes = budgetTimesLessOne.get(i);
//                    if (budgetTimes != null) {
//                        //计算当前如果平台赢了 计算出来的 盈亏比例
//                        if (i != budgetTimesLessOne.size() - 1) {
//                            //平台总投入
//                            BigDecimal toDayBuyMoney = CalcUtils.add(toDayOrderAmount.getToDayBuyMoney(), budgetTimes.getMoney(), 4);
//                            //平台盈利
//                            BigDecimal toDayInMoney = CalcUtils.add(toDayOrderAmount.getToDayIncome(), budgetTimes.getMoney(), 4);
//                            BigDecimal nowScale = CalcUtils.div(toDayInMoney, toDayBuyMoney, 4);
//                            //如果当前这个单用户亏了 会让平台的盈利超过希望的比例，而且不是最后一单 那么可以跳过看下面的订单亏损盈利情况
//                            if (nowScale.compareTo(platHopeMinMoney) > 0) {
//                                continue;
//                            }
//                        }
//                        if (lossLessOneMoney.compareTo(BigDecimal.ZERO) > 0) {
//                            //每次减少 需要亏损的本金
//                            BigDecimal randomPrice = BigDecimal.ZERO;
//                            lossLessOneMoney = CalcUtils.sub(lossLessOneMoney, budgetTimes.getMoney(), 4);
//                            if (UserConstants.BUY_TYPE_1.equals(budgetTimes.getBuyType())) {
//                                //如果是买涨 向下取价格
//                                randomPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(minPirce, budgetTimes.getOpenPrice());
//                            } else if (UserConstants.BUY_TYPE_2.equals(budgetTimes.getBuyType())) {
//                                randomPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(budgetTimes.getOpenPrice(), maxPirce);
//                            }
//                            //更新预结算价格
//                            if (randomPrice.compareTo(BigDecimal.ZERO) > 0) {
//                                OrderInfo orderInfo = new OrderInfo();
//                                orderInfo.setBudgetPrice(randomPrice);
//                                orderInfo.setBudgetFlag(UserConstants.USER_STATUS_0);
//                                QueryWrapper<OrderInfo> orderInfoQueryWrapper = new QueryWrapper<>();
//                                orderInfoQueryWrapper.eq("create_time", budgetTimes.getCreateTime()).eq("coin_symbol", coinSymbol)
//                                        .eq("status", UserConstants.BUY_ORDER_STATUS_1).eq("budget_flag", UserConstants.USER_STATUS_1);
//                                this.update(orderInfo, orderInfoQueryWrapper);
//                            }
//                        } else {
//                            //满足后跳出
//                            break;
//                        }
//                    }
//                }
//            }
//            //2：如果订单是 超过单一订单的情况下  先查询出有多少个时间在根据时间查询当前的订单去做比对
//            List<LocalDateTime> budgetTimesMore = baseMapper.getBudgetTimesMore(coinSymbol);
//            for (LocalDateTime b : budgetTimesMore) {
//                //然后查询每个时间段做比对 价格倒叙
//                QueryWrapper<OrderInfo> orderInfoQueryWrapper = new QueryWrapper<>();
//                orderInfoQueryWrapper.eq("create_time", b).eq("coin_symbol", coinSymbol)
//                        .eq("status", UserConstants.BUY_ORDER_STATUS_1).eq("budget_flag", UserConstants.USER_STATUS_1);
//                orderInfoQueryWrapper.orderByAsc("open_price desc");
//                List<OrderInfo> orderInfos = list(orderInfoQueryWrapper);
//                //获取当前这秒钟用户应该亏损的本金  先统计出当前这秒钟下单的金额
//                OrderInfo info = new OrderInfo();
//                info.setCreateTime(b);
//                info.setCoinSymbol(coinSymbol);
//                BigDecimal nowTotalMoney = baseMapper.getBudgetTimesMoreTotalMoney(info);
//                //计算出当前应该亏损的总额了
//                BigDecimal lossNowTotalMoney = CalcUtils.mul(nowTotalMoney, lossScale, 4);
//                for (int i = 0; i < orderInfos.size(); i++) {
//                    OrderInfo orderInfo = orderInfos.get(i);
//                    BigDecimal nowPrice = BigDecimal.ZERO;
//                    if (i == 0) {
//                        if (UserConstants.BUY_TYPE_1.equals(orderInfo.getBuyType())) {
//                            //如果是买涨 向下取价格
//                            nowPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(minPirce, orderInfo.getOpenPrice());
//                        } else if (UserConstants.BUY_TYPE_2.equals(orderInfo.getBuyType())) {
//                            nowPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfo.getOpenPrice(), maxPirce);
//                        }
//                    } else if (i == orderInfos.size() - 1) {
//                        if (UserConstants.BUY_TYPE_1.equals(orderInfo.getBuyType())) {
//                            //如果是买涨 向下取价格
//                            nowPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(minPirce, orderInfo.getOpenPrice());
//                        } else if (UserConstants.BUY_TYPE_2.equals(orderInfo.getBuyType())) {
//                            nowPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfo.getOpenPrice(), maxPirce);
//                        }
//                    } else {
//                        OrderInfo nextOrderInfo = orderInfos.get(i + 1);
//                        nowPrice = CalcUtils.getRandomRedPacketBetweenMinAndMax(orderInfo.getOpenPrice(), nextOrderInfo.getOpenPrice());
//                    }
//                    //根据当前价计算 当前的亏损金额是否可以满足亏损本金
//                    BigDecimal lossMoreMoeny = getLossMoreMoney(orderInfos, nowPrice);
//                    if (lossMoreMoeny.compareTo(lossNowTotalMoney) > 0) {
//                        //亏损总额满足情况时候  修改订单
//                        this.handleOrderBudgetPrice(orderInfos, nowPrice);
//                        break;
//                    }
//                }
//            }
//        }
//    }

    /**
     * 判断当前订单的亏损金额是否满足最低亏损标准
     *
     * @param orderInfos
     * @param nowPrice
     * @return
     */
    public BigDecimal getLossMoreMoney(List<OrderInfo> orderInfos, BigDecimal nowPrice) {
        BigDecimal lossMoney = BigDecimal.ZERO;
        for (OrderInfo o : orderInfos) {
            if (UserConstants.BUY_TYPE_1.equals(o.getBuyType())) {
                if (nowPrice.compareTo(o.getOpenPrice()) <= 0) {
                    lossMoney = CalcUtils.add(lossMoney, o.getMoney(), 4);
                }
            } else if (UserConstants.BUY_TYPE_2.equals(o.getBuyType())) {
                if (nowPrice.compareTo(o.getOpenPrice()) >= 0) {
                    lossMoney = CalcUtils.add(lossMoney, o.getMoney(), 4);
                }
            }
        }
        return lossMoney;
    }

    /**
     * 修改电当前订单列表价格
     *
     * @param orderInfos
     * @param nowPrice
     */
    public void handleOrderBudgetPrice(List<OrderInfo> orderInfos, BigDecimal nowPrice) {
        for (OrderInfo o : orderInfos) {
            o.setBudgetPrice(nowPrice);
            o.setBudgetFlag(UserConstants.USER_STATUS_0);
            updateById(o);
        }
    }

    @Override
    public int getNowIndemnityNum(OrderInfo orderInfo) {
        return baseMapper.getNowIndemnityNum(orderInfo);
    }

    @Override
    public BigDecimal getBeforeDayTotalmoney() {
        return baseMapper.getBeforeDayTotalmoney();
    }

    @Override
    public BigDecimal getBZWDestoryNum() {
        return baseMapper.getBZWDestoryNum();
    }

    @Override
    public BigDecimal getUSDTNumByDay(OrderInfo info) {
        return baseMapper.getUSDTNumDay(info);
    }

    @Override
    public OrderOverview getOrderOverviewData(GetBuyOrderListDto listDto) {
        String uid = UserContext.getUserID();
        listDto.setUid(uid);
        return baseMapper.getOrderOverviewData(listDto);
    }

    @Override
    public ResultVo getOrderTotal(String coinSymbol) {
        String uid = UserContext.getUserID();
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setUid(uid);
        orderInfo.setCoinSymbol(coinSymbol);
        BZTotalVo totalVo = baseMapper.getOrderIngMoney(orderInfo);

        QueryWrapper<OrderInfo> orderInfoQueryWrapper = new QueryWrapper<>();
        orderInfoQueryWrapper.eq("uid", uid).eq("coin_symbol", coinSymbol)
                .eq("status", UserConstants.ORDER_STATUS_1).eq("order_type", UserConstants.ORDER_TYPE_2);
        int indemnityNum = count(orderInfoQueryWrapper);
        totalVo.setIndemnityNum(indemnityNum);

        OrderRuleVo orderRuleVo = baseClient.getNewlast();
        if (orderRuleVo != null) {
            totalVo.setOverTime(LocalDateTime.of(LocalDate.now(), LocalTime.parse(orderRuleVo.getEndTime())));
        }
        totalVo.setUsdtPrice(redisUtils.getUSDTPrice());

        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", uid).eq("coin_symbol", SysCoinsTypeEnums.USDT.getName());
        WalletInfo walletInfo = iWalletInfoService.getOne(wrapper);
        totalVo.setUsdt(walletInfo == null ? BigDecimal.ZERO : walletInfo.getWalletBalance());
        return ResultVo.success(totalVo);
    }

    @Override
    public ResultVo getOverTime() {
        LocalDateTime time = null;
        OrderRuleVo orderRuleVo = baseClient.getNewlast();
        if (orderRuleVo != null) {
            time = LocalDateTime.of(LocalDate.now(), LocalTime.parse(orderRuleVo.getEndTime()));
        }
        BigDecimal usdtPrice = redisUtils.getUSDTPrice();
        HashMap<String, Object> map = new HashMap<>();
        map.put("time", time);
        map.put("price", usdtPrice);
        return ResultVo.success(map);
    }

    @Override
    public ResultVo getMyTeamPerformance(String uid) {
        TeamPerformanceVo performance = new TeamPerformanceVo();
        //获取个人总流水
        GetBuyOrderListDto overview=new GetBuyOrderListDto();
        overview.setUid(uid);
        performance.setPersonOrderMoney( baseMapper.getOrderOverviewData(overview).getTotalMoney());
        // 获取团队人数
        performance.setTeamNum(iInvitationService.getTeamMembers(uid));
        // 当日总流水
        performance.setDayOrderMoney(iInvitationService.getTeamPerformanceByDay(uid));
        // 当月总流水
        performance.setMonthOrderMoney(iInvitationService.getTeamPerformanceByMonth(uid));
        // 获取有效直推人数
        QueryWrapper<Invitation> wrapper = new QueryWrapper<>();
        wrapper.eq("invitation_uid", uid);
        int directNum = iInvitationService.count(wrapper);
        performance.setDirectNum(directNum);
        //获取今日推广收益
        WalletReward walletReward = new WalletReward();
        walletReward.setUid(uid);
        walletReward.setCreateTime(LocalDateTime.now());
        walletReward.setTypeId(WalletLogTypeEnums.PUSH_REWARD.getTypeId());
        walletReward.setFlag(1);
        BigDecimal todayPromoteMoney = iRewardLogsService.getRewardByTypeId(walletReward);
        if(todayPromoteMoney==null){
            todayPromoteMoney=BigDecimal.ZERO;
        }
        performance.setTodayPromoteMoney(todayPromoteMoney.setScale(2,BigDecimal.ROUND_HALF_UP));
        //获取总推广收益
        walletReward.setFlag(null);
        BigDecimal totalPromoteMoney = iRewardLogsService.getRewardByTypeId(walletReward);
        if(totalPromoteMoney==null){
            totalPromoteMoney=BigDecimal.ZERO;
        }
        performance.setTotalPromoteMoney(totalPromoteMoney.setScale(2,BigDecimal.ROUND_HALF_UP));
        return ResultVo.success(performance);
    }

    @Override
    public BigDecimal getTotalMoneyByUid(String uid) {
        return baseMapper.selectTotalMoneyByUid(uid);
    }

    @Override
    public int getNowBudgetNum(String uid) {
        return baseMapper.getNowBudgetNum(uid);
    }
}
