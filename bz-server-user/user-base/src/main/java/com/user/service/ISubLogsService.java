package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.SubscriptionDetailVo;
import com.user.entity.SubLogs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户认购记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-08-02
 */
public interface ISubLogsService extends IService<SubLogs> {

    BigDecimal getTotalMoney(SubLogs subLogs);

    Page<SubscriptionDetailVo> queryUserSubLogs(Page<SubscriptionDetailVo> page, Map<String, Object> params);
}
