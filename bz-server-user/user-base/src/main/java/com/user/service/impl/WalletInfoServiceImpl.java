package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.config.RedisUtils;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.UserConstants;
import com.common.context.UserContext;
import com.common.enums.wallet.AgreeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.lang.NumberUtils;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.dto.WalletRechargeDto;
import com.mall.model.user.vo.UserWalletQueryVo;
import com.user.entity.RewardAccount;
import com.user.entity.WalletFrozenLogs;
import com.user.entity.WalletInfo;
import com.user.entity.WalletLogs;
import com.user.mapper.WalletInfoMapper;
import com.user.model.AssetsItem;
import com.user.model.UserAssets;
import com.user.model.WalletAddr;
import com.user.service.IRewardAccountService;
import com.user.service.IWalletFrozenLogsService;
import com.user.service.IWalletInfoService;
import com.user.service.IWalletLogsService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户钱包信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
@Service
public class WalletInfoServiceImpl extends ServiceImpl<WalletInfoMapper, WalletInfo> implements IWalletInfoService {

    @Resource
    private IWalletLogsService iWalletLogsService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private IWalletFrozenLogsService iWalletFrozenLogsService;
    @Resource
    private IRewardAccountService iRewardAccountService;

    @Override
    public void queryUserWalletPageByParams(Map<String, Object> params, Page<UserWalletQueryVo> page) {
        baseMapper.queryUserWalletPageByParams(params, page);
    }

    @Override
    public ResultVo handleUserWalletMoney(WalletLogDto wallLogDto) {
        try {
            // 获取钱包信息
            QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", wallLogDto.getUid()).eq("coin_symbol", wallLogDto.getCoinSymbol());
            WalletInfo walletInfo = baseMapper.selectOne(wrapper);
            if (walletInfo == null) {
                return ResultVo.failure("钱包信息不存在");
            }
            // 计算剩余额度
            if (UserConstants.WALLET_IN.equals(wallLogDto.getInOut())) {
                //如果是进账
                BigDecimal money = CalcUtils.add(walletInfo.getWalletBalance(), wallLogDto.getCost(), walletInfo.getWalletDeclimal());
                walletInfo.setWalletBalance(money);
            } else {
                BigDecimal money = CalcUtils.sub(walletInfo.getWalletBalance(), wallLogDto.getCost(), walletInfo.getWalletDeclimal());
                walletInfo.setWalletBalance(money);
            }
            if (walletInfo.getWalletBalance().compareTo(BigDecimal.ZERO) < 0) {
                return ResultVo.failure(walletInfo.getCoinSymbol()+"钱包金额不足");
            }
            if (!this.updateById(walletInfo)) {
                throw new BaseException("更新钱包失败,频繁操作");
            }
            // 保存资金明细
            WalletLogs walletLogs = new WalletLogs();
            BeanUtils.copyProperties(wallLogDto, walletLogs);
            iWalletLogsService.save(walletLogs);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException(e.getMessage(), e);
        }
    }

    @Override
    public ResultVo handleUserFrozenWalletMoney(WalletLogDto wallLogDto) {
        try {
            // 获取钱包信息
            QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("uid", wallLogDto.getUid()).eq("coin_symbol", wallLogDto.getCoinSymbol());
            WalletInfo walletInfo = baseMapper.selectOne(wrapper);
            if (walletInfo == null) {
                return ResultVo.failure("钱包信息不存在");
            }
            // 计算剩余额度
            if (UserConstants.WALLET_IN.equals(wallLogDto.getInOut())) {
                //如果是进账
                BigDecimal money = CalcUtils.add(walletInfo.getWalletFrozenBalance(), wallLogDto.getCost(), walletInfo.getWalletDeclimal());
                walletInfo.setWalletFrozenBalance(money);
            } else {
                BigDecimal money = CalcUtils.sub(walletInfo.getWalletFrozenBalance(), wallLogDto.getCost(), walletInfo.getWalletDeclimal());
                walletInfo.setWalletFrozenBalance(money);
            }
            if (walletInfo.getWalletFrozenBalance().compareTo(BigDecimal.ZERO) < 0) {
                return ResultVo.failure(walletInfo.getCoinSymbol()+"钱包冻结金额不足");
            }
            if (!this.updateById(walletInfo)) {
                throw new BaseException("更新钱包失败,频繁操作");
            }
            // 保存资金明细
            WalletFrozenLogs walletLogs = new WalletFrozenLogs();
            BeanUtils.copyProperties(wallLogDto, walletLogs);
            iWalletFrozenLogsService.save(walletLogs);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException(e.getMessage(), e);
        }
    }

    @Override
    public ResultVo getMyAssets() {
        // 获取钱包信息
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", UserContext.getUserID());
        List<WalletInfo> walletInfoList = baseMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(walletInfoList)) {
            return ResultVo.failure("钱包信息不存在");
        }
        RewardAccount rewardAccount=iRewardAccountService.getById(UserContext.getUserID());
        if(rewardAccount==null){
            rewardAccount=new RewardAccount();
            rewardAccount.setUid(UserContext.getUserID());
            iRewardAccountService.save(rewardAccount);
        }
        BigDecimal USDT = BigDecimal.ZERO;
        UserAssets userAssets = new UserAssets();
        ArrayList<AssetsItem> assetsItems = new ArrayList<>();
        AssetsItem assetsItem = null;
        for (WalletInfo walletInfo : walletInfoList) {
            if (SysCoinsTypeEnums.BZW.getName().equals(walletInfo.getCoinSymbol())) {
                assetsItem = new AssetsItem();
                assetsItem.setCoinSymbol(walletInfo.getCoinSymbol());
                assetsItem.setWalletBalance(walletInfo.getWalletBalance());
                assetsItem.setWalletFrozenBalance(walletInfo.getWalletFrozenBalance());
                //折合USDT
                BigDecimal price = NumberUtils.createBigDecimal((String) redisTemplate.opsForHash().get(RedisKeyConstants.COIN_PRICE_BZW, "price"));
                BigDecimal convertMoney=CalcUtils.mul(price,walletInfo.getWalletBalance(),4);
                USDT = CalcUtils.add(convertMoney, USDT, 4);
                assetsItem.setConvertMoney(convertMoney);
                assetsItems.add(assetsItem);
            } else if (SysCoinsTypeEnums.USDT.getName().equals(walletInfo.getCoinSymbol())) {
                assetsItem = new AssetsItem();
                assetsItem.setCoinSymbol(walletInfo.getCoinSymbol());
                assetsItem.setWalletBalance(walletInfo.getWalletBalance());

                assetsItem.setWalletFrozenBalance(rewardAccount.getUsdtReward());
                //折合人民币
                assetsItem.setConvertMoney(CalcUtils.mul(walletInfo.getWalletBalance(), redisUtils.getUSDTPrice(), 4));
                USDT = CalcUtils.add(walletInfo.getWalletBalance(), USDT, 4);
                assetsItems.add(assetsItem);
            }
        }
        userAssets.setList(assetsItems);
        userAssets.setTotalUSDT(USDT);
        userAssets.setTotalMoney(CalcUtils.mul(USDT, redisUtils.getUSDTPrice(), 4));
        return ResultVo.success(userAssets);
    }

    @Override
    public ResultVo getMyAssetsItem(String coinSymbol) {
        // 获取钱包信息
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", UserContext.getUserID()).eq("coin_symbol", coinSymbol);
        WalletInfo info = baseMapper.selectOne(wrapper);
        if (info == null) {
            return ResultVo.failure("钱包信息不存在");
        }
        AssetsItem assetsItem = new AssetsItem();
        BeanUtils.copyProperties(info, assetsItem);
        if (SysCoinsTypeEnums.BZW.getName().equals(coinSymbol)) {
            // 折合USDT
            BigDecimal price = NumberUtils.createBigDecimal((String) redisTemplate.opsForHash().get(RedisKeyConstants.COIN_PRICE_BZW, "price"));
            assetsItem.setConvertMoney(CalcUtils.mul(price,info.getWalletBalance(),4));
        } else if (SysCoinsTypeEnums.USDT.getName().equals(coinSymbol)) {
            assetsItem.setConvertMoney(CalcUtils.mul(assetsItem.getWalletBalance(), redisUtils.getUSDTPrice(), 4));

            RewardAccount rewardAccount=iRewardAccountService.getById(UserContext.getUserID());
            assetsItem.setWalletFrozenBalance(rewardAccount.getUsdtReward());
        }
        return ResultVo.success(assetsItem);
    }

    @Override
    public List<WalletRechargeDto> getRechargeByCoinSymbol(String coinSymbol) {
        return baseMapper.queryRechargeByCoinSymbol(coinSymbol);
    }

    @Override
    public WalletAddr getWalletAddr(String coinSymbol, String crotocol) {
        // 获取钱包信息
        QueryWrapper<WalletInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", UserContext.getUserID()).eq("coin_symbol", coinSymbol);
        WalletInfo walletInfo = baseMapper.selectOne(wrapper);
        if (walletInfo == null) {
            throw new BaseException("钱包信息不存在");
        }

        WalletAddr walletAddr = new WalletAddr();
        if (SysCoinsTypeEnums.BZW.getName().equals(coinSymbol)) {
            walletAddr.setWalletAddr(walletInfo.getWalletAddr());
            walletAddr.setWalletAddrQrUrl(walletInfo.getWalletQrUrl());
        } else if (SysCoinsTypeEnums.USDT.getName().equals(coinSymbol)) {
            if (AgreeEnums.ERC20.name().equals(crotocol)) {
                walletAddr.setWalletAddr(walletInfo.getWalletAddr());
                walletAddr.setWalletAddrQrUrl(walletInfo.getWalletQrUrl());
            } else if (AgreeEnums.OMNI.name().equals(crotocol)) {
                walletAddr.setWalletAddr(walletInfo.getWalletOminAddr());
                walletAddr.setWalletAddrQrUrl(walletInfo.getWalletOmniQrUrl());
            }
        }
        return walletAddr;
    }
}
