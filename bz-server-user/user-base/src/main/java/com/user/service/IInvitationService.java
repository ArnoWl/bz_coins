package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;
import com.mall.model.user.vo.TeamOrderVo;
import com.mall.model.user.vo.UserVo;
import com.user.entity.Invitation;
import com.user.model.OrderRewardDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户邀请关系表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-12
 */
public interface IInvitationService extends IService<Invitation> {

    /**
     * 添加用户邀请关系链
     * @param invitationCode
     * @param beInvitedUid
     */
    ResultVo addUserInvitation(String invitationCode, String beInvitedUid);

    /**
     * 获取团队总人数 -- 伞下
     * @param uid
     * @return
     */
    int getTeamMembers(String uid);

    /**
     * 获取团队成员列表（下单信息）
     * @param uid
     * @return
     */
    List<TeamOrderVo> getTeamMemberOrders(String uid);

    /**
     * 查询当前关系链下面团队 每个级别的人数
     * @param id
     * @return
     */
    List<Map<String, Object>> getTeamLevel(Integer id);
    /**
     * 获取团队业绩 -- 当日
     * @param uid
     * @return
     */
    BigDecimal getTeamPerformanceByDay(String uid);

    /**
     * 获取团队业绩 -- 当日
     * @param uid
     * @return
     */
    BigDecimal getTeamPerformanceByMonth(String uid);

    /**
     * 下单成功团队奖励返送
     * @param rewardDto
     */
    void buySuccessReferralRewards(OrderRewardDto rewardDto);

    /**
     * 根据关系链id 查询用户某级别会员
     * @param id 关系链id
     * @param levelid 等级id
     * @return
     */
    List<UserVo> queryTeamLevel(Integer id, Integer levelid);

    /**
     * 查询会员团队-所有
     * @param page
     * @param uid
     * @return
     */
    Page<TeamOrderVo> queryTeamMemberOrders(Page<TeamOrderVo> page, String uid);
}
