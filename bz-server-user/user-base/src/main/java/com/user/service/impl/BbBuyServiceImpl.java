package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.context.UserContext;
import com.mall.model.user.vo.*;
import com.user.entity.BbBuy;
import com.user.mapper.BbBuyMapper;
import com.user.service.IBbBuyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 用户币币求购记录主表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Service
public class BbBuyServiceImpl extends ServiceImpl<BbBuyMapper, BbBuy> implements IBbBuyService {

    @Override
    public Page<BBBuyQueryVo> queryBuyList(Map<String, Object> params, Page<BBBuyQueryVo> page) {
        return baseMapper.queryBbBuyPageByParams(params, page);
    }

    @Override
    public Page<BBOrderPlatQueryVo> queryPlatBbBuyPageByParams(Map<String, Object> params, Page<BBOrderPlatQueryVo> page) {
        return baseMapper.queryPlatBbBuyPageByParams(params, page);
    }

    @Override
    public BBDataVo getBuyInfo(Integer id) {
        String uid = UserContext.getUserID();
        return baseMapper.getBuyInfo(id, uid);
    }

    @Override
    public  Page<BBDelegationQueryVo> queryDelegationList(Map<String, Object> params, Page<BBDelegationQueryVo> page) {
        return baseMapper.queryDelegationList(params, page);
    }
}
