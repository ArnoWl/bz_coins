package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.UserConstants;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.codec.CodeUtils;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.vo.UserVo;
import com.user.entity.Info;
import com.user.entity.LevelInfo;
import com.user.mapper.InfoMapper;
import com.user.service.IInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.user.service.ILevelInfoService;
import com.user.service.IOrderInfoService;
import com.user.service.IWalletInfoService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-12
 */
@Service
public class InfoServiceImpl extends ServiceImpl<InfoMapper, Info> implements IInfoService {


    @Resource
    private IOrderInfoService iOrderInfoService;
    @Resource
    private ILevelInfoService iLevelInfoService;
    @Resource
    private IWalletInfoService iWalletInfoService;


    @Override
    public Page<UserVo> queryUserInfoPageByParams(Map<String, Object> params, Page<UserVo> page) {
        return page.setRecords(baseMapper.queryUserInfoPageByParams(params,page));
    }

    @Override
    public void handleV8EveryShare() {
        //获取前一天的订单总额
        BigDecimal totalMoney= iOrderInfoService.getBeforeDayTotalmoney();
        if(totalMoney==null || totalMoney.compareTo(BigDecimal.ZERO)<=0){
            return;
        }
        //查询V8级别所有会员
        List<Info> infos=baseMapper.getShareCus();
        if(infos!=null && infos.size()>0){
            LevelInfo levelInfo=iLevelInfoService.getById(9);
            if(levelInfo!=null){
                //计算分红总额
                BigDecimal totalShareMoney= CalcUtils.mul(totalMoney,levelInfo.getShareScale(),4);
                BigDecimal everyMoney=CalcUtils.div(totalShareMoney,new BigDecimal(infos.size()),4);
                if(everyMoney.compareTo(BigDecimal.ZERO)>0){
                    String orderCode= CodeUtils.generateOrderNo();
                    for(Info i: infos){
                        ResultVo resultVo = iWalletInfoService.handleUserWalletMoney(new WalletLogDto(i.getUid(), SysCoinsTypeEnums.USDT.getName(), orderCode,
                                WalletLogTypeEnums.INDEMNITY_SETTLEMENT.getTypeId(), UserConstants.WALLET_IN, everyMoney, "【"+levelInfo.getName()+"】每日分红"));
                        if (!resultVo.isSuccess()) {
                            throw new BaseException(resultVo.getErrorMessage());
                        }
                    }
                }
            }
        }
    }
}
