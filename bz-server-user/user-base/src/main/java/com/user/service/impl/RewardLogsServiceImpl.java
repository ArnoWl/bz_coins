package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserRewardVo;
import com.user.entity.RewardLogs;
import com.user.mapper.RewardLogsMapper;
import com.user.model.WalletReward;
import com.user.service.IRewardLogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户收益明细表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
@Service
public class RewardLogsServiceImpl extends ServiceImpl<RewardLogsMapper, RewardLogs> implements IRewardLogsService {

    @Override
    public BigDecimal getRewardByTypeId(WalletReward walletReward) {
        return baseMapper.getRewardByTypeId(walletReward);
    }

    @Override
    public Page<UserRewardVo> queryList(Page<UserRewardVo> page, Map<String, String> map) {
        return baseMapper.queryList(page,map);
    }
}
