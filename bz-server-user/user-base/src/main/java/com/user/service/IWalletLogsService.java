package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.model.user.vo.UserWalletLogQueryVo;
import com.user.entity.WalletLogs;

import java.util.Map;

/**
 * <p>
 * 钱包操作明细记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
public interface IWalletLogsService extends IService<WalletLogs> {

    Page<UserWalletLogQueryVo> queryUserWalletLogPageByParams(Map<String, Object> params, Page<UserWalletLogQueryVo> page);
}
