package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import com.user.entity.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.user.entity.OrderOverview;
import com.user.model.ToDayOrderAmount;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
public interface IOrderInfoService extends IService<OrderInfo> {

    /**
     * 获取今日订单金额统计
     * @return
     */
    ToDayOrderAmount getCountTodayOrderMoney();

    /**
     * 获取买入订单列表 -- 分页
     * @param listDto
     * @return
     */
    Page<BuyOrderListVo> getBuyOrderPage(GetBuyOrderListDto listDto);

    /**
     * 结算订单
     * @param orderSn
     * @return
     */
    ResultVo settlementOrder(String orderSn);

    /**
     * 定时处理包赔
     */
    void handleIndemnityOrder();

    /**
     * 处理订单预结算
     * @param  coinSymbol 币种每个币种要分开处理
     */
    void handleBudgetOrder(String coinSymbol);

    /**
     * 查询当日 当前包赔场的下单数量
     * @param orderInfo
     * @return
     */
    int getNowIndemnityNum(OrderInfo orderInfo);


    BigDecimal getBeforeDayTotalmoney();


    BigDecimal getBZWDestoryNum();

    /**
     * 获取某一天的USDT交易量(某一天流水)
     * @param info
     * @return
     */
    BigDecimal getUSDTNumByDay(OrderInfo info);

    /**
     * 订单数据总览
     * @return
     */
    OrderOverview getOrderOverviewData(GetBuyOrderListDto listDto);

    /**
     * 获取币指交易统计数据
     * @return
     */
    ResultVo getOrderTotal(String coinSymbol);

    /**
     * 获取包赔场结束时间
     * @return
     */
    ResultVo getOverTime();

    /**
     * 我的团队业绩
     * @return
     */
    ResultVo getMyTeamPerformance(String uid);

    /**
     * 根据用户UID获取下单总额
     * @param uid
     * @return
     */
    BigDecimal getTotalMoneyByUid(String uid);


    int getNowBudgetNum(String uid);
}
