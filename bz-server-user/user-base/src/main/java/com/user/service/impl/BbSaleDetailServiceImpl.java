package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.context.UserContext;
import com.mall.model.user.vo.BBDetailVo;
import com.mall.model.user.vo.BBOrderDetailPlatQueryVo;
import com.user.entity.BbSaleDetail;
import com.user.mapper.BbSaleDetailMapper;
import com.user.service.IBbSaleDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户币币出售明细记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Service
public class BbSaleDetailServiceImpl extends ServiceImpl<BbSaleDetailMapper, BbSaleDetail> implements IBbSaleDetailService {

    @Override
    public BBDetailVo getOrderDetail( Map<String, Object> params) {
        return baseMapper.getOrderDetail(params);
    }

    @Override
    public Page<BBOrderDetailPlatQueryVo> queryPlatBbSaleDetailsPageByParams(Map<String, Object> params, Page<BBOrderDetailPlatQueryVo> page) {
        return baseMapper.queryPlatBbSaleDetailsPageByParams(page, params);
    }
}
