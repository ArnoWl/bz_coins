package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.dto.WalletRechargeDto;
import com.mall.model.user.vo.UserWalletQueryVo;
import com.mall.model.wallet.dto.WalletCashDto;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.user.entity.WalletInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.user.model.WalletAddr;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户钱包信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
public interface IWalletInfoService extends IService<WalletInfo> {

    /**
     * 根据参数获取用户钱包信息列表  -- 分页
     *
     * @param params
     * @param page
     */
    void queryUserWalletPageByParams(Map<String, Object> params, Page<UserWalletQueryVo> page);

    /**
     * 处理用户钱包账户金额
     *
     * @param wallLogDto
     * @return
     */
    ResultVo handleUserWalletMoney(WalletLogDto wallLogDto);

    /**
     * 处理用户冻结钱包账户金额
     *
     * @param wallLogDto
     * @return
     */
    ResultVo handleUserFrozenWalletMoney(WalletLogDto wallLogDto);

    /**
     * 获取我的资产
     *
     * @return
     */
    ResultVo getMyAssets();

    /**
     * 获取钱包地址
     *
     * @return
     */
    WalletAddr getWalletAddr(String coinSymbol, String crotocol);

    /**
     * 获取单项我的资产
     * @param coinSymbol
     * @return
     */
    ResultVo getMyAssetsItem(String coinSymbol);

    /**
     * 根据币种获取钱包充值信息
     * @param coinSymbol
     * @return
     */
    List<WalletRechargeDto> getRechargeByCoinSymbol(String coinSymbol);
}
