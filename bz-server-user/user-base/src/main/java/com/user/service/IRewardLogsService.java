package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserRewardVo;
import com.user.entity.RewardLogs;
import com.baomidou.mybatisplus.extension.service.IService;
import com.user.model.WalletReward;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户收益明细表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
public interface IRewardLogsService extends IService<RewardLogs> {

    /**
     * 查询收益
     * @param walletReward
     * @return
     */
    BigDecimal getRewardByTypeId(WalletReward walletReward);

    /**
     * 查询奖励记录
     * @param page
     * @param map
     * @return
     */
    Page<UserRewardVo> queryList(Page<UserRewardVo> page,Map<String, String> map);
}
