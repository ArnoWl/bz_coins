package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserMessageQueryVo;
import com.user.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户站内信 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-23
 */
public interface IMessageService extends IService<Message> {

    /**
     * 根据条件获取用户站内信列表 -- 分页
     *
     * @param params
     * @param page
     */
    Page<UserMessageQueryVo> queryUserMessagePageByParams(Map<String, Object> params, Page<UserMessageQueryVo> page);
}
