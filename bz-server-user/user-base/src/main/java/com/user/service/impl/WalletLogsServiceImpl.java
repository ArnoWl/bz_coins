package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.model.user.vo.UserWalletLogQueryVo;
import com.user.entity.WalletLogs;
import com.user.mapper.WalletLogsMapper;
import com.user.service.IWalletLogsService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 钱包操作明细记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
@Service
public class WalletLogsServiceImpl extends ServiceImpl<WalletLogsMapper, WalletLogs> implements IWalletLogsService {

    @Override
    public Page<UserWalletLogQueryVo> queryUserWalletLogPageByParams(Map<String, Object> params, Page<UserWalletLogQueryVo> page) {
        return baseMapper.queryUserWalletLogPageByParams(params,page);
    }
}
