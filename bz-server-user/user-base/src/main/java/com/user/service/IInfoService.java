package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserVo;
import com.user.entity.Info;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-12
 */
public interface IInfoService extends IService<Info> {

    /**
     * 根据条件获取用户列表 -- 分页
     *
     * @param params
     * @param page
     */
    Page<UserVo> queryUserInfoPageByParams(Map<String, Object> params, Page<UserVo> page);


    /**
     * 处理V8每日分红
     */
    void handleV8EveryShare();
}
