package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.BBDataVo;
import com.mall.model.user.vo.BBOrderPlatQueryVo;
import com.mall.model.user.vo.BBSaleQueryVo;
import com.mall.model.user.vo.BBSaleVo;
import com.user.entity.BbSale;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户币币挂售订单主表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface IBbSaleService extends IService<BbSale> {

    /**
     * 我要买列表
     * @param params
     * @param page
     * @return
     */
    Page<BBSaleQueryVo> querySaleList(Map<String, Object> params, Page<BBSaleQueryVo> page);

    /**
     * 平台查询
     * @param params
     * @param page
     * @return
     */
    Page<BBOrderPlatQueryVo> queryPlatBbSalePageByParams(Map<String, Object> params, Page<BBOrderPlatQueryVo> page);


    BBDataVo getSaleInfo(Integer id);
}
