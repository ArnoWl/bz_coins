package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.model.user.vo.*;
import com.user.entity.BbBuy;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户币币求购记录主表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface IBbBuyService extends IService<BbBuy> {

    /**
     * 用户查询
     *
     * @param params
     * @param page
     * @return
     */
    Page<BBBuyQueryVo> queryBuyList(Map<String, Object> params, Page<BBBuyQueryVo> page);

    /**
     * 平台查询
     *
     * @param params
     * @param page
     * @return
     */
    Page<BBOrderPlatQueryVo> queryPlatBbBuyPageByParams(Map<String, Object> params, Page<BBOrderPlatQueryVo> page);

    /**
     * 交易订单信息
     *
     * @param id
     * @return
     */
    BBDataVo getBuyInfo(Integer id);

    /**
     * 查询我的委托列表
     * @param params
     * @param page
     */
    Page<BBDelegationQueryVo> queryDelegationList(Map<String, Object> params, Page<BBDelegationQueryVo> page);
}
