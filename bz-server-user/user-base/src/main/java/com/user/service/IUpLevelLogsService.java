package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserUpLogVo;
import com.user.entity.UpLevelLogs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户升级记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-08-09
 */
public interface IUpLevelLogsService extends IService<UpLevelLogs> {

    Page<UserUpLogVo> queryList(Page<UserUpLogVo> page, Map<String, String> map);
}
