package com.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.constatns.UserConstants;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.enums.wallet.SysCoinsTypeEnums;
import com.common.exception.BaseException;
import com.common.utils.ExtBeanUtils;
import com.common.utils.codec.CodeUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mall.model.user.dto.WalletLogDto;
import com.mall.model.user.vo.UserLevelQueryVo;
import com.user.entity.Info;
import com.user.entity.Invitation;
import com.user.entity.LevelInfo;
import com.user.entity.OrderInfo;
import com.user.mapper.LevelInfoMapper;
import com.user.model.ReferralRewardsInfo;
import com.user.service.IInfoService;
import com.user.service.IInvitationService;
import com.user.service.ILevelInfoService;
import com.user.service.IWalletInfoService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会员等级表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Service
public class LevelInfoServiceImpl extends ServiceImpl<LevelInfoMapper, LevelInfo> implements ILevelInfoService {

    @Override
    public ResultVo queryUserLevel() {
        List<LevelInfo> levelList = this.list();
        if (!CollectionUtils.isEmpty(levelList)) {
            return ResultVo.success(ExtBeanUtils.copyList(levelList, UserLevelQueryVo.class));
        }
        return ResultVo.success(Lists.newArrayList());
    }

}
