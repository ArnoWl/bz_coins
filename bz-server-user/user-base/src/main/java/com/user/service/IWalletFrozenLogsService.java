package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserWalletFrozenLogQueryVo;
import com.mall.model.user.vo.UserWalletLogQueryVo;
import com.user.entity.WalletFrozenLogs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 冻结钱包操作明细记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-28
 */
public interface IWalletFrozenLogsService extends IService<WalletFrozenLogs> {

    Page<UserWalletFrozenLogQueryVo> queryUserWalletFrozenLogPageByParams(Map<String, Object> params, Page<UserWalletFrozenLogQueryVo> page);
}
