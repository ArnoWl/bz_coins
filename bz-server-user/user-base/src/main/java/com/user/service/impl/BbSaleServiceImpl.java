package com.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.context.UserContext;
import com.mall.model.user.vo.BBDataVo;
import com.mall.model.user.vo.BBOrderPlatQueryVo;
import com.mall.model.user.vo.BBSaleQueryVo;
import com.mall.model.user.vo.BBSaleVo;
import com.user.entity.BbSale;
import com.user.mapper.BbSaleMapper;
import com.user.service.IBbSaleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 用户币币挂售订单主表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Service
public class BbSaleServiceImpl extends ServiceImpl<BbSaleMapper, BbSale> implements IBbSaleService {

    @Override
    public Page<BBSaleQueryVo> querySaleList(Map<String, Object> params, Page<BBSaleQueryVo> page) {
        return  baseMapper.queryBbSalePageByParams(params, page);
    }


    public Page<BBOrderPlatQueryVo> queryPlatBbSalePageByParams(Map<String, Object> params, Page<BBOrderPlatQueryVo> page) {
        return  baseMapper.queryPlatBbSalePageByParams(params, page);
    }

    @Override
    public BBDataVo getSaleInfo(Integer id) {
        String uid = UserContext.getUserID();
        return  baseMapper.getSaleInfo(id,uid);
    }
}
