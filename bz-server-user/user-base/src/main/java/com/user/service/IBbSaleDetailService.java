package com.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.BBDetailVo;
import com.mall.model.user.vo.BBOrderDetailPlatQueryVo;
import com.mall.model.user.vo.BBOrderPlatQueryVo;
import com.user.entity.BbSaleDetail;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户币币出售明细记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface IBbSaleDetailService extends IService<BbSaleDetail> {


    BBDetailVo getOrderDetail( Map<String, Object> params);

    Page<BBOrderDetailPlatQueryVo> queryPlatBbSaleDetailsPageByParams(Map<String, Object> params, Page<BBOrderDetailPlatQueryVo> page);
}
