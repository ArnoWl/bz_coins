package com.user.service;

import com.common.base.ResultVo;
import com.mall.model.user.dto.UserLevelDto;
import com.user.entity.LevelInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.user.entity.OrderInfo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 会员等级表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface ILevelInfoService extends IService<LevelInfo> {

    ResultVo queryUserLevel();
}
