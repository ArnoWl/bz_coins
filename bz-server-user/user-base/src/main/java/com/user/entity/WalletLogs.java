package com.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包操作明细记录表
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_wallet_logs")
@ApiModel(value="WalletLogs对象", description="钱包操作明细记录表")
public class WalletLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "钱包类型")
    private String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "操作类型")
    private Integer typeId;

    @ApiModelProperty(value = "1进账 2出账")
    private Integer inOut;

    @ApiModelProperty(value = "操作金额")
    private BigDecimal cost;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "触发人用户id")
    private String targetUid;

    private LocalDateTime createTime;


}
