package com.user.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户收益账户表
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_reward_account")
@ApiModel(value="RewardAccount对象", description="用户收益账户表")
public class RewardAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户Uid")
    @TableId(value = "uid", type = IdType.INPUT)
    private String uid;

    @ApiModelProperty(value = "usdt收益")
    private BigDecimal usdtReward;

    @ApiModelProperty(value = "版本号")
    private Integer version;


}
