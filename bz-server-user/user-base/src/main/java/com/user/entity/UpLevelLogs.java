package com.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户升级记录表
 * </p>
 *
 * @author lt
 * @since 2020-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_up_level_logs")
@ApiModel(value="UpLevelLogs对象", description="用户升级记录表")
public class UpLevelLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String uid;

    @ApiModelProperty(value = "原始级别")
    private Integer oldLevel;

    @ApiModelProperty(value = "最新级别")
    private Integer newLevel;

    @ApiModelProperty(value = "1自动升级 2系统修改")
    private Integer type;

    private LocalDateTime createTime;


}
