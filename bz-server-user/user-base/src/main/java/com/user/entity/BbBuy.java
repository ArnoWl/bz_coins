package com.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户币币求购记录主表
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_bb_buy")
@ApiModel(value="BbBuy对象", description="用户币币求购记录主表")
public class BbBuy implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private String uid;

    @ApiModelProperty(value = "币种数量")
    private String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总数量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "剩余数量")
    private BigDecimal balanceNum;

    @ApiModelProperty(value = "最低购买金额")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最高购买金额")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "版本号")
    private Integer version;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "1交易中  2已结束")
    private Integer status;


}
