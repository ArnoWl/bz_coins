package com.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户币币出售明细记录表
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_bb_sale_detail")
@ApiModel(value="BbSaleDetail对象", description="用户币币出售明细记录表")
public class BbSaleDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private String uid;

    @ApiModelProperty(value = "币种类型")
    private String coinSymbol;

    @ApiModelProperty(value = "币币出售订单主表id")
    private Integer bbSaleId;

    @ApiModelProperty(value = "订单号")
    private String payCode;

    @ApiModelProperty(value = "出售数量")
    private BigDecimal num;

    @ApiModelProperty(value = "总金额")
    private BigDecimal totalMoney;

    private LocalDateTime createTime;


}
