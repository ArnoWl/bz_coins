package com.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户收益明细表
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_reward_logs")
@ApiModel(value="RewardLogs对象", description="用户收益明细表")
public class RewardLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "操作类型")
    private Integer typeId;

    @ApiModelProperty(value = "1进账 2出账")
    private Integer inOut;

    @ApiModelProperty(value = "操作金额")
    private BigDecimal cost;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "触发人用户id")
    private String targetUid;

    private LocalDateTime createTime;


}
