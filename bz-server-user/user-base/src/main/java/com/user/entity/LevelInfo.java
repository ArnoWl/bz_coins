package com.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员等级表
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_level_info")
@ApiModel(value="LevelInfo对象", description="会员等级表")
public class LevelInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "等级名称")
    private String name;

    @ApiModelProperty(value = "升级需要满足个人业绩")
    private BigDecimal personalMoney;

    @ApiModelProperty(value = "升级需要满足团队业绩")
    private BigDecimal teamMoney;

    @ApiModelProperty(value = "直推V1数量")
    private Integer pushNum;

    @ApiModelProperty(value = "团队里面包含上一个级别人数，每条线至少一个")
    private Integer teamNum;

    @ApiModelProperty(value = "团队极差奖励比例")
    private BigDecimal teamScale;

    @ApiModelProperty(value = "最低持有USDT数量")
    private BigDecimal minMoney;

    @ApiModelProperty(value = "团队平级奖励比例")
    private BigDecimal teamCommonScale;

    @ApiModelProperty(value = "全球分红比例")
    private BigDecimal shareScale;

    @ApiModelProperty(value = "升级奖励BZW数量")
    private BigDecimal coinNum;

}
