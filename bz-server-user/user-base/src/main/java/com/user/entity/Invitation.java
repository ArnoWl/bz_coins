package com.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户邀请关系表
 * </p>
 *
 * @author lt
 * @since 2020-07-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_invitation")
@ApiModel(value="Invitation对象", description="用户邀请关系表")
public class Invitation implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "邀请人UID")
    private String invitationUid;

    @ApiModelProperty(value = "被邀请人UID")
    private String beInvitedUid;

    @ApiModelProperty(value = "层级")
    private Integer level;

    @ApiModelProperty(value = "邀请关系链")
    private String invitationRelationship;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "个人业绩")
    private BigDecimal money;


}
