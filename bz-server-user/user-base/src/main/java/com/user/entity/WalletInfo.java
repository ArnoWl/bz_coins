package com.user.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户钱包信息表
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_wallet_info")
@ApiModel(value="WalletInfo对象", description="用户钱包信息表")
public class WalletInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "UID")
    private String uid;

    @ApiModelProperty(value = "货币代码 类似USDT ETH BTC等")
    private String coinSymbol;

    @ApiModelProperty(value = "omin协议钱包地址")
    private String walletOminAddr;

    @ApiModelProperty(value = "钱包地址")
    private String walletAddr;

    @ApiModelProperty(value = "钱包余额")
    private BigDecimal walletBalance;

    @ApiModelProperty(value = "冻结金额")
    private BigDecimal walletFrozenBalance;

    @ApiModelProperty(value = "钱包文件路径")
    private String walletPath;

    @ApiModelProperty(value = "钱包密码")
    private String walletPassWord;

    @ApiModelProperty(value = "钱包账号")
    private String walletAccount;

    @ApiModelProperty(value = "钱包二维码图片地址")
    private String walletQrUrl;

    @ApiModelProperty(value = "omni 协议钱包图片")
    private String walletOmniQrUrl;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "版本号")
    @Version
    private Integer version;

    @ApiModelProperty(value = "钱包精度")
    private Integer walletDeclimal;


}
