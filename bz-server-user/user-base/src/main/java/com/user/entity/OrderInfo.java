package com.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单信息表
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="OrderInfo对象", description="订单信息表")
public class OrderInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户uid")
    private String uid;

    @ApiModelProperty(value = "订单号")
    private String orderCode;

    @ApiModelProperty(value = "下单类型1买涨 2买跌")
    private Integer buyType;

    @ApiModelProperty(value = "币种")
    private String coinSymbol;

    @ApiModelProperty(value = "下单时价格")
    private BigDecimal openPrice;

    @ApiModelProperty(value = "关仓时价格")
    private BigDecimal closePrice;

    @ApiModelProperty(value = "下单货币数量")
    private BigDecimal coinNum;

    @ApiModelProperty(value = "下单花费BZW数量")
    private BigDecimal bzwNum;

    @ApiModelProperty(value = "下单花费金额")
    private BigDecimal money;

    @ApiModelProperty(value = "收益金额 可以为负数")
    private BigDecimal profit;

    @ApiModelProperty(value = "结算金额")
    private BigDecimal settleMoney;

    @ApiModelProperty(value = "下单时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "订单结束时间")
    private LocalDateTime overTime;

    @ApiModelProperty(value = "1进行中   2已结束   3结算完成")
    private Integer status;

    @ApiModelProperty(value = "1普通场次  2包赔场")
    private Integer orderType;

    @ApiModelProperty(value = "订单场次id  目前只有 1分钟的场次 ")
    private Integer orderSessions;

    @ApiModelProperty(value = "预结算价格")
    private BigDecimal budgetPrice;

    @ApiModelProperty(value = "0预结算  1未预结算")
    private Integer budgetFlag;

    @ApiModelProperty(value = "预测的是单个单 还是多个单 1单个 2多单")
    private Integer signFlag;


}
