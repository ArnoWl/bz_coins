package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.*;
import com.user.entity.BbBuy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户币币求购记录主表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface BbBuyMapper extends BaseMapper<BbBuy> {

    Page<BBBuyQueryVo> queryBbBuyPageByParams(@Param("params") Map<String, Object> params, Page<BBBuyQueryVo> page);

    BBDataVo getBuyInfo(@Param("id") Integer id, @Param("uid") String uid);

    Page<BBOrderPlatQueryVo> queryPlatBbBuyPageByParams(@Param("params") Map<String, Object> params, Page<BBOrderPlatQueryVo> page);

    Page<BBDelegationQueryVo> queryDelegationList(@Param("params") Map<String, Object> params, Page<BBDelegationQueryVo> page);
}
