package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.BBDetailQueryVo;
import com.mall.model.user.vo.BBDetailVo;
import com.mall.model.user.vo.BBOrderDetailPlatQueryVo;
import com.user.entity.BbBuyDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户币币求购明细记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface BbBuyDetailMapper extends BaseMapper<BbBuyDetail> {

    Page<BBDetailQueryVo> queryOrderList(@Param("params") Map<String, Object> params, Page<BBDetailQueryVo> page);

    BBDetailVo getOrderDetail(@Param("params") Map<String, Object> params);

    Page<BBOrderDetailPlatQueryVo> queryPlatBbBuyDetailsPageByParams(@Param("params") Map<String, Object> params, Page<BBOrderDetailPlatQueryVo> page);
}
