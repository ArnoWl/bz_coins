package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserWalletFrozenLogQueryVo;
import com.user.entity.WalletFrozenLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 冻结钱包操作明细记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-28
 */
public interface WalletFrozenLogsMapper extends BaseMapper<WalletFrozenLogs> {


    Page<UserWalletFrozenLogQueryVo> queryUserWalletFrozenLogPageByParams(@Param("params") Map<String, Object> params, Page<UserWalletFrozenLogQueryVo> page);
}
