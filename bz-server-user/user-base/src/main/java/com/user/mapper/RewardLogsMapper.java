package com.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserRewardVo;
import com.user.entity.RewardLogs;
import com.user.model.WalletReward;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户收益明细表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
public interface RewardLogsMapper extends BaseMapper<RewardLogs> {

    /**
     * 获取某一日的某个类型下的钱包收益
     *
     * @param walletReward
     * @return
     */
    BigDecimal getRewardByTypeId(@Param("params") WalletReward walletReward);

    /**
     * 查询奖励明细
     * @param page
     * @param map
     * @return
     */
    Page<UserRewardVo> queryList(Page<UserRewardVo> page,  @Param("param") Map<String, String> map);
}
