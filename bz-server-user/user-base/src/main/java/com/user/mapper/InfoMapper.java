package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserVo;
import com.user.entity.Info;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-12
 */
public interface InfoMapper extends BaseMapper<Info> {

    /**
     * 根据条件获取用户列表 -- 分页
     * @param params
     * @param page
     * @return
     */
    List<UserVo> queryUserInfoPageByParams(@Param("params")  Map<String, Object> params, Page<UserVo> page);

    List<Info> getShareCus();
}
