package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.TeamOrderVo;
import com.mall.model.user.vo.UserVo;
import com.user.entity.Invitation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.user.model.ReferralRewardsInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户邀请关系表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-12
 */
public interface InvitationMapper extends BaseMapper<Invitation> {

    /**
     * 查询用户上面层级推荐奖励比例信息
     * @param uid
     * @return
     */
    List<ReferralRewardsInfo> selectRecommendList(@Param("uid") String uid);

    /**
     * 统计团队总人数
     * @param uid
     * @return
     */
    Integer countTeamMembers(@Param("uid") String uid);

    /**
     * 统计团队业绩
     * @param params
     * @return
     */
    BigDecimal countTeamPerformance(@Param("params") Map<String,Object> params);

    /**
     * 统计团队平级数量 -- 伞下
     * @param params
     * @return
     */
    Integer countTeamSameLevel(@Param("params") Map<String,Object> params);

    /**
     * 查询团队成员订单统计信息列表
     * @param uid
     * @return
     */
    List<TeamOrderVo> selectTeamMemberOrders(@Param("uid") String uid);

    List<Map<String, Object>> getTeamLevel(Integer id);

    /**
     * 查询关系链 下当前会员的等级会员
     * @param id
     * @param levelId
     * @return
     */
    List<UserVo> queryTeamLevel(@Param("id") Integer id, @Param("levelId")Integer levelId);

    /**
     * 查询会员所有团队
     * @param page
     * @param uid
     * @return
     */
    Page<TeamOrderVo> queryTeamMemberOrders(Page<TeamOrderVo> page, @Param("uid") String uid);

}
