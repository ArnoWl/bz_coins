package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.SubscriptionDetailVo;
import com.user.entity.SubLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户认购记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-08-02
 */
public interface SubLogsMapper extends BaseMapper<SubLogs> {

    BigDecimal getTotalMoney(SubLogs subLogs);

    Page<SubscriptionDetailVo> queryUserSubLogs(Page<SubscriptionDetailVo> page, @Param("param") Map<String, Object> params);
}
