package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserWalletLogQueryVo;
import com.user.entity.WalletLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.user.model.WalletReward;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 钱包操作明细记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
public interface WalletLogsMapper extends BaseMapper<WalletLogs> {

    /**
     * 根据条件获取用户钱包明细列表 -- 分页
     *
     * @param params
     * @param page
     * @return
     */
    Page<UserWalletLogQueryVo> queryUserWalletLogPageByParams(@Param("params") Map<String, Object> params, Page<UserWalletLogQueryVo> page);

}
