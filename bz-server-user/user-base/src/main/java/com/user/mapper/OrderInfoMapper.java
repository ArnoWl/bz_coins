package com.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import com.mall.model.user.vo.BZTotalVo;
import com.user.entity.OrderInfo;
import com.user.entity.OrderOverview;
import com.user.model.BudgetTimes;
import com.user.model.SumBudgetAmount;
import com.user.model.ToDayBudgetAmount;
import com.user.model.ToDayOrderAmount;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

    /**
     * 普通场统计今日订单金额
     *
     * @return
     */
    ToDayOrderAmount sumToDayOrderAmount(Integer orderType);

    /**
     * 包赔场统计今日订单金额
     *
     * @return
     */
    ToDayOrderAmount sumToDayBudgetOrderAmount(Integer orderType);

    /**
     * 查询当前还未预结算的订单总额
     *
     * @return
     */
    Map<String,BigDecimal> getNotBudgetOrderMoney(@Param("map") Map<String,Object> map);

    /**
     * 查询每秒订单数量小于2的  还未结算的
     *
     * @return
     */
    List<BudgetTimes> getBudgetTimesLessOne(@Param("map")  Map<String,Object> params);

    /**
     * 查询当前每秒多订单的情况下
     *
     * @return
     */
    List<LocalDateTime> getBudgetTimesMore(@Param("map")  Map<String,Object> params);

    /**
     * 获取BZW销毁数量
     */
    BigDecimal getBZWDestoryNum();

    /**
     * 查询当日 当前包赔场的下单数量
     *
     * @param orderInfo
     * @return
     */
    int getNowIndemnityNum(OrderInfo orderInfo);

    BigDecimal getBeforeDayTotalmoney();

    /**
     * 获取某一天的USDT交易量(某一日流水)
     *
     * @param info
     * @return
     */
    BigDecimal getUSDTNumDay(@Param("params") OrderInfo info);

    /**
     * 订单数据总览
     *
     * @param listDto
     * @return
     */
    OrderOverview getOrderOverviewData(GetBuyOrderListDto listDto);

    BZTotalVo getOrderIngMoney(OrderInfo orderInfo);

    /**
     * 根据用户UID查询下单总金额
     * @param uid
     * @return
     */
    BigDecimal selectTotalMoneyByUid(@Param("uid") String uid);

    /**
     * 未结算的包赔场金额
     * @return
     */
    List<SumBudgetAmount> sumBudgetMoney();

    int getNowBudgetNum(String uid);

    List<ToDayBudgetAmount> toDayBudgetMoneyGt();

    List<ToDayBudgetAmount> toDayBudgetMoneylt();

    /**
     * 查询订单数据
     * @param infoPage
     * @param params
     * @return
     */
    Page<BuyOrderListVo> getBuyOrderPage(Page<BuyOrderListVo> infoPage, @Param("param") Map<String, Object> params);

    /**
     * 获取用户个人包赔场订单的盈利金额
     * @param uid
     * @return
     */
    BigDecimal getUserBpcProfitAmount(@Param("uid") String uid);

}
