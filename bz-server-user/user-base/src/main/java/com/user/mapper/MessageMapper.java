package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserMessageQueryVo;
import com.user.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户站内信 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-23
 */
public interface MessageMapper extends BaseMapper<Message> {

    
    Page<UserMessageQueryVo> queryUserMessagePageByParams(@Param("params") Map<String, Object> params, Page<UserMessageQueryVo> page);
}
