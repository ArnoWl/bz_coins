package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.dto.WalletRechargeDto;
import com.mall.model.user.vo.UserWalletQueryVo;
import com.user.entity.WalletInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户钱包信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-14
 */
public interface WalletInfoMapper extends BaseMapper<WalletInfo> {

    /**
     * 根据地址获取钱包信息
     * @param walletAddr
     * @return
     */
    WalletInfo getByWalletAddr(String walletAddr);

    /**
     * 根据条件获取用户钱包列表 -- 分页
     *
     * @param params
     * @param page
     * @return
     */
    Page<UserWalletQueryVo> queryUserWalletPageByParams(@Param("params") Map<String, Object> params, Page<UserWalletQueryVo> page);

    /**
     * 根据币种查询钱包充值信息
     * @param coinSymbol
     * @return
     */
    List<WalletRechargeDto> queryRechargeByCoinSymbol(@Param("coinSymbol") String coinSymbol);

}
