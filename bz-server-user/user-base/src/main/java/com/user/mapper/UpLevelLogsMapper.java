package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.UserUpLogVo;
import com.user.entity.UpLevelLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户升级记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-08-09
 */
public interface UpLevelLogsMapper extends BaseMapper<UpLevelLogs> {

    Page<UserUpLogVo> queryList(Page<UserUpLogVo> page, @Param("param") Map<String, String> map);
}
