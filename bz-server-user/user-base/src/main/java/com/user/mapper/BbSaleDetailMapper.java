package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.BBDetailVo;
import com.mall.model.user.vo.BBOrderDetailPlatQueryVo;
import com.user.entity.BbSaleDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户币币出售明细记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface BbSaleDetailMapper extends BaseMapper<BbSaleDetail> {

    BBDetailVo getOrderDetail(@Param("params") Map<String, Object> params);

    Page<BBOrderDetailPlatQueryVo> queryPlatBbSaleDetailsPageByParams(Page<BBOrderDetailPlatQueryVo> page, @Param("params") Map<String, Object> params);
}
