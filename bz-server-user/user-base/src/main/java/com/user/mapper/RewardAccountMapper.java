package com.user.mapper;

import com.user.entity.RewardAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户收益账户表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-08-05
 */
public interface RewardAccountMapper extends BaseMapper<RewardAccount> {

}
