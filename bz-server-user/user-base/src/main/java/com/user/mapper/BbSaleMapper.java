package com.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.model.user.vo.BBDataVo;
import com.mall.model.user.vo.BBOrderPlatQueryVo;
import com.mall.model.user.vo.BBSaleQueryVo;
import com.mall.model.user.vo.BBSaleVo;
import com.user.entity.BbSale;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户币币挂售订单主表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface BbSaleMapper extends BaseMapper<BbSale> {

    Page<BBSaleQueryVo> queryBbSalePageByParams(@Param("params") Map<String, Object> params, Page<BBSaleQueryVo> page);

    BBDataVo getSaleInfo(@Param("id") Integer id, @Param("uid") String uid);

    Page<BBOrderPlatQueryVo> queryPlatBbSalePageByParams(@Param("params") Map<String, Object> params, Page<BBOrderPlatQueryVo> page);
}
