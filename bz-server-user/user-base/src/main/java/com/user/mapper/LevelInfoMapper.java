package com.user.mapper;

import com.user.entity.LevelInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.user.model.ReferralRewardsInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会员等级表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface LevelInfoMapper extends BaseMapper<LevelInfo> {

}
