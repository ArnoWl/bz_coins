package com.market.provider;

import com.mall.feign.market.MarketClient;
import com.market.websocket.client.HbClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: lt
 * @Date: 2020/8/13 20:40
 */
@Slf4j
@RestController
public class MarketProvider implements MarketClient {

    @Resource
    private HbClient hbClient;

    @Override
    public void refreshTopics() {
        try {
            hbClient.refreshTopics();
        } catch (Exception e) {
            log.error("刷新交易所订阅主题信息失败");
        }
    }

}
