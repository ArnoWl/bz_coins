package com.market.configuration;

import com.auth.client.config.ClientAuthConfig;
import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.mall.model.market.dto.KlineInfo;
import com.market.common.ExchangeKlineFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 初始化自动配置
 * @author lt
 * 2020年3月18日17:07:30
 */
@Configuration
@MapperScan("com.*.mapper")
@ComponentScan({"com.common.config","com.common.handler","com.mall.fallback"})
public class AutoConfiguration {

    /**
     * mybatis-plus 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * mybatis-plus 乐观锁插件
     * @return
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }

    @Bean
    public ExchangeKlineFactory klineFactory(){
        return new ExchangeKlineFactory();
    }

}
