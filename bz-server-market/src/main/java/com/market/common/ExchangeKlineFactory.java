package com.market.common;

import com.common.base.ResultVo;
import com.mall.model.market.dto.KlineInfo;
import lombok.Data;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 交易所最后一次K线信息
 * @Author: lt
 * @Date: 2020/7/27 15:08
 */
public class ExchangeKlineFactory {

    private ConcurrentHashMap<String, KlineInfo> klineMap;

    public ExchangeKlineFactory() {
        klineMap = new ConcurrentHashMap<>();
    }

    public boolean containsKline(String symbol) {
        return klineMap != null && klineMap.containsKey(symbol);
    }

    public KlineInfo getKline(String symbol) {
        return klineMap.get(symbol);
    }

    public ConcurrentHashMap<String, KlineInfo> getKlineMap() {
        return klineMap;
    }

}
