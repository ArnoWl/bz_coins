package com.market.common;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @Description: 订阅主题, 火币网
 *   一般需要订阅以下五种数据类型
 *
 * @author xub
 * @date 2019/7/29 下午8:53
 */
public final class MarketConstant {

    /** ws地址 -- 火币网（https://huobiapi.github.io/docs/spot/v1/cn/#5ea2e0cde2） */
    public static String HB_WS_ADDRESS = "wss://api.huobi.pro/ws";

    /**  ws地址 -- 币安交易所（https://binance-docs.github.io/apidocs/spot/cn）*/
    public static String BAN_WS_ADDRESS = "wss://stream.binance.com:9443/ws";

    /**  ws地址 -- OKEX交易所（https://www.okex.com/docs/zh）*/
    public static String OK_WS_ADDRESS = "wss://real.okex.com:8443/ws/v3";

    /** 交易所类型：1-火币 2-币安 3-OK */
    public static Integer EXCHANGE_TYPE_1 = 1;
    /** 交易所类型：1-火币 2-币安 3-OK */
    public static Integer EXCHANGE_TYPE_2 = 2;
    /** 交易所类型：1-火币 2-币安 3-OK */
    public static Integer EXCHANGE_TYPE_3 = 3;

    /** 比特币/USDT */
    public static final String BTC_USDT = "BTC/USDT";

    /** 比特币/USDT */
    public static final String ETH_USDT = "ETH/USDT";


    /** 订阅通道：比特币/USDT */
    public static final String CHANNEL_BTC_USDT_1 = "BTC/USDT&1";

    /** 订阅通道：比特币/USDT */
    public static final String CHANNEL_BTC_USDT_5 = "BTC/USDT&5";

    /** 订阅通道：比特币/USDT */
    public static final String CHANNEL_BTC_USDT_15 = "BTC/USDT&15";

    /** 订阅通道：比特币/USDT */
    public static final String CHANNEL_BTC_USDT_30 = "BTC/USDT&30";

    /** 订阅通道：比特币/USDT */
    public static final String CHANNEL_BTC_USDT_60 = "BTC/USDT&60";

    /** 订阅通道：比特币/USDT */
    public static final String CHANNEL_BTC_USDT_1_DAY = "BTC/USDT&1440";

    /** 订阅通道：以太坊/USDT */
    public static final String CHANNEL_ETH_USDT_1 = "ETH/USDT&1";

    /** 订阅通道：以太坊/USDT */
    public static final String CHANNEL_ETH_USDT_5 = "ETH/USDT&5";

    /** 订阅通道：以太坊/USDT */
    public static final String CHANNEL_ETH_USDT_15 = "ETH/USDT&15";

    /** 订阅通道：以太坊/USDT */
    public static final String CHANNEL_ETH_USDT_30 = "ETH/USDT&30";

    /** 订阅通道：以太坊/USDT */
    public static final String CHANNEL_ETH_USDT_60 = "ETH/USDT&60";

    /** 订阅通道：以太坊/USDT */
    public static final String CHANNEL_ETH_USDT_1_DAY = "ETH/USDT&1440";

}
