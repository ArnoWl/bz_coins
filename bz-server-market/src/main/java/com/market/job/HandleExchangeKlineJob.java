package com.market.job;

import com.common.exception.BaseException;
import com.market.websocket.client.HbClient;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.common.constatns.ThirdConstants.*;

/**
 * @Author: lt
 * @Date: 2020/7/23 15:07
 */
@Slf4j
@Component
public class HandleExchangeKlineJob {

    @Resource
    private HbClient hbClient;

    /**
     * 开启订阅各大交易所市场K线数据
     * @param param
     * @return
     */
    @XxlJob(TURN_ON_KLINE_SUBSCRIPTION)
    public ReturnT<String> turnOnKlineSubscription(String param) {
        try {
            // 建立与各大交易所的连接
            hbClient.connect();
            return ReturnT.SUCCESS;
        } catch (Exception e) {
            throw new BaseException("建立与交易所的连接失败",e);
        }
    }

    /**
     * 关闭市场K线数据订阅
     * @param param
     * @return
     */
    @XxlJob(CLOSE_KLINE_SUBSCRIPTION)
    public ReturnT<String> closeKlineSubscription(String param) {
        hbClient.close();
        return ReturnT.SUCCESS;
    }

    /**
     * 重启各大交易所市场K线数据订阅
     * @param param
     * @return
     */
    @XxlJob(REBOOT_KLINE_SUBSCRIPTION)
    public ReturnT<String> rebootKlineSubscription(String param) {
        hbClient.reConnect();
        return ReturnT.SUCCESS;
    }

    /**
     * 监测缓存市场K线数据
     * @param param
     * @return
     */
    @XxlJob(MONITORING_CACHE_MARKET_KLINE)
    public ReturnT<String> monitoringCacheMarketKline(String param) {
        if(!hbClient.isAlive()){
           log.info("火币网服务已经断开---尝试重启...");
           hbClient.reConnect();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 更新缓存交易对配置信息
     * @param param
     * @return
     */
    @XxlJob(UPDATE_KLINE_CACHE_CONFIG)
    public ReturnT<String> updateKlineCacheConfig(String param) {
        hbClient.refreshTopics();
        return ReturnT.SUCCESS;
    }

}
