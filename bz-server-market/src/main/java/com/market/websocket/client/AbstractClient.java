package com.market.websocket.client;

import com.google.common.collect.Sets;
import com.market.model.CoinTrans;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.util.Set;

/**
 * @Description: 抽离出 父类
 * 以后主要新接入一家交易所，都需要继承该父类
 *
 * @author xub
 * @date 2019/7/30 下午5:30
 */
@Slf4j
public abstract class AbstractClient {

    protected Channel channel;

    protected EventLoopGroup group;

    /**
     * 各大交易所存放的交易对
     */
    protected Set<CoinTrans> topics = Sets.newHashSet();

    /**
     * 开启连接
     */
    public abstract void connect();

    /**
     * 建立webSocket连接
     * @param uri           连接地址
     * @param handler       自定义handler
     */
    protected void connectWebSocket(final URI uri, SimpleChannelInboundHandler handler) {
        try {
            String scheme = uri.getScheme() == null ? "http" : uri.getScheme();
            final String host = uri.getHost() == null ? "127.0.0.1" : uri.getHost();
            final int port;
            if (uri.getPort() == -1) {
                if ("http".equalsIgnoreCase(scheme) || "ws".equalsIgnoreCase(scheme)) {
                    port = 80;
                } else if ("wss".equalsIgnoreCase(scheme)) {
                    port = 443;
                } else {
                    port = -1;
                }
            } else {
                port = uri.getPort();
            }
            final boolean ssl = "wss".equalsIgnoreCase(scheme);
            final SslContext sslContext;
            if (ssl) {
                sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
            } else {
                sslContext = null;
            }
            group = new NioEventLoopGroup(2);
            // 构建客户端
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group).channel(NioSocketChannel.class).handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    ChannelPipeline pipeline = ch.pipeline();
                    if (sslContext != null) {
                        pipeline.addLast(sslContext.newHandler(ch.alloc(), host, port));
                    }
                    // 自定义处理程序
                    pipeline.addLast(new HttpClientCodec(), new HttpObjectAggregator(1024*64), handler);
                }
            });
            channel = bootstrap.connect(host, port).sync().channel();
        } catch (Exception e) {
            if (group != null) {
                group.shutdownGracefully();
            }
        }
    }

    public boolean isAlive() {
        return this.channel != null && this.channel.isActive();
    }

    public void sendMessage(String msg) {
        if (!isAlive()) {
            log.warn("连接不存在");
            return;
        }
        log.info("send:" + msg);
        this.channel.writeAndFlush(new TextWebSocketFrame(msg));
    }

    /**
     * 发送ping消息
     */
    public abstract void sendPing();

    /**
     * 关闭连接
     */
    public void close(){
        if(group != null){
            group.shutdownGracefully();
            this.group = null;
        }
    }

    /**
     * 重启连接
     */
    public abstract void reConnect();

    /**
     * 接收到服务端消息
     * @param msg
     */
    public abstract void onReceiveMsg(String msg);
}