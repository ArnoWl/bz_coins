package com.market.websocket.client;

import com.alibaba.fastjson.JSONObject;
import com.common.constatns.RedisKeyConstants;
import com.market.common.MarketConstant;
import com.market.handle.HbChannelHandler;
import com.market.handle.MongoDbHandler;
import com.market.model.CoinTrans;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.common.constatns.RedisKeyConstants.*;
import static com.market.common.MarketConstant.*;

/**
 * 火币网webSocket客户端
 * @Author: lt
 * @Date: 2020/7/10 15:26
 */
@Slf4j
@Component
public class HbClient extends AbstractClient {

	@Resource
	private RedisTemplate<String,String> redisTemplate;
	@Resource
	private MongoDbHandler mongoDbHandler;


	@Override
	public void connect() {
		try {
			String url = HB_WS_ADDRESS;
			final URI uri = URI.create(url);
			HbChannelHandler handler = new HbChannelHandler(WebSocketClientHandshakerFactory
					.newHandshaker(uri, WebSocketVersion.V13, null, false, new DefaultHttpHeaders()), this);
			connectWebSocket(uri, handler);
			if (isAlive()) {
				handler.handshakeFuture().sync();
				// 开启首次订阅
				this.subTopics();
			}
		} catch (Exception e) {
			log.error("建立与火币网的连接失败", e);
		}
	}

	/**
	 * 发送ping消息
	 */
	@Override
	public void sendPing() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("ping", System.currentTimeMillis());
		this.sendMessage(jsonObject.toString());
	}

	/**
	 * 订阅主题
	 */
	public void subTopics() {
		if (!isAlive()) {
			return;
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id", UUID.randomUUID().toString());
		// 首次订阅不存在交易对信息：从缓存中读取
		if(CollectionUtils.isEmpty(topics)){
			this.refreshTopics();
		}
		topics.forEach(t -> {
			jsonObject.put("sub", t.getCoinTransCode());
			String msg = jsonObject.toString();
			this.sendMessage(msg);
		});
	}

	/**
	 * 刷新订阅主题信息
	 */
	public void refreshTopics(){
		List<CoinTrans> transList = JSONObject.parseArray(redisTemplate.opsForValue().get(EXCHANGE_KLINE_WS_CONFIG),  CoinTrans.class);
		assert transList != null;
		transList = transList.stream().filter(t -> t.getType().equals(EXCHANGE_TYPE_1)).collect(Collectors.toList());
		topics.addAll(transList);
	}

	/**
	 * 重新建立连接
	 */
	@Override
	public void reConnect() {
		log.info("重新建立火币网连接..................");
		this.close();
		this.connect();
	}

	/**
	 * 接收到服务端消息
	 * @param msg
	 */
	@Override
	public void onReceiveMsg(String msg) {
		// ping 消息
		if (msg.contains("ping")) {
			this.sendMessage(msg.replace("ping", "pong"));
			return;
		}
		if (msg.contains("pong")) {
			this.sendPing();
		}
		mongoDbHandler.saveKline(topics,msg);
	}

}
