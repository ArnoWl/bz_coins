package com.market.handle;

import com.alibaba.fastjson.JSON;
import com.common.utils.lang.StringUtils;
import com.mall.model.market.dto.KlineInfo;
import com.market.common.ExchangeKlineFactory;
import com.market.handle.GlobalChannelHandler;
import com.xxl.job.core.biz.model.ReturnT;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;

import static com.market.common.MarketConstant.*;
import static com.market.common.MarketConstant.CHANNEL_ETH_USDT_1_DAY;
import static com.market.handle.GlobalChannelHandler.*;

/**
 * 处理K线开启订阅/取消订阅
 * @Author: lt
 * @Date: 2020/8/7 14:17
 */
@Configuration
public class HandlePushKline {

    @Resource
    private ExchangeKlineFactory klineFactory;

    /**
     * 向客户端推送实时最新K线数据(秒)
     * @return
     */
    public void pushLatestKlineInfo(){
        // BTC
        this.pushByChannelGroup(btcChannelGroup,klineFactory.getKline(CHANNEL_BTC_USDT_1));
        this.pushByChannelGroup(btcChannelGroup5,klineFactory.getKline(CHANNEL_BTC_USDT_5));
        this.pushByChannelGroup(btcChannelGroup15,klineFactory.getKline(CHANNEL_BTC_USDT_15));
        this.pushByChannelGroup(btcChannelGroup30,klineFactory.getKline(CHANNEL_BTC_USDT_30));
        this.pushByChannelGroup(btcChannelGroup60,klineFactory.getKline(CHANNEL_BTC_USDT_60));
        this.pushByChannelGroup(btcChannelGroup1Day,klineFactory.getKline(CHANNEL_BTC_USDT_1_DAY));
        // 以太坊
        this.pushByChannelGroup(ethChannelGroup,klineFactory.getKline(CHANNEL_ETH_USDT_1));
        this.pushByChannelGroup(ethChannelGroup5,klineFactory.getKline(CHANNEL_ETH_USDT_5));
        this.pushByChannelGroup(ethChannelGroup15,klineFactory.getKline(CHANNEL_ETH_USDT_15));
        this.pushByChannelGroup(ethChannelGroup30,klineFactory.getKline(CHANNEL_ETH_USDT_30));
        this.pushByChannelGroup(ethChannelGroup60,klineFactory.getKline(CHANNEL_ETH_USDT_60));
        this.pushByChannelGroup(ethChannelGroup1Day,klineFactory.getKline(CHANNEL_ETH_USDT_1_DAY));
    }

    private void pushByChannelGroup(ChannelGroup channelGroup, KlineInfo kline){
        if(channelGroup.size() > 0 && kline != null){
            // 日K线
            KlineInfo toDayKline = null;
            if(BTC_USDT.equals(kline.getChannelType())){
                toDayKline = klineFactory.getKline(CHANNEL_BTC_USDT_1_DAY);
            }else if(ETH_USDT.equals(kline.getChannelType())){
                toDayKline = klineFactory.getKline(CHANNEL_ETH_USDT_1_DAY);
            }
            if(toDayKline != null){
                kline.setToDayHighestPrice(toDayKline.getHighestPrice());
                kline.setToDayLowestPrice(toDayKline.getLowestPrice());
            }
            channelGroup.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(kline)));
        }
    }

}
