package com.market.handle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.common.config.RedisUtils;
import com.common.utils.CalcUtils;
import com.common.utils.lang.StringUtils;
import com.mall.model.market.dto.KlineInfo;
import com.market.common.ExchangeKlineFactory;
import com.market.model.CoinTrans;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.*;
import java.util.Set;
import java.util.TimeZone;

import static com.common.constatns.RedisKeyConstants.*;

/**
 * @Author: lt
 * @Date: 2020/7/23 17:29
 */
@Slf4j
@Component
public class MongoDbHandler {

    @Resource
    private MongoTemplate mongoTemplate;
    @Resource
    private RedisTemplate<String,String> redisTemplate;
    @Resource
    private ExchangeKlineFactory klineFactory;
    @Resource
    private HandlePushKline handlePushKline;
    @Resource
    private RedisUtils redisUtils;

    /**
     * 存储K线信息
     * @param coinTrans
     * @param content
     */
    public synchronized void saveKline(Set<CoinTrans> coinTrans, String content){
        JSONObject obj = JSON.parseObject(content);
        if(StringUtils.isBlank(obj.getString("ch"))){
            return;
        }
        // 判断交易所类型转换数据
        KlineInfo kline = this.getHbKline(obj,coinTrans);
        // 更新最新K线
        float min = kline.getLowestPrice().floatValue();
        float max = kline.getHighestPrice().floatValue();
        kline.setClosePrice(new BigDecimal(Math.random() * (max - min) + min).setScale(4,BigDecimal.ROUND_DOWN));
        this.setNewKline(kline);
}

    /**
     * 火币交易所行情信息
     * @param obj
     * @param coinTrans
     */
    private KlineInfo getHbKline(JSONObject obj, Set<CoinTrans> coinTrans){
        KlineInfo kline = new KlineInfo();
        CoinTrans trans= coinTrans.stream()
                .filter(c -> c.getCoinTransCode().equals(obj.getString("ch")))
                .findFirst().get();
        kline.setEType(trans.getType());
        kline.setChannelType(trans.getCoinSymbol());
        kline.setToDayOpenPrice(trans.getOpenPrice());
        kline.setTime(obj.getLong("ts"));
        JSONObject tick = obj.getJSONObject("tick");
        kline.setTime(Long.parseLong(String.format("%010d",kline.getTime()/1000)));
        // 去除秒数
        kline.setTime(kline.getTime() - (kline.getTime()%60));
        kline.setOpenPrice(tick.getBigDecimal("open"));
        kline.setHighestPrice(tick.getBigDecimal("high"));
        kline.setLowestPrice(tick.getBigDecimal("low"));
        kline.setClosePrice(tick.getBigDecimal("close"));
        kline.setVolume(tick.getBigDecimal("vol"));
        kline.setTurnover(tick.getBigDecimal("amount"));
        kline.setCount(tick.getInteger("count"));
        //重置最高最低  每次从火币获取到的代价 根据平台设置的  往上 往下调整金额
        BigDecimal highPrice=CalcUtils.add(kline.getHighestPrice(),trans.getSettleLong(),4);
        kline.setHighestPrice(highPrice);
        BigDecimal lowPrice=CalcUtils.add(kline.getLowestPrice(),trans.getSettleShort(),4);
        kline.setLowestPrice(lowPrice);
        kline.setRate(this.getRate(kline));
        return kline;
    }

    /**
     * 设置最新K线信息
     * @param newKline
     */
    private void setNewKline(KlineInfo newKline) {
        BigDecimal orderPrice = redisUtils.getOrderPrice(newKline.getChannelType());
        if(orderPrice != null){
            newKline.setClosePrice(orderPrice);
        }
        // 分钟K线
        this.realMinuteKline(newKline,1);
        this.realMinuteKline(newKline,5);
        this.realMinuteKline(newKline,15);
        this.realMinuteKline(newKline,30);
        this.realMinuteKline(newKline,60);
        // 日K线
        this.realDayKline(newKline);
        // 推送数据到客户端
        handlePushKline.pushLatestKlineInfo();
        // 保存实时行情K线数据到redis缓存
        redisTemplate.opsForValue().set(LATEST_COIN_MARKET_KLINE + newKline.getChannelType()
                ,JSON.toJSONString(newKline));
    }

    /**
     * 实时更新分钟K线
     * @param newKline
     * @param minute
     */
    private void realMinuteKline(KlineInfo newKline,int minute){
        String symbol = newKline.getChannelType() + "&" + minute;
        if(!klineFactory.containsKline(symbol)){
            KlineInfo klineInfo = new KlineInfo();
            BeanUtils.copyProperties(newKline,klineInfo);
            klineFactory.getKlineMap().put(symbol,klineInfo);
            return;
        }
        KlineInfo oldKline = klineFactory.getKline(symbol);
        // 分钟
        long minuteTime = newKline.getTime() / 60;
        long oldMinuteTime = oldKline.getTime() / 60;
        if(minuteTime % minute == 0 && minuteTime != oldMinuteTime){
            // 存入缓存
            this.saveKline(oldKline,minute);
            BigDecimal openPrice = oldKline.getClosePrice();
            BeanUtils.copyProperties(newKline,oldKline);
            oldKline.setOpenPrice(openPrice);
        }else{
            oldKline.setTime(newKline.getTime());
            oldKline.setClosePrice(newKline.getClosePrice());
            oldKline.setRate(newKline.getRate());
            if(minute == 1){
                oldKline.setCount(newKline.getCount());
                oldKline.setVolume(newKline.getVolume());
                oldKline.setTurnover(newKline.getTurnover());
            }else{
                oldKline.setCount(oldKline.getCount() + newKline.getCount());
                oldKline.setVolume(oldKline.getVolume().add(newKline.getVolume()));
                oldKline.setTurnover(oldKline.getTurnover().add(newKline.getTurnover()));
            }
            if(newKline.getHighestPrice().compareTo(oldKline.getHighestPrice()) > 0){
                oldKline.setHighestPrice(newKline.getHighestPrice());
            }
            if(newKline.getLowestPrice().compareTo(oldKline.getLowestPrice()) < 0){
                oldKline.setLowestPrice(newKline.getLowestPrice());
            }
        }
    }

    /**
     * 实时更新日K线
     * @param newKline
     */
    private void realDayKline(KlineInfo newKline){
        String symbol = newKline.getChannelType() + "&" + 1440;
        if(!klineFactory.containsKline(symbol)){
            KlineInfo klineInfo = new KlineInfo();
            BeanUtils.copyProperties(newKline,klineInfo);
            klineFactory.getKlineMap().put(symbol,klineInfo);
            return;
        }
        KlineInfo oldKline = klineFactory.getKline(symbol);
        LocalDate oldDate = Instant.ofEpochSecond(oldKline.getTime()).atZone(ZoneOffset.ofHours(8)).toLocalDate();
        LocalDate newDate = Instant.ofEpochSecond(newKline.getTime()).atZone(ZoneOffset.ofHours(8)).toLocalDate();
        if(!oldDate.isEqual(newDate)){
            // 存入缓存
            this.saveKline(oldKline,1440);
            BigDecimal openPrice = oldKline.getClosePrice();
            BeanUtils.copyProperties(newKline,oldKline);
            oldKline.setOpenPrice(openPrice);
        }else{
            oldKline.setTime(newKline.getTime());
            oldKline.setClosePrice(newKline.getClosePrice());
            oldKline.setRate(newKline.getRate());
            oldKline.setCount(oldKline.getCount() + newKline.getCount());
            oldKline.setVolume(oldKline.getVolume().add(newKline.getVolume()));
            oldKline.setTurnover(oldKline.getTurnover().add(newKline.getTurnover()));
            if(newKline.getHighestPrice().compareTo(oldKline.getHighestPrice()) > 0){
                oldKline.setHighestPrice(newKline.getHighestPrice());
            }
            if(newKline.getLowestPrice().compareTo(oldKline.getLowestPrice()) < 0){
                oldKline.setLowestPrice(newKline.getLowestPrice());
            }
        }
    }


    /**
     * 获取涨幅  小于0表示跌  大于0表示张
     * @param klineInfo
     * @return
     */
    private BigDecimal getRate(KlineInfo klineInfo){
        BigDecimal rate = BigDecimal.ZERO;
        if (klineInfo.getClosePrice().compareTo(klineInfo.getToDayOpenPrice()) !=0) {
            BigDecimal money = CalcUtils.sub(klineInfo.getClosePrice(), klineInfo.getToDayOpenPrice(), 6);
            rate = CalcUtils.div(money, klineInfo.getToDayOpenPrice(), 5);
        }
        return rate;
    }

    /**
     * 存储K线到缓存
     * @param kline
     * @param minute
     */
    private void saveKline(KlineInfo kline,int minute){
        // 存储最新行情数据到mongoDB缓存库
        Query query = new Query(Criteria.where("time").is(kline.getTime()));
        // 移除旧的秒帧数据
        mongoTemplate.remove(query,EXCHANGE_KLINE + kline.getChannelType() + "_" + minute);
        // 存储新的秒帧数据
        mongoTemplate.insert(kline,EXCHANGE_KLINE + kline.getChannelType() + "_" + minute);
    }

}
