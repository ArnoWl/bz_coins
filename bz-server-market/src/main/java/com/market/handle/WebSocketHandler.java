package com.market.handle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.common.utils.lang.StringUtils;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;


import static com.market.common.MarketConstant.*;

/**
 * webSocket连接处理
 * @Author: lt
 * @Date: 2020/7/11 17:35
 */
@Slf4j
public class WebSocketHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame tws) {
        try {
            JSONObject obj = JSON.parseObject(tws.text());
            String channel = obj.getString("channel");
            String status = obj.getString("status");
            if(StringUtils.isNotBlank(channel) && StringUtils.isNotBlank(status)){
                switch (channel){
                    case CHANNEL_BTC_USDT_1:
                        this.subscribeKline(status,GlobalChannelHandler.btcChannelGroup,ctx);
                        break;
                    case CHANNEL_BTC_USDT_5:
                        this.subscribeKline(status, GlobalChannelHandler.btcChannelGroup5,ctx);
                        break;
                    case CHANNEL_BTC_USDT_15:
                        this.subscribeKline(status, GlobalChannelHandler.btcChannelGroup15,ctx);
                        break;
                    case CHANNEL_BTC_USDT_30:
                        this.subscribeKline(status, GlobalChannelHandler.btcChannelGroup30,ctx);
                        break;
                    case CHANNEL_BTC_USDT_60:
                        this.subscribeKline(status, GlobalChannelHandler.btcChannelGroup60,ctx);
                        break;
                    case CHANNEL_BTC_USDT_1_DAY:
                        this.subscribeKline(status, GlobalChannelHandler.btcChannelGroup1Day,ctx);
                        break;
                    case CHANNEL_ETH_USDT_1:
                        this.subscribeKline(status, GlobalChannelHandler.ethChannelGroup,ctx);
                        break;
                    case CHANNEL_ETH_USDT_5:
                        this.subscribeKline(status, GlobalChannelHandler.ethChannelGroup5,ctx);
                        break;
                    case CHANNEL_ETH_USDT_15:
                        this.subscribeKline(status, GlobalChannelHandler.ethChannelGroup15,ctx);
                        break;
                    case CHANNEL_ETH_USDT_30:
                        this.subscribeKline(status, GlobalChannelHandler.ethChannelGroup30,ctx);
                        break;
                    case CHANNEL_ETH_USDT_60:
                        this.subscribeKline(status,  GlobalChannelHandler.ethChannelGroup60,ctx);
                        break;
                    case CHANNEL_ETH_USDT_1_DAY:
                        this.subscribeKline(status, GlobalChannelHandler.ethChannelGroup1Day,ctx);
                        break;
                    default:
                }
            }
        } catch (Exception e) {
            log.error("解析客户端消息失败:{}",tws.text());
        }
    }

    private void subscribeKline(String status, ChannelGroup channelGroup,ChannelHandlerContext ctx){
        if("subscribe".equals(status)){
            channelGroup.add(ctx.channel());
        }else if("cancel".equals(status)){
            channelGroup.remove(ctx.channel());
        }
    }

    /**
     * 建立与客户端的连接
     * @param ctx
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ctx.channel().writeAndFlush("成功建立客户端连接");
    }

    /**
     * 断开客户端连接
     * @param ctx
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        GlobalChannelHandler.btcChannelGroup.remove(ctx.channel());
        GlobalChannelHandler.ethChannelGroup.remove(ctx.channel());
    }

    /**
     * 超时处理
     * @param ctx
     * @param evt
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {
        ctx.channel().writeAndFlush("未接收到客户端响应，断开连接");
        ctx.channel().close();
    }

}