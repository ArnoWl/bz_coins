package com.market.handle;


import com.common.utils.GZipUtils;
import com.common.utils.lang.StringUtils;
import com.market.websocket.client.HbClient;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.*;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;


/**
 * 火币交易所webSocket消息处理
 * @Author: lt
 * @Date: 2020/7/10 16:10
 */
@Slf4j
public class HbChannelHandler extends SimpleChannelInboundHandler<Object> {

    private HbClient client;

    private ChannelPromise channelPromise;

    private WebSocketClientHandshaker webSocketClientHandshaker;

    /**
     * 该handel获取消息的方法
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        if (!webSocketClientHandshaker.isHandshakeComplete()) {
            webSocketClientHandshaker.finishHandshake(channelHandlerContext.channel(), (FullHttpResponse) o);
            channelPromise.setSuccess();
            return;
        }
        String msg = StringUtils.EMPTY;
        // 数据解压缩
        WebSocketFrame frame = (WebSocketFrame) o;
        if (frame instanceof BinaryWebSocketFrame) {
            BinaryWebSocketFrame binaryFrame = (BinaryWebSocketFrame) frame;
            msg = GZipUtils.decodeByteBuf(binaryFrame.content());
        } else if (frame instanceof TextWebSocketFrame) {
            TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) frame;
            msg = textWebSocketFrame.text();
        }
        if(StringUtils.isBlank(msg)){
            log.error("未能成功接收/解析（火币网）服务端的消息");
        }else{
            client.onReceiveMsg(msg);
        }
    }

    public HbChannelHandler(WebSocketClientHandshaker webSocketClientHandshaker, HbClient client) {
        this.webSocketClientHandshaker = webSocketClientHandshaker;
        this.client = client;
    }

    public ChannelFuture handshakeFuture() {
        return channelPromise;
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        channelPromise = ctx.newPromise();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        webSocketClientHandshaker.handshake(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        log.error("火币网连接服务已断开..........");
        client.reConnect();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        if (!channelPromise.isDone()) {
            channelPromise.setFailure(cause);
        }
        ctx.close();
    }

}
