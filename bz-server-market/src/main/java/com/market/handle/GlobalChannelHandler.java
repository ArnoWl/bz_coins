package com.market.handle;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

/**
 * 全局webSocket连接通道处理
 * @Author: lt
 * @Date: 2020/7/11 16:10
 */
public class GlobalChannelHandler {

    public GlobalChannelHandler(){}

    /** 比特币订阅通道 */
    public static ChannelGroup btcChannelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup btcChannelGroup5 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup btcChannelGroup15 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup btcChannelGroup30 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup btcChannelGroup60 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup btcChannelGroup1Day = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 以太坊订阅通道 */
    public static ChannelGroup ethChannelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup ethChannelGroup5 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup ethChannelGroup15 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup ethChannelGroup30 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup ethChannelGroup60 = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /** 比特币订阅通道 */
    public static ChannelGroup ethChannelGroup1Day = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);


}