package com.market;

import com.auth.client.EnableAuthClient;
import com.common.job.XxlJobConfig;
import com.mall.aspect.WebLogAspect;
import com.market.websocket.server.NettyServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

/**
 * @author tps
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.mall.feign")
@EnableAuthClient
@Import(XxlJobConfig.class)
public class MarketServerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MarketServerApplication.class, args);
		// 启动netty服务
		new NettyServer().start();
	}

}