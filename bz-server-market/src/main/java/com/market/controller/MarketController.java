package com.market.controller;

import com.common.base.ResultVo;
import com.google.common.collect.Lists;
import com.mall.model.market.dto.KlineInfo;
import com.market.common.ExchangeKlineFactory;
import com.market.common.MarketConstant;
import com.market.model.GetHistoryKlineListIn;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import static com.common.constatns.MarketConstants.EXCHANGE_KLINE;
import static com.market.common.MarketConstant.*;

import javax.annotation.Resource;
import java.security.KeyFactory;
import java.util.List;

/**
 * @Author: lt
 * @Date: 2020/7/26 18:14
 */
@CrossOrigin
@Api(description = "市场行情信息模块")
@RestController
public class MarketController {

    @Resource
    private MongoTemplate mongoTemplate;
    @Resource
    private ExchangeKlineFactory exchangeKlineFactory;

    @ApiOperation(value = "获取行情K线信息",response = KlineInfo.class)
    @RequestMapping(value = "kline", method = RequestMethod.POST)
    public ResultVo marketKline(@RequestBody GetHistoryKlineListIn listIn) {
        String collectionName = EXCHANGE_KLINE + listIn.getChannelType() + "_" + listIn.getMinute();
        Query query = new Query().with((Sort.by(Sort.Order.desc("time"))));
        query.skip((listIn.getPageNo() - 1) * listIn.getPageSize());
        query.limit(listIn.getPageSize());
        List<KlineInfo> klineInfos = Lists.newArrayList();
        // 第一页K线 add最新K线数据
        if(listIn.getPageNo().equals(1)){
            klineInfos.add(exchangeKlineFactory.getKline(listIn.getChannelType() + "&" + listIn.getMinute()));
        }
        klineInfos.addAll(mongoTemplate.find(query,KlineInfo.class,collectionName));
        return ResultVo.success(klineInfos);
    }

}
