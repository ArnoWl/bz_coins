package com.market.model;

import com.mall.base.AppBaseParamIn;
import com.mall.base.AppPageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author: lt
 * @Date: 2020/8/10 17:50
 */
@Data
@ApiModel(value = "获取历史K线")
public class GetHistoryKlineListIn extends AppPageParamIn {

    private static final long serialVersionUID = -5558192722889527385L;

    @ApiModelProperty(value = "类型：BTC/USDT ETH/USDT")
    @NotBlank(message = "类型不能为空")
    private String channelType;

    @ApiModelProperty(value = "分钟：1 5 15 30 60 1440")
    @NotBlank(message = "分钟段不能为空")
    private Integer minute;

}
