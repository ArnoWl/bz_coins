package com.market.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 交易对信息
 * @Author: lt
 * @Date: 2020/7/23 11:38
 */
@Data
public class CoinTrans {

    /** 交易所类型：1-火币 2-币安 3-OK */
    private Integer type;

    /** 交易对编号 */
    private String coinSymbol;

    /** 交易所订阅交易对信息 */
    private String coinTransCode;

    /**
     * 开盘价
     */
    private BigDecimal openPrice;

    /**
     * 最低价往下调整金额
     */
    private BigDecimal settleShort;

    /**
     * 最高价往上调整金额
     */
    private BigDecimal settleLong;

}
