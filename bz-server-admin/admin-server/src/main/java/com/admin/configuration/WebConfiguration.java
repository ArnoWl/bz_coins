package com.admin.configuration;

import com.auth.client.interceptor.ClientAuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web拦截器配置
 * @author lt
 * 2020年3月18日14:09:26
 */
@Configuration("adminServerAuth")
@Primary
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * 客户端鉴权拦截处理
     * @return
     */
    @Bean
    ClientAuthInterceptor getClientAuthInterceptor() {
        return new ClientAuthInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getClientAuthInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/**/error");
    }

}
