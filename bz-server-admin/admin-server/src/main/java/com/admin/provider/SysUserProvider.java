package com.admin.provider;

import com.admin.entity.SysUser;
import com.admin.service.ISysUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mall.feign.admin.SysUserClient;
import com.mall.model.admin.dto.SysUserLoginDto;
import com.mall.model.admin.vo.SysUserInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 系统用户信息服务提供
 * @author lt
 * 2020年3月14日17:22:06
 */
@RestController
@RequestMapping(value = "user",method = RequestMethod.POST)
public class SysUserProvider implements SysUserClient {

    @Resource
    private ISysUserService iSysUserService;

    @Override
    public SysUserInfoVo getSysUserInfo(@RequestBody SysUserLoginDto loginDto){
        SysUser user = new SysUser();
        user.setUserAccount(loginDto.getUserAccount());
        user.setPassWord(loginDto.getPassWord());
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.setEntity(user);
        user = iSysUserService.getOne(wrapper);
        SysUserInfoVo infoVo = new SysUserInfoVo();
        if(user != null){
            BeanUtils.copyProperties(user,infoVo);
        }
        return infoVo;
    }

}
