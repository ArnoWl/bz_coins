package com.admin.provider;

import com.admin.entity.*;
import com.admin.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.AdminConstants;
import com.common.exception.BaseException;
import com.common.utils.CalcUtils;
import com.common.utils.ExtBeanUtils;
import com.mall.base.PageParamIn;
import com.mall.feign.admin.BaseClient;
import com.mall.model.admin.dto.CoinAddrDto;
import com.mall.model.admin.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 基础api 模块
 *
 * @author lt
 * 2020年3月14日17:22:06
 */
@RestController
@RequestMapping(value = "base")
public class BaseProvider implements BaseClient {

    @Autowired
    private ISysHelpCenterService sysHelpCenterService;
    @Autowired
    private ISysBannerService sysBannerService;
    @Autowired
    private ISysNoticeInfoService sysNoticeInfoService;
    @Autowired
    private ISysRuleArticleService sysRuleArticleService;
    @Autowired
    private ISysHelpCenterInfoService sysHelpCenterInfoService;
    @Autowired
    private ISysCoinsService sysCoinsService;
    @Autowired
    private ISysContactService sysContactService;
    @Autowired
    private ISysSubscriptionService sysSubscriptionService;
    @Autowired
    private ISysCoinKlineService iSysCoinKlineService;
    @Resource
    private ISysOrderRuleService iSysOrderRuleService;
    @Resource
    private ISysIndemnityRuleService iSysIndemnityRuleService;
    @Resource
    private ISysCoinAddrService iSysCoinAddrService;

    @Override
    public ResultVo queryHelp() {
        return sysHelpCenterService.queryHelp();
    }

    @Override
    public ResultVo getHelpDetail(Integer id) {
        SysHelpCenterInfo centerInfo = sysHelpCenterInfoService.getById(id);
        if (centerInfo == null) {
            return ResultVo.failure("信息已删除");
        }
        HelpDetailVo vo = new HelpDetailVo();
        BeanUtils.copyProperties(centerInfo, vo);
        return ResultVo.success(vo);
    }

    @Override
    public ResultVo queryBanner(Integer type) {
        QueryWrapper<SysBanner> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("position", type);
        List<SysBanner> list = sysBannerService.list(queryWrapper);
        List<BannerVo> listOut = ExtBeanUtils.copyList(list, BannerVo.class);
        return ResultVo.success(listOut);
    }

    @Override
    public ResultVo queryNotice(PageParamIn pageParamIn) {
        IPage<SysNoticeInfo> pageInfo = new Page<>(pageParamIn.getPageNo(), pageParamIn.getPageSize());
        sysNoticeInfoService.page(pageInfo, null);
        IPage<NoticeInfoVo> pageInfoOut = new Page<>();
        BeanUtils.copyProperties(pageInfo, pageInfoOut);
        pageInfoOut.setRecords(ExtBeanUtils.copyList(pageInfo.getRecords(), NoticeInfoVo.class));
        return ResultVo.success(pageInfoOut);
    }

    @Override
    public ResultVo getNoticeDetail(Integer id) {
        SysNoticeInfo sysNoticeInfo = sysNoticeInfoService.getById(id);
        if (sysNoticeInfo == null) {
            return ResultVo.failure("公告已删除");
        }
        NoticeDetailVo detailVo = new NoticeDetailVo();
        BeanUtils.copyProperties(sysNoticeInfo, detailVo);
        return ResultVo.success(detailVo);
    }

    @Override
    public ResultVo getSysRule(Integer type) {
        QueryWrapper<SysRuleArticle> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", type);
        SysRuleArticle rule = sysRuleArticleService.getOne(queryWrapper);
        SysRuleVo vo = new SysRuleVo();
        if (rule != null) {
            BeanUtils.copyProperties(rule, vo);
        }
        return ResultVo.success(rule);
    }

    @Override
    public List<SysCoinsQueryVo> querySysCoin() {
        QueryWrapper<SysCoins> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", AdminConstants.ENABLED_STATUS_0);
        List<SysCoins> list = sysCoinsService.list();
        List<SysCoinsQueryVo> listOut = null;
        if (!CollectionUtils.isEmpty(list)) {
            listOut = ExtBeanUtils.copyList(list, SysCoinsQueryVo.class);
        }
        return listOut;
    }

    @Override
    public List<String> querySysCoinCash() {
        QueryWrapper<SysCoins> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", 0);
        queryWrapper.eq("cash_flag", 0);
        List<SysCoins> list = sysCoinsService.list();
        List<String> strList = new ArrayList<>();
        list.forEach(coins -> strList.add(coins.getName()));
        return strList;
    }

    @Override
    public ResultVo getSysContact() {
        QueryWrapper<SysContact> queryWrapper = new QueryWrapper<>();
        queryWrapper.last("limit 1");
        SysContact sysContact = sysContactService.getOne(queryWrapper);
        ContactOut out = new ContactOut();
        BeanUtils.copyProperties(sysContact, out);
        return ResultVo.success(out);
    }

    @Override
    public ResultVo querySysSubscription() {
        List<SysSubscription> list = sysSubscriptionService.list();
        List<SubscriptionVo> listVo = ExtBeanUtils.copyList(list, SubscriptionVo.class);
        return ResultVo.success(listVo);
    }

    @Override
    public SubscriptionVo getSysSubscriptionPrice(Integer id) {
        SysSubscription subscription = sysSubscriptionService.getById(id);
        SubscriptionVo subscriptionVo= new SubscriptionVo();
        if (subscription != null) {
            BeanUtils.copyProperties(subscription,subscriptionVo);
        }
        return subscriptionVo;
    }

    @Override
    public ResultVo handleSubtractQty(Integer id, BigDecimal qty) {
        try {
            //判断当前单子前面是否还有剩余数量
            QueryWrapper<SysSubscription> queryWrapper=new QueryWrapper<>();
            queryWrapper.lt("id",id).gt("balance",BigDecimal.ZERO);
            int count=sysSubscriptionService.count(queryWrapper);
            if(count>0){
                return ResultVo.failure("即将开启认购");
            }
            SysSubscription subscription = sysSubscriptionService.getById(id);
            if (subscription == null) {
                return ResultVo.failure("未获取到认购数量");
            }
            if (subscription.getBalance().compareTo(qty) < 0) {
                return ResultVo.failure("数量不足");
            }
            if (subscription.getMaxNum().compareTo(qty) < 0) {
                return ResultVo.failure("已超当期购买最大数量");
            }
            subscription.setBalance(CalcUtils.sub(subscription.getBalance(), qty, 4));
            sysSubscriptionService.updateById(subscription);
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("扣除数量失败");
        }
    }

    /**
     * 查询 当前系统币指交易的币种 去重复
     *
     * @return
     */
    @Override
    public List<CoinKlineVo> queryCoinkline() {
        List<CoinKlineVo> coinKlineVos = iSysCoinKlineService.queryBZCoin();
        return coinKlineVos;
    }

    /**
     * 获取最新 包赔场
     *
     * @return
     */
    @Override
    public OrderRuleVo getNewlast() {
        return iSysOrderRuleService.getNewlast();
    }

    /**
     * 根据金额查询符合的规则
     *
     * @param money
     * @return
     */
    @Override
    public IndemnityRuleVo getRuleByUSDT(BigDecimal money) {
        return iSysIndemnityRuleService.getRuleByUSDT(money);
    }


    /**
     * 获取当前币种点差
     * @param coinSymbol
     * @return
     */
    @Override
    public CoinSpreadVo getSpread(String coinSymbol){
        SysCoinKline coinKline=iSysCoinKlineService.getOneByCoinSymbol(coinSymbol);
        if(coinKline==null){
            throw new BaseException("未找到币种信息");
        }
        CoinSpreadVo coinSpreadVo=new CoinSpreadVo();
        BeanUtils.copyProperties(coinKline,coinSpreadVo);
        return coinSpreadVo;
    }

    @Override
    public List<CoinAddrDto> getCoinAddrList() {
        QueryWrapper<SysCoinAddr> sysCoinAddrQueryWrapper=new QueryWrapper<>();
        sysCoinAddrQueryWrapper.eq("status",AdminConstants.ENABLED_STATUS_0);
        return ExtBeanUtils.copyList(iSysCoinAddrService.list(sysCoinAddrQueryWrapper),CoinAddrDto.class);
    }
}
