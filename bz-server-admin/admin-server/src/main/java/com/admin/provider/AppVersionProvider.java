package com.admin.provider;

import com.admin.entity.AppVersionInfo;
import com.admin.service.IAppVersionInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.common.constatns.AdminConstants;
import com.common.exception.BaseException;
import com.mall.base.AppBaseParamIn;
import com.mall.feign.admin.AppVersionClient;
import com.mall.model.admin.vo.AppVersionVo;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 系统版本信息服务提供类
 * @Author: lt
 * @Date: 2020bz_coins/30 16:24
 */
@RestController
@RequestMapping(value = "version")
public class AppVersionProvider implements AppVersionClient {

    @Resource
    private IAppVersionInfoService iAppVersionInfoService;

    @Override
    public AppVersionVo getLatestAppVersion(AppBaseParamIn paramIn) {
        try {
            AppVersionVo versionVo = new AppVersionVo();
            // 获取App最新版本信息
            AppVersionInfo version = new AppVersionInfo();
            version.setAppType(paramIn.getAppType());
            version.setSysType(paramIn.getSysType());
            QueryWrapper<AppVersionInfo> wrapper = new QueryWrapper<>();
            wrapper.setEntity(version);
            wrapper.orderByDesc("version_no").last("limit 1");;
            version = iAppVersionInfoService.getOne(wrapper);
            if(version != null){
                BeanUtils.copyProperties(version,versionVo);
                if(!AdminConstants.APP_VERSION_STATUS_3.equals(versionVo.getStatus())){
                    // 验证当前版本号是否被最新版本兼容
                    if(version.getCompatibleNo() > paramIn.getVersionNo()){
                        // 强制升级
                        versionVo.setStatus(AdminConstants.APP_VERSION_STATUS_3);
                    }
                }
            }
            return versionVo;
        } catch (Exception e) {
            throw new BaseException("获取最新App版本信息失败",e);
        }
    }



}
