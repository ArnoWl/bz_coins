package com.admin.job;

import com.admin.config.HuoBiUtils;
import com.admin.config.Kline;
import com.admin.entity.SysCoinKline;
import com.admin.entity.SysStatis;
import com.admin.service.ISysCoinKlineService;
import com.admin.service.ISysStatisService;
import com.alibaba.fastjson.JSONArray;
import com.common.config.RedisUtils;
import com.mall.model.admin.dto.StatisDayBeforeDto;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static com.common.constatns.ThirdConstants.REFRESH_OPEN_PRICE;
import static com.common.constatns.ThirdConstants.UPDATA_COIN_OPEN_PRICE;
import static com.common.constatns.ThirdConstants.STATIS_DAY;

/**
 * 任务调度：处理平台配置信息
 * @Author: lt
 * @Date: 2020/7/11 13:52
 */
@Slf4j
@Component
public class HandleConfigJob {


    @Resource
    private HuoBiUtils huoBiUtils;
    @Resource
    private ISysCoinKlineService iSysCoinKlineService;
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private ISysStatisService sysStatisService;


    /**
     * 每日修改货币的开盘价
     * @return
     */
    @XxlJob(UPDATA_COIN_OPEN_PRICE)
    public ReturnT<String> updateCoinOpenPrice(String params) {
        List<SysCoinKline> list=iSysCoinKlineService.list();
        for(SysCoinKline c:list){
            String symbol=c.getName().toLowerCase()+"usdt";
            List<Kline> klines=huoBiUtils.queryKline(symbol, "1day", "1");
            if(klines!=null && klines.size()>0){
                Kline kline=klines.get(0);
                c.setOpenPrice(new BigDecimal(kline.getOpen()));
                iSysCoinKlineService.updateById(c);
                iSysCoinKlineService.storeToCache();
                try {
                    Thread.sleep(1000);
                }catch (Exception e){

                }

            }
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 刷新日K 每5S刷新
     * @return
     */
    @XxlJob(REFRESH_OPEN_PRICE)
    public ReturnT<String> refreshOpenPrice(String params) {
        JSONArray array=new JSONArray();
        List<Kline> btcklines=huoBiUtils.queryKline("btcusdt", "1day", "1");
        if(btcklines!=null && btcklines.size()>0){
            array.add(btcklines.get(0));
        }
        List<Kline> ethklines=huoBiUtils.queryKline("ethusdt", "1day", "1");
        if(ethklines!=null && ethklines.size()>0){
            array.add(btcklines.get(0));
        }
        redisUtils.setRefreshOpenPrice(array);
        return ReturnT.SUCCESS;
    }

    /**
     * 获取统计信息
     * @return
     */
    @XxlJob(STATIS_DAY)
    public ReturnT<String> statisDayBefore(String params) {
        SysStatis sysStatis = sysStatisService.statisDayBefore();
        sysStatis.setDay(LocalDate.now().plusDays(-1));
        sysStatisService.save(sysStatis);
        return ReturnT.SUCCESS;
    }
}
