package com.admin.runner;

import com.admin.service.ISysCoinKlineService;
import com.admin.service.ISysCoinPriceService;
import com.admin.service.ISysConfigService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;

/**
 * @author tps
 */
@Configuration
public class AuthServerRunner implements CommandLineRunner {

    @Resource
    private ISysConfigService iSysConfigService;
    @Resource
    private ISysCoinKlineService iSysCoinKlineService;
    @Resource
    private ISysCoinPriceService sysCoinPriceService;

    @Override
    public void run(String... args) {
        // 初始化系统配置信息到缓存
        iSysConfigService.storeToCache();
        // 初始化系统配置币种兑换率
        sysCoinPriceService.storeToCache();
        // 初始化交易所K线订阅配置到缓存
        iSysCoinKlineService.storeToCache();
    }
}
