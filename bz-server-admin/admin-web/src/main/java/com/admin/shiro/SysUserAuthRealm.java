package com.admin.shiro;

import com.admin.entity.SysMenu;
import com.admin.service.ISysMenuService;
import com.common.context.SysUserContext;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * token身份验证、权限校验核心类
 */
public class SysUserAuthRealm extends AuthorizingRealm {

    @Autowired
    private ISysMenuService iSysMenuService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    /**
     * 用户JWT
     * @param auth
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth)
            throws AuthenticationException {
        String token = (String) auth.getCredentials();
        return new SimpleAuthenticationInfo(token, token, "");
    }

    /**
     * 获取用户可用资源权限添加到shiro
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 根据JWT用户ID参数获取用户角色
        Map<String,Object> params = Maps.newHashMap();
        params.put("userId", SysUserContext.getSysUserID());
        List<SysMenu> menus = iSysMenuService.getMenuListByParams(params);
        // 设置权限
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        for (SysMenu menu : menus) {
            if(StringUtils.isNotBlank(menu.getAuthValue())){
                authorizationInfo.addStringPermission(menu.getAuthValue());
            }
        }
        return authorizationInfo;
    }
 
}