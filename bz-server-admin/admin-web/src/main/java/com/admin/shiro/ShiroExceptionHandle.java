package com.admin.shiro;

import com.common.base.ResultVo;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

import static com.common.constatns.CommonConstants.E_009;

@RestControllerAdvice
public class ShiroExceptionHandle {

    // 捕捉shiro的异常
    @ExceptionHandler(ShiroException.class)
    public ResultVo handle401(HttpServletResponse response, ShiroException e) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type,token");
        response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");
        return ResultVo.result(E_009,"暂无权限");
    }

    // 捕捉UnauthorizedException
    @ExceptionHandler(UnauthorizedException.class)
    public ResultVo handle401(HttpServletResponse response, UnauthorizedException e) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type,token");
        response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");
        return ResultVo.result(E_009,"暂无权限");
    }

}