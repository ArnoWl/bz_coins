package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("认购数量列表入参")
@Data
public class SubscriptionListIn extends PageParamIn {

    @ApiModelProperty(value = "名称 模糊查询")
    private String name;


}
