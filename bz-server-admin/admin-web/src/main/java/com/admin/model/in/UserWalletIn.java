package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
@ApiModel("操作用户钱包入参")
@Data
public class UserWalletIn {
    @ApiModelProperty(value = "用户id")
    @NotBlank(message = "请选择操作用户")
    private String uid;

    @ApiModelProperty(value = "钱包类型")
    @NotBlank(message = "请选择钱包类型")
    private String coinSymbol;

    @ApiModelProperty(value = "类型id")
    @NotNull(message = "请选择操作类型")
    private Integer typeId;

    @ApiModelProperty(value = "操作金额 负数未减")
    @NotNull(message = "请填写操作金额")
    private BigDecimal cost;

    @ApiModelProperty(value = "1入账 2出账")
    @NotNull(message = "1入账 2出账")
    private Integer inOut;

    @ApiModelProperty(value = "备注")
    @NotBlank(message = "请填写备注")
    private String remark;
}
