package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel("生成钱包地址入参")
@Data
public class CoinAddrIn {

    @ApiModelProperty(value = "id")
    @NotNull(message = "请选择系统货币")
    private Integer id;

    @ApiModelProperty(value = "类型 主地址 MAIN 手续费地址 FEE")
    @NotBlank(message = "请选择类型")
    private String type;

    @ApiModelProperty(value = "协议类型 OMNI  ERC20")
    @NotBlank(message = "请选择协议类型")
    private String agreeType;

    @ApiModelProperty(value = "归集类型 1系统地址 2场外地址")
    @NotNull(message = "请选择归集类型")
    private Integer imputedType;

    @ApiModelProperty(value = "钱包地址 归集类型为2时必传")
    private String walletAddr;
}
