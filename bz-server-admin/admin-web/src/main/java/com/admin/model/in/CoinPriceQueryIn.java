package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("货币价格查询入参")
@Data
public class CoinPriceQueryIn extends PageParamIn {
    @ApiModelProperty(value = "系统币id ,只有系统币可查询该记录")
    private Integer coinId;
}
