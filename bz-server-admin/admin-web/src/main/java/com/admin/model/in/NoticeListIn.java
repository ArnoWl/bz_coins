package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取公告列表入参实体
 * @Author: lt
 * @Date: 2020/5/6 14:35
 */
@Data
@ApiModel(value = "获取公告列表入参实体")
public class NoticeListIn extends PageParamIn {
    @ApiModelProperty(value = "标题查询 支持模糊查询")
    private String title;


}
