package com.admin.model.in;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel("添加/编辑帮助中心入参")
@Data
public class HelpEditIn {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "图标 如果为一级 必传")
    private String icon;

    @ApiModelProperty(value = "标题")
    @NotBlank(message = "请填写标题")
    private String title;

    @ApiModelProperty(value = "内容 如果为二级 必传")
    private String content;

    @ApiModelProperty(value = "上级id 如果为二级 必传")
    private Integer helpCenterId;

    @ApiModelProperty(value = "级别 1级 2级")
    @NotNull(message = "请选择级别")
    private Integer level;
}
