package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("添加/修改系统货币入参")
@Data
public class CoinsIn {
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "币种名称 如果是修改不可以修改币种名称")
    @NotBlank(message = "请填写币种名称")
    private String name;

    @ApiModelProperty(value = "币种精度")
    @NotNull(message = "请填写币种精度")
    private Integer accuracy;

    @ApiModelProperty(value = "合约地址")
    @NotBlank(message = "请填写合约地址")
    private String contractAddr;

    @ApiModelProperty(value = "0开放充值  1不开放对外充值")
    @NotNull(message = "请选择是否开放充值")
    private Integer rechargeFlag;

    @ApiModelProperty(value = "0开放对外提现  1未开放对外提现")
    @NotNull(message = "请选择是否开放提现")
    private Integer cashFlag;

    @ApiModelProperty(value = "货币类型  1公链  2ERC20代币")
    @NotNull(message = "请填写货币类型")
    private Integer type;

    @ApiModelProperty(value = "0是系统币  1不用是系统币")
    @NotNull(message = "请选择是否设置为系统币")
    private Integer sysFlag;

    @ApiModelProperty(value = "手续费数量")
    @NotNull(message = "请填写手续费 如 按比例 填写比例额  按数量 填写数量 ")
    private BigDecimal tax;

    @ApiModelProperty(value = "手续费类型 1按数量 2按比例")
    @NotNull(message = "请选择手续费类型")
    private Integer taxType;

    @ApiModelProperty(value = "0开启 1关闭")
    @NotNull(message = "请选择状态")
    private Integer status;

    @ApiModelProperty(value = "排序  越大越后")
    private Integer sort;
}
