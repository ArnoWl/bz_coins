package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("添加货币价格入参")
@Data
public class CoinPriceAddIn{

    @ApiModelProperty(value = "系统币id ,只有系统币可查询该记录")
    @NotNull(message = "请选择系统币")
    private Integer coinId;

    @ApiModelProperty(value = "价格")
    @NotNull(message = "请输入价格")
    private BigDecimal price;

    @ApiModelProperty(value = "涨幅")
    @NotNull(message = "请输入系统比涨幅")
    private BigDecimal rate;
}
