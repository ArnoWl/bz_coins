package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统货币钱包地址信息出参
 * @Author: lt
 * @Date: 2020/5/6 14:00
 */
@Data
@ApiModel(value = "系统货币钱包地址信息出参")
public class CoinAddrOut {

    @ApiModelProperty(value = "钱包地址")
    private String walletAddr;

    @ApiModelProperty(value = "1主地址 MAIN 2手续费地址 FEE")
    private String type;

    @ApiModelProperty(value = "协议类型OMNI  ERC20")
    private String agreeType;

    @ApiModelProperty(value = "归集类型  1系统地址 2场外地址 场外地址需要自己填写钱包地址，不需要密码和文件")
    private Integer imputedType;
}
