package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("K线图基础表")
@Data
public class KlineQueryIn extends PageParamIn {

    @ApiModelProperty(value = "币种名称")
    private String name;

    @ApiModelProperty(value = "0开启 1关闭")
    private Integer status;

    @ApiModelProperty(value = "0参与币值交易  1不参与")
    private Integer contractTransFlag;

    @ApiModelProperty(value = "1火币 2币安 3OK")
    private Integer type;
}
