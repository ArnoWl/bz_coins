package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统用户信息详情出参实体
 * @Author: lt
 * @Date: 2020/5/6 14:44
 */
@Data
@ApiModel(value = "系统用户信息详情出参")
public class SysUserDetailOut extends SysUserListOut {
    private static final long serialVersionUID = -4434354532500127369L;

    @ApiModelProperty(value = "用户头像")
    private String portrait;

    @ApiModelProperty(value = "角色Id")
    private Integer roleId;

}
