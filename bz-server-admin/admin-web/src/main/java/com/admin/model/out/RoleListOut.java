package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色信息列表出参实体
 * @Author: lt
 * @Date: 2020/5/6 13:57
 */
@Data
@ApiModel(value = "角色信息列表出参")
public class RoleListOut implements Serializable {

    private static final long serialVersionUID = 6269144330222158680L;
    @ApiModelProperty(value = "角色Id")
    private Integer id;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

}
