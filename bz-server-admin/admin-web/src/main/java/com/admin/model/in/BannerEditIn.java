package com.admin.model.in;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 添加/修改banner入参
 * @Author: cx
 * @Date: 2020/7/22 14:18
 */
@Data
@ApiModel(value = "添加/修改banner入参实体")
public class BannerEditIn {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "图片")
    @NotBlank(message = "请上传banner图")
    private String bannerUrl;

    @ApiModelProperty(value = "位置 1首页顶部 2首页中部")
    @NotNull(message = "请选择banner位置")
    private Integer position;

    @ApiModelProperty(value = "位置 1公告 2分享")
    @NotNull(message = "请选择点击事件")
    private Integer eventType;
}
