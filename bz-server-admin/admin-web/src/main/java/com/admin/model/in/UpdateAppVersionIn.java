package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: lt
 * @Date: 2020/6/10 18:26
 */
@Data
@ApiModel(value = "修改app版本信息入参")
public class UpdateAppVersionIn extends AddAppVersionIn{

    private static final long serialVersionUID = -7134398852055013343L;

    @ApiModelProperty(value = "id")
    @NotNull(message = "id不能为空")
    private Integer id;
}
