package com.admin.model.in;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("修改k线图入参")
@Data
public class CoinKlineEditIn {

    @ApiModelProperty(value = "id")
    @NotNull(message = "请选择修改数据")
    private Integer id;

    @ApiModelProperty(value = "下单点差最低值比例")
    @NotNull(message = "请填写下单点最低值比例")
    private BigDecimal spreadMin;

    @ApiModelProperty(value = "下单点差最大值比例")
    @NotNull(message = "请填写下单点最大值比例")
    private BigDecimal spreadMax;

    @ApiModelProperty(value = "最低价往下调整金额")
    @NotNull(message = "请填写最低价")
    private BigDecimal settleShort;

    @ApiModelProperty(value = "最高价往上调整金额")
    @NotNull(message = "请填写最高价")
    private BigDecimal settleLong;
}
