package com.admin.model.in;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("添加/编辑认购数量入参")
@Data
public class SubscriptionEditIn{

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "名称")
    @NotBlank(message = "请填写认购名称")
    private String name;

    @ApiModelProperty(value = "单价")
    @NotNull(message = "请填写认购单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总量")
    @NotNull(message = "请填写认购总量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "当期购买最大总数量")
    @NotNull(message = "请填写当期购买最大总数量")
    @Min(value = 0,message = "不能小于0")
    private BigDecimal maxNum;


}
