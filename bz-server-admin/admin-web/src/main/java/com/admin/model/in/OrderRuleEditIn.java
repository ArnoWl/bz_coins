package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("新增/编辑包赔场规则入参")
@Data
public class OrderRuleEditIn {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "场次名称")
    @NotBlank(message = "请填写场次名称")
    private String name;

    @ApiModelProperty(value = "开始时间 小时分钟 例如9:30")
    @NotBlank(message = "请填写场次截止时间")
    private String beginTime;

    @ApiModelProperty(value = "借宿时间 小时分钟 例如19:30")
    @NotBlank(message = "请填写场次截止时间")
    private String endTime;

}
