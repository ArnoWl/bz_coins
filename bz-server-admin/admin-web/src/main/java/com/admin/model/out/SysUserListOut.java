package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统用户信息列表出参实体
 * @Author: lt
 * @Date: 2020/5/6 14:39
 */
@Data
@ApiModel(value = "系统用户信息列表出参")
public class SysUserListOut implements Serializable {

    private static final long serialVersionUID = -1985197836194383652L;

    @ApiModelProperty(value = "ID")
    private Integer id;

    @ApiModelProperty(value = "用户账号")
    private String userAccount;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "联系电话")
    private String telPhone;

    @ApiModelProperty(value = "城市分站名称")
    private String citySubstationName;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "状态（0：启用 1：禁用）")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

}
