package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取系统用户列表入参实体
 * @Author: lt
 * @Date: 2020/5/6 14:35
 */
@Data
@ApiModel(value = "获取系统用户列表入参")
public class SysUserListIn extends PageParamIn {
    private static final long serialVersionUID = 3190360061091072440L;

    @ApiModelProperty(value = "用户名")
    private String userAccount;

    @ApiModelProperty(value = "联系电话")
    private String telPhone;

}
