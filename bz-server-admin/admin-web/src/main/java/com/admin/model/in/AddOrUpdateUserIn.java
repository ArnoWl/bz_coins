package com.admin.model.in;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @Author: lt
 * @Date: 2020/5/6 14:46
 */
@Data
@ApiModel(value = "新增/修改系统用户信息")
public class AddOrUpdateUserIn extends BaseParamIn {

    private static final long serialVersionUID = 7206929495938802094L;

    @ApiModelProperty(value = "用户ID,修改传入")
    private Integer id;

    @ApiModelProperty(value = "用户账号")
    @NotBlank(message = "用户账号不能为空")
    private String userAccount;

    @ApiModelProperty(value = "用户密码")
    private String passWord;

    @ApiModelProperty(value = "真实姓名")
    @NotBlank(message = "真实姓名不能为空")
    private String realName;

    @ApiModelProperty(value = "用户头像")
    private String portrait;

    @ApiModelProperty(value = "联系电话")
    @NotBlank(message = "联系电话不能为空")
    private String telPhone;

    @ApiModelProperty(value = "角色Id")
    @NotNull(message = "角色ID不能为空")
    private Integer roleId;


    @ApiModelProperty(value = "状态（0：启用 1：禁用）")
    private String status;

}
