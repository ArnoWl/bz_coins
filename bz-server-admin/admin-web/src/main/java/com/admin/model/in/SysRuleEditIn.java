package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 系统规则入参实体
 * @Author: lt
 * @Date: 2020/5/6 13:57
 */
@Data
@ApiModel(value = "系统规则列表入参")
public class SysRuleEditIn {

    @ApiModelProperty(value = "id")
    @NotNull(message = "请选择规则")
    private Integer id;

    @ApiModelProperty(value = "标题")
    @NotBlank(message = "请填写标题")
    private String title;

    @ApiModelProperty(value = "富文本内容")
    @NotBlank(message = "请填写内容")
    private String content;
}
