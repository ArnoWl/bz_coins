package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 菜单列表入参实体
 * @Author: lt
 * @Date: 2020/5/6 11:38
 */
@Data
@ApiModel(value = "菜单列表入参")
public class MenuListIn extends PageParamIn {

    private static final long serialVersionUID = -8995448228767199796L;

    @ApiModelProperty(value = "菜单名称")
    private String name;

    @ApiModelProperty(value = "父编号")
    private String pid;

}
