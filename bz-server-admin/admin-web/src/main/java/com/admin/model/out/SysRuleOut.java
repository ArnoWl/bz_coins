package com.admin.model.out;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 系统规则列表出参实体
 * @Author: lt
 * @Date: 2020/5/6 13:57
 */
@Data
@ApiModel(value = "系统规则列表出参")
public class SysRuleOut {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "规则类型 1视频有关规则   2订单有关规则 3用户协议")
    private Integer type;
}
