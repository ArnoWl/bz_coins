package com.admin.model.in;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: lt
 * @Date: 2020/6/10 18:25
 */
@Data
@ApiModel(value = "添加app版本信息入参")
public class AddAppVersionIn extends BaseParamIn {

    private static final long serialVersionUID = -2981846622325970667L;

    @ApiModelProperty(value = "App客户端类型：1-用户端 2-商户端 3-代理端")
    @NotNull(message = "客户端类型不能为空")
    private Integer appType;

    @ApiModelProperty(value = "系统类型：0-安卓 1-IOS")
    @NotNull(message = "系统类型不能为空")
    private Integer systemType;

    @ApiModelProperty(value = "版本号")
    @NotNull(message = "版本号不能为空")
    private Integer versionNo;

    @ApiModelProperty(value = "版本号名称(1.1.2)")
    @NotBlank(message = "版本号名称不能为空")
    private String versionName;

    @ApiModelProperty(value = "兼容版本号")
    @NotNull(message = "兼容版本号不能为空")
    private Integer compatibleNo;

    @ApiModelProperty(value = "更新内容")
    @NotBlank(message = "更新内容不能为空")
    private String updateContent;

    @ApiModelProperty(value = "下载地址")
    @NotBlank(message = "下载地址不能为空")
    private String downloadLink;

    @ApiModelProperty(value = "状态：1-提示升级 2-不提示升级 3-强制升级")
    @NotNull(message = "状态不能为空")
    private Integer status;

}
