package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("包赔场规则编辑回显出参")
@Data
public class OrderRuleOut {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "场次名称")
    private String name;

    @ApiModelProperty(value = "开始时间 小时分钟 例如9:30")
    private String beginTime;

    @ApiModelProperty(value = "借宿时间 小时分钟 例如19:30")
    private String endTime;
}
