package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取banner图列表入参实体
 * @Author: lt
 * @Date: 2020/5/6 14:35
 */
@Data
@ApiModel(value = "获取banner图列表入参实体")
public class BannerListIn extends PageParamIn {

    @ApiModelProperty(value = "位置 1首页顶部 2首页中部")
    private Integer position;

    @ApiModelProperty(value = "位置 1公告 2分享")
    private Integer eventType;

}
