package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 编辑认购出参实体
 * @Author: lt
 * @Date: 2020/5/6 13:57
 */
@Data
@ApiModel(value = "编辑认购出参")
public class SubscriptionOut {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "当期购买最大总数量")
    private BigDecimal maxNum;
}
