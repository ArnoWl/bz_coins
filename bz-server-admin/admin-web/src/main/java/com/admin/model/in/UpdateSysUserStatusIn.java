package com.admin.model.in;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: lt
 * @Date: 2020/6/27 13:44
 */
@Data
@ApiModel(value = "修改用户状态入参")
public class UpdateSysUserStatusIn extends BaseParamIn {

    private static final long serialVersionUID = -8910484219816777409L;

    @ApiModelProperty(value = "系统用户Id")
    @NotNull(message = "系统用户Id不能为空")
    private Integer userId;

    @ApiModelProperty(value = "状态（0：启用 1：禁用）")
    @NotBlank(message = "状态不能为空")
    private String status;
}
