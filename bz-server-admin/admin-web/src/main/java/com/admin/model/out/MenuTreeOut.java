package com.admin.model.out;

import com.common.base.TreeNodeVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 菜单树形列表出参实体
 * @Author: lt
 * @Date: 2020/5/6 10:24
 */
@ApiModel(value = "菜单树形列表出参")
@Data
public class MenuTreeOut extends TreeNodeVo {

    private static final long serialVersionUID = -956299502670289392L;
    @ApiModelProperty(value = "菜单icon")
    private String icon;

    @ApiModelProperty(value = "标题")
    private String name;

    @ApiModelProperty(value = "URL")
    private String url;

}
