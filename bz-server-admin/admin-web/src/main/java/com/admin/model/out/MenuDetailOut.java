package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 菜单详情信息出参实体
 * @Author: lt
 * @Date: 2020/5/6 14:00
 */
@Data
@ApiModel(value = "菜单详情信息出参")
public class MenuDetailOut extends MenuListOut {

    private static final long serialVersionUID = 1725371159858800049L;

    @ApiModelProperty(value = "菜单icon")
    private String icon;

}
