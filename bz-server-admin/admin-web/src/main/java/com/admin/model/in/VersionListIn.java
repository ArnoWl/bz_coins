package com.admin.model.in;

import com.baomidou.mybatisplus.annotation.Version;
import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: lt
 * @Date: 2020/5/30 10:44
 */
@Data
@ApiModel(value = "版本管理列表入参")
public class VersionListIn extends PageParamIn {

    private static final long serialVersionUID = 7911507349340124514L;

    @ApiModelProperty(value = "系统类型：0-安卓 1-IOS")
    private Integer systemType;

    @ApiModelProperty(value = "App客户端类型：1-用户端 2-商户端 3-代理端")
    private Integer appType;

    @ApiModelProperty(value = "版本号")
    @Version
    private String versionNo;

    @ApiModelProperty(value = "状态：1-提 2-不提示升级 3-强制升级")
    private Integer status;
}
