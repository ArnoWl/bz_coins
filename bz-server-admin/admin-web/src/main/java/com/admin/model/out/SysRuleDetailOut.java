package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 系统规则详情出参实体
 * @Author: lt
 * @Date: 2020/5/6 13:57
 */
@Data
@ApiModel(value = "系统规则详情出参")
public class SysRuleDetailOut {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "富文本内容")
    private String content;
}
