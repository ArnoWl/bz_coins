package com.admin.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel("客服信息入参")
@Data
public class ContactEditIn {

    @ApiModelProperty(value = "主键")
    @NotNull(message = "请选择数据")
    private Integer id;

    @ApiModelProperty(value = "头像")
    @NotBlank(message = "请上传头像")
    private String headUrl;

    @ApiModelProperty(value = "微信")
    @NotBlank(message = "请上传微信号")
    private String wx;

    @ApiModelProperty(value = "微信二维码")
    @NotBlank(message = "请上传微信二维码")
    private String wxQr;
}
