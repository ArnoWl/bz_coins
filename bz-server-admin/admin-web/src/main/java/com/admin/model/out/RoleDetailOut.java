package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 角色详情信息出参实体
 * @Author: lt
 * @Date: 2020/5/6 13:56
 */
@Data
@ApiModel(value = "角色详情信息出参")
public class RoleDetailOut extends RoleListOut {
    private static final long serialVersionUID = -2760872107549705169L;

    @ApiModelProperty(value = "菜单资源ID集合")
    private List<Integer> menuIds;
}
