package com.admin.model.in;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 新增/修改角色信息入参实体
 * @Author: lt
 * @Date: 2020/5/6 13:42
 */
@Data
@ApiModel(value = "新增/修改角色信息入参")
public class AddOrUpdateRoleIn extends BaseParamIn {

    private static final long serialVersionUID = 6898097729448353035L;

    @ApiModelProperty(value = "角色Id，修改传入")
    private Integer id;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "关联权限菜单资源Id")
    private List<Integer> menuIds;

}
