package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel("货币地址查询入参")
@Data
public class CoinAddrQueryIn extends PageParamIn {

    @ApiModelProperty(value = "id")
    @NotNull(message = "请选择系统货币")
    private Integer id;

    @ApiModelProperty(value = "主地址 MAIN 手续费地址 FEE")
    private String type;

    @ApiModelProperty(value = "协议类型 OMNI  ERC20")
    private String agreeType;
}
