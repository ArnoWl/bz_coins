package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@ApiModel("系统货币价格记录出参")
@Data
public class CoinPriceListOut {

    @ApiModelProperty(value = "货币名称")
    private String name;
    @ApiModelProperty(value = "价格")
    private BigDecimal price;
    @ApiModelProperty(value = "涨幅")
    private BigDecimal rate;
    @ApiModelProperty(value = "操作时间")
    private LocalDateTime createTime;
}
