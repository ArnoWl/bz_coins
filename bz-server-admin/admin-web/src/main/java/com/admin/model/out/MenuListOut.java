package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 菜单列表出参实体
 * @Author: lt
 * @Date: 2020/5/6 11:43
 */
@Data
@ApiModel(value = "菜单列表出参")
public class MenuListOut implements Serializable {
    private static final long serialVersionUID = -7531155228503744302L;

    @ApiModelProperty(value = "ID")
    private Integer id;

    @ApiModelProperty(value = "父级ID")
    private Integer parentId;

    @ApiModelProperty(value = "标题")
    private String name;

    @ApiModelProperty(value = "菜单类型：0-菜单 1-按钮")
    private Integer type;

    @ApiModelProperty(value = "URL")
    private String url;

    @ApiModelProperty(value = "授权标识")
    private String authValue;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态（0：启用 1：禁用）")
    private Integer status;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

}
