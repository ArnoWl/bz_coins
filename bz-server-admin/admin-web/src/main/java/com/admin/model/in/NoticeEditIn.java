package com.admin.model.in;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 添加/编辑公告入参实体
 * @Author: lt
 * @Date: 2020/5/6 14:35
 */
@Data
@ApiModel(value = "添加/编辑公告入参实体")
public class NoticeEditIn {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "描述")
    private String descVal;

    @ApiModelProperty(value = "内容")
    private String content;
}
