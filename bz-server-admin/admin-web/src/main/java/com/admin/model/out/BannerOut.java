package com.admin.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * banner 图信息出参
 * @Author: lt
 * @Date: 2020/5/6 14:00
 */
@Data
@ApiModel(value = "banner图信息出参")
public class BannerOut {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "banner图")
    private String bannerUrl;

    @ApiModelProperty(value = "位置 1首页顶部   2首页中部")
    private Integer position;

    @ApiModelProperty(value = "事件类型  1：跳转公告   2：分享  ")
    private Integer eventType;

}
