package com.admin.model.in;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("统计入参")
@Data
public class StatisQueryIn extends PageParamIn {

    @ApiModelProperty("查询时间 格式yyyy-MM-dd~yyyy-MM-dd")
    private String queryTime;
}
