package com.admin.service.impl;

import com.admin.entity.SysCoinAddr;
import com.admin.entity.SysCoinPrice;
import com.admin.entity.SysCoins;
import com.admin.model.in.*;
import com.admin.model.out.CoinAddrOut;
import com.admin.model.out.CoinPriceListOut;
import com.admin.model.out.CoinsOut;
import com.admin.service.CoinsService;
import com.admin.service.ISysCoinAddrService;
import com.admin.service.ISysCoinPriceService;
import com.admin.service.ISysCoinsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.AdminConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.enums.wallet.AgreeEnums;
import com.common.exception.BaseException;
import com.common.utils.ExtBeanUtils;
import com.common.utils.codec.CodeUtils;
import com.common.utils.lang.StringUtils;
import com.mall.base.PageParamIn;
import com.mall.feign.wallet.WalletClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CoinsServiceImpl implements CoinsService {

    @Resource
    private ISysCoinsService sysCoinsService;
    @Resource
    private ISysCoinAddrService sysCoinAddrService;
    @Resource
    private WalletClient walletClient;
    @Resource
    private ISysCoinPriceService sysCoinPriceService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public ResultVo queryCoin(PageParamIn pageParamIn) {
        IPage<SysCoins> pageInfo = new Page<>(pageParamIn.getPageNo(), pageParamIn.getPageSize());
        sysCoinsService.page(pageInfo, null);
        IPage<CoinsOut> pageInfoOut = new Page<>();
        BeanUtils.copyProperties(pageInfo, pageInfoOut);
        List<CoinsOut> list = ExtBeanUtils.copyList(pageInfo.getRecords(), CoinsOut.class);
        pageInfoOut.setRecords(list);
        return ResultVo.success(pageInfoOut);
    }

    @Override
    public ResultVo editCoin(CoinsIn coinsIn) {
        SysCoins coins = new SysCoins();
        BeanUtils.copyProperties(coinsIn, coins);
        sysCoinsService.saveOrUpdate(coins);
        return ResultVo.success();
    }

    @Override
    public ResultVo editShow(Integer id) {
        SysCoins coins = sysCoinsService.getById(id);
        if (coins == null) {
            return ResultVo.failure("未获取到币种信息");
        }
        CoinsOut out = new CoinsOut();
        BeanUtils.copyProperties(coins, out);
        return ResultVo.success(out);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo handleCoinAddr(CoinAddrIn addrIn) {
        SysCoins sysCoins = sysCoinsService.getById(addrIn.getId());
        if (sysCoins == null) {
            return ResultVo.failure("货币不存在,请刷新后重试");
        }
        try {
            ResultVo resultVo;
            if (addrIn.getImputedType().equals(AdminConstants.SYS_IMPUTED_TYPE_1)) {
                String payCode = CodeUtils.generateOrderNo();
                if (addrIn.getAgreeType().equalsIgnoreCase(AgreeEnums.OMNI.name())) {
                    resultVo = walletClient.createOmniWallet(payCode);
                } else if (addrIn.getAgreeType().equalsIgnoreCase(AgreeEnums.ERC20.name())) {
                    resultVo = walletClient.createERC20Wallet(payCode);
                } else {
                    return ResultVo.failure("协议类型不存在");
                }
                if (!resultVo.isSuccess()) {
                    return ResultVo.failure("生成货币钱包地址失败");
                }
                Map<String, String> map = (Map<String, String>) resultVo.getData();
                SysCoinAddr coinAddr = new SysCoinAddr();
                BeanUtils.copyProperties(addrIn,coinAddr);
                coinAddr.setCoinId(sysCoins.getId());
                coinAddr.setWalletAddr(map.get("walletAddr"));
                coinAddr.setWalletPath(map.get("walletPath"));
                coinAddr.setWalletPassword(payCode);
                sysCoinAddrService.save(coinAddr);
                return ResultVo.success();
            } else if (addrIn.getImputedType().equals(AdminConstants.SYS_IMPUTED_TYPE_2)) {
                if(StringUtils.isBlank(addrIn.getWalletAddr())){
                    return ResultVo.failure("请填写钱包地址");
                }
                SysCoinAddr coinAddr = new SysCoinAddr();
                BeanUtils.copyProperties(addrIn,coinAddr);
                coinAddr.setCoinId(sysCoins.getId());
                sysCoinAddrService.save(coinAddr);
                return ResultVo.success();
            }else{
                return ResultVo.failure("参数错误");
            }
        } catch (Exception e) {
            throw new BaseException("生成货币钱包地址失败");
        }
    }

    @Override
    public ResultVo queryCoinAddr(CoinAddrQueryIn queryIn) {
        QueryWrapper<SysCoinAddr> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("coin_id", queryIn.getId());
        queryWrapper.orderByDesc("id");
        if (StringUtils.isNotBlank(queryIn.getType())) {
            queryWrapper.eq("type", queryIn.getType());
        }
        if (StringUtils.isNotBlank(queryIn.getAgreeType())) {
            queryWrapper.eq("agree_type", queryIn.getAgreeType());
        }
        IPage<SysCoinAddr> pageInfo = new Page<>(queryIn.getPageNo(), queryIn.getPageSize());
        sysCoinAddrService.page(pageInfo, queryWrapper);

        Page<CoinAddrOut> pageInfoOut = new Page<>();
        BeanUtils.copyProperties(pageInfo, pageInfoOut);
        pageInfoOut.setRecords(ExtBeanUtils.copyList(pageInfo.getRecords(), CoinAddrOut.class));
        return ResultVo.success(pageInfoOut);
    }

    @Override
    public ResultVo queryCoinPrice(CoinPriceQueryIn queryIn) {
        QueryWrapper<SysCoinPrice> queryWrapper = new QueryWrapper<>();
        if (queryIn.getCoinId() != null) {
            queryWrapper.eq("coin_id", queryIn.getCoinId());
        }
        IPage<SysCoinPrice> pageInfo = new Page<>(queryIn.getPageNo(), queryIn.getPageSize());
        sysCoinPriceService.page(pageInfo, queryWrapper);
        IPage<CoinPriceListOut> pageInfoOut = new Page<>();
        BeanUtils.copyProperties(pageInfo, pageInfoOut);
        for (SysCoinPrice coinPrice : pageInfo.getRecords()) {
            SysCoins coins = sysCoinsService.getById(coinPrice.getId());
            if (coins != null) {
                coinPrice.setName(coins.getName());
            }
        }
        pageInfoOut.setRecords(ExtBeanUtils.copyList(pageInfo.getRecords(), CoinPriceListOut.class));
        return ResultVo.success(pageInfoOut);
    }

    @Override
    public ResultVo addCoinPrice(CoinPriceAddIn addIn) {
        SysCoins sysCoins = sysCoinsService.getById(addIn.getCoinId());
        if (sysCoins == null) {
            return ResultVo.failure("数据已删除");
        }
        if (AdminConstants.SYS_COINS_NO.equals(sysCoins.getSysFlag())) {
            return ResultVo.failure("不是系统币,无法设置价格");
        }
        SysCoinPrice coinPrice = new SysCoinPrice();
        BeanUtils.copyProperties(addIn, coinPrice);
        sysCoinPriceService.save(coinPrice);

        //缓存
        Map<String, Object> map = new HashMap<>();
        map.put("price", coinPrice.getPrice().toString());
        map.put("rate", coinPrice.getRate().toString());
        redisTemplate.opsForHash().putAll(RedisKeyConstants.SYS_COIN_PRICE + sysCoins.getName(), map);
        return ResultVo.success();
    }
}
