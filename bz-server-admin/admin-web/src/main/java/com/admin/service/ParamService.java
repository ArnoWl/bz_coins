package com.admin.service;

import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.model.admin.dto.SysConfigDto;

public interface ParamService {
    ResultVo queryParam(PageParamIn pageParamIn);

    ResultVo editParam(SysConfigDto configDto);

    ResultVo editShow(Integer id);
}
