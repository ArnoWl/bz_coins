package com.admin.service.impl;

import com.admin.entity.SysBanner;
import com.admin.model.in.BannerEditIn;
import com.admin.model.in.BannerListIn;
import com.admin.model.out.BannerOut;
import com.admin.service.BannerService;
import com.admin.service.ISysBannerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.utils.ExtBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerServiceImpl implements BannerService {
    @Autowired
    private ISysBannerService sysBannerService;

    @Override
    public ResultVo queryBanner(BannerListIn bannerListIn) {
        QueryWrapper<SysBanner> queryWrapper=new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        if(bannerListIn.getPosition()!=null) {
            queryWrapper.eq("position",bannerListIn.getPosition());
        }
        if(bannerListIn.getEventType()!=null) {
            queryWrapper.eq("event_type",bannerListIn.getEventType());
        }
        IPage<SysBanner> pageInfo =new Page<>(bannerListIn.getPageNo(),bannerListIn.getPageSize());
        sysBannerService.page(pageInfo,queryWrapper);
        IPage<BannerOut> pageInfoOut =new Page<>();
        BeanUtils.copyProperties(pageInfo,pageInfoOut);
        List<BannerOut> list = ExtBeanUtils.copyList(pageInfo.getRecords(), BannerOut.class);
        pageInfoOut.setRecords(list);
        return ResultVo.success(pageInfoOut);
    }

    @Override
    public ResultVo editBanner(BannerEditIn editIn) {
        SysBanner banner=new SysBanner();
        BeanUtils.copyProperties(editIn,banner);
        sysBannerService.saveOrUpdate(banner);
        return ResultVo.success();
    }

    @Override
    public ResultVo delBanner(Integer id) {
        sysBannerService.removeById(id);
        return ResultVo.success();
    }

    @Override
    public ResultVo editShow(Integer id) {
        SysBanner banner = sysBannerService.getById(id);
        if(banner==null){
            return ResultVo.failure("banner图已删除");
        }
        BannerOut out =new BannerOut();
        BeanUtils.copyProperties(banner,out);
        return ResultVo.success(out);
    }
}
