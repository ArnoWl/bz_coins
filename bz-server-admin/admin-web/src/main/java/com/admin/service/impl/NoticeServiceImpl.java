package com.admin.service.impl;
import com.admin.entity.SysNoticeInfo;
import com.admin.model.in.NoticeEditIn;
import com.admin.model.in.NoticeListIn;
import com.admin.service.ISysNoticeInfoService;
import com.admin.service.NoticeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.utils.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    private ISysNoticeInfoService sysNoticeInfoService;

    @Override
    public ResultVo queryNotice(NoticeListIn listIn) {
        QueryWrapper<SysNoticeInfo> queryWrapper=new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        if(StringUtils.isNotBlank(listIn.getTitle())) {
            queryWrapper.eq("title",listIn.getTitle());
        }
        IPage<SysNoticeInfo> pageInfo =new Page<>(listIn.getPageNo(),listIn.getPageSize());
        sysNoticeInfoService.page(pageInfo,queryWrapper);
        return ResultVo.success(pageInfo);
    }

    @Override
    public ResultVo editNotice(NoticeEditIn editIn) {
        SysNoticeInfo noticeInfo=new SysNoticeInfo();
        BeanUtils.copyProperties(editIn,noticeInfo);
        sysNoticeInfoService.saveOrUpdate(noticeInfo);
        return ResultVo.success();
    }

    @Override
    public ResultVo editNoticeShow(Integer id) {
        SysNoticeInfo noticeInfo = sysNoticeInfoService.getById(id);
        if(noticeInfo==null){
            return ResultVo.failure("公告已删除");
        }
        return ResultVo.success(noticeInfo);
    }

    @Override
    public ResultVo delNotice(Integer id) {
        sysNoticeInfoService.removeById(id);
        return ResultVo.success();
    }
}
