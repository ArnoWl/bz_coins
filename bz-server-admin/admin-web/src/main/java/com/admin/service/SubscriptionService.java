package com.admin.service;

import com.admin.entity.SysSubscription;
import com.admin.model.in.SubscriptionEditIn;
import com.admin.model.in.SubscriptionListIn;
import com.common.base.ResultVo;
import com.mall.model.user.dto.SubscriptionDetailDto;

public interface SubscriptionService {
    ResultVo querySubscription(SubscriptionListIn listIn);

    ResultVo editSubscription(SubscriptionEditIn editIn);

    ResultVo delSubscription(Integer id);

    ResultVo editSubscriptionShow(Integer id);
}
