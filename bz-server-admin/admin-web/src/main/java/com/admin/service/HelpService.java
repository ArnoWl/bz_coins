package com.admin.service;

import com.admin.model.in.HelpEditIn;
import com.common.base.ResultVo;

public interface HelpService {

    ResultVo editHelp(HelpEditIn editIn);

    ResultVo editShow(Integer id,Integer level);

    ResultVo delHelp(Integer id, Integer level);

    ResultVo queryList();
}
