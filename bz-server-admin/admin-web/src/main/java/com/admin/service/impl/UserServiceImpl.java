package com.admin.service.impl;

import com.admin.model.in.UserWalletIn;
import com.admin.service.UserService;
import com.common.base.ResultVo;
import com.common.enums.user.WalletLogTypeEnums;
import com.common.utils.codec.CodeUtils;
import com.mall.feign.user.UserInfoClient;
import com.mall.model.user.dto.WalletLogDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserInfoClient userInfoClient;

    @Override
    public ResultVo handleUserWalletMoney(UserWalletIn walletIn) {
        String payCode = CodeUtils.generateOrderNo();
        WalletLogDto dto =new WalletLogDto();
        dto.setPayCode(payCode);
        BeanUtils.copyProperties(walletIn,dto);
        ResultVo resultVo = userInfoClient.handleUserWalletMoney(dto);
        if(!resultVo.isSuccess()){
            return resultVo;
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo queryRewardType() {
        return ResultVo.success(WalletLogTypeEnums.getList());
    }
}
