package com.admin.service;

import com.admin.model.in.CoinKlineEditIn;
import com.admin.model.in.KlineQueryIn;
import com.common.base.ResultVo;
import org.springframework.stereotype.Service;

@Service
public interface KlineService {
    ResultVo queryKline(KlineQueryIn queryIn);

    ResultVo klineEdit(CoinKlineEditIn editIn);
}
