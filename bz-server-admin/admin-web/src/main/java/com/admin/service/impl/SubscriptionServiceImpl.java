package com.admin.service.impl;

import com.admin.entity.SysSubscription;
import com.admin.model.in.SubscriptionEditIn;
import com.admin.model.in.SubscriptionListIn;
import com.admin.model.out.SubscriptionOut;
import com.admin.service.ISysSubscriptionService;
import com.admin.service.SubscriptionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.utils.lang.StringUtils;
import com.mall.model.user.dto.SubscriptionDetailDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    @Autowired
    private ISysSubscriptionService sysSubscriptionService;

    @Override
    public ResultVo querySubscription(SubscriptionListIn listIn) {
        QueryWrapper<SysSubscription> queryWrapper=new QueryWrapper<>();
        if(StringUtils.isNotBlank(listIn.getName())) {
            queryWrapper.like("name", listIn.getName());
        }
        IPage<SysSubscription> pageInfo=new Page<>(listIn.getPageNo(),listIn.getPageSize());
        sysSubscriptionService.page(pageInfo,queryWrapper);
        return ResultVo.success(pageInfo);
    }

    @Override
    public ResultVo editSubscription(SubscriptionEditIn editIn) {
        SysSubscription subscription=new SysSubscription();
        BeanUtils.copyProperties(editIn,subscription);
        subscription.setBalance(subscription.getTotalNum());
        sysSubscriptionService.saveOrUpdate(subscription);
        return ResultVo.success();
    }

    @Override
    public ResultVo delSubscription(Integer id) {
        sysSubscriptionService.removeById(id);
        return ResultVo.success();
    }

    @Override
    public ResultVo editSubscriptionShow(Integer id) {
        SysSubscription subscription = sysSubscriptionService.getById(id);
        if(subscription==null){
            ResultVo.failure("数据已删除");
        }
        SubscriptionOut out =new SubscriptionOut();
        BeanUtils.copyProperties(subscription,out);
        return ResultVo.success(out);
    }
}
