package com.admin.service.impl;

import com.admin.entity.SysRole;
import com.admin.entity.SysRoleMenu;
import com.admin.model.in.AddOrUpdateRoleIn;
import com.admin.model.out.RoleDetailOut;
import com.admin.model.out.RoleListOut;
import com.admin.service.ISysRoleMenuService;
import com.admin.service.ISysRoleService;
import com.admin.service.RoleApiService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.context.SysUserContext;
import com.common.exception.BaseException;
import com.common.utils.ExtBeanUtils;
import com.google.common.collect.Lists;
import com.mall.base.PageParamIn;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 角色信息Api实现类
 * @Author: lt
 * @Date: 2020/5/6 14:05
 */
@Service
public class RoleApiServiceImpl implements RoleApiService {

    @Resource
    private ISysRoleService iSysRoleService;
    @Resource
    private ISysRoleMenuService iSysRoleMenuService;

    @Override
    public ResultVo getRoleList(PageParamIn paramIn) {
        try {
            Page<SysRole> rolePage = new Page<>(paramIn.getPageNo(),paramIn.getPageSize());
            iSysRoleService.page(rolePage);
            // 组装出参
            Page<RoleListOut> outPage = new Page<>();
            BeanUtils.copyProperties(rolePage,outPage);
            if(!rolePage.getRecords().isEmpty()){
                outPage.setRecords(ExtBeanUtils.copyList(rolePage.getRecords(), RoleListOut.class));
            }
            return ResultVo.success(outPage);
        } catch (Exception e) {
            throw new BaseException("获取角色列表失败",e);
        }
    }

    @Override
    public ResultVo getRoleDetail(Integer roleId) {
        try {
            SysRole role = iSysRoleService.getById(roleId);
            if(role != null){
                RoleDetailOut detailOut = new RoleDetailOut();
                BeanUtils.copyProperties(role,detailOut);
                detailOut.setMenuIds(iSysRoleMenuService.getMenuIdsByRoleId(roleId));
                return ResultVo.success(detailOut);
            }
            return ResultVo.failure("角色信息不存在");
        } catch (Exception e) {
            throw new BaseException("获取角色详情失败",e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo addOrUpdateRole(AddOrUpdateRoleIn roleIn) {
        try {
            SysRole role;
            if(roleIn.getId() != null){     // 修改
                role = iSysRoleService.getById(roleIn.getId());
                if(role != null){
                    BeanUtils.copyProperties(roleIn,role);
                    role.setUpdateBy(SysUserContext.getSysUserID());
                    role.setUpdateTime(LocalDateTime.now());
                    iSysRoleService.updateById(role);
                    // 先执行移除原有角色与菜单管理信息
                    QueryWrapper<SysRoleMenu> wrapper = new QueryWrapper<>();
                    wrapper.eq("role_id",role.getId());
                    iSysRoleMenuService.remove(wrapper);
                }else {
                    ResultVo.failure("角色ID有误，修改角色信息失败");
                }
            }else{  // 新增
                role = new SysRole();
                BeanUtils.copyProperties(roleIn,role);
                role.setCreateBy(SysUserContext.getSysUserID());
                iSysRoleService.save(role);
            }
            // 新增现选中的菜单资源信息
            if(!CollectionUtils.isEmpty(roleIn.getMenuIds())){
                List<SysRoleMenu> roleMenus = Lists.newArrayList();
                SysRoleMenu roleMenu;
                for (Integer menuId : roleIn.getMenuIds()) {
                    roleMenu = new SysRoleMenu();
                    roleMenu.setRoleId(role.getId());
                    roleMenu.setMenuId(menuId);
                    roleMenus.add(roleMenu);
                }
                iSysRoleMenuService.saveBatch(roleMenus);
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("新增/修改角色信息失败",e);
        }
    }

    @Override
    public ResultVo removeRole(Integer roleId) {
        try {
            SysRole role = iSysRoleService.getById(roleId);
            if(role != null){
                iSysRoleService.removeById(roleId);
                return ResultVo.success();
            }
            return ResultVo.failure("角色ID有误，移除角色失败");
        } catch (Exception e) {
            throw new BaseException("移除角色失败",e);
        }
    }
}
