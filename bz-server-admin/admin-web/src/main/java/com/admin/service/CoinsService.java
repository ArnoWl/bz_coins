package com.admin.service;

import com.admin.model.in.*;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;

public interface CoinsService {
    ResultVo queryCoin(PageParamIn pageParamIn);

    ResultVo editCoin(CoinsIn coinsIn);

    ResultVo editShow(Integer id);

    ResultVo handleCoinAddr(CoinAddrIn addrIn);

   ResultVo queryCoinAddr(CoinAddrQueryIn queryIn);

    ResultVo queryCoinPrice(CoinPriceQueryIn queryIn);

    ResultVo addCoinPrice(CoinPriceAddIn addIn);
}
