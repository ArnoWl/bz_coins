package com.admin.service;

import com.admin.model.in.AddOrUpdateRoleIn;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;

/**
 * 角色信息Api接口类
 * @Author: lt
 * @Date: 2020/5/6 13:34
 */
public interface RoleApiService {

    /**
     * 获取角色列表
     * @return
     */
    ResultVo getRoleList(PageParamIn paramIn);

    /**
     * 获取角色信息详情
     * @param roleId
     * @return
     */
    ResultVo getRoleDetail(Integer roleId);

    /**
     * 新增/修改角色信息
     * @param roleIn
     * @return
     */
    ResultVo addOrUpdateRole(AddOrUpdateRoleIn roleIn);

    /**
     * 移除角色
     * @param roleId
     * @return
     */
    ResultVo removeRole(Integer roleId);
}
