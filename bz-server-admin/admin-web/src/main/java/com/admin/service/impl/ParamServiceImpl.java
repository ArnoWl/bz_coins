package com.admin.service.impl;

import com.admin.entity.SysConfig;
import com.admin.service.ISysConfigService;
import com.admin.service.ParamService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.RedisKeyConstants;
import com.common.context.SysUserContext;
import com.common.utils.ExtBeanUtils;
import com.mall.base.PageParamIn;
import com.mall.model.admin.dto.SysConfigDto;
import com.mall.model.admin.vo.SysConfigDetailVo;
import com.mall.model.admin.vo.SysConfigVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ParamServiceImpl implements ParamService {

    @Autowired
    private ISysConfigService sysConfigService;

    @Override
    public ResultVo queryParam(PageParamIn pageParamIn) {
        IPage<SysConfig> pageInfo=new Page<>(pageParamIn.getPageNo(),pageParamIn.getPageSize());
        sysConfigService.page(pageInfo, null);
        IPage<SysConfigVo> pageInfoOut=new Page<>();
        BeanUtils.copyProperties(pageInfo,pageInfoOut);
        List<SysConfigVo> list = ExtBeanUtils.copyList(pageInfo.getRecords(), SysConfigVo.class);
        pageInfoOut.setRecords(list);
        return ResultVo.success(pageInfoOut);
    }

    @Override
    public ResultVo editParam(SysConfigDto configDto) {
        SysConfig config = sysConfigService.getById(configDto.getId());
        BeanUtils.copyProperties(configDto,config);
        config.setUpdateTime(LocalDateTime.now());
        config.setUpdateBy(SysUserContext.getSysUsername());
        sysConfigService.updateById(config);
        sysConfigService.storeToCache();
        return ResultVo.success();
    }

    @Override
    public ResultVo editShow(Integer id) {
        SysConfig sysConfig = sysConfigService.getById(id);
        if(sysConfig==null){
            return ResultVo.failure("参数已删除");
        }
        SysConfigDetailVo vo=new SysConfigDetailVo();
        BeanUtils.copyProperties(sysConfig,vo);
        return ResultVo.success(vo);
    }
}
