package com.admin.service.impl;

import com.admin.entity.SysStatis;
import com.admin.model.in.StatisQueryIn;
import com.admin.service.ISysStatisService;
import com.admin.service.StatisService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.utils.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class StatisServceImpl implements StatisService {
    @Resource
    private ISysStatisService sysStatisService;

    @Override
    public ResultVo queryList(StatisQueryIn queryIn) {
        Page<SysStatis> page = new Page<>(queryIn.getPageNo(), queryIn.getPageSize());
        QueryWrapper<SysStatis> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(queryIn.getQueryTime())) {
            String[] split = queryIn.getQueryTime().split("~");
            queryWrapper.ge("day", split[0]);
            queryWrapper.le("day", split[1]);
        }
        queryWrapper.orderByDesc("day");
        sysStatisService.page(page,queryWrapper);
        return ResultVo.success(page);
    }
}
