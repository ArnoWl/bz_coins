package com.admin.service.impl;

import com.admin.entity.SysRuleArticle;
import com.admin.model.in.SysRuleEditIn;
import com.admin.model.out.SysRuleDetailOut;
import com.admin.model.out.SysRuleOut;
import com.admin.service.ISysRuleArticleService;
import com.admin.service.RuleService;
import com.common.base.ResultVo;
import com.common.utils.ExtBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RuleServiceImpl implements RuleService {

    @Autowired
    private ISysRuleArticleService sysRuleArticleService;

    @Override
    public ResultVo queryRule() {
        List<SysRuleArticle> list = sysRuleArticleService.list();
        List<SysRuleOut> listOut = ExtBeanUtils.copyList(list, SysRuleOut.class);
        return ResultVo.success(listOut);
    }

    @Override
    public ResultVo editShow(Integer id) {
        SysRuleArticle ruleArticle = sysRuleArticleService.getById(id);
        SysRuleDetailOut out=new SysRuleDetailOut();
        if(ruleArticle!=null){
            BeanUtils.copyProperties(ruleArticle,out);
        }
        return ResultVo.success(out);
    }

    @Override
    public ResultVo editRule(SysRuleEditIn editIn) {
        SysRuleArticle ruleArticle=new SysRuleArticle();
        BeanUtils.copyProperties(editIn,ruleArticle);
        sysRuleArticleService.updateById(ruleArticle);
        return ResultVo.success();
    }
}
