package com.admin.service;

import com.admin.model.in.StatisQueryIn;
import com.common.base.ResultVo;

public interface StatisService {
    ResultVo queryList(StatisQueryIn queryIn);
}
