package com.admin.service;

import com.admin.model.in.BannerEditIn;
import com.admin.model.in.BannerListIn;
import com.common.base.ResultVo;


public interface BannerService {

    ResultVo queryBanner(BannerListIn bannerListIn);

    ResultVo editBanner(BannerEditIn editIn);

    ResultVo delBanner(Integer id);

    ResultVo editShow(Integer id);
}
