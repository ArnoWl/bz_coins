package com.admin.service;

import com.admin.model.in.AddOrUpdateUserIn;
import com.admin.model.in.SysUserListIn;
import com.admin.model.in.UpdateSysUserStatusIn;
import com.common.base.ResultVo;

/**
 * 系统用户信息Api接口类
 * @Author: lt
 * @Date: 2020/5/6 14:34
 */
public interface SysUserApiService {

    /**
     * 获取用户列表
     * @return
     */
    ResultVo getUserList(SysUserListIn listIn);

    /**
     * 获取用户信息详情
     * @param userId
     * @return
     */
    ResultVo getUserDetail(Integer userId);

    /**
     * 新增/修改用户信息
     * @param userIn
     * @return
     */
    ResultVo addOrUpdateUser(AddOrUpdateUserIn userIn);

    /**
     * 修改用户状态
     * @param statusIn
     * @return
     */
    ResultVo updateUserStatus(UpdateSysUserStatusIn statusIn);

    /**
     * 移除用户
     * @param userId
     * @return
     */
    ResultVo removeUser(Integer userId);
}
