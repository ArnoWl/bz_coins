package com.admin.service;

import com.admin.model.in.SysRuleEditIn;
import com.common.base.ResultVo;
import org.springframework.stereotype.Service;

@Service
public interface RuleService {
    ResultVo queryRule();

    ResultVo editShow(Integer id);

    ResultVo editRule(SysRuleEditIn editIn);
}
