package com.admin.service.impl;

import com.admin.entity.SysHelpCenter;
import com.admin.entity.SysHelpCenterInfo;
import com.admin.model.in.HelpEditIn;
import com.admin.service.HelpService;
import com.admin.service.ISysHelpCenterInfoService;
import com.admin.service.ISysHelpCenterService;
import com.common.base.ResultVo;
import com.common.utils.lang.StringUtils;
import com.mall.feign.admin.BaseClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelpServiceImpl implements HelpService {

    @Autowired
    private ISysHelpCenterService sysHelpCenterService;
    @Autowired
    private ISysHelpCenterInfoService sysHelpCenterInfoService;
    @Autowired
    private BaseClient adminClient;

    @Override
    public ResultVo editHelp(HelpEditIn editIn) {
        if(editIn.getLevel()==1){
            if(StringUtils.isBlank(editIn.getIcon())){
                return ResultVo.failure("请上传图标");
            }
            SysHelpCenter center=new SysHelpCenter();
            BeanUtils.copyProperties(editIn,center);
            sysHelpCenterService.saveOrUpdate(center);
        }else{
            if(StringUtils.isBlank(editIn.getContent())){
                return ResultVo.failure("请填写内容");
            }
            if(editIn.getHelpCenterId()==null){
                return ResultVo.failure("请选择上级");
            }
            SysHelpCenterInfo info=new SysHelpCenterInfo();
            BeanUtils.copyProperties(editIn,info);
            sysHelpCenterInfoService.saveOrUpdate(info);
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo editShow(Integer id,Integer level) {
        if(level==1){
            SysHelpCenter sysHelpCenter = sysHelpCenterService.getById(id);
            return ResultVo.success(sysHelpCenter);
        }else{
            SysHelpCenterInfo sysHelpCenterInfo = sysHelpCenterInfoService.getById(id);
            return ResultVo.success(sysHelpCenterInfo);
        }
    }

    @Override
    public ResultVo delHelp(Integer id, Integer level) {
        if(level==1){
            sysHelpCenterService.removeById(id);
        }else{
            sysHelpCenterInfoService.removeById(id);
        }
        return ResultVo.success();
    }

    @Override
    public ResultVo queryList() {
        return adminClient.queryHelp();
    }
}
