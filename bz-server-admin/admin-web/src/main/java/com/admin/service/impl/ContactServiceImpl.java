package com.admin.service.impl;

import com.admin.entity.SysContact;
import com.admin.model.in.ContactEditIn;
import com.admin.service.ContactService;
import com.admin.service.ISysContactService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.common.base.ResultVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ISysContactService sysContactService;

    @Override
    public ResultVo getContactInfo() {
        QueryWrapper<SysContact> queryWrapper=new QueryWrapper<>();
        queryWrapper.last("limit 1");
        SysContact one = sysContactService.getOne(queryWrapper);
        return ResultVo.success(one);
    }

    @Override
    public ResultVo getContactDetail(Integer id) {
        SysContact detail = sysContactService.getById(id);
        if(detail==null){
            return ResultVo.failure("数据已删除");
        }
        return ResultVo.success(detail);
    }

    @Override
    public ResultVo editContactDetail(ContactEditIn editIn) {
        SysContact contact=new SysContact();
        BeanUtils.copyProperties(editIn,contact);
        sysContactService.updateById(contact);
        return ResultVo.success();
    }
}
