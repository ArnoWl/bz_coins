package com.admin.service.impl;

import com.admin.entity.SysRole;
import com.admin.entity.SysRoleUser;
import com.admin.entity.SysUser;
import com.admin.model.SysUserVo;
import com.admin.model.in.AddOrUpdateUserIn;
import com.admin.model.in.SysUserListIn;
import com.admin.model.in.UpdateSysUserStatusIn;
import com.admin.model.out.SysUserDetailOut;
import com.admin.model.out.SysUserListOut;
import com.admin.service.ISysRoleService;
import com.admin.service.ISysRoleUserService;
import com.admin.service.ISysUserService;
import com.admin.service.SysUserApiService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.context.SysUserContext;
import com.common.exception.BaseException;
import com.common.utils.ExtBeanUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * 系统用户信息Api实现类
 * @Author: lt
 * @Date: 2020/5/6 14:51
 */
@Service
public class SysUserApiServiceImpl implements SysUserApiService {

    @Resource
    private ISysUserService iSysUserService;
    @Resource
    private ISysRoleService iSysRoleService;
    @Resource
    private ISysRoleUserService iSysRoleUserService;

    @Override
    public ResultVo getUserList(SysUserListIn listIn) {
        try {
            Map<String,Object> params = Maps.newHashMap();
            params.put("userAccount",listIn.getUserAccount());
            params.put("telPhone",listIn.getTelPhone());
            Page<SysUserVo> userPage = new Page<>(listIn.getPageNo(),listIn.getPageSize());
            iSysUserService.getSysUserPageByParams(params,userPage);
            // 组装出参
            Page<SysUserListOut> outPage = new Page<>();
            BeanUtils.copyProperties(userPage,outPage);
            if(!userPage.getRecords().isEmpty()){
                outPage.setRecords(ExtBeanUtils.copyList(userPage.getRecords(), SysUserListOut.class));
            }
            return ResultVo.success(outPage);
        } catch (Exception e) {
            throw new BaseException("获取系统用户列表失败",e);
        }
    }

    @Override
    public ResultVo getUserDetail(Integer userId) {
        try {
            SysUser user = iSysUserService.getById(userId);
            if(user != null){
                SysUserDetailOut detailOut = new SysUserDetailOut();
                BeanUtils.copyProperties(user,detailOut);
                SysRole sysRole = iSysRoleService.getRoleByUserId(user.getId());
                if(sysRole != null){
                    detailOut.setRoleId(sysRole.getId());
                    detailOut.setRoleName(sysRole.getName());
                }
                return ResultVo.success(detailOut);
            }
            return ResultVo.failure("用户信息不存在");
        } catch (Exception e) {
            throw new BaseException("获取系统用户详情失败",e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo addOrUpdateUser(AddOrUpdateUserIn userIn) {
        try {
            SysUser user;
            if(userIn.getId() != null){     // 修改
                user = iSysUserService.getById(userIn.getId());
                if(user != null){
                    BeanUtils.copyProperties(userIn,user);
                    user.setUpdateBy(SysUserContext.getSysUserID());
                    user.setUpdateTime(LocalDateTime.now());
                    iSysUserService.updateById(user);
                }else {
                    return ResultVo.failure("用户ID有误，修改用户信息失败");
                }
            }else{  // 新增
                user = new SysUser();
                BeanUtils.copyProperties(userIn,user);
                user.setCreateBy(SysUserContext.getSysUserID());
                iSysUserService.save(user);
            }
            if(userIn.getRoleId() != null){
                // 移除原绑定角色信息
                QueryWrapper<SysRoleUser> wrapper = new QueryWrapper<>();
                wrapper.eq("user_id",user.getId());
                iSysRoleUserService.remove(wrapper);
                // 添加绑定角色
                SysRoleUser roleUser = new SysRoleUser();
                roleUser.setRoleId(userIn.getRoleId());
                roleUser.setUserId(user.getId());
                iSysRoleUserService.save(roleUser);
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("新增/修改系统用户信息失败",e);
        }
    }

    @Override
    public ResultVo updateUserStatus(UpdateSysUserStatusIn statusIn) {
        try {
            SysUser user = iSysUserService.getById(statusIn.getUserId());
            if(user != null){
                user.setStatus(statusIn.getStatus());
                user.setUpdateTime(LocalDateTime.now());
                user.setUpdateBy(SysUserContext.getSysUserID());
                iSysUserService.updateById(user);
                return ResultVo.success();
            }
            return ResultVo.failure("用户ID有误，修改状态失败");
        } catch (Exception e) {
            throw new BaseException("修改系统用户状态失败",e);
        }
    }

    @Override
    public ResultVo removeUser(Integer userId) {
        try {
            SysUser user = iSysUserService.getById(userId);
            if(user != null){
                iSysUserService.removeById(userId);
                return ResultVo.success();
            }
            return ResultVo.failure("用户ID有误，移除系统用户失败");
        } catch (Exception e) {
            throw new BaseException("移除系统用户失败",e);
        }
    }

}
