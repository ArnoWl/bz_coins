package com.admin.service;

import com.admin.model.in.UserWalletIn;
import com.common.base.ResultVo;

public interface UserService {

    ResultVo handleUserWalletMoney(UserWalletIn walletIn);

    ResultVo queryRewardType();
}
