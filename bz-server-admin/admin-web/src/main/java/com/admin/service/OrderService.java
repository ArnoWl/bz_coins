package com.admin.service;

import com.admin.model.in.OrderRuleEditIn;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;

public interface OrderService {
    ResultVo queryIndemnity(PageParamIn pageParamIn);

    ResultVo indemnityEdit(OrderRuleEditIn editIn);

    ResultVo indemnityEditShow(Integer id);

    ResultVo orderRuleEditStatus(Integer id,Integer status);

    ResultVo orderRuleDel(Integer id);
}
