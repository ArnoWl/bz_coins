package com.admin.service;

import com.admin.model.in.ContactEditIn;
import com.common.base.ResultVo;

public interface ContactService {
    ResultVo getContactInfo();

    ResultVo getContactDetail(Integer id);

    ResultVo editContactDetail(ContactEditIn editIn);
}
