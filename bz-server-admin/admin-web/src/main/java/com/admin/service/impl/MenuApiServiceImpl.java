package com.admin.service.impl;

import com.admin.entity.SysMenu;
import com.admin.entity.SysRole;
import com.admin.entity.SysRoleMenu;
import com.admin.model.in.AddOrUpdateMenuIn;
import com.admin.model.in.MenuListIn;
import com.admin.model.out.MenuDetailOut;
import com.admin.model.out.MenuListOut;
import com.admin.model.out.MenuTreeOut;
import com.admin.service.ISysMenuService;
import com.admin.service.ISysRoleMenuService;
import com.admin.service.ISysRoleService;
import com.admin.service.MenuApiService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.AdminConstants;
import com.common.context.SysUserContext;
import com.common.exception.BaseException;
import com.common.utils.ExtBeanUtils;
import com.common.utils.TreeUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 菜单信息Api实现类
 * @Author: lt
 * @Date: 2020/5/6 10:38
 */
@Service
public class MenuApiServiceImpl implements MenuApiService {

    @Resource
    private ISysMenuService iSysMenuService;
    @Resource
    private ISysRoleService iSysRoleService;
    @Resource
    private ISysRoleMenuService iSysRoleMenuService;

    @Override
    public ResultVo getMenuTreeList(Integer menuType) {
        try {
            Map<String,Object> params = Maps.newHashMap();
            SysRole role = iSysRoleService.getRoleByUserId(SysUserContext.getSysUserID());
            if(!AdminConstants.ADMINISTRATOR.equals(role.getCode())){   // 非超级管理员
                params.put("userId", SysUserContext.getSysUserID());
            }
            if(menuType != null && menuType >= 0){
                params.put("type", menuType);
            }
            List<SysMenu> menuList = iSysMenuService.getMenuListByParams(params);
            if(!CollectionUtils.isEmpty(menuList)){
                List<MenuTreeOut> trees = ExtBeanUtils.copyList(menuList, MenuTreeOut.class);
                return ResultVo.success(TreeUtil.bulid(trees, 0));
            }else {
                return ResultVo.success(TreeUtil.bulid(Lists.newArrayList(), 0));
            }
        } catch (Exception e) {
            throw new BaseException("获取菜单树形列表失败",e);
        }
    }

    @Override
    public ResultVo getMenuList(MenuListIn listIn) {
        try {
            QueryWrapper<SysMenu> wrapper = new QueryWrapper<>();
            if(StringUtils.isNotBlank(listIn.getName())){
                wrapper.like("name",listIn.getName());
            }
            if(StringUtils.isNotBlank(listIn.getPid())){
                wrapper.eq("parent_id",listIn.getPid());
            }
            Page<SysMenu> menuPage = new Page<>(listIn.getPageNo(),listIn.getPageSize());
            iSysMenuService.page(menuPage,wrapper);
            // 组装出参
            Page<MenuListOut> outPage = new Page<>();
            BeanUtils.copyProperties(menuPage,outPage);
            if(!menuPage.getRecords().isEmpty()){
                outPage.setRecords(ExtBeanUtils.copyList(menuPage.getRecords(), MenuListOut.class));
            }
            return ResultVo.success(outPage);
        } catch (Exception e) {
            throw new BaseException("获取菜单列表失败",e);
        }
    }

    @Override
    public ResultVo getMenuDetail(Integer menuId) {
        try {
            SysMenu menu = iSysMenuService.getById(menuId);
            if(menu != null){
                MenuDetailOut detailOut = new MenuDetailOut();
                BeanUtils.copyProperties(menu,detailOut);
                return ResultVo.success(detailOut);
            }
            return ResultVo.failure("菜单信息不存在");
        } catch (Exception e) {
            throw new BaseException("获取菜单详情失败",e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultVo addOrUpdateMenu(AddOrUpdateMenuIn menuIn) {
        try {
            SysMenu menu;
            if(menuIn.getId() != null){     // 修改
                menu = iSysMenuService.getById(menuIn.getId());
                if(menu != null){
                    BeanUtils.copyProperties(menuIn,menu);
                    menu.setUpdateBy(SysUserContext.getSysUserID());
                    menu.setUpdateTime(LocalDateTime.now());
                    iSysMenuService.updateById(menu);
                }else {
                    ResultVo.failure("菜单ID有误，修改菜单信息失败");
                }
            }else{  // 新增
                menu = new SysMenu();
                BeanUtils.copyProperties(menuIn,menu);
                menu.setCreateBy(SysUserContext.getSysUserID());
                iSysMenuService.save(menu);
                // 为超级管理员角色添加该菜单权限
                List<SysRole> roles = iSysRoleService.getAdministratorRoles();
                if(!CollectionUtils.isEmpty(roles)){
                    roles.forEach(r->{
                        SysRoleMenu roleMenu = new SysRoleMenu();
                        roleMenu.setRoleId(r.getId());
                        roleMenu.setMenuId(menu.getId());
                        iSysRoleMenuService.save(roleMenu);
                    });
                }
            }
            return ResultVo.success();
        } catch (Exception e) {
            throw new BaseException("新增/修改菜单信息失败",e);
        }
    }

    @Override
    public ResultVo removeMenu(Integer menuId) {
        try {
            SysMenu menu = iSysMenuService.getById(menuId);
            if(menu != null){
                iSysMenuService.removeById(menuId);
                return ResultVo.success();
            }
            return ResultVo.failure("菜单ID有误，移除菜单失败");
        } catch (Exception e) {
            throw new BaseException("移除菜单失败",e);
        }
    }
}
