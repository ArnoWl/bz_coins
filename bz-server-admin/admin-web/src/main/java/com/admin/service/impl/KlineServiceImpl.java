package com.admin.service.impl;

import com.admin.entity.SysCoinKline;
import com.admin.entity.SysConfig;
import com.admin.model.in.CoinKlineEditIn;
import com.admin.model.in.KlineQueryIn;
import com.admin.service.ISysCoinKlineService;
import com.admin.service.KlineService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.constatns.CommonConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.utils.lang.StringUtils;
import com.mall.feign.market.MarketClient;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class KlineServiceImpl implements KlineService {

    @Resource
    private ISysCoinKlineService sysCoinKlineService;
    @Resource
    private MarketClient marketClient;
    @Resource
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public ResultVo queryKline(KlineQueryIn queryIn) {
        QueryWrapper<SysCoinKline> queryWrapper=new QueryWrapper<>();
        if(queryIn.getContractTransFlag() != null){
            queryWrapper.eq("contract_trans_flag",queryIn.getContractTransFlag());
        }
        if(queryIn.getType() != null){
            queryWrapper.eq("type",queryIn.getType());
        }
        if(queryIn.getStatus() != null){
            queryWrapper.eq("status",queryIn.getStatus());
        }
        if(StringUtils.isNotBlank(queryIn.getName())){
            queryWrapper.like("name",queryIn.getName());
        }
        Page<SysCoinKline> page=new Page<>(queryIn.getPageNo(),queryIn.getPageSize());
        sysCoinKlineService.page(page,queryWrapper);
        return ResultVo.success(page);
    }

    @Override
    public ResultVo klineEdit(CoinKlineEditIn editIn) {
        SysCoinKline coinKline=new SysCoinKline();
        BeanUtils.copyProperties(editIn,coinKline);
        sysCoinKlineService.updateById(coinKline);

        QueryWrapper<SysCoinKline> wrapper = new QueryWrapper<>();
        wrapper.eq("status", CommonConstants.DEL_FLAG_0);
        List<SysCoinKline> coinKlineList = sysCoinKlineService.list(wrapper);
        //缓存
        redisTemplate.opsForValue().set(RedisKeyConstants.EXCHANGE_KLINE_WS_CONFIG
                , JSON.toJSONString(coinKlineList));
        //刷新
        marketClient.refreshTopics();
        return ResultVo.success();
    }
}
