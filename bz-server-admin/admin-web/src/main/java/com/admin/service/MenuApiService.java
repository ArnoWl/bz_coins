package com.admin.service;

import com.admin.model.in.AddOrUpdateMenuIn;
import com.admin.model.in.MenuListIn;
import com.common.base.ResultVo;

/**
 * 菜单信息Api接口类
 * @Author: lt
 * @Date: 2020/5/6 10:36
 */
public interface MenuApiService {

    /**
     * 获取菜单树形列表
     * @return
     */
    ResultVo getMenuTreeList(Integer menuType);

    /**
     * 获取菜单列表
     * @return
     */
    ResultVo getMenuList(MenuListIn listIn);

    /**
     * 获取菜单详情
     * @return
     */
    ResultVo getMenuDetail(Integer menuId);

    /**
     * 新增/修改菜单信息
     * @param menuIn
     * @return
     */
    ResultVo addOrUpdateMenu(AddOrUpdateMenuIn menuIn);

    /**
     * 移除菜单
     * @param menuId
     * @return
     */
    ResultVo removeMenu(Integer menuId);

}
