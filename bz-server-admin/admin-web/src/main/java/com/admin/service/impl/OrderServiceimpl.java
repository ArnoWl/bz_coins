package com.admin.service.impl;

import com.admin.entity.SysOrderRule;
import com.admin.model.in.OrderRuleEditIn;
import com.admin.model.in.OrderRuleOut;
import com.admin.service.ISysOrderRuleService;
import com.admin.service.OrderService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceimpl implements OrderService {

    @Autowired
    private ISysOrderRuleService sysOrderRuleService;

    @Override
    public ResultVo queryIndemnity(PageParamIn pageParamIn) {
        IPage<SysOrderRule> pageInfo=new Page<>(pageParamIn.getPageNo(),pageParamIn.getPageSize());
        sysOrderRuleService.page(pageInfo,null);
        return ResultVo.success(pageInfo);
    }

    @Override
    public ResultVo indemnityEdit(OrderRuleEditIn editIn) {
        SysOrderRule sysOrderRule=new SysOrderRule();
        BeanUtils.copyProperties(editIn,sysOrderRule);
        sysOrderRuleService.saveOrUpdate(sysOrderRule);
        return ResultVo.success();
    }

    @Override
    public ResultVo indemnityEditShow(Integer id) {
        SysOrderRule orderRule = sysOrderRuleService.getById(id);
        if(orderRule == null){
            return ResultVo.failure("数据已删除");
        }
        OrderRuleOut orderRuleOut=new OrderRuleOut();
        BeanUtils.copyProperties(orderRule,orderRuleOut);
        return ResultVo.success(orderRuleOut);
    }

    @Override
    public ResultVo orderRuleEditStatus(Integer id,Integer status) {
        SysOrderRule sysOrderRule = sysOrderRuleService.getById(id);
        if(sysOrderRule == null){
            return ResultVo.failure("数据已删除");
        }
        sysOrderRule.setStatus(status);
        sysOrderRuleService.updateById(sysOrderRule);
        return ResultVo.success();
    }

    @Override
    public ResultVo orderRuleDel(Integer id) {
        sysOrderRuleService.removeById(id);
        return ResultVo.success();
    }
}
