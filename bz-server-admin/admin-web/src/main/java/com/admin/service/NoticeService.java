package com.admin.service;


import com.admin.model.in.NoticeEditIn;
import com.admin.model.in.NoticeListIn;
import com.common.base.ResultVo;

public interface NoticeService {
    ResultVo queryNotice(NoticeListIn listIn);

    ResultVo editNotice(NoticeEditIn editIn);

    ResultVo editNoticeShow(Integer id);

    ResultVo delNotice(Integer id);
}
