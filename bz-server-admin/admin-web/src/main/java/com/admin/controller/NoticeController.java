package com.admin.controller;

import com.admin.entity.SysNoticeInfo;
import com.admin.model.in.BannerListIn;
import com.admin.model.in.NoticeEditIn;
import com.admin.model.in.NoticeListIn;
import com.admin.model.out.BannerOut;
import com.admin.service.BannerService;
import com.admin.service.NoticeService;
import com.common.base.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  系统公告业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "系统公告业务模块")
@RestController
@RequestMapping(value = "notice")
@Slf4j
public class NoticeController {

    @Resource
    private NoticeService noticeService;

    @RequiresPermissions("sys:notice:list")
    @ApiOperation(value = "系统公告列表",response = SysNoticeInfo.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryBanner(@RequestBody NoticeListIn listIn){
        return noticeService.queryNotice(listIn);
    }

    @RequiresPermissions("sys:notice:edit")
    @ApiOperation(value = "添加/编辑公告")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editNotice(@RequestBody NoticeEditIn editIn){
        return noticeService.editNotice(editIn);
    }

    @ApiOperation(value = "编辑公告回显",response = SysNoticeInfo.class)
    @RequestMapping(value = "editShow", method = RequestMethod.GET)
    public ResultVo editNoticeShow(@RequestParam("id") Integer id){
        return noticeService.editNoticeShow(id);
    }

    @RequiresPermissions("sys:notice:del")
    @ApiOperation(value = "删除公告")
    @RequestMapping(value = "del", method = RequestMethod.GET)
    public ResultVo delNotice(@RequestParam("id") Integer id){
        return noticeService.delNotice(id);
    }
}
