package com.admin.controller;

import com.admin.entity.SysOrderRule;
import com.admin.model.in.OrderRuleEditIn;
import com.admin.model.in.OrderRuleOut;
import com.admin.service.OrderService;
import com.admin.service.ParamService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.feign.user.OrderInfoClient;
import com.mall.feign.user.TransactionClient;
import com.mall.model.admin.vo.SysConfigVo;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import com.mall.model.user.dto.BBOrderDetailsDto;
import com.mall.model.user.dto.BBOrderDto;
import com.mall.model.user.vo.BBOrderDetailPlatQueryVo;
import com.mall.model.user.vo.BBOrderPlatQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  订单业务模块
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "订单业务模块")
@RestController
@RequestMapping(value = "order")
@Slf4j
public class OrderController {

    @Resource
    private OrderService orderService;
    @Resource
    private OrderInfoClient orderInfoClient;
    @Resource
    private TransactionClient transactionClient;

    @RequiresPermissions("order:rule:list")
    @ApiOperation(value = "包赔场设置",response = SysOrderRule.class)
    @RequestMapping(value = "orderRuleList", method = RequestMethod.POST)
    public ResultVo queryIndemnity(@RequestBody PageParamIn pageParamIn){
        return orderService.queryIndemnity(pageParamIn);
    }

    @RequiresPermissions("order:rule:edit")
    @ApiOperation(value = "新增/编辑包赔场")
    @RequestMapping(value = "orderRuleEdit", method = RequestMethod.POST)
    public ResultVo indemnityEdit(@RequestBody OrderRuleEditIn editIn){
        return orderService.indemnityEdit(editIn);
    }

    @ApiOperation(value = "编辑包赔场回显",response = OrderRuleOut.class)
    @RequestMapping(value = "orderRuleEditShow", method = RequestMethod.GET)
    public ResultVo indemnityEditShow(@RequestParam("id") Integer id){
        return orderService.indemnityEditShow(id);
    }

    @RequiresPermissions("order:rule:status")
    @ApiOperation(value = "开启/关闭")
    @RequestMapping(value = "orderRuleEditStatus", method = RequestMethod.GET)
    public ResultVo orderRuleEditStatus(@RequestParam("id") Integer id,@RequestParam("status") Integer status){
        return orderService.orderRuleEditStatus(id,status);
    }

    @RequiresPermissions("order:rule:del")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "orderRuleDel", method = RequestMethod.GET)
    public ResultVo orderRuleDel(@RequestParam("id") Integer id){
        return orderService.orderRuleDel(id);
    }

    @RequiresPermissions("order:info:list")
    @ApiOperation(value = "订单列表",response =BuyOrderListVo.class )
    @RequestMapping(value = "orderList", method = RequestMethod.POST)
    public ResultVo getOrderListPage(@RequestBody GetBuyOrderListDto dto){
        Page<BuyOrderListVo> orderListPage = orderInfoClient.getOrderListPage(dto);
        return ResultVo.success(orderListPage);
    }

    @RequiresPermissions("order:buy:list")
    @ApiOperation(value = "币币交易我要买列表",response = BBOrderPlatQueryVo.class)
    @RequestMapping(value = "queryBBBuyOrder", method = RequestMethod.POST)
    public ResultVo queryBBBuyOrder(@RequestBody BBOrderDto dto){
        return transactionClient.queryBBBuyOrder(dto);
    }

    @RequiresPermissions("order:sale:list")
    @ApiOperation(value = "币币交易我要卖列表",response = BBOrderPlatQueryVo.class)
    @RequestMapping(value = "queryBBSaleOrder", method = RequestMethod.POST)
    public ResultVo queryBBSaleOrder(@RequestBody BBOrderDto dto){
        return transactionClient.queryBBSaleOrder(dto);
    }

    @RequiresPermissions("order:sale:detail")
    @ApiOperation(value = "币币交易我要买列表详情",response = BBOrderDetailPlatQueryVo.class)
    @RequestMapping(value = "queryBBBuyOrderDetails", method = RequestMethod.POST)
    public ResultVo queryBBBuyOrderDetails(@RequestBody BBOrderDetailsDto dto){
        return transactionClient.queryBBBuyOrderDetails(dto);
    }

    @RequiresPermissions("order:sale:detail")
    @ApiOperation(value = "币币交易我要卖列表详情",response = BBOrderDetailPlatQueryVo.class)
    @RequestMapping(value = "queryBBSaleOrderDetails", method = RequestMethod.POST)
    public ResultVo queryBBSaleOrderDetails(@RequestBody BBOrderDetailsDto dto){
        return transactionClient.queryBBSaleOrderDetails(dto);
    }

}
