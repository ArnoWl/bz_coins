package com.admin.controller;

import com.admin.model.in.SysRuleEditIn;
import com.admin.model.out.SysRuleDetailOut;
import com.admin.model.out.SysRuleOut;
import com.admin.service.RuleService;
import com.common.base.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  系统规则业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "系统规则业务模块")
@RestController
@RequestMapping(value = "rule")
@Slf4j
public class RuleController {

    @Autowired
    private RuleService ruleService;

    @RequiresPermissions("sys:rule:list")
    @ApiOperation(value = "规则列表",response = SysRuleOut.class)
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResultVo queryRule(){
        return ruleService.queryRule();
    }

    @ApiOperation(value = "编辑规则回显",response = SysRuleDetailOut.class)
    @RequestMapping(value = "editShow", method = RequestMethod.GET)
    public ResultVo editShow(@RequestParam("id") Integer id){
        return ruleService.editShow(id);
    }

    @RequiresPermissions("sys:rule:edit")
    @ApiOperation(value = "编辑规则")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editRule(@RequestBody SysRuleEditIn editIn){
        return ruleService.editRule(editIn);
    }
}
