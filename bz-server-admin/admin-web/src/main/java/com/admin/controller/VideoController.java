package com.admin.controller;


import com.common.exception.BaseException;
import com.mall.feign.video.VideoClient;
import com.mall.model.video.dto.VideoExamineDto;
import com.common.base.ResultVo;
import com.mall.model.video.dto.VideoPlatQueryDto;
import com.mall.model.video.dto.VideoUpdateDto;
import com.mall.model.video.vo.VideoPlatQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  视频业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "视频业务模块")
@RestController
@RequestMapping(value = "video")
@Slf4j
public class VideoController {

    @Resource
    private VideoClient videoClient;

    @RequiresPermissions("sys:video:list")
    @ApiOperation(value = "视频列表",response = VideoPlatQueryVo.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryVideo(@RequestBody VideoPlatQueryDto dto){
        return videoClient.queryPlatVideo(dto);
    }

    @RequiresPermissions("sys:video:examine")
    @ApiOperation(value = "审核")
    @RequestMapping(value = "examine", method = RequestMethod.POST)
    public ResultVo handleVideoExamine(@RequestBody VideoExamineDto examineDto){
        return videoClient.handleExamine(examineDto);
    }

    @RequiresPermissions("sys:video:update")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResultVo updateVideo(@RequestBody VideoUpdateDto updateDto){
        return videoClient.updateVideo(updateDto);
    }

    @RequiresPermissions("sys:video:del")
    @ApiOperation(value = "视频删除")
    @RequestMapping(value = "del", method = RequestMethod.GET)
    public ResultVo delVideo(@RequestParam("id")Integer id){
        return videoClient.delVideo(id);
    }
}
