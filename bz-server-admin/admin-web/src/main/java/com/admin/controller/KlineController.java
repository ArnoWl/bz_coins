package com.admin.controller;

import com.admin.entity.SysCoinKline;
import com.admin.entity.SysNoticeInfo;
import com.admin.model.in.CoinKlineEditIn;
import com.admin.model.in.KlineQueryIn;
import com.admin.model.in.NoticeListIn;
import com.admin.service.KlineService;
import com.common.base.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@CrossOrigin
@Api(description = "K线图基础表")
@RestController
@RequestMapping(value = "kline")
@Slf4j
public class KlineController {
    @Resource
    private KlineService klineService;

    @RequiresPermissions("sys:kline:list")
    @ApiOperation(value = "K线图基础列表",response = SysCoinKline.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryKline(@RequestBody KlineQueryIn queryIn){
        return klineService.queryKline(queryIn);
    }

    @RequiresPermissions("sys:kline:edit")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo klineEdit(@RequestBody CoinKlineEditIn editIn){
        return klineService.klineEdit(editIn);
    }
}
