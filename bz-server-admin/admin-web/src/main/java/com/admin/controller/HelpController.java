package com.admin.controller;

import com.admin.entity.SysHelpCenter;
import com.admin.model.in.HelpEditIn;
import com.admin.model.out.CoinsOut;
import com.admin.service.HelpService;
import com.admin.service.ISysHelpCenterService;
import com.auth.client.annotation.IgnoreUserToken;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 帮助中心
 * @Author lt
 * @Date 2020/5/3 14:40
 */
@CrossOrigin
@Api(description = "系统管理--帮助中心")
@RestController
@RequestMapping(value = "help")
@IgnoreUserToken
public class HelpController {

    @Resource
    private HelpService helpService;

    @Resource
    private ISysHelpCenterService sysHelpCenterService;

    @RequiresPermissions("sys:help:list")
    @ApiOperation(value = "帮助中心列表",response = SysHelpCenter.class)
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResultVo queryHelp(){
        return sysHelpCenterService.queryHelp();
    }

    @RequiresPermissions("sys:help:edit")
    @ApiOperation(value = "添加/编辑帮助中心")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editHelp(@RequestBody HelpEditIn editIn){
        return helpService.editHelp(editIn);
    }

    @ApiOperation(value = "编辑帮助中心回显")
    @RequestMapping(value = "editShow", method = RequestMethod.GET)
    public ResultVo editShow(@RequestParam("id") Integer id,@RequestParam("level")  Integer level){
        return helpService.editShow(id,level);
    }

    @RequiresPermissions("sys:help:del")
    @ApiOperation(value = "删除帮助中心")
    @RequestMapping(value = "del", method = RequestMethod.GET)
    public ResultVo delHelp(@RequestParam("id") Integer id,@RequestParam("level") Integer level){
        return helpService.delHelp(id,level);
    }

}
