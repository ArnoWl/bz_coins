package com.admin.controller;

import com.admin.entity.SysIndemnityRule;
import com.admin.entity.SysOrderRule;
import com.admin.service.ISysIndemnityRuleService;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  系统公告业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "包赔配置规则表")
@RestController
@RequestMapping(value = "indemnity")
@Slf4j
public class IndemnityController {

    @Autowired
    private ISysIndemnityRuleService sysIndemnityRuleService;

    @RequiresPermissions("indemnity:rule:list")
    @ApiOperation(value = "包赔场规则列表",response = SysIndemnityRule.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryIndemnity(@RequestBody PageParamIn pageParamIn){
        return sysIndemnityRuleService.queryIndemnity(pageParamIn);
    }

    @ApiOperation(value = "编辑回显",response =SysIndemnityRule.class)
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResultVo editIndemnityShow(@RequestParam Integer id){
        return sysIndemnityRuleService.editIndemnityShow(id);
    }

    @RequiresPermissions("indemnity:rule:edit")
    @ApiOperation(value = "添加/编辑")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editIndemnity(@RequestBody SysIndemnityRule  rule){
        return sysIndemnityRuleService.editIndemnity(rule);
    }

    @RequiresPermissions("indemnity:rule:del")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "del", method = RequestMethod.GET)
    public ResultVo delIndemnity(@RequestParam Integer id){
        return sysIndemnityRuleService.delIndemnity(id);
    }
}
