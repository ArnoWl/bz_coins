package com.admin.controller.sys;

import com.admin.model.in.AddOrUpdateMenuIn;
import com.admin.model.in.MenuListIn;
import com.admin.model.out.MenuDetailOut;
import com.admin.model.out.MenuListOut;
import com.admin.model.out.MenuTreeOut;
import com.admin.service.MenuApiService;
import com.common.base.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 系统菜单信息控制器
 * @Author lt
 * @Date 2020/5/3 14:40
 */
@CrossOrigin
@Api(description = "系统管理--菜单管理")
@RestController
@RequestMapping(value = "menu")
public class MenuController {

    @Resource
    private MenuApiService menuApiService;

    @ApiOperation(value = "菜单信息树形列表",response = MenuTreeOut.class)
    @RequestMapping(value = "treeList/{menuType}", method = RequestMethod.GET)
    public ResultVo getMenuTreeList(@PathVariable Integer menuType) {
        return menuApiService.getMenuTreeList(menuType);
    }

    @RequiresPermissions("sys:menu:list")
    @ApiOperation(value = "菜单信息列表",response = MenuListOut.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo getMenuList(@RequestBody MenuListIn listIn) {
        return menuApiService.getMenuList(listIn);
    }

    @RequiresPermissions("sys:menu:detail")
    @ApiOperation(value = "菜单信息详情",response = MenuDetailOut.class)
    @RequestMapping(value = "detail/{menuId}", method = RequestMethod.GET)
    public ResultVo getMenuDetail(@PathVariable("menuId") Integer menuId) {
        return menuApiService.getMenuDetail(menuId);
    }

    @RequiresPermissions("sys:menu:edit")
    @ApiOperation(value = "添加/修改菜单")
    @RequestMapping(value = "addOrUpdate", method = RequestMethod.POST)
    public ResultVo addOrUpdateMenu(@RequestBody AddOrUpdateMenuIn menuIn) {
        return menuApiService.addOrUpdateMenu(menuIn);
    }

    @RequiresPermissions("sys:menu:del")
    @ApiOperation(value = "删除菜单")
    @RequestMapping(value = "remove/{menuId}", method = RequestMethod.GET)
    public ResultVo removeMenu(@PathVariable("menuId") Integer menuId) {
        return menuApiService.removeMenu(menuId);
    }

}
