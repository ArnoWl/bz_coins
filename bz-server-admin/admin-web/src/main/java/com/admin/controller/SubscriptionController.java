package com.admin.controller;

import com.admin.entity.SysSubscription;
import com.admin.model.in.SubscriptionEditIn;
import com.admin.model.in.SubscriptionListIn;
import com.admin.model.out.SubscriptionOut;
import com.admin.service.SubscriptionService;
import com.common.base.ResultVo;
import com.mall.feign.user.TransactionClient;
import com.mall.model.user.dto.SubscriptionDetailDto;
import com.mall.model.user.dto.UserInfoQueryDto;
import com.mall.model.user.vo.SubscriptionDetailVo;
import com.mall.model.user.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  BZW认购数量业务模块
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "BZW认购数量业务模块")
@RestController
@RequestMapping(value = "subscription")
@Slf4j
public class SubscriptionController {
    @Resource
    private SubscriptionService subscriptionService;
    @Resource
    private TransactionClient transactionClient;

    @RequiresPermissions("sys:subscription:list")
    @ApiOperation(value = "认购列表",response = SysSubscription.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo querySubscription(@RequestBody SubscriptionListIn listIn){
        return subscriptionService.querySubscription(listIn);
    }

    @RequiresPermissions("sys:subscription:detail")
    @ApiOperation(value = "认购明细",response = SubscriptionDetailVo.class)
    @RequestMapping(value = "detail", method = RequestMethod.POST)
    public ResultVo querySubscriptionDetail(@RequestBody SubscriptionDetailDto detailIn){
        return transactionClient.queryUserSubLogs(detailIn);
    }

    @RequiresPermissions("sys:subscription:edit")
    @ApiOperation(value = "添加/编辑")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editSubscription(@RequestBody SubscriptionEditIn editIn){
        return subscriptionService.editSubscription(editIn);
    }

    @ApiOperation(value = "编辑回显",response = SubscriptionOut.class)
    @RequestMapping(value = "editShow", method = RequestMethod.GET)
    public ResultVo editSubscriptionShow(@RequestParam("id") Integer id){
        return subscriptionService.editSubscriptionShow(id);
    }

    @RequiresPermissions("sys:subscription:del")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "del", method = RequestMethod.GET)
    public ResultVo delSubscription(@RequestParam("id") Integer id){
        return subscriptionService.delSubscription(id);
    }
}
