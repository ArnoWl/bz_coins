package com.admin.controller.sys;

import com.admin.model.in.AddOrUpdateRoleIn;
import com.admin.model.out.RoleDetailOut;
import com.admin.model.out.RoleListOut;
import com.admin.service.RoleApiService;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 系統角色管理控制器
 * @Author lt
 * @Date 2020/5/3 14:40
 */
@CrossOrigin
@Api(description = "系统管理--角色管理")
@RestController
@RequestMapping(value = "role")
public class RoleController {

    @Resource
    private RoleApiService roleApiService;

    @RequiresPermissions("sys:role:list")
    @ApiOperation(value = "角色信息列表",response = RoleListOut.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo getRoleList(@RequestBody PageParamIn paramIn) {
        return roleApiService.getRoleList(paramIn);
    }

    @RequiresPermissions("sys:role:detail")
    @ApiOperation(value = "角色信息详情",response = RoleDetailOut.class)
    @RequestMapping(value = "detail/{roleId}", method = RequestMethod.GET)
    public ResultVo getRoleDetail(@PathVariable("roleId") Integer roleId) {
        return roleApiService.getRoleDetail(roleId);
    }

    @RequiresPermissions("sys:role:edit")
    @ApiOperation(value = "添加/修改角色")
    @RequestMapping(value = "addOrUpdate", method = RequestMethod.POST)
    public ResultVo addOrUpdateRole(@RequestBody AddOrUpdateRoleIn roleIn) {
        return roleApiService.addOrUpdateRole(roleIn);
    }

    @RequiresPermissions("sys:role:del")
    @ApiOperation(value = "删除角色")
    @RequestMapping(value = "remove/{roleId}", method = RequestMethod.GET)
    public ResultVo removeRole(@PathVariable("roleId") Integer roleId) {
        return roleApiService.removeRole(roleId);
    }

}
