package com.admin.controller;

import com.admin.entity.SysOrderRule;
import com.admin.entity.SysStatis;
import com.admin.model.StatisVo;
import com.admin.model.in.StatisQueryIn;
import com.admin.service.ISysOrderRuleService;
import com.admin.service.ISysStatisService;
import com.admin.service.StatisService;
import com.common.base.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;

/**
 * <p>
 *  统计业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "统计业务模块")
@RestController
@RequestMapping(value = "statis")
@Slf4j
public class StatisController {

    @Resource
    private StatisService statisService;

    @RequiresPermissions("sys:statis:list")
    @ApiOperation(value = "统计数据列表",response = SysStatis.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryList(@RequestBody StatisQueryIn queryIn){
        return statisService.queryList(queryIn);
    }

}
