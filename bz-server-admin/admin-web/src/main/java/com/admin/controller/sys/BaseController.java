package com.admin.controller.sys;

import com.admin.entity.SysLog;
import com.admin.model.UserInfoVo;
import com.admin.service.ISysLogService;
import com.admin.service.ISysUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.fileUpload.QiNiuQunClient;
import com.mall.base.PageParamIn;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * <p>
 *  基础业务控制器
 * </p>
 *
 * @author zhangq
 * @since 2020/3/24
 */
@CrossOrigin
@Api(description = "基础业务模块")
@RestController
@RequestMapping(value = "base")
@Slf4j
public class BaseController {

    @Resource
    private ISysLogService iSysLogService;
    @Resource
    private ISysUserService iSysUserService;

    @ApiOperation(value = "上传图片")
    @RequestMapping(value = "uploadImg", method = RequestMethod.POST)
    public ResultVo sendSmsCode(@RequestParam("file") MultipartFile file){
        try {
            return ResultVo.success(QiNiuQunClient.uploadFile(file));
        } catch (Exception e) {
            log.error("上传图片失败",e);
            return ResultVo.failure("上传图片失败");
        }
    }

    @ApiOperation(value = "系统日志列表",response = SysLog.class)
    @RequestMapping(value = "logList", method = RequestMethod.POST)
    public ResultVo getSysLogList(@RequestBody PageParamIn paramIn){
        QueryWrapper<SysLog> wrapper = new QueryWrapper<>();
        Page<SysLog> logPage = new Page<>(paramIn.getPageNo(),paramIn.getPageSize());
        wrapper.orderByDesc("create_time");
        iSysLogService.page(logPage);
        return ResultVo.success(logPage);
    }

    @ApiOperation(value = "获取登陆用户信息",response = UserInfoVo.class)
    @RequestMapping(value = "getUserInfo", method = RequestMethod.GET)
    public ResultVo getUserInfo(){
        return iSysUserService.getUserInfo();
    }

}
