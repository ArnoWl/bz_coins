package com.admin.controller;


import com.admin.model.in.UserWalletIn;
import com.admin.service.UserService;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.feign.user.UserInfoClient;
import com.mall.model.user.dto.*;
import com.mall.model.user.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  用户信息业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "用户信息业务模块")
@RestController
@RequestMapping(value = "userInfo")
@Slf4j
public class UserInfoController {

    @Resource
    private UserInfoClient userInfoClient;
    @Resource
    private UserService userService;

    @RequiresPermissions("user:info:list")
    @ApiOperation(value = "用户列表",response = UserVo.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryUser(@RequestBody UserInfoQueryDto dto){
        return userInfoClient.queryUserInfo(dto);
    }



    @RequiresPermissions("user:info:team")
    @ApiOperation(value = "查询会员团队",response = TeamOrderVo.class)
    @RequestMapping(value = "team", method = RequestMethod.POST)
    public ResultVo queryUserTeam(@RequestBody UserTeamDto userTeamDto){
        return userInfoClient.queryUserTeam(userTeamDto);
    }


    @RequiresPermissions("user:info:team")
    @ApiOperation(value = "查询会员团队业绩",response = TeamPerformanceVo.class)
    @RequestMapping(value = "getMyTeamPerformance", method = RequestMethod.GET)
    public ResultVo getMyTeamPerformance(@RequestParam("uid")String uid){
        return userInfoClient.getMyTeamPerformance(uid);
    }

    @RequiresPermissions("user:info:uplog")
    @ApiOperation(value = "会员升级记录",response = UserUpLogVo.class)
    @RequestMapping(value = "queryUserUpLog", method = RequestMethod.POST)
    public ResultVo queryUserUpLog(@RequestBody UserUpLogDto dto){
        return userInfoClient.queryUserUpLog(dto);
    }

    @RequiresPermissions("user:info:team")
    @ApiOperation(value = "查询会员团队各等级人数")
    @RequestMapping(value = "getTeamLevel", method = RequestMethod.GET)
    public ResultVo getTeamLevel(@RequestParam("id")Integer id){
        return userInfoClient.getTeamLevel(id);
    }

    @RequiresPermissions("user:info:team")
    @ApiOperation(value = "查询会员各级别会员",response = UserVo.class)
    @RequestMapping(value = "queryTeamLevel", method = RequestMethod.GET)
    public ResultVo queryTeamLevel(@RequestParam("id")Integer id,@RequestParam("levelId")Integer levelId){
        return userInfoClient.queryTeamLevel(id,levelId);
    }

    @RequiresPermissions("user:info:handleMoney")
    @ApiOperation(value = "操作用户钱包金额")
    @RequestMapping(value = "handleUserWalletMoney", method = RequestMethod.POST)
    public ResultVo handleUserWalletMoney(@RequestBody UserWalletIn walletIn){
        return userService.handleUserWalletMoney(walletIn);
    }

    @RequiresPermissions("user:reward:list")
    @ApiOperation(value = "奖励明细",response = UserRewardVo.class)
    @RequestMapping(value = "reward", method = RequestMethod.POST)
    public ResultVo queryReward(@RequestBody UserRewardDto rewardDto){
        return userInfoClient.queryReward(rewardDto);
    }

    @ApiOperation(value = "获取奖励类型")
    @RequestMapping(value = "rewardType", method = RequestMethod.GET)
    public ResultVo queryRewardType(){
        return userService.queryRewardType();
    }

    @RequiresPermissions("user:info:frozenMoney")
    @ApiOperation(value = "冻结金额明细",response = UserWalletFrozenLogQueryVo.class)
    @RequestMapping(value = "frozenMoney", method = RequestMethod.POST)
    public ResultVo queryUserWalletFrozenLog(@RequestBody UserWalletFrozenLogQueryDto dto){
        return userInfoClient.queryUserWalletFrozenLog(dto);
    }

    @RequiresPermissions("user:info:edit")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo updateUserInfo(@RequestBody UpdateUserDto dto){
        return userInfoClient.updateUserInfo(dto);
    }

    @RequiresPermissions("user:wallet:list")
    @ApiOperation(value = "用户钱包列表",response = UserWalletQueryVo.class)
    @RequestMapping(value = "walletList", method = RequestMethod.POST)
    public ResultVo queryUserWallet(@RequestBody UserWalletQueryDto dto){
        return userInfoClient.queryUserWallet(dto);
    }

    @RequiresPermissions("user:wallet:log")
    @ApiOperation(value = "用户钱包记录",response = UserWalletLogQueryVo.class)
    @RequestMapping(value = "walletLogList", method = RequestMethod.POST)
    public ResultVo queryUserWalletLog(@RequestBody UserWalletLogQueryDto dto){
        return userInfoClient.queryUserWalletLog(dto);
    }

    @RequiresPermissions("user:level:list")
    @ApiOperation(value = "会员等级列表",response = UserLevelQueryVo.class)
    @RequestMapping(value = "userLevelList", method = RequestMethod.GET)
    public ResultVo queryUserLevel(){
        return userInfoClient.queryUserLevel();
    }

    @RequiresPermissions("user:level:edit")
    @ApiOperation(value = "编辑会员等级")
    @RequestMapping(value = "updateUserLevel", method = RequestMethod.POST)
    public ResultVo updateUserLevel(@RequestBody UserLevelDto dto){
        return userInfoClient.updateUserLevel(dto);
    }

    @ApiOperation(value = "编辑会员等级回显",response = UserLevelVo.class)
    @RequestMapping(value = "getUserLevel", method = RequestMethod.GET)
    public ResultVo getUserLevel(@RequestParam("id") Integer id){
        return userInfoClient.getUserLevel(id);
    }

    @RequiresPermissions("user:message:list")
    @ApiOperation(value = "用户站内信",response = UserMessageQueryVo.class)
    @RequestMapping(value = "queryUserMessage", method = RequestMethod.POST)
    public ResultVo queryUserMessage(@RequestBody UserMessageQueryDto queryDto){
        return userInfoClient.queryUserMessage(queryDto);
    }

    @RequiresPermissions("user:message:reply")
    @ApiOperation(value = "回复")
    @RequestMapping(value = "handleUserMessageReply", method = RequestMethod.POST)
    public ResultVo handleUserMessageReply(@RequestBody UserMessageReplyDto replyDto){
        return userInfoClient.handleUserMessageReply(replyDto);
    }

    @RequiresPermissions("user:message:content")
    @ApiOperation(value = "查看内容",response = MessageReplyVo.class)
    @RequestMapping(value = "getMessageContent", method = RequestMethod.GET)
    public ResultVo getMessageContent(@RequestParam("id")Integer id){
        return userInfoClient.getMessageContent(id);
    }
}
