package com.admin.controller.sys;

import com.admin.model.in.AddOrUpdateUserIn;
import com.admin.model.in.SysUserListIn;
import com.admin.model.in.UpdateSysUserStatusIn;
import com.admin.model.out.SysUserDetailOut;
import com.admin.model.out.SysUserListOut;
import com.admin.service.SysUserApiService;
import com.common.base.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 系统平台用户信息控制器
 * @Author lt
 * @Date 2020/5/3 14:40
 */
@CrossOrigin
@Api(description = "系统管理--用戶管理")
@RestController
@RequestMapping(value = "user")
public class UserController {

    @Resource
    private SysUserApiService sysUserApiService;

    @RequiresPermissions("sys:user:list")
    @ApiOperation(value = "用户信息列表",response = SysUserListOut.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo getUserList(@RequestBody SysUserListIn listIn) {
        return sysUserApiService.getUserList(listIn);
    }

    @RequiresPermissions("sys:user:detail")
    @ApiOperation(value = "用户信息详情",response = SysUserDetailOut.class)
    @RequestMapping(value = "detail/{userId}", method = RequestMethod.GET)
    public ResultVo getUserDetail(@PathVariable("userId") Integer userId) {
        return sysUserApiService.getUserDetail(userId);
    }

    @RequiresPermissions("sys:user:edit")
    @ApiOperation(value = "添加/修改用户")
    @RequestMapping(value = "addOrUpdate", method = RequestMethod.POST)
    public ResultVo addOrUpdateUser(@RequestBody AddOrUpdateUserIn userIn) {
        return sysUserApiService.addOrUpdateUser(userIn);
    }

    @RequiresPermissions("sys:user:edit")
    @ApiOperation(value = "修改系统用户状态")
    @RequestMapping(value = "updateStatus", method = RequestMethod.POST)
    public ResultVo updateStatus(@RequestBody UpdateSysUserStatusIn statusIn) {
        return sysUserApiService.updateUserStatus(statusIn);
    }

    @RequiresPermissions("sys:user:del")
    @ApiOperation(value = "删除用户")
    @RequestMapping(value = "remove/{userId}", method = RequestMethod.GET)
    public ResultVo removeUser(@PathVariable("userId") Integer userId) {
        return sysUserApiService.removeUser(userId);
    }

}
