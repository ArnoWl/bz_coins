package com.admin.controller.sys;

import com.admin.entity.AppVersionInfo;
import com.admin.entity.AppVersionInfo;
import com.admin.model.in.AddAppVersionIn;
import com.admin.model.in.UpdateAppVersionIn;
import com.admin.model.in.VersionListIn;
import com.admin.service.IAppVersionInfoService;
import com.admin.service.IAppVersionInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.context.SysUserContext;
import com.common.utils.lang.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * @Author: lt
 * @Date: 2020/5/30 10:42
 */
@CrossOrigin
@Api(description = "系统管理--版本管理")
@RestController
@RequestMapping(value = "version")
public class VersionController {

    @Resource
    private IAppVersionInfoService iAppVersionInfoService;

    @RequiresPermissions("sys:version:list")
    @ApiOperation(value = "版本信息列表",response = AppVersionInfo.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo getVersionList(@RequestBody VersionListIn listIn) {
        QueryWrapper<AppVersionInfo> wrapper = new QueryWrapper<>();
        if(listIn.getSystemType() != null){
            wrapper.eq("sys_type", listIn.getSystemType());
        }
        if(listIn.getAppType() != null && listIn.getAppType() > 0){
            wrapper.eq("app_type", listIn.getAppType());
        }
        if(StringUtils.isNotBlank(listIn.getVersionNo())){
            wrapper.like("version_no",listIn.getVersionNo())
                    .or().eq("version_name",listIn.getVersionNo());
        }
        if(listIn.getStatus() != null && listIn.getStatus() > 0){
            wrapper.eq("status",listIn.getStatus());
        }
        wrapper.orderByDesc("version_no");
        Page<AppVersionInfo> versionPage = new Page<>(listIn.getPageNo(),listIn.getPageSize());
        iAppVersionInfoService.page(versionPage,wrapper);
        return ResultVo.success(versionPage);
    }

    @RequiresPermissions("sys:version:detail")
    @ApiOperation(value = "版本信息详情",response = AppVersionInfo.class)
    @RequestMapping(value = "detail/{id}", method = RequestMethod.GET)
    public ResultVo getVersionDetail(@PathVariable("id") Integer id) {
        return ResultVo.success(iAppVersionInfoService.getById(id));
    }

    @RequiresPermissions("sys:version:add")
    @ApiOperation(value = "添加版本")
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public ResultVo addVersion(@RequestBody AddAppVersionIn versionIn) {
        AppVersionInfo appVersion = new AppVersionInfo();
        BeanUtils.copyProperties(versionIn,appVersion);
        appVersion.setSysType(versionIn.getSystemType());
        appVersion.setCreateBy(SysUserContext.getSysUserID());
        appVersion.setCreateDate(LocalDateTime.now());
        if(iAppVersionInfoService.save(appVersion)){
            return ResultVo.success();
        }
        return ResultVo.failure("添加版本信息失败");
    }

    @RequiresPermissions("sys:version:edit")
    @ApiOperation(value = "修改版本")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResultVo updateVersion(@RequestBody UpdateAppVersionIn versionIn) {
        AppVersionInfo appVersion = iAppVersionInfoService.getById(versionIn.getId());
        if(appVersion != null){
            BeanUtils.copyProperties(versionIn,appVersion);
            appVersion.setSysType(versionIn.getSystemType());
            appVersion.setUpdateBy(SysUserContext.getSysUserID());
            appVersion.setUpdateDate(LocalDateTime.now());
            if(iAppVersionInfoService.updateById(appVersion)){
                return ResultVo.success();
            }
        }
        return ResultVo.failure("修改版本信息失败");
    }


}
