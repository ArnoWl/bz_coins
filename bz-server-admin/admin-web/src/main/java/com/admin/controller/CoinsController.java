package com.admin.controller;

import com.admin.model.in.*;
import com.admin.model.out.CoinAddrOut;
import com.admin.model.out.CoinPriceListOut;
import com.admin.model.out.CoinsOut;
import com.admin.service.CoinsService;
import com.auth.client.annotation.IgnoreUserToken;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 系统货币信息控制器
 * @Author lt
 * @Date 2020/5/3 14:40
 */
@CrossOrigin
@Api(description = "系统管理--系统货币")
@RestController
@RequestMapping(value = "coins")
public class CoinsController {

    @Autowired
    private CoinsService coinsService;

    @RequiresPermissions("sys:coins:list")
    @ApiOperation(value = "系统货币列表",response = CoinsOut.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryCoin(@RequestBody PageParamIn pageParamIn){
        return coinsService.queryCoin(pageParamIn);
    }

    @RequiresPermissions("sys:coins:edit")
    @ApiOperation(value = "添加/编辑系统货币")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editCoin(@RequestBody CoinsIn coinsIn){
        return coinsService.editCoin(coinsIn);
    }

    @ApiOperation(value = "编辑系统货币回显",response = CoinsOut.class)
    @RequestMapping(value = "editShow", method = RequestMethod.GET)
    public ResultVo editShow(@RequestParam("id") Integer id){
        return coinsService.editShow(id);
    }

    @RequiresPermissions("sys:coins:addr")
    @ApiOperation(value = "生成货币钱包地址")
    @RequestMapping(value = "handleCoinsAddr", method = RequestMethod.POST)
    public ResultVo handleCoinAddr(@RequestBody CoinAddrIn addrIn){
        return coinsService.handleCoinAddr(addrIn);
    }

    @RequiresPermissions("sys:coins:queryaddr")
    @ApiOperation(value = "查询货币钱包地址",response = CoinAddrOut.class)
    @RequestMapping(value = "queryCoinAddr", method = RequestMethod.POST)
    public ResultVo queryCoinAddr(@RequestBody CoinAddrQueryIn queryIn){
        return coinsService.queryCoinAddr(queryIn);
    }

    @RequiresPermissions("sys:coins:price")
    @ApiOperation(value = "查询系统货币价格记录",response = CoinPriceListOut.class)
    @RequestMapping(value = "queryCoinPrice", method = RequestMethod.POST)
    public ResultVo queryCoinPrice(@RequestBody CoinPriceQueryIn queryIn){
        return coinsService.queryCoinPrice(queryIn);
    }

    @RequiresPermissions("sys:coins:addprice")
    @ApiOperation(value = "添加系统货币价格记录")
    @RequestMapping(value = "addCoinPrice", method = RequestMethod.POST)
    public ResultVo addCoinPrice(@RequestBody CoinPriceAddIn addIn){
        return coinsService.addCoinPrice(addIn);
    }




}
