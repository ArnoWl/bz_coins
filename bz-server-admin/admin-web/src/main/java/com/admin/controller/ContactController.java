package com.admin.controller;

import com.admin.entity.SysContact;
import com.admin.model.StatisVo;
import com.admin.model.in.ContactEditIn;
import com.admin.service.ContactService;
import com.common.base.ResultVo;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  联系客服业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "客服业务模块")
@RestController
@RequestMapping(value = "contact")
@Slf4j
public class ContactController {

    @Autowired
    private ContactService contactService;

    @RequiresPermissions("sys:contact:list")
    @ApiOperation(value = "联系客服数据",response = SysContact.class)
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResultVo getContactInfo(){
        return contactService.getContactInfo();
    }

    @RequiresPermissions("sys:contact:detail")
    @ApiOperation(value = "详情",response = SysContact.class)
    @RequestMapping(value = "detail", method = RequestMethod.GET)
    public ResultVo getContactDetail(@RequestParam("id")Integer id){
        return contactService.getContactDetail(id);
    }

    @RequiresPermissions("sys:contact:edit")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editContactDetail(@RequestBody ContactEditIn editIn){
        return contactService.editContactDetail(editIn);
    }
}
