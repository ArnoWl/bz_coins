package com.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.common.context.SysUserContext;
import com.mall.feign.wallet.WalletClient;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.mall.model.wallet.vo.RechargeLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 钱包业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "钱包业务模块")
@RestController
@RequestMapping(value = "wallet")
@Slf4j
public class WalletController {
    @Resource
    private WalletClient walletClient;

    @RequiresPermissions("wallet:cash:list")
    @ApiOperation(value = "提现列表", response = CashLogVo.class)
    @RequestMapping(value = "cashList", method = RequestMethod.POST)
    public ResultVo queryWalletCash(@RequestBody WalletCashListDto dto) {
        return ResultVo.success(walletClient.queryWalletCash(dto));
    }

    @RequiresPermissions("wallet:cash:examine")
    @ApiOperation(value = "提现审核")
    @RequestMapping(value = "handleWalletCashExamine", method = RequestMethod.GET)
    public ResultVo handleWalletCashExamine(@RequestParam("cashId") Integer cashId,@RequestParam("status") Integer status) {
        return walletClient.updateCashStatus(cashId, status, SysUserContext.getSysUsername());
    }

    @RequiresPermissions("wallet:recharge:list")
    @ApiOperation(value = "充值记录", response = RechargeLogVo.class)
    @RequestMapping(value = "rechargeList", method = RequestMethod.POST)
    public ResultVo queryWalletRecharge(@RequestBody WalletRechargeListDto dto) {
        Page<RechargeLogVo> rechargeLogVoPage = walletClient.queryWalletRecharge(dto);
        return ResultVo.success(rechargeLogVoPage);
    }

}
