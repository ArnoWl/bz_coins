package com.admin.controller;

import com.admin.model.in.BannerEditIn;
import com.admin.model.in.BannerListIn;
import com.admin.model.out.BannerOut;
import com.admin.service.BannerService;
import com.common.base.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  banner图业务控制器
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "banner图业务模块")
@RestController
@RequestMapping(value = "banner")
@Slf4j
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @RequiresPermissions("sys:banner:list")
    @ApiOperation(value = "banner图列表",response = BannerOut.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryBanner(@RequestBody BannerListIn bannerListIn){
        return bannerService.queryBanner(bannerListIn);
    }

    @RequiresPermissions("sys:banner:edit")
    @ApiOperation(value = "添加/修改banner图")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editBanner(@RequestBody BannerEditIn editIn){
        return bannerService.editBanner(editIn);
    }

    @ApiOperation(value = "修改banner图回显",response = BannerOut.class)
    @RequestMapping(value = "editShow", method = RequestMethod.GET)
    public ResultVo editShow(@RequestParam("id") Integer id){
        return bannerService.editShow(id);
    }

    @RequiresPermissions("sys:banner:del")
    @ApiOperation(value = "删除banner图")
    @RequestMapping(value = "del", method = RequestMethod.GET)
    public ResultVo delBanner(@RequestParam("id") Integer id){
        return bannerService.delBanner(id);
    }


}
