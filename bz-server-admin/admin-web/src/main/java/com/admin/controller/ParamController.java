package com.admin.controller;

import com.admin.model.out.SysRuleOut;
import com.admin.service.ParamService;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.model.admin.dto.SysConfigDto;
import com.mall.model.admin.vo.SysConfigDetailVo;
import com.mall.model.admin.vo.SysConfigVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  系统基础参数表
 * </p>
 *
 * @author cx
 * @since 2020/7/22
 */
@CrossOrigin
@Api(description = "系统基础参数业务模块")
@RestController
@RequestMapping(value = "param")
@Slf4j
public class ParamController {

    @Autowired
    private ParamService paramService;

    @RequiresPermissions("sys:param:list")
    @ApiOperation(value = "参数列表",response = SysConfigVo.class)
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResultVo queryParam(@RequestBody PageParamIn pageParamIn){
        return paramService.queryParam(pageParamIn);
    }

    @ApiOperation(value = "修改回显",response = SysConfigDetailVo.class)
    @RequestMapping(value = "editShow", method = RequestMethod.GET)
    public ResultVo editShow(@RequestParam("id") Integer id){
        return paramService.editShow(id);
    }

    @RequiresPermissions("sys:param:edit")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResultVo editParam(@RequestBody SysConfigDto configDto){
        return paramService.editParam(configDto);
    }
}
