package com.admin.configuration;

import com.auth.client.interceptor.UserAuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web拦截器配置
 * @author lt
 * 2020年3月18日14:09:26
 */
@Configuration("adminWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * 用户鉴权拦截处理
     * @return
     */
    @Bean
    UserAuthInterceptor getUserAuthRestInterceptor() {
        return new UserAuthInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getUserAuthRestInterceptor())
                // 拦截所有路径
                .addPathPatterns("/**")
                // 放开swagger
                .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**")
                .excludePathPatterns("/**/error");
    }


}
