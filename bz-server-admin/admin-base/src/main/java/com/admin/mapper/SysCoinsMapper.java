package com.admin.mapper;

import com.admin.entity.SysCoins;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统货币信息 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface SysCoinsMapper extends BaseMapper<SysCoins> {

}
