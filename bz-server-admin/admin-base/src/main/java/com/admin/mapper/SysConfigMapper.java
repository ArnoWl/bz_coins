package com.admin.mapper;

import com.admin.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统配置信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-10
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
