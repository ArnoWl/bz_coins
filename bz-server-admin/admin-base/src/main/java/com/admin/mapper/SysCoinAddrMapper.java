package com.admin.mapper;

import com.admin.entity.SysCoinAddr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统货币钱包地址表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface SysCoinAddrMapper extends BaseMapper<SysCoinAddr> {

}
