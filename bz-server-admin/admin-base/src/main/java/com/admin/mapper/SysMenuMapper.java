package com.admin.mapper;

import com.admin.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统菜单信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据条件参数查询菜单资源列表
     * @param params
     * @return
     */
    List<SysMenu> selectMenuListByParams(@Param("params") Map<String, Object> params);
}
