package com.admin.mapper;

import com.admin.entity.SysCoinPrice;
import com.admin.model.CoinPriceVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 系统货币的价格记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
public interface SysCoinPriceMapper extends BaseMapper<SysCoinPrice> {

    List<CoinPriceVo> queryGroupByTime();
}
