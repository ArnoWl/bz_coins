package com.admin.mapper;

import com.admin.entity.SysRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

}
