package com.admin.mapper;

import com.admin.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统操作日志信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-06-02
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
