package com.admin.mapper;

import com.admin.entity.SysUser;
import com.admin.model.SysUserVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统用户信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-03-16
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据条件查询系统用户信息列表
     * @param params
     * @param page
     * @return
     */
    List<SysUserVo> selectSysUserListByParams(@Param("params") Map<String, Object> params, Page<SysUserVo> page);
}
