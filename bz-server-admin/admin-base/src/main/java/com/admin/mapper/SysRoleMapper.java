package com.admin.mapper;

import com.admin.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户角色信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
