package com.admin.mapper;

import com.admin.entity.AppVersionInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * App版本管理信息表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-05
 */
public interface AppVersionInfoMapper extends BaseMapper<AppVersionInfo> {

}
