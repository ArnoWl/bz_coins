package com.admin.mapper;

import com.admin.entity.SysNoticeInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统公告 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface SysNoticeInfoMapper extends BaseMapper<SysNoticeInfo> {

}
