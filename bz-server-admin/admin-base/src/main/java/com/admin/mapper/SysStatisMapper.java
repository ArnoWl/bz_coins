package com.admin.mapper;

import com.admin.entity.SysStatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.model.admin.dto.StatisDayBeforeDto;

/**
 * <p>
 * 统计表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-08-10
 */
public interface SysStatisMapper extends BaseMapper<SysStatis> {

    SysStatis statisDayBefore();
}
