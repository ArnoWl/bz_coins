package com.admin.mapper;

import com.admin.entity.SysCoinKline;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.model.admin.vo.CoinKlineVo;

import java.util.List;

/**
 * <p>
 * K线图基础表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface SysCoinKlineMapper extends BaseMapper<SysCoinKline> {

    List<CoinKlineVo> queryBZCoin();

    SysCoinKline getOneByCoinSymbol(String coinSymbol);
}
