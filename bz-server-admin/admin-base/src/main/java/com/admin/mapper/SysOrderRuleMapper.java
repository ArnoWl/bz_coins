package com.admin.mapper;

import com.admin.entity.SysOrderRule;
import com.admin.model.StatisVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.base.ResultVo;
import com.mall.model.admin.vo.OrderRuleVo;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 订单包赔场，场次配置表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
public interface SysOrderRuleMapper extends BaseMapper<SysOrderRule> {

    List<StatisVo> queryCountList(@Param("startDate")String startDate, @Param("endDate")String endDate);

    OrderRuleVo getNewlast();
}
