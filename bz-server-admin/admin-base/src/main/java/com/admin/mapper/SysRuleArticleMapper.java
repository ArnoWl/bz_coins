package com.admin.mapper;

import com.admin.entity.SysRuleArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统规则信息记录表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface SysRuleArticleMapper extends BaseMapper<SysRuleArticle> {

}
