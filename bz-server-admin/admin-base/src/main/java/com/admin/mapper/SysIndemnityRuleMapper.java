package com.admin.mapper;

import com.admin.entity.SysIndemnityRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.model.admin.vo.IndemnityRuleVo;

import java.math.BigDecimal;

/**
 * <p>
 * 包赔配置规则表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-28
 */
public interface SysIndemnityRuleMapper extends BaseMapper<SysIndemnityRule> {

    /**
     * 根据金额查询符合的规则
     * @param money
     * @return
     */
    IndemnityRuleVo getRuleByUSDT(BigDecimal money);

}
