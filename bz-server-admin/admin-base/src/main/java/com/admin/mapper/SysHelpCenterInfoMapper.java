package com.admin.mapper;

import com.admin.entity.SysHelpCenterInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface SysHelpCenterInfoMapper extends BaseMapper<SysHelpCenterInfo> {

}
