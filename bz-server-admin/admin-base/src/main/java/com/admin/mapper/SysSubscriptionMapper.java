package com.admin.mapper;

import com.admin.entity.SysSubscription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * BZW认购数量 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
public interface SysSubscriptionMapper extends BaseMapper<SysSubscription> {

}
