package com.admin.mapper;

import com.admin.entity.SysContact;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客服表 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-27
 */
public interface SysContactMapper extends BaseMapper<SysContact> {

}
