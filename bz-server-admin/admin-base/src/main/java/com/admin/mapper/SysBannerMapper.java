package com.admin.mapper;

import com.admin.entity.SysBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * banner 图 Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface SysBannerMapper extends BaseMapper<SysBanner> {

}
