package com.admin.mapper;

import com.admin.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lt
 * @since 2020-05-06
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
