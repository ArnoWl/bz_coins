package com.admin.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * BZW认购数量
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysSubscription对象", description="BZW认购数量")
public class SysSubscription implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名称")
    @NotBlank(message = "请填写认购名称")
    private String name;

    @ApiModelProperty(value = "单价")
    @NotNull(message = "请填写认购单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总量")
    @NotNull(message = "请填写认购总量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "剩余数量")
    private BigDecimal balance;

    @ApiModelProperty(value = "当期购买最大总数量")
    private BigDecimal maxNum;

    @ApiModelProperty(value = "版本号")
    @Version
    private Integer version;


}
