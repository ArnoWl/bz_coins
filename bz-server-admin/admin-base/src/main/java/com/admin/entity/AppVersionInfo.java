package com.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * App版本管理信息表
 * </p>
 *
 * @author lt
 * @since 2020-07-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AppVersionInfo对象", description="App版本管理信息表")
public class AppVersionInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "App客户端类型：1-用户端")
    private Integer appType;

    @ApiModelProperty(value = "系统类型：0-安卓 1-IOS")
    private Integer sysType;

    @ApiModelProperty(value = "版本号")
    private Integer versionNo;

    @ApiModelProperty(value = "版本号名称(1.1.2)")
    private String versionName;

    @ApiModelProperty(value = "兼容版本号")
    private Integer compatibleNo;

    @ApiModelProperty(value = "更新内容")
    private String updateContent;

    @ApiModelProperty(value = "下载地址")
    private String downloadLink;

    @ApiModelProperty(value = "状态：1-提示升级 2-不提示升级 3-强制升级 ")
    private Integer status;

    @ApiModelProperty(value = "删除标记")
    private String delFlag;

    @ApiModelProperty(value = "创建人")
    private Integer createBy;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createDate;

    @ApiModelProperty(value = "更新人")
    private Integer updateBy;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateDate;


}
