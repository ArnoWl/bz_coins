package com.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * banner 图
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysBanner对象", description="banner 图")
public class SysBanner implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "banner图")
    private String bannerUrl;

    @ApiModelProperty(value = "位置 1首页顶部   2首页中部")
    private Integer position;

    @ApiModelProperty(value = "事件类型  1：跳转公告   2：分享  ")
    private Integer eventType;

    @ApiModelProperty(value = "参数")
    private String params;


}
