package com.admin.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统货币的价格记录表
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysCoinPrice对象", description="系统货币的价格记录表")
public class SysCoinPrice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "系统币id")
    private Integer coinId;

    @ApiModelProperty(value = "当前最新价格")
    private BigDecimal price;

    @ApiModelProperty(value = "涨幅跌幅")
    private BigDecimal rate;

    @ApiModelProperty(value = "系统币名称")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "系统币名称")
    @TableField(exist = false)
    private String name;


}
