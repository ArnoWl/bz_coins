package com.admin.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * K线图基础表
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysCoinKline对象", description="K线图基础表")
public class SysCoinKline implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "币种名称")
    private String name;

    @ApiModelProperty(value = "币种符号")
    private String coinSymbol;

    @ApiModelProperty(value = "货币交易对 针对usdt交易对 针对火币对标")
    private String coinTransCode;

    @ApiModelProperty(value = "开盘价 我们保留4位小数")
    private BigDecimal openPrice;

    @ApiModelProperty(value = "0开启 1关闭")
    private Integer status;

    @ApiModelProperty(value = "0参与币值交易  1不参与")
    private Integer contractTransFlag;

    @ApiModelProperty(value = "排序  越大越后")
    private Integer sort;

    private String sockerUrl;

    @ApiModelProperty(value = "1火币 2币安 3OK")
    private Integer type;

    @ApiModelProperty(value = "下单点差最低值比例")
    private BigDecimal spreadMin;

    @ApiModelProperty(value = "下单点差最大值比例")
    private BigDecimal spreadMax;

    @ApiModelProperty(value = "最低价往下调整金额")
    private BigDecimal settleShort;

    @ApiModelProperty(value = "最高价往上调整金额")
    private BigDecimal settleLong;



}
