package com.admin.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 统计表
 * </p>
 *
 * @author lt
 * @since 2020-08-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysStatis对象", description="统计表")
public class SysStatis implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "时间")
    private LocalDate day;

    @ApiModelProperty(value = "充值金额")
    private BigDecimal rechargeMoney;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal cashMoney;

    @ApiModelProperty(value = "普通场订单数量")
    private Integer ordinaryNum;

    @ApiModelProperty(value = "普通场订单总额")
    private BigDecimal ordinaryMoney;

    @ApiModelProperty(value = "普通场订单盈亏")
    private BigDecimal ordinaryProfit;

    @ApiModelProperty(value = "包赔场订单数量")
    private Integer indemnityNum;

    @ApiModelProperty(value = "包赔场订单总额")
    private BigDecimal indemnityMoney;

    @ApiModelProperty(value = "包赔场总盈亏")
    private BigDecimal indemnityProfit;

    @ApiModelProperty(value = "注册用户数量")
    private Integer registerNum;

    @ApiModelProperty(value = "订单数量")
    private Integer orderNum;

    @ApiModelProperty(value = "usdt收益")
    private BigDecimal usdtProfit;

    @ApiModelProperty(value = "bzw赠送")
    private BigDecimal bzwGive;

    @ApiModelProperty("bzw赠送冻结部分")
    private BigDecimal bzwFrozen;

    @ApiModelProperty(value = "bzw销毁")
    private BigDecimal bzwDestruction;


}
