package com.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单包赔场，场次配置表
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysOrderRule对象", description="订单包赔场，场次配置表")
public class SysOrderRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "场次名称")
    private String name;

    @ApiModelProperty(value = "开始时间 小时分钟 例如9:30")
    private String beginTime;

    @ApiModelProperty(value = "借宿时间 小时分钟 例如19:30")
    private String endTime;

    @ApiModelProperty(value = "0开启 1关闭")
    private Integer status;


}
