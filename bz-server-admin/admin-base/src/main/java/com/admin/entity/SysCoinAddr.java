package com.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统货币钱包地址表
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysCoinAddr对象", description="系统货币钱包地址表")
public class SysCoinAddr implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "货币主键id")
    private Integer coinId;

    @ApiModelProperty(value = "钱包地址")
    private String walletAddr;

    @ApiModelProperty(value = "钱包密码")
    private String walletPassword;

    @ApiModelProperty(value = "钱包文件路径")
    private String walletPath;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;

    @ApiModelProperty(value = "1主地址 MAIN 2手续费地址 FEE")
    private String type;

    @ApiModelProperty(value = "协议类型OMNI  ERC20")
    private String agreeType;

    @ApiModelProperty(value = "归集类型  1系统地址 2场外地址 场外地址需要自己填写钱包地址，不需要密码和文件")
    private Integer imputedType;


    @ApiModelProperty(value = "有效0  无效1")
    private Integer status;





}
