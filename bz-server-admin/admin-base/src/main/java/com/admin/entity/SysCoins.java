package com.admin.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统货币信息
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysCoins对象", description="系统货币信息")
public class SysCoins implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "币种名称")
    private String name;

    @ApiModelProperty(value = "0开启 1关闭")
    private Integer status;

    @ApiModelProperty(value = "币种精度")
    private Integer accuracy;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;

    @ApiModelProperty(value = "0开放充值  1不开放对外充值")
    private Integer rechargeFlag;

    @ApiModelProperty(value = "0开放对外提现  1未开放对外提现")
    private Integer cashFlag;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "货币类型  1公链  2ERC20代币")
    private Integer type;

    @ApiModelProperty(value = "0是系统币  1不用是系统币")
    private Integer sysFlag;

    @ApiModelProperty(value = "手续费数量")
    private BigDecimal tax;

    @ApiModelProperty(value = "手续费类型 1按数量 2按比例")
    private Integer taxType;

    @ApiModelProperty(value = "排序  越大越后")
    private Integer sort;


}
