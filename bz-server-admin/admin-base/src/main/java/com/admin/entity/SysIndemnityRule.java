package com.admin.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 包赔配置规则表
 * </p>
 *
 * @author lt
 * @since 2020-07-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysIndemnityRule对象", description="包赔配置规则表")
public class SysIndemnityRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "钱包USDT最低金额")
    @NotNull(message = "请输入USDT最低金额")
    private BigDecimal minMoney;

    @ApiModelProperty(value = "钱包USDT最大金额")
    @NotNull(message = "请输入钱包USDT最大金额")
    private BigDecimal maxMoney;

    @ApiModelProperty(value = "包赔订单数量每场")
    @NotNull(message = "请输入包赔订单数量每场")
    private Integer indemnityNum;

    @ApiModelProperty(value = "包赔每次最大金额")
    @NotNull(message = "请输入包赔每次最大金额")
    private BigDecimal indemnityMaxMoney;


}
