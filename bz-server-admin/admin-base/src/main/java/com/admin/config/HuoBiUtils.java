package com.admin.config;

import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.List;

/**
 * 货币获取价格
 * @author arno
 *
 */
@Configuration
public class HuoBiUtils {


	@Resource
	private HBConfig hbConfig;
	/**
	 * 货币获取K线图
	 * @param symbol 交易对 格式 btcusdt
	 * @param type 1min, 5min, 15min, 30min, 60min, 1day, 1mon, 1week, 1year
	 * @param size 1-2000
	 * @return
	 */
	public List<Kline> queryKline(String symbol, String type, String size){
		HBApi client = new HBApi(hbConfig.getAccessKey(), hbConfig.getSecretKey());
		KlineResponse<List<Kline>> kline = client.kline(symbol, type,size);
		return kline.data;
	}
}
