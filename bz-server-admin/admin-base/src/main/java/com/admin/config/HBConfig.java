package com.admin.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-26
 */
@Data
public class HBConfig {

    @Value("${hb.access_key}")
    private String accessKey;

    @Value("${hb.secret_key}")
    private String secretKey;

}
