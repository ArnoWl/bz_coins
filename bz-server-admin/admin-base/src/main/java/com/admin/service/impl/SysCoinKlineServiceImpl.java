package com.admin.service.impl;

import com.admin.entity.SysCoinKline;
import com.admin.mapper.SysCoinKlineMapper;
import com.admin.service.ISysCoinKlineService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.constatns.CommonConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.utils.Query;
import com.google.common.collect.Lists;
import com.mall.model.admin.vo.CoinKlineVo;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * K线图基础表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysCoinKlineServiceImpl extends ServiceImpl<SysCoinKlineMapper, SysCoinKline> implements ISysCoinKlineService {

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public void storeToCache() {
        QueryWrapper<SysCoinKline> wrapper = new QueryWrapper<>();
        wrapper.eq("status", CommonConstants.DEL_FLAG_0);
        List<SysCoinKline> coinKlineList = this.list(wrapper);
        redisTemplate.opsForValue().set(RedisKeyConstants.EXCHANGE_KLINE_WS_CONFIG
                ,JSON.toJSONString(coinKlineList));
    }

    @Override
    public List<CoinKlineVo> queryBZCoin() {
        return baseMapper.queryBZCoin();
    }

    @Override
    public SysCoinKline getOneByCoinSymbol(String coinSymbol) {
        return baseMapper.getOneByCoinSymbol(coinSymbol);
    }

}
