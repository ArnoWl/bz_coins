package com.admin.service.impl;

import com.admin.entity.SysMenu;
import com.admin.mapper.SysMenuMapper;
import com.admin.service.ISysMenuService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统菜单信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {


    @Override
    public List<SysMenu> getMenuListByParams(Map<String, Object> params) {
        return baseMapper.selectMenuListByParams(params);
    }

    @Override
    public String getMenuFullName(String authValue) {
        QueryWrapper<SysMenu> wrapper = new QueryWrapper<>();
        wrapper.eq("auth_value",authValue).last("limit 1");
        SysMenu menu = this.getOne(wrapper);
        if (menu != null){
            List<String> strList = Lists.newArrayList();
            strList.add(menu.getName());
            return this.getSysMenuName(menu,strList);
        }
        return null;
    }

    private String getSysMenuName(SysMenu menu, List<String> menuNames){
        if(!menu.getParentId().equals(0)){
            SysMenu parent = this.getById(menu.getParentId());
            if(parent != null){
                if(!menuNames.contains(parent.getName())){
                    menuNames.add(parent.getName());
                }
                return this.getSysMenuName(parent,menuNames);
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = menuNames.size() - 1; i >= 0; i--) {
            sb.append(menuNames.get(i)).append("-");
        }
        return sb.substring(0,sb.length()-1);
    }
}
