package com.admin.service;

import com.admin.entity.SysCoinKline;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.model.admin.vo.CoinKlineVo;

import java.util.List;

/**
 * <p>
 * K线图基础表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysCoinKlineService extends IService<SysCoinKline> {

    /**
     * 存储配置信息到缓存
     */
    void storeToCache();

    List<CoinKlineVo> queryBZCoin();

    SysCoinKline getOneByCoinSymbol(String coinSymbol);
}
