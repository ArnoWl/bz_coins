package com.admin.service;

import com.admin.entity.SysHelpCenterInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysHelpCenterInfoService extends IService<SysHelpCenterInfo> {

}
