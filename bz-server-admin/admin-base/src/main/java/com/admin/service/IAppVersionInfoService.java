package com.admin.service;

import com.admin.entity.AppVersionInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * App版本管理信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-05
 */
public interface IAppVersionInfoService extends IService<AppVersionInfo> {

}
