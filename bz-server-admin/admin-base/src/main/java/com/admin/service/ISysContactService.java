package com.admin.service;

import com.admin.entity.SysContact;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客服表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-27
 */
public interface ISysContactService extends IService<SysContact> {

}
