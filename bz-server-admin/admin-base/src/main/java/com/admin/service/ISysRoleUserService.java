package com.admin.service;

import com.admin.entity.SysRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
public interface ISysRoleUserService extends IService<SysRoleUser> {

}
