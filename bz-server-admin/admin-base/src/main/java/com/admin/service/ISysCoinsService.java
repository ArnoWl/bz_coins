package com.admin.service;

import com.admin.entity.SysCoins;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统货币信息 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysCoinsService extends IService<SysCoins> {

    SysCoins getByName(String name);
}
