package com.admin.service.impl;

import com.admin.entity.SysConfig;
import com.admin.mapper.SysConfigMapper;
import com.admin.service.ISysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.constatns.RedisKeyConstants;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 系统配置信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-10
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public void storeToCache() {
        List<SysConfig> config = this.list();
        config.forEach(c ->{
            redisTemplate.opsForValue().set(RedisKeyConstants.SYS_KEY + c.getConfigKey(),c.getConfigVal());
        });
    }
}
