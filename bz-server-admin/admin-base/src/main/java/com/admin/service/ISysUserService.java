package com.admin.service;

import com.admin.entity.SysUser;
import com.admin.model.SysUserVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;

import java.util.Map;

/**
 * <p>
 * 系统用户信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-03-16
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据条件获取系统用户列表 -- 分页
     * @param page
     * @return
     */
    Page<SysUserVo> getSysUserPageByParams(Map<String, Object> params, Page<SysUserVo> page);

    ResultVo getUserInfo();
}
