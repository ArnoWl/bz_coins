package com.admin.service;

import com.admin.entity.SysBanner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * banner 图 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysBannerService extends IService<SysBanner> {

}
