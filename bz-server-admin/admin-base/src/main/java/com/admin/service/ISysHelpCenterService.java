package com.admin.service;

import com.admin.entity.SysHelpCenter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysHelpCenterService extends IService<SysHelpCenter> {

    ResultVo queryHelp();
}
