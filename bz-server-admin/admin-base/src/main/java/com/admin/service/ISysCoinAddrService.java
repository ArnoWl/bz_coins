package com.admin.service;

import com.admin.entity.SysCoinAddr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统货币钱包地址表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysCoinAddrService extends IService<SysCoinAddr> {

}
