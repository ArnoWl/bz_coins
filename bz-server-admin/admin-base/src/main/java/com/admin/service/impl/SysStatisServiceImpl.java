package com.admin.service.impl;

import com.admin.entity.SysStatis;
import com.admin.mapper.SysStatisMapper;
import com.admin.service.ISysStatisService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.model.admin.dto.StatisDayBeforeDto;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 统计表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-08-10
 */
@Service
public class SysStatisServiceImpl extends ServiceImpl<SysStatisMapper, SysStatis> implements ISysStatisService {

    @Override
    public SysStatis statisDayBefore() {
        return baseMapper.statisDayBefore();
    }
}
