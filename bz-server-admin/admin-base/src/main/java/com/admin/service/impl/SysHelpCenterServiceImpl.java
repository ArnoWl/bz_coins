package com.admin.service.impl;

import com.admin.entity.SysHelpCenter;
import com.admin.entity.SysHelpCenterInfo;
import com.admin.mapper.SysHelpCenterMapper;
import com.admin.service.ISysHelpCenterInfoService;
import com.admin.service.ISysHelpCenterService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.utils.ExtBeanUtils;
import com.mall.model.admin.vo.HelpCenterInfoVo;
import com.mall.model.admin.vo.HelpCenterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysHelpCenterServiceImpl extends ServiceImpl<SysHelpCenterMapper, SysHelpCenter> implements ISysHelpCenterService {
    @Autowired
    private ISysHelpCenterInfoService sysHelpCenterInfoService;

    @Override
    public ResultVo queryHelp() {
        List<SysHelpCenter> list = this.list();
        List<HelpCenterVo> listVo = ExtBeanUtils.copyList(list, HelpCenterVo.class);
        for (HelpCenterVo help:listVo) {
            QueryWrapper<SysHelpCenterInfo> queryWrapper=new QueryWrapper<>();
            queryWrapper.eq("help_center_id",help.getId());
            List<SysHelpCenterInfo> listInfo = sysHelpCenterInfoService.list(queryWrapper);
            List<HelpCenterInfoVo> listInfoVo = ExtBeanUtils.copyList(listInfo, HelpCenterInfoVo.class);
            help.setList(listInfoVo);
        }
        return ResultVo.success(list);
    }
}
