package com.admin.service.impl;

import com.admin.entity.SysRoleMenu;
import com.admin.mapper.SysRoleMenuMapper;
import com.admin.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-05-06
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

    @Override
    public List<Integer> getMenuIdsByRoleId(Integer roleId) {
        QueryWrapper<SysRoleMenu> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id",roleId);
        List<SysRoleMenu> roleMenus = this.list(wrapper);
        if(!CollectionUtils.isEmpty(roleMenus)){
            List<Integer> menuIds = Lists.newArrayList();
            roleMenus.forEach(r -> menuIds.add(r.getMenuId()));
            return menuIds;
        }
        return Lists.newArrayList();
    }
}
