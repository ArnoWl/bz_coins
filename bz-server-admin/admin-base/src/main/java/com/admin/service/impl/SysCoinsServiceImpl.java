package com.admin.service.impl;

import com.admin.entity.SysCoins;
import com.admin.mapper.SysCoinsMapper;
import com.admin.service.ISysCoinsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统货币信息 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysCoinsServiceImpl extends ServiceImpl<SysCoinsMapper, SysCoins> implements ISysCoinsService {

    @Override
    public SysCoins getByName(String name) {
        QueryWrapper<SysCoins> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("name",name);
        return this.getOne(queryWrapper);
    }
}
