package com.admin.service.impl;

import com.admin.entity.AppVersionInfo;
import com.admin.mapper.AppVersionInfoMapper;
import com.admin.service.IAppVersionInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * App版本管理信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-05
 */
@Service
public class AppVersionInfoServiceImpl extends ServiceImpl<AppVersionInfoMapper, AppVersionInfo> implements IAppVersionInfoService {

}
