package com.admin.service.impl;

import com.admin.entity.SysOrderRule;
import com.admin.mapper.SysOrderRuleMapper;
import com.admin.model.StatisVo;
import com.admin.service.ISysOrderRuleService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.constatns.RedisKeyConstants;
import com.common.utils.lang.DateUtils;
import com.common.utils.lang.StringUtils;
import com.mall.model.admin.vo.OrderRuleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 订单包赔场，场次配置表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
@Service
public class SysOrderRuleServiceImpl extends ServiceImpl<SysOrderRuleMapper, SysOrderRule> implements ISysOrderRuleService {

    @Override
    public OrderRuleVo getNewlast() {
        return baseMapper.getNewlast();
    }

}
