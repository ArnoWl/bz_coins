package com.admin.service.impl;

import com.admin.entity.SysLog;
import com.admin.mapper.SysLogMapper;
import com.admin.service.ISysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统操作日志信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-06-02
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

}
