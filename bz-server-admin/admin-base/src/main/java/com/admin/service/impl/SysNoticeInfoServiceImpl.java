package com.admin.service.impl;

import com.admin.entity.SysNoticeInfo;
import com.admin.mapper.SysNoticeInfoMapper;
import com.admin.service.ISysNoticeInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统公告 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysNoticeInfoServiceImpl extends ServiceImpl<SysNoticeInfoMapper, SysNoticeInfo> implements ISysNoticeInfoService {

}
