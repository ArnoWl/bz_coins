package com.admin.service.impl;

import com.admin.entity.SysIndemnityRule;
import com.admin.mapper.SysIndemnityRuleMapper;
import com.admin.service.ISysIndemnityRuleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.model.admin.vo.IndemnityRuleVo;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 * 包赔配置规则表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-28
 */
@Service
public class SysIndemnityRuleServiceImpl extends ServiceImpl<SysIndemnityRuleMapper, SysIndemnityRule> implements ISysIndemnityRuleService {

    @Override
    public IndemnityRuleVo getRuleByUSDT(BigDecimal money) {
        return baseMapper.getRuleByUSDT(money);
    }

    @Override
    public ResultVo queryIndemnity(PageParamIn pageParamIn) {
        IPage<SysIndemnityRule> pageInfo=new Page<>(pageParamIn.getPageNo(),pageParamIn.getPageSize());
        this.page(pageInfo,null);
        return ResultVo.success(pageInfo);
    }

    @Override
    public ResultVo editIndemnityShow(Integer id) {
        SysIndemnityRule sysIndemnityRule = this.getById(id);
        return ResultVo.success(sysIndemnityRule);
    }

    @Override
    public ResultVo editIndemnity(SysIndemnityRule rule) {
        this.saveOrUpdate(rule);
        return ResultVo.success();
    }

    @Override
    public ResultVo delIndemnity(Integer id) {
        this.removeById(id);
        return ResultVo.success();
    }
}
