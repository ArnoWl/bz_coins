package com.admin.service.impl;

import com.admin.entity.SysCoinAddr;
import com.admin.mapper.SysCoinAddrMapper;
import com.admin.service.ISysCoinAddrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统货币钱包地址表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysCoinAddrServiceImpl extends ServiceImpl<SysCoinAddrMapper, SysCoinAddr> implements ISysCoinAddrService {

}
