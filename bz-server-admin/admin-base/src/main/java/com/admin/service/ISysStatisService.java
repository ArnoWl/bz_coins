package com.admin.service;

import com.admin.entity.SysStatis;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;
import com.mall.model.admin.dto.StatisDayBeforeDto;

import java.util.Map;

/**
 * <p>
 * 统计表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-08-10
 */
public interface ISysStatisService extends IService<SysStatis> {

    SysStatis statisDayBefore();
}
