package com.admin.service;

import com.admin.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lt
 * @since 2020-05-06
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 根据角色ID获取菜单资源Id集合
     * @param roleId
     * @return
     */
    List<Integer> getMenuIdsByRoleId(Integer roleId);
}
