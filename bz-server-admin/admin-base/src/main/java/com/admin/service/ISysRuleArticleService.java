package com.admin.service;

import com.admin.entity.SysRuleArticle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统规则信息记录表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysRuleArticleService extends IService<SysRuleArticle> {

}
