package com.admin.service.impl;

import com.admin.entity.SysHelpCenterInfo;
import com.admin.mapper.SysHelpCenterInfoMapper;
import com.admin.service.ISysHelpCenterInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysHelpCenterInfoServiceImpl extends ServiceImpl<SysHelpCenterInfoMapper, SysHelpCenterInfo> implements ISysHelpCenterInfoService {

}
