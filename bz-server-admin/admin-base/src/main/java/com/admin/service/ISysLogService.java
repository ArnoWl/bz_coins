package com.admin.service;

import com.admin.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统操作日志信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-06-02
 */
public interface ISysLogService extends IService<SysLog> {

}
