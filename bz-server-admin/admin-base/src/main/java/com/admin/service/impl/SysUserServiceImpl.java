package com.admin.service.impl;

import com.admin.entity.SysUser;
import com.admin.mapper.SysUserMapper;
import com.admin.model.SysUserVo;
import com.admin.model.UserInfoVo;
import com.admin.service.ISysUserService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.base.ResultVo;
import com.common.context.SysUserContext;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping;

import java.util.Map;

/**
 * <p>
 * 系统用户信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-03-16
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Override
    public Page<SysUserVo> getSysUserPageByParams(Map<String, Object> params, Page<SysUserVo> page) {
        return page.setRecords(baseMapper.selectSysUserListByParams(params,page));
    }

    @Override
    public ResultVo getUserInfo() {
        SysUser sysUser = this.getById(SysUserContext.getSysUserID());
        if(sysUser!=null){
            UserInfoVo vo = new UserInfoVo();
            BeanUtils.copyProperties(sysUser,vo);
            return ResultVo.success(vo);
        }
        return null;
    }
}
