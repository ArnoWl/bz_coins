package com.admin.service;

import com.admin.entity.SysNoticeInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统公告 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
public interface ISysNoticeInfoService extends IService<SysNoticeInfo> {

}
