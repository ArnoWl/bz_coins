package com.admin.service.impl;

import com.admin.entity.SysBanner;
import com.admin.mapper.SysBannerMapper;
import com.admin.service.ISysBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * banner 图 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysBannerServiceImpl extends ServiceImpl<SysBannerMapper, SysBanner> implements ISysBannerService {

}
