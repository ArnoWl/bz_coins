package com.admin.service;

import com.admin.entity.SysIndemnityRule;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.model.admin.vo.IndemnityRuleVo;

import java.math.BigDecimal;

/**
 * <p>
 * 包赔配置规则表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-28
 */
public interface ISysIndemnityRuleService extends IService<SysIndemnityRule> {

    /**
     * 根据金额查询符合的规则
     * @param money
     * @return
     */
    IndemnityRuleVo getRuleByUSDT(BigDecimal money);

    /**
     * 包赔场规则设置列表
     * @param pageParamIn
     * @return
     */
    ResultVo queryIndemnity(PageParamIn pageParamIn);

    /**
     * 编辑回显
     * @param id
     * @return
     */
    ResultVo editIndemnityShow(Integer id);

    /**
     * 添加或修改
     * @param rule
     * @return
     */
    ResultVo editIndemnity(SysIndemnityRule rule);

    /**
     * 删除
     * @param id
     * @return
     */
    ResultVo delIndemnity(Integer id);
}
