package com.admin.service;

import com.admin.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统菜单信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 根据条件参数获取菜单资源
     * @param params
     * @return
     */
    List<SysMenu> getMenuListByParams(Map<String, Object> params);

    /**
     * 根据授权标识获取菜单全称
     * @param authValue
     * @return
     */
    String getMenuFullName(String authValue);
}
