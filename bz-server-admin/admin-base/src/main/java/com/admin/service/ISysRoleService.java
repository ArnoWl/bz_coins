package com.admin.service;

import com.admin.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统用户角色信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 获取管理员角色列表
     * @return
     */
    List<SysRole> getAdministratorRoles();

    /**
     * 根据用户id获取角色信息
     * @param userId
     * @return
     */
    SysRole getRoleByUserId(Integer userId);
}
