package com.admin.service;

import com.admin.entity.SysSubscription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * BZW认购数量 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
public interface ISysSubscriptionService extends IService<SysSubscription> {

}
