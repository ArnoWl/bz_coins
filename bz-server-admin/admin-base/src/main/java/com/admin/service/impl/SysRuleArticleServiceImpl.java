package com.admin.service.impl;

import com.admin.entity.SysRuleArticle;
import com.admin.mapper.SysRuleArticleMapper;
import com.admin.service.ISysRuleArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统规则信息记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-22
 */
@Service
public class SysRuleArticleServiceImpl extends ServiceImpl<SysRuleArticleMapper, SysRuleArticle> implements ISysRuleArticleService {

}
