package com.admin.service;

import com.admin.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统配置信息表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-10
 */
public interface ISysConfigService extends IService<SysConfig> {

    /**
     * 存储配置信息到缓存
     */
    void storeToCache();
}
