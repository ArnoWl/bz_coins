package com.admin.service.impl;

import com.admin.entity.SysSubscription;
import com.admin.mapper.SysSubscriptionMapper;
import com.admin.service.ISysSubscriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * BZW认购数量 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
@Service
public class SysSubscriptionServiceImpl extends ServiceImpl<SysSubscriptionMapper, SysSubscription> implements ISysSubscriptionService {

}
