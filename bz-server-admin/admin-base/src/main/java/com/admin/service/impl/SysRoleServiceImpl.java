package com.admin.service.impl;

import com.admin.entity.SysRole;
import com.admin.entity.SysRoleUser;
import com.admin.mapper.SysRoleMapper;
import com.admin.service.ISysRoleService;
import com.admin.service.ISysRoleUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.constatns.AdminConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 系统用户角色信息表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-05-03
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    private ISysRoleUserService iSysRoleUserService;

    @Override
    public List<SysRole> getAdministratorRoles() {
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        wrapper.eq("code", AdminConstants.ADMINISTRATOR);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public SysRole getRoleByUserId(Integer userId) {
        QueryWrapper<SysRoleUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId);
        SysRoleUser roleUser = iSysRoleUserService.getOne(wrapper);
        if(roleUser != null){
            return this.getById(roleUser.getRoleId());
        }
        return null;
    }
}
