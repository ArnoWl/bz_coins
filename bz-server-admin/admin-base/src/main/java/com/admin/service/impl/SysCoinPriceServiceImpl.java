package com.admin.service.impl;

import com.admin.entity.SysCoinPrice;
import com.admin.mapper.SysCoinPriceMapper;
import com.admin.model.CoinPriceVo;
import com.admin.service.ISysCoinPriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.constatns.RedisKeyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统货币的价格记录表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-24
 */
@Service
public class SysCoinPriceServiceImpl extends ServiceImpl<SysCoinPriceMapper, SysCoinPrice> implements ISysCoinPriceService {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Override
    public void storeToCache() {
        List<CoinPriceVo> listVo= baseMapper.queryGroupByTime();
        for (CoinPriceVo vo:listVo) {
            Map<String,Object> map=new HashMap<>();
            map.put("price", String.valueOf(vo.getPrice()));
            map.put("rate", String.valueOf(vo.getRate().toString()));
            redisTemplate.opsForHash().putAll(RedisKeyConstants.SYS_COIN_PRICE+vo.getName(),map);
        }
    }
}
