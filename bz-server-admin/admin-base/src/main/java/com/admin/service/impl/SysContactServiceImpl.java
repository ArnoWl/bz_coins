package com.admin.service.impl;

import com.admin.entity.SysContact;
import com.admin.mapper.SysContactMapper;
import com.admin.service.ISysContactService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客服表 服务实现类
 * </p>
 *
 * @author lt
 * @since 2020-07-27
 */
@Service
public class SysContactServiceImpl extends ServiceImpl<SysContactMapper, SysContact> implements ISysContactService {

}
