package com.admin.service;

import com.admin.entity.SysOrderRule;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.base.ResultVo;
import com.mall.model.admin.vo.OrderRuleVo;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

/**
 * <p>
 * 订单包赔场，场次配置表 服务类
 * </p>
 *
 * @author lt
 * @since 2020-07-25
 */
public interface ISysOrderRuleService extends IService<SysOrderRule> {
    /**
     * 获取目前最新的包赔场时间
     * @return
     */
    OrderRuleVo getNewlast();
}
