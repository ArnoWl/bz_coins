package com.admin.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("登陆用户信息")
@Data
public class UserInfoVo {
    @ApiModelProperty("用户头像")
    private String portrait;

    @ApiModelProperty("用户名称")
    private String realName;
}
