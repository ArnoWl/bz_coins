package com.admin.model;

import com.admin.entity.SysUser;
import lombok.Data;

/**
 * @Author: lt
 * @Date: 2020/5/6 15:05
 */
@Data
public class SysUserVo extends SysUser {

    private static final long serialVersionUID = -2816872530330886727L;

    /** 角色名称 */
    private String roleName;

}
