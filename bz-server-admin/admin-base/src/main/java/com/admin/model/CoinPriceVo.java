package com.admin.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CoinPriceVo {

    @ApiModelProperty("币种名称")
    private String name;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @ApiModelProperty("价格")
    private BigDecimal rate;

}
