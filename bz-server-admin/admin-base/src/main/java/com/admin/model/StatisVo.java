package com.admin.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class StatisVo {
    @ApiModelProperty(value = "时间")
    private LocalDate date;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderMoney;

    @ApiModelProperty(value = "利润")
    private BigDecimal orderProfit;

    @ApiModelProperty(value = "普通场订单数量")
    private Integer ordinaryCount;

    @ApiModelProperty(value = "包赔场订单数量")
    private Integer indemnityCount;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal cashMoney;

    @ApiModelProperty(value = "充值金额")
    private BigDecimal rechaegeMoney;
}
