package com.mall.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.common.base.ResultVo;
import com.common.constatns.JWTConstants;
import com.common.constatns.RedisKeyConstants;
import com.common.constatns.ThirdConstants;
import com.common.exception.UserTokenException;
import com.common.exception.VerifySignatureException;
import com.common.jwt.JWTHelper;
import com.common.jwt.RsaKeyHelper;
import com.common.utils.codec.AESUtils;
import com.common.utils.codec.Md5Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.common.constatns.CommonConstants.*;

/**
 * 验证参数签名切面处理
 * @Author: lt
 * @Date: 2020/3/17 16:25
 */
@Slf4j
@Aspect
@Component
public class VerifySignatureAspect {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Value("${auth.user.token-header}")
    private String tokenHeader;

    /** 请求参数签名参数键值 */
    public static final String SIGN = "sign";

    @Pointcut("@annotation(com.mall.annotation.VerifySignature)")
    public void verifySignature(){

    }

    @Pointcut("@annotation(com.mall.annotation.DynamicVerifySignature)")
    public void dynamicVerifySignature(){

    }


    @Around("verifySignature()")
    public Object verifySignature(ProceedingJoinPoint joinPoint) {
        try {
            // 先根据服务器存储的AES加密规则进行验签
            String encryptionRules = ThirdConstants.AES_ENCRYPTION_RULES;
            ResultVo resultVo = this.verifySignature(JSON.toJSONString(joinPoint.getArgs()[0]),encryptionRules);
            if(!resultVo.isSuccess()){  // 验签失败
                return resultVo;
            }
            return joinPoint.proceed();
        } catch (Throwable throwable) {
            log.error("验证签名信息失败",throwable);
            throw new VerifySignatureException("验证签名信息失败");
        }
    }

    @Around("dynamicVerifySignature()")
    public Object dynamicVerifySignature(ProceedingJoinPoint joinPoint) {
        try {
            // 获取登录用户信息
            Map<String,Object> loginInfo = this.getLoginUserInfo();
            if(loginInfo != null){
                String subject = loginInfo.get(JWTConstants.JWT_SUBJECT).toString();
                // 动态签名：redis存储键值兑换卡号
                String redisSignKey = RedisKeyConstants.USER_AES_ENCRYPT_PREFIX + subject + ":"
                        + JWTHelper.getUserIdBySubject(subject,loginInfo);
                // 根据用户类型和ID从缓存中获取动态验签的加密规则
                String encryptionRules = redisTemplate.opsForValue().get(redisSignKey);
                if(StringUtils.isBlank(encryptionRules)){
                    log.info("登录失效，用户签名加密规则不存在");
                    return ResultVo.result(E_004,"动态验签失败，请重新获取后再试");
                }
                ResultVo resultVo = this.verifySignature(JSON.toJSONString(joinPoint.getArgs()[0]),encryptionRules);
                if(!resultVo.isSuccess()){
                    return resultVo;
                }
                return joinPoint.proceed();
            }
            throw new UserTokenException("用户未登录");
        } catch (Throwable throwable) {
            log.error("验证签名信息失败",throwable);
            throw new VerifySignatureException("验证签名信息失败");
        }
    }

    /**
     * 获取当前登录用户信息
     * @return
     */
    private Map<String,Object> getLoginUserInfo(){
        Map<String,Object> userInfo = null;
        try {
            // 登录用户token信息
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            assert requestAttributes != null;
            HttpServletRequest request = requestAttributes.getRequest();
            String token = request.getHeader(tokenHeader);
            if(StringUtils.isNotBlank(token)){
                userInfo =  JWTHelper.getInfoFromToken(token,
                        RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(RedisKeyConstants.REDIS_USER_PUB_KEY)));
            }
        } catch (Exception e) {
            log.error("获取用户登录信息失败",e);
            throw new UserTokenException("获取用户登录信息失败");
        }
        return userInfo;
    }

    /**
     * 验证签名
     * @param params            请求参数
     * @param encryptionRules   对称加密规则
     * @return
     */
    private ResultVo verifySignature(String params,String encryptionRules) {
        JSONObject obj = JSON.parseObject(params, Feature.OrderedField);
        String sign = obj.get(SIGN).toString();
        if(StringUtils.isBlank(sign)){
            return ResultVo.result(E_003,"缺少签名参数");
        }
        obj.remove(SIGN);
        log.info("客户端参数：{}", obj.toJSONString());
        // 对称加密，对请求参数+加密规则进行加密比较
        String str = AESUtils.getHexStr(obj.toJSONString(),encryptionRules.getBytes());
        log.info("客户端签名：{}", sign);
        log.info("服务端签名：{}", str);
        if(!sign.equals(str)){
            return ResultVo.result(E_005,"验证签名失败，参数有误");
        }
        return ResultVo.success();
    }

    public static void main(String[] args) {
        System.out.println(Md5Utils.md5("111111"));
    }
}
