package com.mall.aspect;


import com.common.base.ResultVo;
import com.common.exception.BaseException;
import com.common.utils.IpUtils;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

/**
 * web 请求参数日志切面
 * @author lt
 * 2020年3月17日14:02:48
 */
@Aspect
@Component
public class WebLogAspect {

    private static final Logger LOG = LoggerFactory.getLogger(WebLogAspect.class);

    /**
     * 请求参数日志切面
     */
    @Pointcut(value = "execution(* com.*.controller.*.*(..)) || execution(* com.*.controller.*.*.*(..)) || execution(* com.*.provider.*.*(..))")
    public void logPointCut() { }

    /**
     * 验证参数日志切面
     */
    @Pointcut(value = "execution(* com.*.controller.*.*(..)) || execution(* com.*.controller.*.*.*(..))")
    public void verifyLogPointCut() { }



    //在切入点开始处切入内容
    @Before("logPointCut()")
    public void doBefore(JoinPoint joinPoint) {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        LOG.info("请求地址 : " + request.getRequestURL().toString());
        LOG.info("HTTP METHOD : " + request.getMethod());
        LOG.info("IP : " + IpUtils.getRemoteAddr(request));
        LOG.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "."
                + joinPoint.getSignature().getName());
        LOG.info("参数 : " + Arrays.toString(joinPoint.getArgs()));
    }

    //在切入点return内容之后切入内容（可以用来对处理返回值做一些加工处理）
    @AfterReturning(returning = "ret", pointcut = "logPointCut()")// returning的值和doAfterReturning的参数名一致
    public void doAfterReturning(Object ret) {
        // 处理完请求，返回内容
        LOG.info("返回值 : " + ret);
    }

    //在切入点前后切入内容，并自己控制何时执行切入点自身的内容
    @Around("verifyLogPointCut()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        // 验证入参
        String msg = validationObj(pjp.getArgs());
        if(StringUtils.isNotBlank(msg)){
            return ResultVo.failure(msg);
        }
        long startTime = System.currentTimeMillis();
        Object ob = pjp.proceed();
        LOG.info("耗时 : " + (System.currentTimeMillis() - startTime));
        return ob;
    }

    protected String validationObj(Object[] objects) {
        // JWT - 已拦截处理用户登录信息，无入参接口不对参数进行校验
        if (null == objects || objects.length == 0) {
            return null;
        }
        //HashMap<Object,String> resultMap = Maps.newHashMap();
        try {
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();
            for (Object dto : objects) {
                Set<ConstraintViolation<Object>> constraintViolations = validator.validate(dto);
                for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
                    //resultMap.put(constraintViolation.getPropertyPath(), constraintViolation.getMessage());
                    return constraintViolation.getMessage();
                }
            }
            return null;
        } catch (Exception e) {
            throw new BaseException("验证参数失败",e);
        }
    }
}
