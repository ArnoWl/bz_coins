package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
@Data
public class SysCoinsQueryVo {

    @ApiModelProperty(value = "币种名称")
    private String name;

    @ApiModelProperty(value = "0开启 1关闭")
    private Integer status;

    @ApiModelProperty(value = "币种精度")
    private Integer accuracy;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;

    @ApiModelProperty(value = "货币类型  1公链  2ERC20代币")
    private Integer type;

    @ApiModelProperty(value = "手续费数量")
    private BigDecimal tax;

    @ApiModelProperty(value = "手续费类型 1按数量 2按比例")
    private Integer taxType;

    @ApiModelProperty(value = "0是系统币  1不用是系统币")
    private Integer sysFlag;

}
