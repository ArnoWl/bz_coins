package com.mall.model.admin.dto;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 后台系统用户登录参数实体
 * @Author: lt
 * @Date: 2020/3/14 17:09
 */
@ApiModel(value = "后台系统用户登录参数")
@Data
public class SysUserLoginDto extends BaseParamIn {

    private static final long serialVersionUID = -6219221168271794939L;

    @ApiModelProperty(value = "登录用户名")
    @NotBlank(message = "登录用户名不能为空")
    private String userAccount;

    @ApiModelProperty(value = "登录密码")
    @NotBlank(message = "登录密码不能为空")
    private String passWord;

}
