package com.mall.model.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: lt
 * @Date: 2020/5/30 16:16
 */
@Data
@ApiModel(value = "系统参数入参")
public class SysConfigDto {
    @ApiModelProperty(value = "id")
    @NotNull(message = "请选择参数")
    private Integer id;

    @ApiModelProperty(value = "值")
    @NotBlank(message = "请填写值")
    private String configVal;

    @ApiModelProperty(value = "备注")
    @NotBlank(message = "请填写改参数备注")
    private String remark;
}
