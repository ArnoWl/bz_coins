package com.mall.model.admin.vo;

import lombok.Data;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-28
 */
@Data
public class OrderRuleVo {

    private Integer id;

    /**
     * 场次名称
     */
    private String name;

    /**
     *  开始时间 小时分钟 例如9:30
     */
    private String beginTime;

    /**
     * 借宿时间 小时分钟 例如19:30
     */
    private String endTime;

    /**
     * 0开启 1关闭
     */
    private Integer status;
}
