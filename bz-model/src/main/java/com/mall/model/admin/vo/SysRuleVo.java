package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统用户信息
 * @Author: lt
 * @Date: 2020/3/14 17:18
 */
@Data
public class SysRuleVo {
    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "富文本内容")
    private String content;
}
