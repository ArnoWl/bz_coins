package com.mall.model.admin.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: lt
 * @Date: 2020/5/30 16:16
 */
@Data
@ApiModel(value = "帮助中心下级信息出参")
public class HelpCenterInfoVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "帮助中心id")
    private Integer helpCenterId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String content;
}
