package com.mall.model.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: lt
 * @Date: 2020/8/12 14:06
 */
@Data
@ApiModel(value = "平台货币钱包地址信息")
public class CoinAddrDto implements Serializable {

    private static final long serialVersionUID = -6772741396795175391L;

    @ApiModelProperty(value = "货币主键id")
    private Integer coinId;

    @ApiModelProperty(value = "钱包地址")
    private String walletAddr;

    @ApiModelProperty(value = "钱包密码")
    private String walletPassword;

    @ApiModelProperty(value = "钱包文件路径")
    private String walletPath;

    @ApiModelProperty(value = "1主地址 MAIN 2手续费地址 FEE")
    private String type;

    @ApiModelProperty(value = "协议类型 OMNI  ERC20")
    private String agreeType;

    @ApiModelProperty(value = "归集类型  1系统地址 2场外地址 场外地址需要自己填写钱包地址，不需要密码和文件")
    private Integer imputedType;

}
