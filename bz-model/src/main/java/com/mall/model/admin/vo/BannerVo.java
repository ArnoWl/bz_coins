package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: lt
 * @Date: 2020/5/30 16:16
 */
@Data
@ApiModel(value = "banner信息出参")
public class BannerVo {

    @ApiModelProperty(value = "banner图")
    private String bannerUrl;

    @ApiModelProperty(value = "事件类型  1：跳转公告   2：分享  ")
    private Integer eventType;

}
