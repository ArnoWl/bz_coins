package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author arno
 * @version 1.0
 * @date 2020-08-04
 */
@Data
public class CoinSpreadVo {


    private BigDecimal spreadMin;
    private BigDecimal spreadMax;
}
