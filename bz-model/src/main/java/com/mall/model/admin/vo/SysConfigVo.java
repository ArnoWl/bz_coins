package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: lt
 * @Date: 2020/5/30 16:16
 */
@Data
@ApiModel(value = "系统参数出参")
public class SysConfigVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "值")
    private String configVal;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    private String updateBy;
}
