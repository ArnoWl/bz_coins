package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author: lt
 * @Date: 2020/5/30 16:16
 */
@Data
@ApiModel(value = "帮助中详情信息")
public class HelpDetailVo {
    private String title;

    private String content;
}
