package com.mall.model.admin.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;

@ApiModel("认购数量表")
@Data
public class SubscriptionVo {
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "剩余数量")
    private BigDecimal balance;

    @ApiModelProperty(value = "当期购买最大总数量")
    private BigDecimal maxNum;
}
