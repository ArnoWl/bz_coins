package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: lt
 * @Date: 2020/5/30 16:16
 */
@Data
@ApiModel(value = "系统参数回显出参")
public class SysConfigDetailVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "值")
    private String configVal;

    @ApiModelProperty(value = "备注")
    private String remark;
}
