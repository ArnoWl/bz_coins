package com.mall.model.admin.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-28
 */
@Data
public class IndemnityRuleVo {



    private Integer id;

    /**
     *钱包USDT最低金额
     */
    private BigDecimal minMoney;

    /**
     *  钱包USDT最大金额
     */
    private BigDecimal maxMoney;

    /**
     * 包赔订单数量 每场
     */
    private Integer indemnityNum;

    /**
     * 包赔每次最大金额
     */
    private BigDecimal indemnityMaxMoney;
}
