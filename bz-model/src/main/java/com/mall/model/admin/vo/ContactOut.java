package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("客服信息")
@Data
public class ContactOut {

    @ApiModelProperty(value = "头像")
    private String headUrl;

    @ApiModelProperty(value = "微信")
    private String wx;

    @ApiModelProperty(value = "微信二维码")
    private String wxQr;
}
