package com.mall.model.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel("统计数据")
@Data
public class StatisDayBeforeDto {
    @ApiModelProperty("充值额度")
    private BigDecimal rechargeMoney;

    @ApiModelProperty("提现额度")
    private BigDecimal cashMoney;

    @ApiModelProperty("普通场订单数量")
    private Integer ordinaryNum;

    @ApiModelProperty("普通场订单总额")
    private BigDecimal ordinaryMoney;

    @ApiModelProperty("普通场订单盈亏")
    private BigDecimal ordinaryProfit;

    @ApiModelProperty("包赔场订单数量")
    private Integer indemnityNum;

    @ApiModelProperty("包赔场订单总额")
    private BigDecimal indemnityMoney;

    @ApiModelProperty("包赔场总盈亏")
    private BigDecimal indemnityProfit;

    @ApiModelProperty("注册用户数量")
    private Integer registerNum;

    @ApiModelProperty("订单数量")
    private Integer orderNum;

    @ApiModelProperty("usdt收益")
    private BigDecimal usdtProfit;

    @ApiModelProperty("bzw赠送")
    private BigDecimal bzwGive;

    @ApiModelProperty("bzw赠送冻结部分")
    private BigDecimal bzwFrozen;

    @ApiModelProperty("bzw销毁")
    private BigDecimal bzwDestruction;



}
