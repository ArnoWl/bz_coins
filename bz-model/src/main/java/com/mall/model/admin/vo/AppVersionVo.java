package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: lt
 * @Date: 2020/5/30 16:16
 */
@Data
@ApiModel(value = "app版本信息出参")
public class AppVersionVo implements Serializable {

    private static final long serialVersionUID = -8622309001839407208L;

    @ApiModelProperty(value = "状态：1-提示升级 2-不提示升级 3-强制升级")
    private Integer status;

    @ApiModelProperty(value = "最新版本号")
    private Integer versionNo;

    @ApiModelProperty(value = "最新版本号名称")
    private String versionName;

    @ApiModelProperty(value = "最新版本下载地址")
    private String downloadLink;

    @ApiModelProperty(value = "版本更新说明")
    private String updateContent;

}
