package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统用户信息
 * @Author: lt
 * @Date: 2020/3/14 17:18
 */
@Data
public class SysUserInfoVo implements Serializable {

    private static final long serialVersionUID = 8123840642732461788L;

    @ApiModelProperty(value = "系统用户Id")
    private Integer id;

    @ApiModelProperty(value = "用户账号")
    private String userAccount;

    @ApiModelProperty(value = "状态（0：启用 1：禁用）")
    private String status;
}
