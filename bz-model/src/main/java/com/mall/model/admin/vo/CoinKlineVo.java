package com.mall.model.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-28
 */
@Data
@ApiModel(value = "查询系统目前币指交易的币种出参")
public class CoinKlineVo {

    @ApiModelProperty(value = "币种名称")
    private String name;

    @ApiModelProperty(value = "币种代码")
    private String coinSymbol;
}
