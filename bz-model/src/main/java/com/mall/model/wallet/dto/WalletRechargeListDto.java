package com.mall.model.wallet.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author 充值列表查询入参
 * @version 1.0
 * @date 2020-07-14
 */
@ApiModel("充值列表查询入参")
@Data
public class WalletRechargeListDto extends PageParamIn {

    @ApiModelProperty("uid")
    private String uid;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty("会员账号")
    private String phone;

    @ApiModelProperty("货币类型")
    private String coinSymbol;

    @ApiModelProperty(value = "发送方地址")
    private String walletSendAddr;

    @ApiModelProperty(value = "接收地址")
    private String walletReceivedAddr;

    @ApiModelProperty(value = "同步交易hash单号")
    private String transHashCode;

    @ApiModelProperty("0已同步 1未同步")
    private String status;

    @ApiModelProperty("开始时间格式 yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty("结束时间格式 yyyy-MM-dd")
    private LocalDate endDate;
}
