package com.mall.model.wallet.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author 提现列表查询入参
 * @version 1.0
 * @date 2020-07-14
 */
@ApiModel("提现记录查询入参")
@Data
public class WalletCashListDto extends PageParamIn {

    @ApiModelProperty("uid")
    private String uid;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("会员账号")
    private String phone;

    @ApiModelProperty("货币类型")
    private String coinSymbol;

    @ApiModelProperty("1待审核  5审核，待转币 10转币，待确认  15已确认 20已驳回")
    private Integer status;

    @ApiModelProperty("开始时间格式 yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty("结束时间格式 yyyy-MM-dd")
    private LocalDate endDate;

    @ApiModelProperty("审核开始时间格式 yyyy-MM-dd")
    private LocalDate startHandleTime;

    @ApiModelProperty("审核结束时间格式 yyyy-MM-dd")
    private LocalDate endHandleTime;

}
