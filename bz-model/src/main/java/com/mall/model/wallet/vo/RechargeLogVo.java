package com.mall.model.wallet.vo;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 充值列表查询入参
 * @version 1.0
 * @date 2020-07-14
 */
@Data
@ApiModel(value = "场外充值记录出参")
public class RechargeLogVo{

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "会员账号")
    private String phone;

    @ApiModelProperty(value = "货币类型")
    private String coinSymbol;

    @ApiModelProperty(value = "区块充值交易单hash单号")
    private String hashCode;

    @ApiModelProperty(value = "发送方地址")
    private String walletSendAddr;

    @ApiModelProperty(value = "接收地址")
    private String walletReceivedAddr;

    @ApiModelProperty(value = "货币数量")
    private BigDecimal coinNum;

    @ApiModelProperty(value = "0已同步 1未同步")
    private Integer status;

    @ApiModelProperty(value = "同步交易hash单号")
    private String transHashCode;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "同步时间")
    private LocalDateTime synTime;
}
