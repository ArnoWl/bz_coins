package com.mall.model.wallet.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 钱包信息
 * @Author: lt
 * @Date: 2020/8/1 18:56
 */
@Data
public class WalletInfoDto implements Serializable {

    private static final long serialVersionUID = 6204229401295152296L;

    /** 货币代码 */
    private String coinSymbol;

    /** 合约地址 */
    private String contractAddr;

    /** 币种精度 */
    private Integer accuracy;

    /** 钱包地址 -- Ecr20 */
    private String walletEcr20Addr;

    /** 钱包路径 -- Ecr20 */
    private String walletEcr20Path;

    /** 钱包地址 -- Omin */
    private String walletOminAddr;

}
