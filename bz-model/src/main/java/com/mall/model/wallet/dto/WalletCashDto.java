package com.mall.model.wallet.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
@ApiModel("提币实体信息")
@Data
public class WalletCashDto {

    @ApiModelProperty("uid")
    private String uid;

    @ApiModelProperty("货币名称")
    private String coinSymbol;

    @ApiModelProperty("协议类型")
    private String agreeType;

    @ApiModelProperty("发送方地址")
    private String walletSendAddr;

    @ApiModelProperty("接收方地址不能为空")
    private String walletReceivedAddr;

    @ApiModelProperty("金额")
    private BigDecimal money;

    @ApiModelProperty("手续费数量")
    private BigDecimal tax;

    @ApiModelProperty("货币精度不能为空")
    private int decimal;

    @ApiModelProperty("0是系统内部互转 1不是系统内部互转")
    private Integer sysFlag;

    @ApiModelProperty("合约地址")
    private String contractAddr;
}
