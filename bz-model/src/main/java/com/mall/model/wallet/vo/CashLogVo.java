package com.mall.model.wallet.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
@Data
@ApiModel(value = "提现记录出参")
public class CashLogVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "会员账号")
    private String phone;

    @ApiModelProperty(value = "货币类型")
    private String coinSymbol;

    @ApiModelProperty(value = "协议类型 OMNI  ERC20")
    private String agreeType;

    @ApiModelProperty(value = "交易hash单号")
    private String hashCode;

    @ApiModelProperty(value = "发送方地址")
    private String walletSendAddr;

    @ApiModelProperty(value = "接收方地址")
    private String walletReceivedAddr;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal money;

    @ApiModelProperty(value = "手续费数量")
    private BigDecimal tax;

    @ApiModelProperty(value = "实际到账数量")
    private BigDecimal realMoney;

    @ApiModelProperty(value = "状态 1待审核  5审核，待转币 10转币，待确认  15已确认 20已驳回")
    private Integer status;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "审批时间")
    private LocalDateTime handleTime;

    @ApiModelProperty(value = "审批人")
    private String handleBy;

    @ApiModelProperty(value = "0是系统内部互转 1不是系统内部互转")
    private Integer sysFlag;

}
