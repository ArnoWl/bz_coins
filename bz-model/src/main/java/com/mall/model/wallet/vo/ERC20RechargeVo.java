package com.mall.model.wallet.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
@Data
@ApiModel(value = "ERC20充值记录出参")
public class ERC20RechargeVo {

    @ApiModelProperty(value = "交易hash单号")
    String hashCode;
    @ApiModelProperty(value = "时间戳")
    String timestamp;
    @ApiModelProperty(value = "发送方地址")
    String walletSendAddr;
    @ApiModelProperty(value = "合约地址")
    String contractaddress;
    @ApiModelProperty(value = "接收方地址")
    String walletReceivedAddr;
    @ApiModelProperty(value = "货币数量已转化")
    BigDecimal coinNum;
    @ApiModelProperty(value = "代币名称")
    String tokenname;
    @ApiModelProperty(value = "代币符号")
    String coinSymbol;
    @ApiModelProperty(value = "精度")
    int tokendecimal;
}
