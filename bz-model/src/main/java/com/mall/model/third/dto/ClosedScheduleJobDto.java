package com.mall.model.third.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 关闭调度任务参数实体
 * @Author: lt
 * @Date: 2020/6/11 17:02
 */
@Data
public class ClosedScheduleJobDto implements Serializable {

    private static final long serialVersionUID = 8861482849046195078L;

    /**执行器任务handler */
    private String executorHandler;

    /**执行器任务参数 */
    private String executorParam;

}
