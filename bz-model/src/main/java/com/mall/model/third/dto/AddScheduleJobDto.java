package com.mall.model.third.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 添加任务调度参数实体
 * @Author: lt
 * @Date: 2020bz_coins/15 14:24
 */
@Data
public class AddScheduleJobDto implements Serializable {

    private static final long serialVersionUID = -2641522390945013921L;

    /**执行器主键ID */
    private Integer jobGroup;

    /**任务执行CRON */
    private String jobCron;

    /**任务描述 */
    private String jobDesc;

    /**执行器任务handler */
    private String executorHandler;

    /**执行器任务参数 */
    private String executorParam;

}
