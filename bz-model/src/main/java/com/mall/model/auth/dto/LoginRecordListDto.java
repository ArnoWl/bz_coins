package com.mall.model.auth.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 用户登录日志
 * @Author: lt
 * @Date: 2020bz_coins/18 14:47
 */
@Data
@ApiModel(value = "用户登录日志")
public class LoginRecordListDto extends PageParamIn {

    private static final long serialVersionUID = -7453957919771485824L;

    @ApiModelProperty(value = "用户UId")
    @NotNull(message = "用户UId不能为空")
    private String uid;

}
