package com.mall.model.auth.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户登录账户信息视图数据实体
 * @Author: lt
 * @Date: 2020/5/1 14:20
 */
@ApiModel(value = "用户登录账户信息")
@Data
public class UserLoginAccountVo implements Serializable {

    private static final long serialVersionUID = -226689930393180170L;

    @ApiModelProperty(value = "用户手机账号")
    private String phoneAccount;

    @ApiModelProperty(value = "绑定的微信openId")
    private String openId;

    @ApiModelProperty(value = "绑定的微信号")
    private String userWx;
}
