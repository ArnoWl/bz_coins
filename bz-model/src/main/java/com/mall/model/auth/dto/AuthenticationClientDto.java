package com.mall.model.auth.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 验证客户端参数实体
 * @Author: lt
 * @Date: 2020/3/23 11:17
 */
@Data
public class AuthenticationClientDto implements Serializable {

    private static final long serialVersionUID = 1462304975719141129L;

    /** 客户端服务编码 */
    private String clientCode;

    /** 客户端Secret */
    private String clientSecret;

}
