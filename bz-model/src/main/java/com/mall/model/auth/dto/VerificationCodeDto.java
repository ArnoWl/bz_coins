package com.mall.model.auth.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户信息注册参数数据实体
 *
 * @Author: lt
 * @Date: 2020/5/27 14:11
 */
@Data
@ApiModel(value = "用户注册-发送验证码")
public class VerificationCodeDto implements Serializable {

    private static final long serialVersionUID = -7627338500603237779L;

    @ApiModelProperty(value = "手机号")
    @NotNull(message = "手机号不能为空")
    private String phoneAccount;

    @ApiModelProperty(value = "验证码")
    @NotNull(message = "验证码不能为空")
    private String verificationCode;

    @ApiModelProperty(value = "邀请码")
    private String invitationCode;


}
