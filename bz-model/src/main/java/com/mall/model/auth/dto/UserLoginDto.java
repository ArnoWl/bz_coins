package com.mall.model.auth.dto;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@Data
@ApiModel(value = "用户登录")
public class UserLoginDto extends BaseParamIn {

    @ApiModelProperty(value = "手机号")
    private String phoneAccount;

    @ApiModelProperty(value = "密码")
    private String password;
}
