package com.mall.model.auth.dto;

import com.mall.base.BaseParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: lt
 * @Date: 2020/6/2 14:13
 */
@Data
@ApiModel(value = "修改会员登录密码")
public class UpdatePassWordDto extends BaseParamIn {

    private static final long serialVersionUID = -2040759339270635368L;

    @ApiModelProperty(value = "用户UId")
    @NotNull(message = "用户UId不能为空")
    private String uid;

    @ApiModelProperty(value = "登录密码")
    @NotBlank(message = "登录密码不能为空")
    private String passWord;

}
