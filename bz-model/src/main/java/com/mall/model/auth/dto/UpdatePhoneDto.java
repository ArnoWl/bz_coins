package com.mall.model.auth.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 修改绑定手机号参数实体
 * @Author: lt
 * @Date: 2020/3/25 16:42
 */
@ApiModel(value = "修改绑定手机号参数")
@Data
public class UpdatePhoneDto implements Serializable {

    private static final long serialVersionUID = -3742403762364859714L;

    @ApiModelProperty(value = "用户Uid")
    private String uid;

    @ApiModelProperty(value = "换绑手机号")
    private String telphone;

    @ApiModelProperty(value = "登录密码")
    private String password;

}
