package com.mall.model.auth.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 登录记录列表参数实体
 * @Author: lt
 * @Date: 2020bz_coins/18 14:43
 */
@Data
@ApiModel(value = "登录记录列表参数")
public class LoginRecordListVo implements Serializable {

    private static final long serialVersionUID = -4589832226681717672L;

    @ApiModelProperty(value = "登录终端：0-Android 1-IOS 2-小程序 3-PC/H5 4-第三方")
    private Integer terminal;

    @ApiModelProperty(value = "ip地址")
    private String ipAddr;

    @ApiModelProperty(value = "ip所在城市")
    private String ipArea;

    @ApiModelProperty(value = "登录时间")
    private LocalDateTime loginTime;
}
