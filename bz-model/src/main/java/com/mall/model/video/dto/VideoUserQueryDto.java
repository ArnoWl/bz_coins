package com.mall.model.video.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 视频列表查询入参
 */
@ApiModel("视频列表查询入参")
@Data
public class VideoUserQueryDto extends PageParamIn {

    @ApiModelProperty(value = "视频标题查询 模糊查询")
    private String key;

    @ApiModelProperty(value = "登陆用户id", hidden = true)
    private String uid;
}
