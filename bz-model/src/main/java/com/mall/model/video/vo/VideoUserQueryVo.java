package com.mall.model.video.vo;

import com.common.constatns.VideoConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "视频用户查询出参")
@Data
public class VideoUserQueryVo {
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "用户名称")
    private String nickName;

    @ApiModelProperty(value = "头像地址")
    private String portrait;

    @ApiModelProperty(value = "描述内容")
    private String descVal;

    @ApiModelProperty(value = "视频封面图")
    private String coverImgUrl;

    @ApiModelProperty(value = "点赞数")
    private Integer thumbsNum;

    @ApiModelProperty(value = "是否已点赞 1已点赞 0未点赞")
    private Integer thumbs= VideoConstants.THUMB_FAIL;
}
