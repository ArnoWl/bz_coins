package com.mall.model.video.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@ApiModel(value = "视频平台查询出参")
@Data
public class VideoPlatQueryVo {
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "用户名称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "头像地址")
    private String portrait;

    @ApiModelProperty(value = "描述内容")
    private String descVal;

    @ApiModelProperty(value = "视频封面图")
    private String coverImgUrl;

    @ApiModelProperty(value = "视频url链接")
    private String videoUrl;

    @ApiModelProperty(value = "点赞数")
    private Integer thumbsNum;

    @ApiModelProperty(value = "1待审核   2已审核   3已驳回")
    private Integer status;

    @ApiModelProperty(value = "发布时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "审核时间")
    private LocalDateTime handleTime;
}
