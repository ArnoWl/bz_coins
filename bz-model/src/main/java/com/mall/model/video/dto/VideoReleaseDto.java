package com.mall.model.video.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("视频发布入参")
@Data
public class VideoReleaseDto {

    @ApiModelProperty(value = "uid用户id",hidden = true)
    private String uid;

    @ApiModelProperty(value = "描述内容")
    @NotBlank(message = "请填写描述内容")
    private String descVal;

    @ApiModelProperty(value = "视频地址")
    @NotBlank(message = "请上传视频")
    private String videoUrl;

    @ApiModelProperty(value = "视频封面图")
    @NotBlank(message = "请上传视频封面图")
    private String coverImgUrl;
}
