package com.mall.model.video.vo;

import com.common.constatns.VideoConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@ApiModel("用户视频申请出参")
@Data
public class VideoUserVo {

    @ApiModelProperty(value = "描述内容")
    private String descVal;

    @ApiModelProperty(value = "视频封面图")
    private String coverImgUrl;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "1待审核   2已审核   3已驳回")
    private Integer status;
}
