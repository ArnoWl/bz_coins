package com.mall.model.video.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 视频列表查询入参
 */
@ApiModel("视频列表查询入参")
@Data
public class VideoPlatQueryDto extends PageParamIn {

    @ApiModelProperty(value = "用户名称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "视频内容查询 模糊查询")
    private String key;

    @ApiModelProperty(value = "状态 1待审核   2已审核   3已驳回")
    private String status;

    @ApiModelProperty(value = "上传时间格式: yyyy-MM-dd~yyyy-mm-dd")
    private String queryTime;

    @ApiModelProperty(value = "审核时间格式: yyyy-MM-dd~yyyy-mm-dd")
    private String handleTime;
}
