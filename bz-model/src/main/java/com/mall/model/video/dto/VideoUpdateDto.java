package com.mall.model.video.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("视频修改入参")
@Data
public class VideoUpdateDto {
    @ApiModelProperty(value = "视频")
    @NotNull(message = "请选择视频")
    private Integer id;

    @ApiModelProperty(value = "视频描述内容")
    @NotBlank(message = "请输入描述内容")
    private String descVal;
}
