package com.mall.model.video.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户发布视频列表查询入参
 */
@ApiModel("视频列表查询入参")
@Data
public class VideoUserDto extends PageParamIn {

    @ApiModelProperty(value = "登陆用户id", hidden = true)
    private String uid;
}
