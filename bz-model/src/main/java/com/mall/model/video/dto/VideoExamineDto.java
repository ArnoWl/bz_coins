package com.mall.model.video.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("视频审核入参")
@Data
public class VideoExamineDto {
    @ApiModelProperty(value = "视频")
    @NotNull(message = "请选择视频")
    private Integer id;

    @ApiModelProperty(value = "2审核通过 3审核驳回")
    @NotNull(message = "请选择操作类型")
    private Integer status;

    @ApiModelProperty(value = "如果审核通过 则必须输入赠送额度")
    private BigDecimal qty;

    @ApiModelProperty(value = "赠送币种 BZW")
    private String coinSymbol;
}
