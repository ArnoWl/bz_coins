package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
@Data
public class BBDetailVo {

    private Integer id;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "买家昵称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "币种名称")
    private String coinSymbol;

    @ApiModelProperty(value = "出售数量")
    private BigDecimal num;

    @ApiModelProperty(value = "总金额")
    private BigDecimal totalMoney;

    private LocalDateTime createTime;

}
