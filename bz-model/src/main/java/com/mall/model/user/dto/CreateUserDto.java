package com.mall.model.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 创建普通用户信息实体
 * @Author: lt
 * @Date: 2020/5/21 14:20
 */
@Data
@ApiModel("创建普通用户信息实体")
public class CreateUserDto implements Serializable {

    private static final long serialVersionUID = -5776292694250952512L;

    @ApiModelProperty(value = "用户UID")
    private String uid;

    @ApiModelProperty(value = "支付密码")
    private String payPassword;

    @ApiModelProperty(value = "邀请码")
    private String invitationCode;
}
