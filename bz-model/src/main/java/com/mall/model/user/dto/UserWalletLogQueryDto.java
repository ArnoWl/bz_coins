package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/22
 */
@Data
@ApiModel("用户钱包记录列表实体")
public class UserWalletLogQueryDto extends PageParamIn {

    @ApiModelProperty(value = "用户uid")
    private String uid;

    @ApiModelProperty(value = "用户昵称")
    private  String nickName;

    @ApiModelProperty(value = "真实姓名")
    private  String realName;

    @ApiModelProperty(value = "用户手机号")
    private  String phone;

    @ApiModelProperty(value = "币种")
    private  String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private  String payCode;

    @ApiModelProperty(value = "1进账 2出账")
    private  Integer inOut;

    @ApiModelProperty(value = "操作类型")
    private  Integer typeId;

    @ApiModelProperty(value = "查询时间段 格式为: yyyy-MM-dd~yyyy-MM-dd")
    private String queryTime;

    @ApiModelProperty(value = "查询金额段 格式为: 开始金额:结束金额")
    private String queryMoney;

}

