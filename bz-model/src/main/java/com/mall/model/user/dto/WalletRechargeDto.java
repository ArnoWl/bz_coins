package com.mall.model.user.dto;

import lombok.Data;

/**
 * @author arno
 * @version 1.0
 * @date 2020-08-14
 */
@Data
public class WalletRechargeDto {

    /** 用户UID */
    private String uid;

    /** 钱包地址 */
    private String contractAddr;

    /** 合约地址 */
    private String walletAddr;
}
