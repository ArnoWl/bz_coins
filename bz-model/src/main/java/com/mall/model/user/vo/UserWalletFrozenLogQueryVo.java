package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/22
 */
@Data
public class UserWalletFrozenLogQueryVo {
    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户手机号")
    private String phone;

    @ApiModelProperty(value = "钱包类型")
    private String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "操作类型")
    private Integer typeId;

    @ApiModelProperty(value = "操作金额")
    private BigDecimal cost;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "触发人用户id")
    private String targetUid;

    private LocalDateTime createTime;
}
