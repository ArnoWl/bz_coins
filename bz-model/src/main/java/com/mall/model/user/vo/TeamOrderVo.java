package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/30
 */
@Data
public class TeamOrderVo {

    @ApiModelProperty(value = "推荐关系链id")
    private Integer id;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "code")
    private String code;

    @ApiModelProperty(value = "层级")
    private Integer level;

    @ApiModelProperty(value = "会员等级")
    private String levelName;

    @ApiModelProperty(value = "下单金额")
    private BigDecimal money;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "团队金额")
    private BigDecimal totalMoney;

}
