package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/22
 */
@Data
public class UserVo{

    @ApiModelProperty(value = "用户uid")
    private  String uid;

    @ApiModelProperty(value = "用户手机号")
    private  String phone;

    @ApiModelProperty(value = "用户等级")
    private  String levelId;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "用户码")
    private String code;

    @ApiModelProperty(value = "邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "身份证号")
    private String identityCard;

    @ApiModelProperty(value = "头像地址")
    private String portrait;

    @ApiModelProperty(value = "是否实名认证：0-否 1-是")
    private Integer isVerified;

    @ApiModelProperty(value = "认证时间")
    private Integer verifiedTime;

    @ApiModelProperty(value = "用户状态：0-正常  1-禁用")
    private Integer status;

    @ApiModelProperty(value = "二维码信息")
    private String qrCodeInfo;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createDate;

    @ApiModelProperty(value = "来源渠道：0-注册")
    private Integer channelSource;

    @ApiModelProperty(value = "钱包信息")
    private List<WalletVo> list;

}
