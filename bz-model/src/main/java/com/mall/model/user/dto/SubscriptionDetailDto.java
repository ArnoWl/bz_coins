package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 认购明细查询入参
 */
@ApiModel("认购明细入参")
@Data
public class SubscriptionDetailDto extends PageParamIn {

    @ApiModelProperty("ID号 实际为邀请码")
    private String invitationCode;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("真实姓名")
    private String realName;
}
