package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/27
 */
@Data

public class BBDelegationQueryVo {

    private Integer id;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "币种类型")
    private String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总数量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "已成交数量")
    private BigDecimal dealNum;

    @ApiModelProperty(value = "最低购买金额")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最高购买金额")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "类型:1:求购单 2:出售单 ")
    private Integer type;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "1交易中  2已结束")
    private Integer status;
}
