package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@Data
public class UserMessageQueryVo {

    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "ID号 实际为邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "0已读  1未读")
    private Integer readFlag;

    @ApiModelProperty(value = "1未回复 0已回复")
    private Integer reply;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime createTime;


}
