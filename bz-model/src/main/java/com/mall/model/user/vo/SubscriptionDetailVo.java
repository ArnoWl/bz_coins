package com.mall.model.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@ApiModel("认购明细出参")
@Data
public class SubscriptionDetailVo {
    @ApiModelProperty("ID号 实际为邀请码")
    private String invitationCode;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("单价")
    private BigDecimal price;

    @ApiModelProperty("购买金额")
    private BigDecimal money;

    @ApiModelProperty("购买数量")
    private BigDecimal num;

    @ApiModelProperty("购买时间")
    private LocalDateTime createTime;
}
