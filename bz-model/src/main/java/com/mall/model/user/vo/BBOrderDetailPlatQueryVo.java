package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Data
public class BBOrderDetailPlatQueryVo {

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "币种类型")
    private String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "购买数量")
    private BigDecimal num;

    @ApiModelProperty(value = "购买总金额")
    private BigDecimal totalMoney;

    private LocalDateTime createTime;

}
