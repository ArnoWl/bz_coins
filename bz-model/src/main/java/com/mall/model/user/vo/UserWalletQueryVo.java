package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/22
 */
@Data
public class UserWalletQueryVo {

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户手机号")
    private String phoneAccount;

    @ApiModelProperty(value = "用户等级")
    private String levelName;

    @ApiModelProperty(value = "'已提现金额")
    private String withdrawAmount;

    @ApiModelProperty(value = "用户uid")
    private String uid;

    @ApiModelProperty(value = "充值金额")
    private BigDecimal rechargeMoney;

    @ApiModelProperty(value = "货币代码 类似USDT ETH BTC等")
    private String coinSymbol;

    @ApiModelProperty(value = "协议类型 1公链(自主链)  2  OMNI(针对泰达币)   3ERC20代币")
    private Integer agreeType;

    @ApiModelProperty(value = "omin协议钱包地址")
    private String walletOminAddr;

    @ApiModelProperty(value = "钱包地址")
    private String walletAddr;

    @ApiModelProperty(value = "钱包余额")
    private BigDecimal walletBalance;

    @ApiModelProperty(value = "冻结金额")
    private BigDecimal walletFrozenBalance;

    @ApiModelProperty(value = "钱包账号")
    private String walletAccount;

    @ApiModelProperty(value = "钱包二维码图片地址")
    private String walletQrUrl;

    @ApiModelProperty(value = "omni 协议钱包图片")
    private String walletOmniQrUrl;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;

    @ApiModelProperty(value = "钱包精度")
    private Integer walletDeclimal;

    @ApiModelProperty(value = "操作时间")
    private LocalDateTime createTime;
}
