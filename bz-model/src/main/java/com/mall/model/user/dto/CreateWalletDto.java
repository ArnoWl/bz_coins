package com.mall.model.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
@Data
@ApiModel("创建钱包信息实体")
public class CreateWalletDto {

    @ApiModelProperty(value = "UID")
    private String uid;

    @ApiModelProperty(value = "货币代码 类似USDT ETH BTC等")
    private String coinSymbol;

    @ApiModelProperty(value = "omin协议钱包地址")
    private String walletOminAddr;

    @ApiModelProperty(value = "钱包地址")
    private String walletAddr;

    @ApiModelProperty(value = "钱包余额")
    private BigDecimal walletBalance;

    @ApiModelProperty(value = "冻结金额")
    private BigDecimal walletFrozenBalance;

    @ApiModelProperty(value = "钱包文件路径")
    private String walletPath;

    @ApiModelProperty(value = "钱包密码")
    private String walletPassWord;

    @ApiModelProperty(value = "钱包账号")
    private String walletAccount;

    @ApiModelProperty(value = "钱包二维码图片地址")
    private String walletQrUrl;

    @ApiModelProperty(value = "omni 协议钱包图片")
    private String walletOmniQrUrl;

    @ApiModelProperty(value = "合约地址")
    private String contractAddr;

    @ApiModelProperty(value = "货币类型  1公链  2ERC20代币")
    private Integer type;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "版本号")
    private Integer version;

    @ApiModelProperty(value = "钱包精度")
    private Integer walletDeclimal;
}
