package com.mall.model.user.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/26
 */
@Data
@ApiModel("修改用户信息实体")
public class UpdateUserDto {

    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "会员等级ID")
    private Integer levelId;

    @ApiModelProperty(value = "登录密码(MD5)")
    private String loginPassword;

    @ApiModelProperty(value = "手机号")
    private String phoneAccount;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "支付密码(MD5)")
    private String payPassword;

    @ApiModelProperty(value = "用户状态：0-正常  1-禁用")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;
}
