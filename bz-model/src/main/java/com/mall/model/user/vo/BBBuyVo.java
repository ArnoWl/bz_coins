package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Data
public class BBBuyVo {

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "剩余数量")
    private BigDecimal balanceNum;

    @ApiModelProperty(value = "钱包余额")
    private BigDecimal walletBalance;

    @ApiModelProperty(value = "换算比例")
    private BigDecimal conversionRatio;

    @ApiModelProperty(value = "完成数")
    private Integer num;

    @ApiModelProperty(value = "认证等级")
    private String levelName;

    @ApiModelProperty(value = "放币时效")
    private BigDecimal timeLong;

    @ApiModelProperty(value = "最低购买金额")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最高购买金额")
    private BigDecimal maxPrice;

}
