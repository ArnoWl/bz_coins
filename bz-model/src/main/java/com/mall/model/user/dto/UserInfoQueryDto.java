package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/22
 */
@Data
@ApiModel("查询用户信息列表实体")
public class UserInfoQueryDto extends PageParamIn {

    @ApiModelProperty(value = "用户昵称")
    private  String nickName;

    @ApiModelProperty(value = "真实姓名")
    private  String realName;

    @ApiModelProperty(value = "用户手机号")
    private  String phone;

    @ApiModelProperty(value = "邀请码")
    private  String invitationCode;

    @ApiModelProperty(value = "用户等级")
    private  Integer levelId;

    @ApiModelProperty(value = "用户状态")
    private  Integer status;

    @ApiModelProperty(value = "用户创建时间 格式为: yyyy-MM-dd~yyyy-MM-dd")
    private String queryTime;

    @ApiModelProperty(value = "用户认证时间 格式为: yyyy-MM-dd~yyyy-MM-dd")
    private String verifiedTime;
}
