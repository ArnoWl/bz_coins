package com.mall.model.user.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
@Data
public class BBDetailQueryVo {

    private Integer id;

    @ApiModelProperty(value = "1:购买单 2:出售单")
    private Integer type;

    @ApiModelProperty(value = "对方人昵称")
    private String nickName;

    @ApiModelProperty(value = "币种名称")
    private String coinSymbol;

    @ApiModelProperty(value = "出售数量")
    private BigDecimal num;

    @ApiModelProperty(value = "总金额")
    private BigDecimal totalMoney;

    private LocalDateTime createTime;
}
