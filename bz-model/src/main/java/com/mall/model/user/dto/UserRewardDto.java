package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("奖励查询入参")
@Data
public class UserRewardDto extends PageParamIn {

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "时间格式yyyy-MM-dd~yyyy-MM-dd")
    private String queryTime;

    @ApiModelProperty(value = "类型")
    private String typeId;

}
