package com.mall.model.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */

@Data
@ApiModel("用户钱包信息实体")
public class WalletLogDto implements Serializable {

    @ApiModelProperty(value = "UID不能为空")
    private String uid;

    @ApiModelProperty(value = "钱包类型: 1 USDT  2 BZW")
    private String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "操作类型不能为空 见枚举类 WalletLogTypeEnums")
    private Integer typeId;

    @ApiModelProperty(value = "1进账 2出账")
    private Integer inOut;

    @ApiModelProperty(value = "操作金额不能为空 负数就是减")
    private BigDecimal cost;

    @ApiModelProperty(value = "描述不能为空")
    private String remark;

    @ApiModelProperty(value = "触发人用户uid")
    private String targetUid;

    public WalletLogDto() {

    }

    public WalletLogDto(String uid, String coinSymbol, String payCode, Integer typeId,Integer inOut, BigDecimal cost, String remark) {
        this.uid = uid;
        this.coinSymbol = coinSymbol;
        this.payCode = payCode;
        this.typeId = typeId;
        this.inOut=inOut;
        this.cost = cost;
        this.remark = remark;
    }

    public WalletLogDto(String uid, String coinSymbol, String payCode, Integer typeId,Integer inOut, BigDecimal cost, String remark, String targetUid) {
        this.uid = uid;
        this.coinSymbol = coinSymbol;
        this.payCode = payCode;
        this.typeId = typeId;
        this.inOut=inOut;
        this.cost = cost;
        this.remark = remark;
        this.targetUid = targetUid;
    }
}
