package com.mall.model.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@ApiModel("用户升级记录出参")
@Data
public class UserUpLogVo {

    @ApiModelProperty(value = "用户昵称")
    private  String nickName;

    @ApiModelProperty(value = "用户姓名")
    private  String realName;

    @ApiModelProperty(value = "用户手机号")
    private  String phone;

    @ApiModelProperty(value = "原始等级")
    private  String oldLevelName;

    @ApiModelProperty(value = "升级等级")
    private  String newLevelName;

    @ApiModelProperty(value = "升级时间")
    private LocalDateTime createTime;
}
