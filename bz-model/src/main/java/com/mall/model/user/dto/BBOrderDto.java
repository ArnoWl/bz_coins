package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/26
 */
@Data
@ApiModel("币币交易订单信息实体")
public class BBOrderDto extends PageParamIn {

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "ID号 实际为邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "创建时间 格式为: yyyy-MM-dd HH:mm:ss~yyyy-MM-dd HH:mm:ss")
    private String queryTime;

}


