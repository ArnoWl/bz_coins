package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("平台查询会员团队")
@Data
public class UserTeamDto extends PageParamIn {
    @ApiModelProperty(value = "用户uid")
    private String uid;
}
