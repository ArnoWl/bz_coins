package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-29
 */
@Data
public class BZTotalVo {


    @ApiModelProperty(value = "保证金")
    private BigDecimal totalMoney;
    @ApiModelProperty(value = "进行中订单数量")
    private Integer orderIngNum;
    @ApiModelProperty(value = "当日包赔订单数量")
    private Integer indemnityNum;
    @ApiModelProperty(value = "包赔场结束时间 如果为NULL 表示还未开始")
    private LocalDateTime overTime;
    @ApiModelProperty(value = "USDT当前价格")
    private BigDecimal usdtPrice;
    @ApiModelProperty(value = "当前钱包USDT余额")
    private BigDecimal usdt;
}
