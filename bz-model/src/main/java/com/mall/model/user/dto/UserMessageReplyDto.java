package com.mall.model.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("回复站内信入参")
@Data
public class UserMessageReplyDto {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "回复内容")
    private String content;

}
