package com.mall.model.user.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/25
 */
@Data
@ApiModel("用户等级信息实体")
public class UserLevelDto {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "等级名称")
    private String name;

    @ApiModelProperty(value = "升级需要满足个人业绩")
    private BigDecimal personalMoney;

    @ApiModelProperty(value = "升级需要满足团队业绩")
    private BigDecimal teamMoney;

    @ApiModelProperty(value = "直推V1数量")
    private Integer pushNum;

    @ApiModelProperty(value = "团队里面包含上一个级别人数，每条线至少一个")
    private Integer teamNum;

    @ApiModelProperty(value = "团队极差奖励比例")
    private BigDecimal teamScale;

    @ApiModelProperty(value = "团队平级奖励比例")
    private BigDecimal teamCommonScale;

    @ApiModelProperty(value = "全球分红比例")
    private BigDecimal shareScale;
}
