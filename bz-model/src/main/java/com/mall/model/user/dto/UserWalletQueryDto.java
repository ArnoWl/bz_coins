package com.mall.model.user.dto;
import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/22
 */
@Data
@ApiModel("用户钱包列表实体")
public class UserWalletQueryDto extends PageParamIn {

    @ApiModelProperty(value = "用户昵称")
    private  String nickName;

    @ApiModelProperty(value = "用户姓名")
    private  String realName;

    @ApiModelProperty(value = "用户手机号")
    private  String phoneAccount;

    @ApiModelProperty(value = "用户等级")
    private  Integer levelId;

    @ApiModelProperty(value = "钱包类型")
    private  String coinSymbol;

    @ApiModelProperty(value = "查询金额段 格式为: 开始金额~结束金额")
    private String queryMoney;
}
