package com.mall.model.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("钱包信息")
@Data
public class WalletVo {

    @ApiModelProperty("币种")
    private String coinSymbol;

    @ApiModelProperty("余额")
    private String walletBalance;

}
