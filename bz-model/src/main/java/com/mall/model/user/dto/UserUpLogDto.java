package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("升级记录入参")
@Data
public class UserUpLogDto extends PageParamIn {

    @ApiModelProperty(value = "uid")
    private  String uid;

    @ApiModelProperty(value = "用户昵称")
    private  String nickName;

    @ApiModelProperty(value = "用户姓名")
    private  String realName;

    @ApiModelProperty(value = "用户手机号")
    private  String phone;

    @ApiModelProperty(value = "升级时间 格式 yyyy-MM-dd~yyyy-MM-dd")
    private  String queryTime ;



}
