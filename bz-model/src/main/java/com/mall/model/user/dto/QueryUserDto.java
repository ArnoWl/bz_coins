package com.mall.model.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询用户列表入参
 * @Author: cx
 * @Date: 2020/7/22
 */
@Data
@ApiModel("查询用户列表实体")
public class QueryUserDto {

    @ApiModelProperty(value = "手机号")
    private String phone;
}
