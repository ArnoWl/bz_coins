package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Data
public class BBBuyQueryVo {

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "交易单号")
    private BigDecimal payCode;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总数量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "剩余数量")
    private BigDecimal balanceNum;

    @ApiModelProperty(value = "最低购买金额")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最高购买金额")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "成交笔数")
    private Integer num;

}
