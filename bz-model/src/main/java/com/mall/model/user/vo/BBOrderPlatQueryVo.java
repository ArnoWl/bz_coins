package com.mall.model.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/24
 */
@Data
public class BBOrderPlatQueryVo {

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户手机号")
    private String phone;

    @ApiModelProperty(value = "ID号 实际为 邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "币种类型")
    private String coinSymbol;

    @ApiModelProperty(value = "交易单号")
    private String payCode;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "总数量")
    private BigDecimal totalNum;

    @ApiModelProperty(value = "剩余数量")
    private BigDecimal balanceNum;

    @ApiModelProperty(value = "最低购买金额")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最高购买金额")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "已购单数")
    private Integer num;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "1交易中  2已结束")
    private Integer status;




}
