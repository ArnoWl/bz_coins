package com.mall.model.user.dto;

import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/23
 */
@Data
@ApiModel("用户站内信列表实体")
public class UserMessageQueryDto extends PageParamIn {

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户姓名")
    private String realName;

    @ApiModelProperty(value = "用户手机号")
    private String phone;

    @ApiModelProperty(value = "1未回复 0已回复")
    private Integer reply;

    @ApiModelProperty(value = "查询时间段 格式为: yyyy-MM-dd~yyyy-MM-dd")
    private String queryTime;
}
