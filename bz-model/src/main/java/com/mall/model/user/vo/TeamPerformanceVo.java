package com.mall.model.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangq
 * @since 2020/7/29
 */
@Data
@ApiModel("团队业绩")
public class TeamPerformanceVo {

    @ApiModelProperty(value = "个人总流水")
    private BigDecimal personOrderMoney = BigDecimal.ZERO;

    @ApiModelProperty(value = "当日总流水")
    private BigDecimal dayOrderMoney = BigDecimal.ZERO;

    @ApiModelProperty(value = "当月总流水")
    private BigDecimal monthOrderMoney = BigDecimal.ZERO;

    @ApiModelProperty(value = "今日推广收益")
    private BigDecimal todayPromoteMoney;

    @ApiModelProperty(value = "总推广收益")
    private BigDecimal totalPromoteMoney;

    @ApiModelProperty(value = "团队人数")
    private Integer teamNum;

    @ApiModelProperty(value = "有效直推人数")
    private Integer directNum;
}
