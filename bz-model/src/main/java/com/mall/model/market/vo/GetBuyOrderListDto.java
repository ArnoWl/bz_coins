package com.mall.model.market.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mall.base.PageParamIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @Author: lt
 * @Date: 2020/7/25 16:27
 */
@Data
@ApiModel(value = "买入涨/跌订单列表")
public class GetBuyOrderListDto extends PageParamIn {

    private static final long serialVersionUID = -3725376913745664032L;

    @ApiModelProperty(value = "状态：1-进行中   2-已结束  3已结算")
    private Integer status;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户手机号")
    private String phone;

    @ApiModelProperty(value = "ID号 实际为邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "用户UID",hidden = true)
    private String uid;

    @ApiModelProperty(value = "场次类型: 1普通场次  2包赔场")
    private Integer orderType;

    @ApiModelProperty(value = "查询时间：yyyy-MM-dd HH:mm:ss")
    private String queryTime;

    @ApiModelProperty(value = "币种")
    private String coinSymbol;
}
