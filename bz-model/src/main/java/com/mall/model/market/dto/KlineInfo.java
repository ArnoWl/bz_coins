package com.mall.model.market.dto;

import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * K线信息
 * @Author: lt
 * @Date: 2020/7/23 17:45
 */
@Data
public class KlineInfo implements Serializable {

    private static final long serialVersionUID = -8574220091033582413L;

    /** 交易所类型 */
    private Integer eType;

    /** 交易对类型 */
    private String channelType;

    /** 时间戳-秒 */
    private long time;

    /** 开盘价 */
    private BigDecimal openPrice;

    /** 最高价 */
    private BigDecimal highestPrice;

    /** 最低价 */
    private BigDecimal lowestPrice;

    /** 收盘价 -> 最新成交价 */
    private BigDecimal closePrice;

    /** 成交量 */
    private BigDecimal volume;

    /** 成交额 */
    private BigDecimal turnover;

    /** 成交笔数 */
    private int count;

    /**
     * 当日开盘价
     */
    private BigDecimal toDayOpenPrice;

    /**
     * 涨跌
     */
    private BigDecimal rate;

    /**
     * 当天最高价
     */
    private BigDecimal toDayHighestPrice;

    /**
     * 当天最低价
     */
    private BigDecimal toDayLowestPrice;

}
