package com.mall.model.market.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: lt
 * @Date: 2020/7/25 16:22
 */
@Data
@ApiModel(value = "订单统计数据出参")
public class BuyOrderSumVo {
    @ApiModelProperty(value = "普通场订单数量")
    private Integer orderNum;

    @ApiModelProperty(value = "普通场订单总额")
    private BigDecimal orderMoney;

    @ApiModelProperty(value = "包赔场订单数量")
    private Integer indemnityOrderNum;

    @ApiModelProperty(value = "包赔场订单总额")
    private BigDecimal indemnityOrderMoney;

    @ApiModelProperty(value = "下单总额")
    private BigDecimal orderSum;

    @ApiModelProperty(value = "总盈利")
    private BigDecimal orderProfit;
}
