package com.mall.model.market.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author: lt
 * @Date: 2020/7/25 16:22
 */
@Data
@ApiModel(value = "订单信息列表")
public class BuyOrderListVo implements Serializable {

    private static final long serialVersionUID = -1813004675179803381L;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户手机号")
    private String phone;

    @ApiModelProperty(value = "ID号 实际为 邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "订单号")
    private String orderCode;

    @ApiModelProperty(value = "下单类型：1-买涨 2-买跌")
    private Integer buyType;

    @ApiModelProperty(value = "币种")
    private String coinSymbol;

    @ApiModelProperty(value = "开仓价")
    private BigDecimal openPrice;

    @ApiModelProperty(value = "关仓价")
    private BigDecimal closePrice;

    @ApiModelProperty(value = "下单花费金额")
    private BigDecimal money;

    @ApiModelProperty(value = "收益金额 可以为负数")
    private BigDecimal profit;

    @ApiModelProperty(value = "下单时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime overTime;

    @ApiModelProperty(value = "1-进行中 2-已结束 3-结算完成")
    private Integer status;

    @ApiModelProperty(value = "1-普通场次  2-包赔场")
    private Integer orderType;

    @ApiModelProperty(value = "剩余倒计时秒")
    private Long seconds=60L;

}
