package com.mall.feign.video;

import com.common.base.ResultVo;
import com.mall.fallback.video.VideoClientFallBack;
import com.mall.model.video.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author chenxing
 * @version 1.0
 * @date 2020-07-16
 */
@FeignClient(value = "video-server",path = "video",fallback = VideoClientFallBack.class)
public interface VideoClient {
    /**
     * 用户查询视频
     * @param dto 查询入参
     * @return
     */
    @PostMapping(value = "queryUserVideo")
    ResultVo queryUserVideo(@RequestBody VideoUserQueryDto dto);

    /**
     * 视频详情
     * @param id
     * @param uid
     * @return
     */
    @GetMapping(value = "getVideoDetail")
    ResultVo getVideoDetail(@RequestParam("id") Integer id,@RequestParam("uid") String uid);

    /**
     * 用户视频点赞
     * @param uid 用户id
     * @param id 视频id
     * @return
     */
    @GetMapping(value = "handleThumb")
    ResultVo handleThumb(@RequestParam("uid") String uid,@RequestParam("id") Integer id);

    /**
     * 用户发布视频列表
     * @param dto
     * @return
     */
    @PostMapping(value = "queryVideoRelease")
    ResultVo queryVideoRelease(@RequestBody VideoUserDto dto);

    /**
     * 用户发布视频
     * @param dto 发布视频入参
     * @return
     */
    @PostMapping(value = "handleVideoRelease")
    ResultVo handleVideoRelease(@RequestBody VideoReleaseDto dto);


    /**
     * 平台查询视频
     * @param dto 查询入参
     * @return
     */
    @PostMapping(value = "queryPlatVideo")
    ResultVo queryPlatVideo(@RequestBody VideoPlatQueryDto dto);

    /**
     * 平台审核
     * @param  examineDto
     * @return
     */
    @PostMapping(value = "handleExamine")
    ResultVo handleExamine(@RequestBody VideoExamineDto examineDto);

    /**
     * 平台删除视频
     * @param id 视频id
     * @return
     */
    @GetMapping(value = "delVideo")
    ResultVo delVideo(@RequestParam("id") Integer id);

    /**
     * 修改视频描述内容
     * @param updateDto
     * @return
     */
    @PostMapping(value = "updateVideo")
    ResultVo updateVideo(@RequestBody VideoUpdateDto updateDto);
}
