package com.mall.feign.admin;

import com.mall.base.AppBaseParamIn;
import com.mall.fallback.admin.AppVersionClientFallBack;
import com.mall.model.admin.vo.AppVersionVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 系统版本信息服务消费接口定义类
 * @Author: lt
 * @Date: 2020/5/30 16:20
 */
@FeignClient(value = "admin-server",path = "admin/version",fallback = AppVersionClientFallBack.class)
public interface AppVersionClient {

    /**
     * 获取最新的app版本信息
     * @param paramIn
     * @return
     */
    @PostMapping("getLatestAppVersion")
    AppVersionVo getLatestAppVersion(@RequestBody AppBaseParamIn paramIn);

}
