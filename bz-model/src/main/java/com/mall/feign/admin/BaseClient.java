package com.mall.feign.admin;

import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.fallback.admin.BaseClientFallBack;
import com.mall.model.admin.dto.CoinAddrDto;
import com.mall.model.admin.vo.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

/**
 * 基础信息服务接口
 *
 * @Author: lt
 * @Date: 2020/5/30 16:20
 */
@FeignClient(value = "admin-server", path = "admin/base",fallback = BaseClientFallBack.class)
public interface BaseClient {

    /**
     * 获取帮助中心信息
     *
     * @return
     */
    @GetMapping("queryHelp")
    ResultVo queryHelp();

    /**
     * 获取帮助中心内容详情
     *
     * @return
     */
    @GetMapping("getHelpDetail")
    ResultVo getHelpDetail(@RequestParam("id") Integer id);

    /**
     * 获取banner
     *
     * @param type 1首页 2首页中部
     * @return
     */
    @GetMapping("queryBanner")
    ResultVo queryBanner(@RequestParam("type") Integer type);

    /**
     * 获取公告
     *
     * @param pageParamIn 分页参数
     * @return
     */
    @PostMapping("queryNotice")
    ResultVo queryNotice(@RequestBody PageParamIn pageParamIn);

    /**
     * 获取公告详情
     *
     * @param id 公告id
     * @return
     */
    @GetMapping("getNoticeDetail")
    ResultVo getNoticeDetail(@RequestParam("id") Integer id);

    /**
     * 获取规则
     *
     * @param type 1视频有关规则   2订单有关规则 3用户协议
     * @return
     */
    @GetMapping("getSysRule")
    ResultVo getSysRule(@RequestParam("type") Integer type);

    /**
     * 获取系统币种
     *
     * @return
     */
    @GetMapping("querySysCoin")
    List<SysCoinsQueryVo> querySysCoin();

    /**
     * 获取可提现币种
     */
    @GetMapping("querySysCoinCash")
    List<String> querySysCoinCash();

    /**
     * 获取客服信息
     */
    @GetMapping("getSysContact")
    ResultVo getSysContact();

    /**
     * 获取认购数量列表
     */
    @GetMapping("querySysSubscription")
    ResultVo querySysSubscription();

    /**
     * 获取认购金额
     */
    @GetMapping("getSysSubscriptionPrice")
    SubscriptionVo getSysSubscriptionPrice(@RequestParam("id")Integer id);


    /**
     * 认购扣除数量
     */
    @GetMapping("handleSubtractQty")
    ResultVo handleSubtractQty(@RequestParam("id")Integer id,@RequestParam("qty") BigDecimal qty);

    /**
     * 查询目前存在多少个币指交易的币种
     * @return
     */
    @GetMapping("queryCoinkline")
    List<CoinKlineVo> queryCoinkline();

    /**
     * 查询最新的包赔场次信息
     * @return
     */
    @GetMapping("getNewlast")
    OrderRuleVo getNewlast();

    /**
     * 根据金额查询符合的规则
     * @param money
     * @return
     */
    @GetMapping("getRuleByUSDT")
    IndemnityRuleVo getRuleByUSDT(@RequestParam("money") BigDecimal money);

    /**
     * 获取当前币种点差
     * @param coinSymbol
     * @return
     */
    @GetMapping("getSpread")
    CoinSpreadVo getSpread(@RequestParam("coinSymbol") String coinSymbol);

    /**
     * 获取平台货币钱包地址信息
     * @return
     */
    @GetMapping("coinAddrList")
    List<CoinAddrDto> getCoinAddrList();
}
