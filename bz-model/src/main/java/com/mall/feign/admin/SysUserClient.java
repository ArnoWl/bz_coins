package com.mall.feign.admin;

import com.mall.fallback.admin.SysUserClientFallBack;
import com.mall.model.admin.dto.SysUserLoginDto;
import com.mall.model.admin.vo.SysUserInfoVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 系统用户信息服务消费定义接口类
 * @Author: lt
 * @Date: 2020/5/21 13:53
 */
@FeignClient(value = "admin-server",path = "admin/user",fallback = SysUserClientFallBack.class)
public interface SysUserClient {

    /**
     * 获取系统用户登录信息
     * @param loginDto
     * @return
     */
    @PostMapping(value = "loginInfo")
    SysUserInfoVo getSysUserInfo(@RequestBody SysUserLoginDto loginDto);

}
