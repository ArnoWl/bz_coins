package com.mall.feign.auth;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.model.auth.dto.LoginRecordListDto;
import com.mall.model.auth.vo.LoginRecordListVo;
import com.mall.model.auth.vo.UserLoginAccountVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 登录账户服务消费定义接口类
 *
 * @Author: lt
 * @Date: 2020/5/22 13:17
 */
@FeignClient(value = "auth-server", path = "auth/loginAccount")
public interface LoginAccountClient {

    /**
     * 获取用户登录账号信息
     *
     * @param uid
     * @return
     */
    @GetMapping(value = "getInfo")
    UserLoginAccountVo getUserLoginAccount(@RequestParam("uid") String uid);

    /**
     * 根据手机账号获取用户UID
     *
     * @param phoneAccount
     * @return
     */
    @GetMapping(value = "getUidByPhoneAccount")
    String getUidByPhoneAccount(@RequestParam("phoneAccount") String phoneAccount);

    /**
     * 修改用户手机账号
     *
     * @param uid
     * @param phoneAccount
     * @return
     */
    @PostMapping(value = "updateUserPhoneAccount")
    ResultVo updateUserPhoneAccount(@RequestParam("uid") String uid, @RequestParam("phoneAccount") String phoneAccount);

    /**
     * 平台修改用户登录密码
     *
     * @param uid
     * @param newPassword (MD5)
     * @return
     */
    @PostMapping(value = "updateUserPassword")
    ResultVo updateUserPassword(@RequestParam("uid") String uid, @RequestParam("newPassword") String newPassword);

    /**
     * 获取用户登录日志列表
     *
     * @param listDto
     * @return
     */
    @PostMapping(value = "logList")
    Page<LoginRecordListVo> getLogList(@RequestBody LoginRecordListDto listDto);

}
