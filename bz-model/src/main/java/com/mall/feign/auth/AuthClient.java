package com.mall.feign.auth;

import com.common.base.ResultVo;
import com.mall.model.auth.dto.AuthenticationClientDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 认证服务消费定义接口类
 * @Author: lt
 * @Date: 2020/5/22 13:02
 */
@FeignClient(value = "auth-server",path = "auth/client")
public interface AuthClient {

    /**
     * 验证客户端-获取用户鉴权公钥
     * @param clientDto
     * @return
     */
    @PostMapping(value = "getUserPubKey")
    ResultVo getUserPubKey(@RequestBody AuthenticationClientDto clientDto);

    /**
     * 验证客户端-获取客户端鉴权公钥
     * @param clientDto
     * @return
     */
    @PostMapping(value = "getClientPubKey")
    ResultVo getClientPubKey(@RequestBody AuthenticationClientDto clientDto);

    /**
     * 获取客户端鉴权Token
     * @param clientDto
     * @return
     */
    @PostMapping(value = "getAccessToken")
    ResultVo getAccessToken(@RequestBody AuthenticationClientDto clientDto);

}
