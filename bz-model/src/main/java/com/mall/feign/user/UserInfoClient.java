package com.mall.feign.user;

import com.common.base.ResultVo;
import com.mall.fallback.user.UserInfoClientFallBack;
import com.mall.model.user.dto.*;
import com.mall.model.user.vo.WalletInfoVo;
import com.mall.model.wallet.dto.WalletInfoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户信息服务消费定义接口类
 *
 * @Author: lt
 * @Date: 2020/4/22 10:36
 */
@FeignClient(value = "user-server", path = "user/info", fallback = UserInfoClientFallBack.class)
public interface UserInfoClient {

    /**
     * 创建用户钱包
     * @param uid               用户UID
     * @param walletInfoDtoList 钱包地址信息
     * @return
     */
    @PostMapping(value = "createWallet")
    ResultVo createWallet(@RequestParam("uid") String uid,@RequestBody List<WalletInfoDto> walletInfoDtoList);

    /**
     * 查询所有会员等级
     *
     * @return
     */
    @RequestMapping(value = "queryUserLevel", method = RequestMethod.GET)
    ResultVo queryUserLevel();

    /**
     * 编辑会员等级
     *
     * @return
     */
    @RequestMapping(value = "updateUserLevel", method = RequestMethod.POST)
    ResultVo updateUserLevel(@RequestBody UserLevelDto userLevelDto);

    /**
     * 获取会员等级
     *
     * @return
     */
    @RequestMapping(value = "getUserLevel", method = RequestMethod.GET)
    ResultVo getUserLevel(@RequestParam("id") Integer id);

    /**
     * 修改用户信息
     *
     * @param updateUserDto
     * @return
     */
    @PostMapping(value = "updateUserInfo")
    ResultVo updateUserInfo(@RequestBody UpdateUserDto updateUserDto);

    /**
     * 查询用户列表
     *
     * @param userInfoQueryDto
     * @return
     */
    @PostMapping(value = "queryUserInfo")
    ResultVo queryUserInfo(@RequestBody UserInfoQueryDto userInfoQueryDto);

    /**
     * 查询用户钱包列表
     *
     * @param userWalletQueryDto
     * @return
     */
    @PostMapping(value = "queryUserWallet")
    ResultVo queryUserWallet(@RequestBody UserWalletQueryDto userWalletQueryDto);

    /**
     * 查询用户钱包记录
     *
     * @param userWalletLogQueryDto
     * @return
     */
    @PostMapping(value = "queryUserWalletLog")
    ResultVo queryUserWalletLog(@RequestBody UserWalletLogQueryDto userWalletLogQueryDto);

    /**
     * 查询用户钱包冻结记录
     *
     * @param userWalletFrozenLogQueryDto
     * @return
     */
    @PostMapping(value = "queryUserWalletFrozenLog")
    ResultVo queryUserWalletFrozenLog(@RequestBody UserWalletFrozenLogQueryDto userWalletFrozenLogQueryDto);

    /**
     * 创建普通用户信息
     *
     * @param userDto
     * @return
     */
    @PostMapping(value = "createUser")
    ResultVo createUser(@RequestBody CreateUserDto userDto);

    /**
     * 根据用户标识验证普通用户状态信息
     *
     * @param uid
     * @return
     */
    @GetMapping(value = "validateUserByUid")
    ResultVo validateUserByUid(@RequestParam("uid") String uid);

    /**
     * 调整会员的钱包金额
     *
     * @param wallLogDto
     * @return
     */
    @PostMapping(value = "handleUserWalletMoney")
    ResultVo handleUserWalletMoney(@RequestBody WalletLogDto wallLogDto);

    /**
     * 调整会员的钱包金额
     *
     * @param wallLogDto
     * @return
     */
    @PostMapping(value = "handleUserWalletMoneyMore")
    ResultVo handleUserWalletMoneyMore(@RequestBody List<WalletLogDto> wallLogDto);

    /**
     * 处理用户冻结钱包账户金额
     *
     * @param wallLogDto
     * @return
     */
    @PostMapping(value = "handleUserFrozenWalletMoney")
    ResultVo handleUserFrozenWalletMoney(WalletLogDto wallLogDto);

    /**
     * 根据钱包地址ERC20获取用户钱包信息
     *
     * @param walletAddr
     * @return
     */
    @PostMapping(value = "getERC20WalletByAddr")
    WalletInfoVo getERC20WalletByAddr(@RequestParam("walletAddr") String walletAddr);

    /**
     * 根据钱包地址omni获取用户钱包信息
     *
     * @param walletAddr
     * @return
     */
    @PostMapping(value = "getOMNIWalletByAddr")
    WalletInfoVo getOMNIWalletByAddr(@RequestParam("walletAddr") String walletAddr);

    /**
     * 发送用户站内信
     *
     * @return
     */
    @PostMapping(value = "saveUserMessage")
    ResultVo saveUserMessage(@RequestParam("uid") String uid, @RequestParam("title") String title, @RequestParam("content") String content);

    /**
     * 平台查询用户站内信列表
     *
     * @return
     */
    @PostMapping(value = "queryUserMessage")
    ResultVo queryUserMessage(@RequestBody UserMessageQueryDto userMessageQueryDto);

    /**
     * 回复站内信
     * @param replyDto
     */
    @PostMapping(value = "handleUserMessageReply")
    ResultVo handleUserMessageReply(@RequestBody UserMessageReplyDto replyDto);

    /**
     * 查询回复内容
     * @param id
     * @return
     */
    @PostMapping(value = "getMessageContent")
    ResultVo getMessageContent(@RequestParam("id")Integer id);

    /**
     * 查询用户团队
     * @param dto
     * @return
     */
    @PostMapping(value = "queryUserTeam")
    ResultVo queryUserTeam(@RequestBody UserTeamDto dto);


    /**
     * 查询用户团队业绩
     * @param uid
     * @return
     */
    @GetMapping(value = "getMyTeamPerformance")
    ResultVo getMyTeamPerformance(@RequestParam("uid") String uid);

    /**
     * 查询用户团队各等级人数
     * @param id 会员团队数据id
     * @return
     */
    @GetMapping(value = "getTeamLevel")
    ResultVo getTeamLevel(@RequestParam("id") Integer id);


    /**
     * 获取会员团队当前等级人数
     * @param id
     * @return
     */
    @GetMapping(value = "queryTeamLevel")
    ResultVo queryTeamLevel(@RequestParam("id") Integer id,@RequestParam("levelid")Integer levelid);

    /**
     * 会员升级记录
     * @param dto
     * @return
     */
    @PostMapping(value = "queryUserUpLog")
    ResultVo queryUserUpLog(@RequestBody UserUpLogDto dto);

    /**
     * 根据币种获取钱包充值信息
     * @param coinSymbol
     * @return
     */
    @GetMapping(value = "getRechargeByCoinSymbol")
    List<WalletRechargeDto> getRechargeByCoinSymbol(@RequestParam("coinSymbol") String coinSymbol);

    /**
     * 查询奖励记录
     * @param rewardDto
     * @return
     */
    @PostMapping(value = "queryReward")
    ResultVo queryReward(@RequestBody UserRewardDto rewardDto);
}
