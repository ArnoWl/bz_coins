package com.mall.feign.user;

import com.common.base.ResultVo;
import com.mall.base.PageParamIn;
import com.mall.fallback.user.TransactionClientFallBack;
import com.mall.fallback.user.UserInfoClientFallBack;
import com.mall.model.user.dto.*;
import com.mall.model.user.vo.WalletInfoVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 用户信息服务消费定义接口类
 *
 * @Author: lt
 * @Date: 2020/4/22 10:36
 */
@FeignClient(value = "user-server", path = "user/trans", fallback = TransactionClientFallBack.class)
public interface TransactionClient {

    /**
     * 查询币币交易我要买列表
     *
     * @return
     */
    @RequestMapping(value = "queryBBBuyOrder", method = RequestMethod.POST)
    ResultVo queryBBBuyOrder(@RequestBody BBOrderDto bbOrderDto);

    /**
     * 查询币币交易我要卖列表
     *
     * @return
     */
    @RequestMapping(value = "queryBBSaleOrder", method = RequestMethod.POST)
    ResultVo queryBBSaleOrder(@RequestBody BBOrderDto bbOrderDto);

    /**
     * 查询币币交易我要买列表详情
     *
     * @return
     */
    @RequestMapping(value = "queryBBBuyOrderDetails", method = RequestMethod.POST)
    ResultVo queryBBBuyOrderDetails(@RequestBody BBOrderDetailsDto bbOrderDetailsDto);

    /**
     * 查询币币交易我要卖列表详情
     *
     * @return
     */
    @RequestMapping(value = "queryBBSaleOrderDetails", method = RequestMethod.POST)
    ResultVo queryBBSaleOrderDetails(@RequestBody BBOrderDetailsDto bbOrderDetailsDto);

    /**
     * 查询认购明细
     * @param detailDto
     * @return
     */
    @RequestMapping(value = "queryUserSubLogs", method = RequestMethod.POST)
    ResultVo queryUserSubLogs(@RequestBody SubscriptionDetailDto detailDto);

}
