package com.mall.feign.user;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.fallback.user.OrderInfoClientFallBack;
import com.mall.fallback.user.UserInfoClientFallBack;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.BuyOrderSumVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

/**
 * 买入订单服务消费定义接口类
 * @Author: lt
 * @Date: 2020/7/26 15:59
 */
@FeignClient(value = "user-server", path = "user/order", fallback = OrderInfoClientFallBack.class)
public interface OrderInfoClient {

    /**
     * 获取买入订单列表 -- 分页
     * @param listDto
     * @return
     */
    @PostMapping("list")
    Page<BuyOrderListVo> getOrderListPage(@RequestBody GetBuyOrderListDto listDto);
}
