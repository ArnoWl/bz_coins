package com.mall.feign.user;

import com.common.base.ResultVo;
import com.mall.model.wallet.dto.WalletInfoDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author: lt
 * @Date: 2020/8/6 10:57
 */
public interface UserWalletClient {

    /**
     * 创建用户钱包
     * @param uid               用户UID
     * @param walletInfoDtoList 钱包地址信息
     * @return
     */
    @PostMapping(value = "createWallet")
    ResultVo createWallet(@RequestParam("uid") String uid, @RequestBody List<WalletInfoDto> walletInfoDtoList);

}
