package com.mall.feign.market;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: lt
 * @Date: 2020/8/13 20:36
 */
@FeignClient(value = "market-server", path = "market")
public interface MarketClient {

    /**
     * 刷新订阅主题信息
     */
    @GetMapping(path = "refreshTopics")
    void refreshTopics();

}
