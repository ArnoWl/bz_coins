package com.mall.feign.wallet;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.fallback.wallet.WalletClientFallBack;
import com.mall.model.user.dto.WalletRechargeDto;
import com.mall.model.wallet.dto.WalletCashDto;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.mall.model.wallet.vo.RechargeLogVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
@FeignClient(value = "wallet-server",path = "wallet",fallback = WalletClientFallBack.class)
public interface WalletClient {

    /**
     * 修改提现状态
     * @param cashId
     * @param status
     * @param handleBy
     * @return
     */
    @GetMapping(value = "updateCashStatus")
    ResultVo updateCashStatus(@RequestParam("cashId") Integer cashId, @RequestParam("status") Integer status,@RequestParam("handleBy") String handleBy);

    /**
     * 处理提现申请
     * @param walletCashDto
     * @return
     */
    @PostMapping(value = "handleApplyCoinCash")
    ResultVo handleApplyCoinCash(@RequestBody WalletCashDto walletCashDto);

    /**
     * 创建ERC20 钱包
     * @param passWord
     * @return
     */
    @GetMapping(value = "createERC20Wallet")
    ResultVo createERC20Wallet(@RequestParam("passWord") String passWord);

    /**
     * 创建OMNI 钱包
     * @return
     */
    @GetMapping(value = "createOmniWallet")
    ResultVo createOmniWallet(@RequestParam("account") String account);

    /**
     * 获取钱包余额
     * @param walletAddr  钱包地址
     * @param type 类型  1获取BTC余额  2获取ETH余额  3获取OMNI USDT余额，4获取ERC20 代币余额
     * @return
     */
    @GetMapping(value = "getWalletBalance")
    String getWalletBalance(@RequestParam("walletAddr") String walletAddr, @RequestParam("contractAddr") String contractAddr,
                            @RequestParam("type") int type);

    /**
     * 获取提现记录
     * @return
     */
    @PostMapping(value = "queryWalletCash")
    Page<CashLogVo> queryWalletCash(@RequestBody WalletCashListDto cashListDto);

    /**
     * 获取充值记录
     * @return
     */
    @PostMapping(value = "queryWalletRecharge")
    Page<RechargeLogVo> queryWalletRecharge(@RequestBody WalletRechargeListDto rechargeListDto);

}
