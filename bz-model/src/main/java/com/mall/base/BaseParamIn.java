package com.mall.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 基础入参
 * @Author: lt
 * @Date: 2020/3/18 18:05
 */
@ApiModel(value = "基础入参")
@Data
public class BaseParamIn implements Serializable {

    private static final long serialVersionUID = -8016447301312078130L;

    @ApiModelProperty(value = "签名参数-验签接口传入")
    private String sign;

    @ApiModelProperty(value = "系统类型：0:Android 1:IOS 2:小程序 3:PC/H5 4:第三方", example = "3", required = true)
    @NotNull(message = "系统类型不能为空")
    private Integer sysType;

}
