package com.mall.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * App客户端基础入参
 * @author lt
 * 2020年3月17日17:57:50
 */
@Data
@ApiModel(value = "App客户端基础入参")
public class AppBaseParamIn extends BaseParamIn {

	private static final long serialVersionUID = -1687088171011408059L;

	@ApiModelProperty(value = "AppId", example = "AD45464661")
	private String appId;

	@ApiModelProperty(value = "手机型号", example = "IPHONE 8", required = true)
	@NotBlank(message = "手机型号不能为空.")
	private String phoneModel;

	@ApiModelProperty(value = "App应用版本号", example = "1", required = true)
	@NotNull(message = "App应用版本号不能为空")
	@Min(value = 1, message = "App应用版本号需要>=1开始")
	private Integer versionNo;

	@ApiModelProperty(value = "app应用版本名称",example = "1.0", required = true)
	@NotBlank(message = "app应用版本名称不能为空")
	private String appVersion;

	@ApiModelProperty(value = "App客户端类型", hidden = true)
	private Integer appType;
}
