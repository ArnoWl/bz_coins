package com.mall.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * App分页参数入参实体
 * @Author: lt
 * @Date: 2020/5/14 11:08
 */
@Data
@ApiModel(value = "App分页参数入参")
public class AppPageParamIn extends AppBaseParamIn{

    private static final long serialVersionUID = -2917279423254357251L;

    @ApiModelProperty(value = "分页-页码", example = "1", required = true)
    @NotNull(message = "页码不能为空")
    private Integer pageNo;

    @ApiModelProperty(value = "分页-每页行数", example = "10", required = true)
    @NotNull(message = "每页行数不能为空")
    private Integer pageSize;

}
