package com.mall.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 分页参数入参实体
 * @Author: lt
 * @Date: 2020/3/18 18:05
 */
@ApiModel(value = "分页参数入参")
@Data
public class PageParamIn extends BaseParamIn {

    private static final long serialVersionUID = -8016447301312078130L;

    @ApiModelProperty(value = "分页-页码", example = "1", required = true)
    @NotNull(message = "页码不能为空")
    private Integer pageNo;

    @ApiModelProperty(value = "分页-每页行数", example = "20", required = true)
    @NotNull(message = "每页行数不能为空")
    private Integer pageSize;

}
