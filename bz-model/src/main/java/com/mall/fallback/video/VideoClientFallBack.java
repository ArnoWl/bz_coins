package com.mall.fallback.video;

import com.common.base.ResultVo;
import com.mall.feign.video.VideoClient;
import com.mall.model.video.dto.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 视频服务熔断处理
 * @author chenxing
 * @version 1.0
 * @date 2020-07-16
 */
@Component
public class VideoClientFallBack implements VideoClient {

    @Override
    public ResultVo queryUserVideo(VideoUserQueryDto dto) {
        return ResultVo.failure("视频服务中断，查询用户端视频失败");
    }

    @Override
    public ResultVo getVideoDetail(Integer id, String uid) {
        return ResultVo.failure("视频服务中断，查询视频详情失败");
    }

    @Override
    public ResultVo handleThumb(String uid, Integer id) {
        return ResultVo.failure("视频服务中断，点赞失败");
    }

    @Override
    public ResultVo queryVideoRelease(VideoUserDto dto) {
        return ResultVo.failure("视频服务中断，查询用户发布视频列表失败");
    }

    @Override
    public ResultVo handleVideoRelease(VideoReleaseDto dto) {
        return ResultVo.failure("视频服务中断，发布视频失败");
    }

    @Override
    public ResultVo queryPlatVideo(VideoPlatQueryDto dto) {
        return ResultVo.failure("视频服务中断，查询平台视频失败");
    }

    @Override
    public ResultVo handleExamine(VideoExamineDto dto) { return ResultVo.failure("视频服务中断，审核视频失败");}

    @Override
    public ResultVo delVideo(Integer id) {
        return ResultVo.failure("视频服务中断，删除视频失败");
    }

    @Override
    public ResultVo updateVideo(VideoUpdateDto updateDto) {
        return ResultVo.failure("视频服务中断，修改视频失败");
    }
}
