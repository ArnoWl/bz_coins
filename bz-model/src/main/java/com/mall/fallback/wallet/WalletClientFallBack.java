package com.mall.fallback.wallet;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.base.ResultVo;
import com.mall.feign.wallet.WalletClient;
import com.mall.model.user.dto.WalletRechargeDto;
import com.mall.model.wallet.dto.WalletCashDto;
import com.mall.model.wallet.dto.WalletCashListDto;
import com.mall.model.wallet.dto.WalletRechargeListDto;
import com.mall.model.wallet.vo.CashLogVo;
import com.mall.model.wallet.vo.RechargeLogVo;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 钱包服务熔断处理
 *
 * @author arno
 * @version 1.0
 * @date 2020-07-14
 */
@Component
public class WalletClientFallBack implements WalletClient {

    @Override
    public ResultVo updateCashStatus(Integer cashId,Integer status,String handleBy) {
        return ResultVo.failure("钱包服务中断，提现审核失败");
    }

    @Override
    public ResultVo handleApplyCoinCash(WalletCashDto walletCashDto) {
        return ResultVo.failure("钱包服务中断，钱包创建失败");
    }

    @Override
    public ResultVo createERC20Wallet(String passWord) {
        return ResultVo.failure("钱包服务中断，钱包创建失败");
    }

    @Override
    public ResultVo createOmniWallet(String account) {
        return ResultVo.failure("钱包服务中断，钱包创建失败");
    }

    @Override
    public String getWalletBalance(String walletAddr, String contractAddr, int type) {
        return "0.00";
    }

    @Override
    public Page<CashLogVo> queryWalletCash(WalletCashListDto cashListDto) {
        return null;
    }

    @Override
    public Page<RechargeLogVo> queryWalletRecharge(WalletRechargeListDto rechargeListDto) {
        return null;
    }

}
