package com.mall.fallback.user;

import com.common.base.ResultVo;
import com.google.common.collect.Lists;
import com.mall.feign.user.UserInfoClient;
import com.mall.model.user.dto.*;
import com.mall.model.user.vo.WalletInfoVo;
import com.mall.model.wallet.dto.WalletInfoDto;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户信息服务熔断处理
 *
 * @Author: lt
 * @Date: 2020/4/22 10:43
 */
@Component
public class UserInfoClientFallBack implements UserInfoClient {

    @Override
    public ResultVo createWallet(String uid, List<WalletInfoDto> walletInfoDtoList) {
        return ResultVo.failure("用户中心服务中断，创建钱包账户失败");
    }

    @Override
    public ResultVo queryUserLevel() {
        return ResultVo.failure("用户中心服务中断，查询会员等级信息失败");
    }

    @Override
    public ResultVo updateUserLevel(UserLevelDto userLevelDto) {
        return ResultVo.failure("用户中心服务中断，修改会员等级信息失败");
    }

    @Override
    public ResultVo getUserLevel(Integer id) {
        return ResultVo.failure("用户中心服务中断，获取会员等级信息失败");
    }

    @Override
    public ResultVo updateUserInfo(UpdateUserDto updateUserDto) {
        return ResultVo.failure("用户中心服务中断，修改用户信息失败");
    }

    @Override
    public ResultVo queryUserInfo(UserInfoQueryDto userInfoQueryDto) {
        return ResultVo.failure("用户中心服务中断，查询用户列表信息失败");
    }

    @Override
    public ResultVo queryUserWallet(UserWalletQueryDto userWalletQueryDto) {
        return ResultVo.failure("用户中心服务中断，查询用户钱包信息失败");
    }

    @Override
    public ResultVo queryUserWalletLog(UserWalletLogQueryDto userWalletLogQueryDto) {
        return ResultVo.failure("用户中心服务中断，查询用户钱包记录失败");
    }

    @Override
    public ResultVo queryUserWalletFrozenLog(UserWalletFrozenLogQueryDto userWalletFrozenLogQueryDto) {
        return ResultVo.failure("用户中心服务中断，查询用户钱包冻结记录失败");
    }

    @Override
    public ResultVo handleUserFrozenWalletMoney(WalletLogDto wallLogDto) {
        return ResultVo.failure("用户中心服务中断，处理用户冻结钱包账户金额失败");
    }

    @Override
    public ResultVo createUser(CreateUserDto userDto) {
         return ResultVo.failure("用户中心服务中断，创建用户信息失败");
    }

    @Override
    public ResultVo validateUserByUid(String uid) {
        return ResultVo.failure("用户中心服务中断，验证用户信息失败");
    }

    @Override
    public ResultVo handleUserWalletMoney(WalletLogDto wallLogDto) {
        return ResultVo.failure("用户中心服务中断，验证用户信息失败");
    }

    @Override
    public ResultVo handleUserWalletMoneyMore(List<WalletLogDto> wallLogDto) {
        return ResultVo.failure("用户中心服务中断，验证用户信息失败");
    }

    @Override
    public WalletInfoVo getERC20WalletByAddr(String walletAddr) {
        return null;
    }

    @Override
    public WalletInfoVo getOMNIWalletByAddr(String walletAddr) {
        return null;
    }

    @Override
    public ResultVo saveUserMessage(String uid, String title, String content) {
        return ResultVo.failure("用户中心服务中断，发送用户站内信失败");
    }

    @Override
    public ResultVo queryUserMessage(UserMessageQueryDto userMessageQueryDto) {
        return ResultVo.failure("用户中心服务中断，查询用户站内信列表失败");
    }

    @Override
    public ResultVo handleUserMessageReply(UserMessageReplyDto replyDto) {
        return ResultVo.failure("用户中心服务中断，回复用户站内信失败");
    }

    @Override
    public ResultVo getMessageContent(Integer id) {
        return ResultVo.failure("用户中心服务中断，查询回复内容信息失败");
    }

    @Override
    public ResultVo queryUserTeam(UserTeamDto dto) {
        return ResultVo.failure("用户中心服务中断，查询会员团队信息列表失败");
    }

    @Override
    public ResultVo getMyTeamPerformance(String uid) {
        return ResultVo.failure("用户中心服务中断，查询会员团队业绩失败");
    }

    @Override
    public ResultVo getTeamLevel(Integer id) {
        return ResultVo.failure("用户中心服务中断,查询会员各等级人数失败");
    }

    @Override
    public ResultVo queryTeamLevel(Integer id, Integer levelid) {
        return ResultVo.failure("用户中心服务中断,查询会员失败");
    }

    @Override
    public ResultVo queryUserUpLog(UserUpLogDto dto) {
        return ResultVo.failure("用户中心服务中断,查询会员升级记录失败");
    }

    @Override
    public List<WalletRechargeDto> getRechargeByCoinSymbol(String coinSymbol) {
        return Lists.newArrayList();
    }

    @Override
    public ResultVo queryReward(UserRewardDto rewardDto) {
        return ResultVo.failure("用户中心服务中断,查询会员奖励记录失败");
    }
}
