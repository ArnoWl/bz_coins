package com.mall.fallback.user;

import com.common.base.ResultVo;
import com.mall.feign.user.TransactionClient;
import com.mall.feign.user.UserInfoClient;
import com.mall.model.user.dto.*;
import com.mall.model.user.vo.WalletInfoVo;
import org.springframework.stereotype.Component;

/**
 * 用户信息服务熔断处理
 *
 * @Author: lt
 * @Date: 2020/4/22 10:43
 */
@Component
public class TransactionClientFallBack implements TransactionClient {

    @Override
    public ResultVo queryBBBuyOrder(BBOrderDto bbOrderDto) {
        return ResultVo.failure("用户中心服务中断，查询币币交易我要买列表失败");
    }

    @Override
    public ResultVo queryBBSaleOrder(BBOrderDto bbOrderDto) {
        return ResultVo.failure("用户中心服务中断，查询币币交易我要卖列表失败");
    }

    @Override
    public ResultVo queryBBBuyOrderDetails(BBOrderDetailsDto bbOrderDetailsDto) {
        return ResultVo.failure("用户中心服务中断，查询币币交易我要买列表详情失败");
    }

    @Override
    public ResultVo queryBBSaleOrderDetails(BBOrderDetailsDto bbOrderDetailsDto) {
        return ResultVo.failure("用户中心服务中断，查询币币交易我要卖列表详情失败");
    }

    @Override
    public ResultVo queryUserSubLogs(SubscriptionDetailDto detailDto) {
        return ResultVo.failure("用户中心服务中断，查询认购明细失败");
    }
}
