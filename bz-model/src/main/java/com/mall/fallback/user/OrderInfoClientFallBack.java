package com.mall.fallback.user;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.feign.user.OrderInfoClient;
import com.mall.model.market.vo.BuyOrderListVo;
import com.mall.model.market.vo.BuyOrderSumVo;
import com.mall.model.market.vo.GetBuyOrderListDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @Author: lt
 * @Date: 2020/7/26 16:00
 */
@Component
public class OrderInfoClientFallBack implements OrderInfoClient {


    @Override
    public Page<BuyOrderListVo> getOrderListPage(GetBuyOrderListDto listDto) {
        return new Page<>();
    }


}
