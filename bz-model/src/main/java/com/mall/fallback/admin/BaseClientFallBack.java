package com.mall.fallback.admin;

import com.common.base.ResultVo;
import com.google.common.collect.Lists;
import com.mall.base.PageParamIn;
import com.mall.feign.admin.BaseClient;
import com.mall.model.admin.dto.CoinAddrDto;
import com.mall.model.admin.vo.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 基础信息服务熔断处理
 *
 * @Author: lt
 * @Date: 2020/5/30 16:22
 */
@Component
public class BaseClientFallBack implements BaseClient {

    @Override
    public ResultVo queryHelp() {
        return ResultVo.failure("基础服务中断，查询帮助中心数据失败");
    }

    @Override
    public ResultVo getHelpDetail(Integer id) {
        return ResultVo.failure("基础服务中断，查询帮助中心数据失败");
    }

    @Override
    public ResultVo queryBanner(Integer type) {
        return ResultVo.failure("基础服务中断，查询banner数据失败");
    }

    @Override
    public ResultVo queryNotice(PageParamIn pageParamIn) {
        return ResultVo.failure("基础服务中断，查询公告数据失败");
    }

    @Override
    public ResultVo getNoticeDetail(Integer id) {
        return ResultVo.failure("基础服务中断，查询公告详情失败");
    }

    @Override
    public ResultVo getSysRule(Integer type) {
        return ResultVo.failure("基础服务中断,获取规则失败");
    }

    @Override
    public List<SysCoinsQueryVo> querySysCoin() {
        return null;
    }

    @Override
    public List<String> querySysCoinCash() {
        return new ArrayList<>();
    }

    @Override
    public ResultVo getSysContact() {
        return ResultVo.failure("基础服务中断,获取客服信息失败");
    }

    @Override
    public ResultVo querySysSubscription() {
        return ResultVo.failure("基础服务中断,获取认购数据失败");
    }

    @Override
    public SubscriptionVo getSysSubscriptionPrice(Integer id) {
        return null;
    }

    @Override
    public ResultVo handleSubtractQty(Integer id, BigDecimal qty) {
        return ResultVo.failure("基础服务中断,扣除认购数据失败");
    }

    @Override
    public List<CoinKlineVo> queryCoinkline() {
        return null;
    }

    @Override
    public OrderRuleVo getNewlast() {
        return null;
    }

    @Override
    public IndemnityRuleVo getRuleByUSDT(BigDecimal money) {
        return null;
    }

    @Override
    public CoinSpreadVo getSpread(String coinSymbol) {
        return null;
    }

    @Override
    public List<CoinAddrDto> getCoinAddrList() {
        return Lists.newArrayList();
    }
}
