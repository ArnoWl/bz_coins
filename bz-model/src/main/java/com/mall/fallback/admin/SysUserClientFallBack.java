package com.mall.fallback.admin;

import com.mall.feign.admin.SysUserClient;
import com.mall.model.admin.dto.SysUserLoginDto;
import com.mall.model.admin.vo.SysUserInfoVo;
import org.springframework.stereotype.Component;

/**
 * 系统用户信息服务熔断处理
 * @Author: lt
 * @Date: 2020/5/21 13:55
 */
@Component
public class SysUserClientFallBack implements SysUserClient {

    @Override
    public SysUserInfoVo getSysUserInfo(SysUserLoginDto loginDto) {
        return null;
    }

}
