package com.mall.fallback.admin;

import com.mall.base.AppBaseParamIn;
import com.mall.feign.admin.AppVersionClient;
import com.mall.model.admin.vo.AppVersionVo;
import org.springframework.stereotype.Component;

/**
 * 系统版本信息服务熔断处理
 * @Author: lt
 * @Date: 2020/5/30 16:22
 */
@Component
public class AppVersionClientFallBack implements AppVersionClient {

    @Override
    public AppVersionVo getLatestAppVersion(AppBaseParamIn paramIn) {
        return new AppVersionVo();
    }

}
